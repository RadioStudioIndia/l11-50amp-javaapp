import java.io.DataInput;
import java.io.IOException;

public class LastMode_Structure {
	public boolean WLEnable;
	public boolean ENVEnable;
	public boolean IRISEnable;
	public boolean RedEnable;
	public boolean GreenEnable;
	public boolean BlueEnable;
	public boolean IRIS1Enable;
	public boolean IRIS2Enable;
	
	public byte WLMode;
	public byte ENVMode;
	public byte IRISMode;
	public byte WLSrc;
	public byte ENVSrc;
	public byte IRISSrc;
	public byte PWM_WL_TYPE;
	public byte PWM_ENV_TYPE;
	public byte PWM_WL_DUTY;
	public byte PWM_ENV_DUTY;
	
	public byte RedIntensity;
	public byte GreenIntensity;
	public byte BlueIntensity;
	public byte WhiteIntensity;
	public byte ENVIntensity;
	public byte IRIS1Intensity;
	public byte IRIS2Intensity;
	public byte PowerSupplyFanSpeed;
	public byte HeatSinkFanSpeed;
	public byte IRISFanSpeed;
	
	
	public LastMode_Structure(DataInput in) throws IOException
	{
		Initialize(in);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}
	public void Initialize(DataInput in) throws IOException
	{
		this.WLEnable = in.readBoolean();
		System.out.print("WL_ENABLE : ");
		System.out.println(WLEnable);
		
		this.ENVEnable = in.readBoolean();
		System.out.print("ENV_ENABLE : ");
		System.out.println(ENVEnable);
		
		this.IRISEnable = in.readBoolean();
		System.out.print("IRIS_ENABLE : ");
		System.out.println(IRISEnable);
		
		this.RedEnable = in.readBoolean();
		System.out.print("RED_ENABLE : ");
		System.out.println(RedEnable);
		
		this.GreenEnable = in.readBoolean();
		System.out.print("GREEN_ENABLE : ");
		System.out.println(GreenEnable);
		
		this.BlueEnable = in.readBoolean();
		System.out.print("BLUE_ENABLE : ");
		System.out.println(BlueEnable);
		
		this.IRIS1Enable = in.readBoolean();
		System.out.print("IRIS1_ENABLE : ");
		System.out.println(IRIS1Enable);
		
		this.IRIS2Enable = in.readBoolean();
		System.out.print("IRIS2_ENABLE : ");
		System.out.println(IRIS2Enable);
		
		this.WLMode = in.readByte();
		System.out.print("WL_Mode : ");
		System.out.println(WLMode);
		
		this.ENVMode = in.readByte();
		System.out.print("ENV_Mode : ");
		System.out.println(ENVMode);
		
		this.IRISMode = in.readByte();
		System.out.print("IRIS_Mode : ");
		System.out.println(IRISMode);
		
		this.WLSrc = in.readByte();
		System.out.print("WL_Src : ");
		System.out.println(WLSrc);
		
		this.ENVSrc = in.readByte();
		System.out.print("ENV_Src : ");
		System.out.println(ENVSrc);
		
		this.IRISSrc = in.readByte();
		System.out.print("IRIS_Src : ");
		System.out.println(IRISSrc);
		
		this.PWM_WL_TYPE = in.readByte();
		System.out.print("WL_PWM_TYPE : ");
		System.out.println(PWM_WL_TYPE);
		
		this.PWM_ENV_TYPE = in.readByte();
		System.out.print("ENV_PWM_TYPE : ");
		System.out.println(PWM_ENV_TYPE);
		
		this.PWM_WL_DUTY = in.readByte();
		System.out.print("WL_PWM_DUTY : ");
		System.out.println(PWM_WL_DUTY);
		
		this.PWM_ENV_DUTY = in.readByte();
		System.out.print("ENV_PWM_DUTY : ");
		System.out.println(PWM_ENV_DUTY);
		
		this.RedIntensity = in.readByte();
		System.out.print("RED_Intensity : ");
		System.out.println(RedIntensity);
		
		this.GreenIntensity = in.readByte();
		System.out.print("GREEN_Intensity : ");
		System.out.println(GreenIntensity);
		
		this.BlueIntensity = in.readByte();
		System.out.print("BLUE_Intensity : ");
		System.out.println(BlueIntensity);
		
		this.ENVIntensity = in.readByte();
		System.out.print("ENV_Intensity : ");
		System.out.println(ENVIntensity);
		
		this.IRIS1Intensity = in.readByte();
		System.out.print("IRIS1_Intensity : ");
		System.out.println(IRIS1Intensity);
		
		this.IRIS2Intensity = in.readByte();
		System.out.print("IRIS2_Intensity : ");
		System.out.println(IRIS2Intensity);
		
		this.WhiteIntensity = in.readByte();
		System.out.print("WHITE_Intensity : ");
		System.out.println(WhiteIntensity);
		
		this.PowerSupplyFanSpeed = in.readByte();
		System.out.print("PowerSupplyFan_Intensity : ");
		System.out.println(PowerSupplyFanSpeed);
		
		this.HeatSinkFanSpeed = in.readByte();
		System.out.print("HeatSinkFan_Intensity : ");
		System.out.println(HeatSinkFanSpeed);
		
		this.IRISFanSpeed = in.readByte();
		System.out.print("IRIS Fan Speed : ");
		System.out.println(IRISFanSpeed);
	}

}
