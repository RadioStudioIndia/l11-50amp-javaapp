public class SignedtoUnsigned {
    public static void main(String[] args) {
	byte x = (byte) 37;
	byte y = (byte) 150;
	printByte(x);
	printByte(y);
    }

    public static int printByte(byte b) {
	int value = 0;
	if (b < 0) {
	    value = 256 + b;
	} else {
	    value = b;
	}
	System.out.println(value);
	return value;
    }
}