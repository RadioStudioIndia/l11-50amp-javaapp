import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.JCheckBox;

public class HashMaptoTable {
	public ArrayList<String> tablecontent = new ArrayList<String>() ;
	
	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HashMaptoTable window = new HashMaptoTable();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public HashMaptoTable() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		tablecontent.add("N/A");
		tablecontent.add("-");
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		final Map<String, String> st=new TreeMap<String, String>();
		JCheckBox chckbxb = new JCheckBox("New check box");
	    chckbxb.setBounds(41, 132, 97, 23);
	    st.put("ucicomtest", "uC1COMTEST");
	    st.put("uc2comtest", "uC2COMTEST");
	    st.put("ipctest","IPCTEST");
	    st.put("uc1pwrtest","uC1PWRTEST");
	    st.put("uc2pwrtest","uC2PWRTEST");
	    if(chckbxb.isSelected())
	    {
	    	
	    }
	    
	    
	    st.put("uC1COM", null);
	    st.put("4","");
	    st.put("5","");
	    st.put("6","");
	    JTable t=new JTable(st.size(),5);
	    t.setBounds(17, 5, 150, 0);
	    t.setModel(toTableModel(st));
	    JPanel p=new JPanel();
	    p.setLayout(null);
	    p.add(t);
	    for(int i=0;i<t.getModel().getRowCount();i++)
	    {
	    	t.getModel().setValueAt(i+1, i, 0);
	    }
	    JFrame f=new JFrame();
	    f.getContentPane().add(p);
	    
	    p.add(chckbxb);
	    f.setSize(200,200);
	    f.setVisible(true);
	}		
	public static TableModel toTableModel(Map<?,?> map) {
		DefaultTableModel model = new DefaultTableModel(new Object[] { "Key", "Value" }, 0);
		for (Map.Entry<?,?> entry : map.entrySet()) {
			 model.addRow(new Object[] { entry.getKey(), entry.getValue() });
		}
		return model;
	}
			
	}


