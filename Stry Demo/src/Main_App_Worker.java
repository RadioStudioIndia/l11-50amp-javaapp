import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.JToggleButton;
import javax.swing.SwingWorker;
import javax.tools.SimpleJavaFileObject;
import javax.xml.bind.DatatypeConverter;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;








public class Main_App_Worker extends SwingWorker<String, String> {
	
	// Enumerator that holds different Error Flag Values
		public enum RespError_Flag {
			Response_OK						((byte)(0x0)), 
			Framing_Error 					((byte)(0x1)),
			Checksum_Error					((byte)(0x2)), 
			Stuffing_Error					((byte)(0x3)), 
			Unknown_Cmd_Error				((byte)(0x4)),
			Payload_Length_Error			((byte)(0x5)),
			IPCMutex_Timeout_Error 			((byte)(0x6)),
			Unknown_MsgType_Error			((byte)(0x7)),
			Unknown_State_Error				((byte)(0x8)),
			Timeout_Error					((byte)(0x9)),
			IPC_Error						((byte)(0xA)),
			Cmd_Execution_Error				((byte)(0xB)),
			ResponseType_MismatchError		((byte)(0x4)), 
			CommandMode_MismatchError		((byte)(0x5)), 
			InvalidCommand_ModeError		((byte)(0x6));
			
			
			
			private byte value;

			private RespError_Flag(byte value) {
				this.value = value;
			}
		}
		//Error Message Definition
		public static int Error_Message = JOptionPane.ERROR_MESSAGE;
	//lockObject Definition
	public final static Object lockObject = new Object();
		
	//Schedule Future Description
	public static ScheduledFuture<?> future; //for serialport incoming data event
	
	public static  ScheduledFuture<?> futuree; //for triggering timeout timer
	
	//Scheduler Description
	public static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(3);
	//Constructor Parameters Definitions
	
	public static JComponent Component;
	public static int delay = 1000;
	//Timer Definitions
	public Timer t;
	public Timer live_timer_on = null;
	public Timer live_timer_off = null;
	public static String Current_Control = "";
	//public static String Current_Control = "";
	public static String INVALID_PORT = "INVALID_PORT";
	public static String SP_OPEN_ERROR = "SP_OPEN_ERROR";
	public static String INIT_OK = "{OK,";
	public static String INIT_FAIL = "{FAIL,";
	
	public static boolean switch_flag = false;
	static byte[] destuff_buffer;
	public static ArrayList<Byte> IPCPacket;
	public static byte LIGHT_SOURCE;
	public static byte STATE ;
	public static byte MODE;
	public static byte PWM_SOURCE;
	public static byte PWM_TYPE;
	public static byte PWM_FREQ;
	public static byte TYPE_VAL;
	
	public static boolean Null_Charflag = false;
	public static boolean Listener_Flag;
	public static boolean Write_flag = false;
	public static boolean live_flag = false;
	
	public static SerialPort serialport; 
	public static int totalbytes = 0;
	public static int Timeout_Flag = 0;
	public static int Notify_Flag = 0;
	public static int resp_error_type = 0x0;
	public static int reference_index = 0;
	public static int slider_value = 0;
	
	public static byte Expected_Cmd_Id;
	public static byte Expected_Msg_Type;
	public static byte SOURCE;
	
	//String Definitions
	public static final String Calibration = "CALIBRATION";
	public static final String Connect = "CONNECT";
	public static final String Disconnect = "DISCONNECT";
	public static final String Power_Supply_Slider = "LIGHT_ENGINE_FAN_CONTROL";
	public static final String Heat_Sink_Slider = "HEAT_SINK_FAN_CONTROL";
	public static final String Iris_Fan_Slider = "IRIS_FAN_CONTROL";
	public static final String Save = "SAVE";
	public static final String Last = "LAST_MODE";
	public static final String ReadAll = "READ_ALL";
	public static final String Group_Slider = "WL_SLIDER";
	public static final String Slider = "SLIDER";
	public static final String Reset_uC1 = "RESET1";
	public static final String Reset_uC2 = "RESET2";
	public static final String LOAD_DEF1 = "LD1";
	public static final String LOAD_DEF2 = "LD2";
	public static final String ENABLE_DISABLE = "ENABLE_DISABLE";
	public static final String IRIS_POWER_SHUTDOWN = "IRIS_PWR_SDN";
	public static final String LIVE= "LIVE";
	public static final String LIVE_OFF = "LIVE_OFF";
	public static byte[] recv_buff;
	public static final String White_Light= "WL_POWER";
	public static final String Laser_Light = "LASER_POWER";
	public static final String Iris_Light = "IRIS_POWER";
	public static final String INDIVIDUAL = "RDBTN_INDIVIDUAL";
	public static final String WL_PON = "WL_POWER_ON";
	public static final String WL_POFF = "WL_POWER_OFF";
	public static final String ENV_IRIS_ON = "ENV_IRIS_ON";
	public static final String ENV_IRIS_OFF = "ENV_IRIS_OFF";
	public static final String LIGHT = "LIGHT";
	public static final String CWM_NR_ON = "CWM_NR_ON";
	public static final String CWM_NR_OFF = "CWM_NR_OFF";
	public static final String ENV_IRIS_SEL_FALSE = "FALSE";
	public static final String INDIVIDUAL_CONTROL = "IND";
	
	public static final String WL_FAKE_ON = "WL_FAKE_ON";
	public static final String WL_FAKE_OFF = "WL_FAKE_OFF";
	public static  final String ENV_IRIS_FAKE_ON = "ENV_IRIS_FAKE_ON";
	public static final String ENV_IRIS_FAKE_OFF = "ENV_IRIS_FAKE_OFF";
	
	protected static final ByteArrayOutputStream full_buffer = new ByteArrayOutputStream();

	
	public static long serial_read_cmd_timeout = 2000; //Max time in ms after which timeout is triggered if at all no data is received
	public static long serial_read_byte_timeout = 50;
	
	
	public Main_App_Worker(JComponent Component,String Current_Control, ArrayList<Byte> IPCPacket,byte STATE, byte SOURCE,byte MODE, byte PWM_SOURCE, byte PWM_TYPE, byte PWM_VAL, boolean Listener_Flag, SerialPort sp, int slider_value, byte PWM_FREQ) {
		// TODO Auto-generated constructor stub
		//Simple Animator = new Simple();
		//System.out.println("STATE in DIB : " + STATE);
		//System.out.println("SOURCE in DIB : "+ SOURCE);
		this.Component = Component;
		//this.Current_Control = Current_Control;
		this.Current_Control = Current_Control;
		this.IPCPacket = IPCPacket;
		this.LIGHT_SOURCE = LIGHT_SOURCE;
		this.STATE = STATE;
		this.MODE = MODE;
		this.PWM_SOURCE = PWM_SOURCE;
		this.PWM_TYPE = PWM_TYPE;
		this.TYPE_VAL = PWM_VAL;
		this.Listener_Flag = Listener_Flag;
		this.serialport = sp;
		this.SOURCE = SOURCE;
		this.slider_value = slider_value;
		this.PWM_FREQ = PWM_FREQ;
		System.out.println("Inside Constructor!!");
		System.out.println("**********************STATE : " + STATE + "****************************");
		System.out.println("***********************SOURCE : " + SOURCE + "*****************************");
	}

	@Override
	protected String doInBackground() throws Exception {
		
		//System.out.println("Inside Do In BackGround");
		System.out.println("Current_Control : " + Current_Control);
		bytedefinitions b = new bytedefinitions();
		List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
		Stuffed_Buffer.clear();
		byte[] SerialBytes = null;
		// TODO Auto-generated method stub
		byte[] payload_length = new byte[2];//common for all IPC Commands
		byte[] payload_byte = new byte[2];//Common for all IPC Commands
		int chk_sum;//Common for all IPC Commands
		byte[] chksum = new byte[2];// Common for all IPC Commands
		if(!serialport.isOpen()) {
			System.out.println(LocalDateTime.now());
			serialport.openPort();
			System.out.println(LocalDateTime.now());
		}
		
			System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$Before Switch Case$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			switch(Current_Control)
			{
			case Calibration:
				SerialBytes = null;
				SerialBytes = "{CAL_INIT}".getBytes();
				String Calibration_String = Current_Control;
				System.out.println("Inside Calibration Command Case!!");
				publish("Sending Calibration Command!!\r\n");
				Send_Cmd_and_wait_for_Timeout(Calibration_String,SerialBytes, Listener_Flag);
				System.out.println("Returning value for doInBackground");
				return Calibration_String;
				
			case Connect:
				String Connect_String = Current_Control;
				System.out.println("Inside Connect Command Case...");
				publish("Sending Connect Command\r\n");
				
				Expected_Msg_Type = b.MSG_TYPE_RESP; //Expected Message type 
				Expected_Cmd_Id = b.CONNECT_COMMAND; //Expected Command ID
				 // Current Control String
				IPCPacket.add(b.MSG_TYPE_CMD); // Message type is command Message
				IPCPacket.add((byte) b.CONNECT_COMMAND); // Add Byte for Connect Command's Command ID
				payload_length[0] = (byte) (0x0 & 0xff); //NO Payload and hence PalyLoad LSB and MSB are zero
				payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
				
				//Add Pay load Bytes
				IPCPacket.add(payload_length[0]); //LSB
				IPCPacket.add(payload_length[1]); //MSB
				
				//Add Reserved Field Bytes
				for (int i = 0; i < b.RESERVED_FIELDS.length; i++) 
				{
					IPCPacket.add(b.RESERVED_FIELDS[i]);// Reserved Fields added here
				}
				System.out.println("After for loop");
				chk_sum = b.calculateCRC(IPCPacket.subList(0, IPCPacket.size()));
				chksum[0] = (byte) (chk_sum & 0xff);
				chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
				IPCPacket.add(chksum[0]);
				IPCPacket.add(chksum[1]);
				
				//Function to Stuff the Actual Buffer
				Stuffed_Buffer.clear();
				Stuffed_Buffer = Stuff_Packet(IPCPacket.subList(0, IPCPacket.size())); //Stuff the buffer 
				
				IPCPacket.clear(); //Clear the Old ArrayList 
				IPCPacket = new ArrayList(Stuffed_Buffer); //populate it with the stuffed ArrayList
				
				IPCPacket.add(0, b.START_byte);  //add start byte at the 0'th location of the ArrayList
				IPCPacket.add(b.END_byte); //add end byte at the end of the ArrayList
				
				// #print_al(w);
				SerialBytes = null;
				SerialBytes = al_to_byte(IPCPacket);
				System.out.println("Before try");
				
				// 	Send data frame to serial port, trigger timer and wait for a response
				try
				{
					System.out.println("Sending Connect Command!!");
					Send_Cmd_and_wait_for_Timeout(Connect_String,SerialBytes, Listener_Flag);
					System.out.println("Returning value for doInBackground");
					return Connect_String;
					
				}catch(Exception ex)
				{
					System.out.println("Exception While Sending " + Connect_String + " Command!!");
					ex.printStackTrace();
				}
				System.out.println("Breaking Connect Command!!");
				
				break;
				
				
			//Send Disconnect Command	
			case Disconnect:
				String Disconnect_String = Current_Control;
				System.out.println("Inside Disconnect Command Case!!");
				Live disconnect = new Live("Waiting for Live Update to complete...");
				disconnect.Animation.setVisible(true);
				
				synchronized(lockObject)
				{
					while(live_flag)
					{
						System.out.println("Waiting For Live Flag Inside Disconnect Case...");
						lockObject.wait(10);
						
					}
					
				}
				disconnect.Animation.setVisible(false);
				
				boolean Disconnect_Listener_Flag = Listener_Flag;
				
				
				System.out.println("Inside Disconnect Command Case...");
				publish("Sending Disconnect Command\r\n");
				
				Expected_Msg_Type = b.MSG_TYPE_RESP; //Expected Message type 
				Expected_Cmd_Id = b.DISCONNECT_COMMAND; //Expected Command ID
				 // Current Control String
				IPCPacket.add(b.MSG_TYPE_CMD); // Message type is command Message
				IPCPacket.add((byte) b.DISCONNECT_COMMAND); // Add Byte for Connect Command's Command ID
				payload_length[0] = (byte) (0x0 & 0xff); //NO Payload and hence PalyLoad LSB and MSB are zero
				payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
				
				//Add Pay load Bytes
				IPCPacket.add(payload_length[0]); //LSB
				IPCPacket.add(payload_length[1]); //MSB
				
				//Add Reserved Field Bytes
				for (int i = 0; i < b.RESERVED_FIELDS.length; i++) 
				{
					IPCPacket.add(b.RESERVED_FIELDS[i]);// Reserved Fields added here
				}
				System.out.println("After for loop");
				chk_sum = b.calculateCRC(IPCPacket.subList(0, IPCPacket.size()));
				chksum[0] = (byte) (chk_sum & 0xff);
				chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
				IPCPacket.add(chksum[0]);
				IPCPacket.add(chksum[1]);
				
				//Function to Stuff the Actual Buffer
				Stuffed_Buffer.clear();
				Stuffed_Buffer = Stuff_Packet(IPCPacket.subList(0, IPCPacket.size())); //Stuff the buffer 
				
				IPCPacket.clear(); //Clear the Old ArrayList 
				IPCPacket = new ArrayList(Stuffed_Buffer); //populate it with the stuffed ArrayList
				
				IPCPacket.add(0, b.START_byte);  //add start byte at the 0'th location of the ArrayList
				IPCPacket.add(b.END_byte); //add end byte at the end of the ArrayList
				
				// #print_al(w);
				SerialBytes = null;
				SerialBytes = al_to_byte(IPCPacket);
				//System.out.println("Before try");
				
				// 	Send data frame to serial port, trigger timer and wait for a response
				try
				{
					System.out.println("Sending Disconnect Command!!");
					Send_Cmd_and_wait_for_Timeout(Disconnect_String,SerialBytes, Disconnect_Listener_Flag);
					return Disconnect_String;
					
				}catch(Exception ex)
				{
					System.out.println("Exception While Sending " + Disconnect_String + " Command!!");
					ex.printStackTrace();
				}
				
				
				if(serialport.isOpen())
				{
					serialport.closePort();
				}
				break;
				
			case Save:
				String Save_String = Current_Control;
				System.out.println("Inside Save Command Case!!");
				Live Save = new Live("Waiting for Live Update to complete...");
				Save.Animation.setVisible(true);
				
				synchronized(lockObject)
				{
					while(live_flag)
					{
						System.out.println("Waiting For Live_Flag inside Save Command Case ");
						lockObject.wait(10);
						
					}
					
				}
				Save.Animation.setVisible(false);
				
				boolean Save_Listener = Listener_Flag;
				Expected_Msg_Type = b.MSG_TYPE_RESP; //Expected Message type 
				Expected_Cmd_Id = b.SAVE_COMMAND;
				//System.out.println("Inside Save Command Case");
				IPCPacket.add(b.MSG_TYPE_CMD);
				IPCPacket.add((byte) b.SAVE_COMMAND);
				//No Payload Length for Save Command
				payload_length[0] = (byte) (0x0 & 0xff);
				payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
				IPCPacket.add(payload_length[0]);
				IPCPacket.add(payload_length[1]);
				for (int i = 0; i < b.RESERVED_FIELDS.length; i++) 
				{
					IPCPacket.add(b.RESERVED_FIELDS[i]);
				}
				
				chk_sum = b.calculateCRC(IPCPacket.subList(0, IPCPacket.size()));
				chksum[0] = (byte) (chk_sum & 0xff);
				chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
				IPCPacket.add(chksum[0]);
				IPCPacket.add(chksum[1]);
				Stuffed_Buffer.clear();
				//Function to Stuff the Actual Buffer
				Stuffed_Buffer = Stuff_Packet(IPCPacket.subList(0, IPCPacket.size())); //Stuff the buffer 
				//Buffer contents for stuffing - buffer without start byte and end byte
				//LOG$print("###################Stuffed Buffer###################");
				//LOG$for(byte by : Stuffed_Buffer)
				//LOG${
				//LOG$	print(Hex(by,2));
				//LOG$}
				IPCPacket.clear(); //Clear the Old ArrayList 
				IPCPacket = new ArrayList(Stuffed_Buffer); //populate it with the stuffed ArrayList
				
				IPCPacket.add(0, b.START_byte);  //add start byte at the 0'th location of the ArrayList
				IPCPacket.add(b.END_byte);
				// #print_al(w);
				SerialBytes = null;
				SerialBytes = al_to_byte(IPCPacket);
				try
				{
					System.out.println("Sending Save Configuration Command!!");
					Send_Cmd_and_wait_for_Timeout(Save_String,SerialBytes, Save_Listener);
					return Save_String;
					
				}catch(Exception ex)
				{
					System.out.println("Exception While Sending " + Save_String + " Command!!");
					ex.printStackTrace();
				}
				break;
				
			case Last:
				String Last_Mode = Current_Control;
				System.out.println("Inside Last mode Command Case!!");
				Live Last = new Live("Waiting for Live Update to complete...");
				Last.Animation.setVisible(true);
				synchronized(lockObject)
				{
					while(live_flag)
					{
						System.out.println("Waiting For Live_Flag inside Last Mode Command Case!!");
						lockObject.wait(10);
						
					}
					
				}
				Last.Animation.setVisible(false);
				
				boolean Last_Mode_Listener = Listener_Flag;
				Expected_Msg_Type = b.MSG_TYPE_RESP; //Expected Message type 
				Expected_Cmd_Id = b.RESTORE_CMD;
				//System.out.println("Inside Last Command Case");
				IPCPacket.add(b.MSG_TYPE_CMD);
				IPCPacket.add((byte) b.RESTORE_CMD);
				//No Payload Length for Save Command
				payload_length[0] = (byte) (0x0 & 0xff);
				payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
				IPCPacket.add(payload_length[0]);
				IPCPacket.add(payload_length[1]);
				for (int i = 0; i < b.RESERVED_FIELDS.length; i++) 
				{
					IPCPacket.add(b.RESERVED_FIELDS[i]);
				}
				
				chk_sum = b.calculateCRC(IPCPacket.subList(0, IPCPacket.size()));
				chksum[0] = (byte) (chk_sum & 0xff);
				chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
				IPCPacket.add(chksum[0]);
				IPCPacket.add(chksum[1]);
				Stuffed_Buffer.clear();
				//Function to Stuff the Actual Buffer
				Stuffed_Buffer = Stuff_Packet(IPCPacket.subList(0, IPCPacket.size())); //Stuff the buffer 
				//Buffer contents for stuffing - buffer without start byte and end byte
				//LOG$print("###################Stuffed Buffer###################");
				//LOG$for(byte by : Stuffed_Buffer)
				//LOG${
				//LOG$	print(Hex(by,2));
				//LOG$}
				IPCPacket.clear(); //Clear the Old ArrayList 
				IPCPacket = new ArrayList(Stuffed_Buffer); //populate it with the stuffed ArrayList
				
				IPCPacket.add(0, b.START_byte);  //add start byte at the 0'th location of the ArrayList
				IPCPacket.add(b.END_byte);
				// #print_al(w);
				SerialBytes = null;
				SerialBytes = al_to_byte(IPCPacket);
				try
				{
					System.out.println("Sending Last Mode Command!!");
					Send_Cmd_and_wait_for_Timeout(Last_Mode,SerialBytes, Last_Mode_Listener);
					return Last_Mode;
					
				}catch(Exception ex)
				{
					System.out.println("Exception While Sending " + Last_Mode + " Command!!");
					ex.printStackTrace();
				}
				break;
				
				
			//Send Read All Command
			case ReadAll:
				String ReadAll = Current_Control;
				System.out.println("Inside ReadAll Command Case!!");
				Live Read = new Live("Waiting for Live Update to complete...");
				Read.Animation.setVisible(true);
				synchronized(lockObject)
				{
					while(live_flag)
					{
						System.out.println("Waiting For Live_Flag inside ReadAll Command Case!!");
						lockObject.wait(10);
						
					}
					
				}
				Read.Animation.setVisible(false);
				
				
				boolean Read_All_Listener = Listener_Flag;
				//byte Expected_Read_Cmd_Id = Expected_Cmd_Id;
				Expected_Msg_Type = b.MSG_TYPE_RESP; //Expected Message type 
				Expected_Cmd_Id = b.SENSING_COMMAND;
				
				IPCPacket.add(b.MSG_TYPE_CMD);
				
				IPCPacket.add((byte) b.SENSING_COMMAND);
				payload_length[0] = (byte) (0x0 & 0xff);
				payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
				IPCPacket.add(payload_length[0]);
				IPCPacket.add(payload_length[1]);
				for (int i = 0; i < b.RESERVED_FIELDS.length; i++) 
				{
					IPCPacket.add(b.RESERVED_FIELDS[i]);
				}
				
				
				//List to hold the bytes except StartByte and End Byte - For CheckSum Calculation
				
				//Get the sublist into a collection (Buffer without Start and End Byte)
				// w.subList(1, w.size());//1-MsgType Upto w.Size() - Omits Start Byte
				////////////////Buffer//////////////////////////////////////////////////////////////////////////////
				////////|MSG_TYPE|CMD_ID|PAYLOADLEN-LOW|PAYLOADLEN-HIGH|RESFIELD0|RESFIELD1|RESFIELD2|PAYLOAD|//////
				////////////////////////////////////////////////////////////////////////////////////////////////////
				//Variable that stores the above values
				chk_sum = b.calculateCRC(IPCPacket.subList(0, IPCPacket.size())); // Variable that holds the checkSum
				chksum[0] = (byte) (chk_sum & 0xff);
				chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
				IPCPacket.add(chksum[0]);
				IPCPacket.add(chksum[1]);
				//List<Byte> d = new ArrayList<Byte>();
				//d = w.subList(1, w.size());
				//LOG$print("##############Actual Buffer####################");
				//LOG$for(byte byt : d)
				//LOG${
					//LOG$print(Hex(byt,2));
				//LOG$}
				
				//An Empty Buffer to Store the Stuffed Version of the input buffer
				Stuffed_Buffer.clear();
				//Function to Stuff the Actual Buffer
				Stuffed_Buffer = Stuff_Packet(IPCPacket.subList(0, IPCPacket.size()));
				//LOG$print("###################Stuffed Buffer###################");
				//LOG$for(byte by : Stuffed_Buffer)
				//LOG${
				//LOG$	print(Hex(by,2));
				//LOG$}
				IPCPacket.clear();
				IPCPacket = new ArrayList(Stuffed_Buffer);
				IPCPacket.add(0, b.START_byte);
			
				IPCPacket.add(b.END_byte);
				// #print_al(w);
				SerialBytes = null;
				SerialBytes = al_to_byte(IPCPacket);
				try
				{
					System.out.println("Sending Read All Command!!");
					Send_Cmd_and_wait_for_Timeout(ReadAll,SerialBytes, Read_All_Listener);
					return ReadAll;
					
				}catch(Exception ex)
				{
					System.out.println("Exception While Sending " + ReadAll + " Command!!");
					ex.printStackTrace();
				}
				break;
				//LOAD DEFAULT CASE
			case LOAD_DEF1:
				String Load_Def1 = Current_Control;
				Live LD1 = new Live("Waiting for Live Update to complete...");
				LD1.Animation.setVisible(true);
				System.out.println("Inside LoadDefault uC1 Command Case!!");
				synchronized(lockObject)
				{
					while(live_flag)
					{
						System.out.println("Waiting For Live_Flag inside Load Default uC1 Command Case!!");
						lockObject.wait(10);
						
					}
					
				}
				LD1.Animation.setVisible(false);
				
				
				boolean LD1_Listener = Listener_Flag;
				byte Which_controller_1 = STATE;
				Expected_Msg_Type = b.MSG_TYPE_RESP; //Expected Message type 
				Expected_Cmd_Id = b.LOAD_DEFAULT_COMMAND;
				IPCPacket.add(b.MSG_TYPE_NOTIFY);
				IPCPacket.add((byte) b.LOAD_DEFAULT_COMMAND);
				payload_length[0] = (byte) (0x1 & 0xff);
				payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
				IPCPacket.add(payload_length[0]);
				IPCPacket.add(payload_length[1]);
				for (int i = 0; i < b.RESERVED_FIELDS.length; i++) 
				{
					IPCPacket.add(b.RESERVED_FIELDS[i]);
				}
				
				IPCPacket.add(Which_controller_1);
				//#print("Size : " + Integer.toString(w.size()));
				
				chk_sum = b.calculateCRC(IPCPacket.subList(0, IPCPacket.size()));
				chksum[0] = (byte) (chk_sum & 0xff);
				chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
				IPCPacket.add(chksum[0]);
				IPCPacket.add(chksum[1]);
				Stuffed_Buffer.clear();
				//Function to Stuff the Actual Buffer
				Stuffed_Buffer = Stuff_Packet(IPCPacket.subList(0, IPCPacket.size())); //Stuff the buffer 
				//Buffer contents for stuffing - buffer without start byte and end byte
				//LOG$print("###################Stuffed Buffer###################");
				//LOG$for(byte by : Stuffed_Buffer)
				//LOG${
				//LOG$	print(Hex(by,2));
				//LOG$}
				IPCPacket.clear(); //Clear the Old ArrayList 
				IPCPacket = new ArrayList(Stuffed_Buffer); //populate it with the stuffed ArrayList
				
				IPCPacket.add(0, b.START_byte);  //add start byte at the 0'th location of the ArrayList
				IPCPacket.add(b.END_byte);
				// #print_al(w);
				SerialBytes = null;
				SerialBytes = al_to_byte(IPCPacket);
				// Send data frame to serialport and wait for a response


				try
				{
					System.out.println("Sending " + Load_Def1 + " Command!!");
					publish("CHECK_ENABLE," + Load_Def1);
					if(serialport!=null)
					{
						serialport.writeBytes(SerialBytes, SerialBytes.length);
					}
					//Send_Cmd_and_wait_for_Timeout(Current_Control,SerialBytes, Listener_Flag);
					Thread.sleep(10);
					Stryk_Demo.pause_flag = false;
					return Load_Def1;
					//Notify_Flag = 1;
					
				}catch(Exception ex)
				{
					System.out.println("Exception While Sending " + Load_Def1 + " Command!!");
					ex.printStackTrace();
				}
				break;
				//LOAD DEFAULT 2
			case LOAD_DEF2:
				String Load_Def2 = Current_Control;
				System.out.println("Inside LoadDefault uC2 Command Case!!");
				Live LD2 = new Live("Waiting for Live Update to complete...");
				LD2.Animation.setVisible(true);
				synchronized(lockObject)
				{
					while(live_flag)
					{
						System.out.println("Waiting For Live_Flag inside Load Default uC2 Command Case!!");
						lockObject.wait(10);
						
					}
					
				}
				LD2.Animation.setVisible(false);
				
				
				boolean LD2_Listener = Listener_Flag;
				byte Which_controller_2 = STATE;
				Expected_Msg_Type = b.MSG_TYPE_RESP; //Expected Message type 
				Expected_Cmd_Id = b.LOAD_DEFAULT_COMMAND;
				IPCPacket.add(b.MSG_TYPE_NOTIFY);
				IPCPacket.add((byte) b.LOAD_DEFAULT_COMMAND);
				payload_length[0] = (byte) (0x1 & 0xff);
				payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
				IPCPacket.add(payload_length[0]);
				IPCPacket.add(payload_length[1]);
				for (int i = 0; i < b.RESERVED_FIELDS.length; i++) 
				{
					IPCPacket.add(b.RESERVED_FIELDS[i]);
				}
				
				IPCPacket.add(Which_controller_2);
				//#print("Size : " + Integer.toString(w.size()));
				
				chk_sum = b.calculateCRC(IPCPacket.subList(0, IPCPacket.size()));
				chksum[0] = (byte) (chk_sum & 0xff);
				chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
				IPCPacket.add(chksum[0]);
				IPCPacket.add(chksum[1]);
				Stuffed_Buffer.clear();
				//Function to Stuff the Actual Buffer
				Stuffed_Buffer = Stuff_Packet(IPCPacket.subList(0, IPCPacket.size())); //Stuff the buffer 
				//Buffer contents for stuffing - buffer without start byte and end byte
				//LOG$print("###################Stuffed Buffer###################");
				//LOG$for(byte by : Stuffed_Buffer)
				//LOG${
				//LOG$	print(Hex(by,2));
				//LOG$}
				IPCPacket.clear(); //Clear the Old ArrayList 
				IPCPacket = new ArrayList(Stuffed_Buffer); //populate it with the stuffed ArrayList
				
				IPCPacket.add(0, b.START_byte);  //add start byte at the 0'th location of the ArrayList
				IPCPacket.add(b.END_byte);
				// #print_al(w);
				SerialBytes = null;
				SerialBytes = al_to_byte(IPCPacket);
				try
				{
					System.out.println("Sending " + Load_Def2 + " Command!!");
					publish("CHECK_ENABLE,"+ Load_Def2);
					if(serialport!=null)
					{
						serialport.writeBytes(SerialBytes, SerialBytes.length);
					}
					//Send_Cmd_and_wait_for_Timeout(Current_Control,SerialBytes, Listener_Flag);
					Thread.sleep(10);
					Stryk_Demo.pause_flag = false;
					return Load_Def2;
					
				}catch(Exception ex)
				{
					System.out.println("Exception While Sending " + Current_Control + " Command!!");
					ex.printStackTrace();
				}
				break;
		//For White Light Command
		case White_Light:
			break;
		//For Laser Light Command
		case Laser_Light:
			break;
		//For IRIS Light Command
		case Iris_Light:
			break;
			
		
		case Slider:
			
			String Slider_Control = Current_Control;
			System.out.println("Inside Slider Command Case!!");
			Live SLD = new Live("Waiting for Live Update to complete...");
			SLD.Animation.setVisible(true);
			synchronized(lockObject)
			{
				while(live_flag)
				{
					System.out.println("Waiting For Live_Flag Inside Slider Command Case...");
					lockObject.wait(10);
					
				}
				
			}
			SLD.Animation.setVisible(false);
			
			
			byte Slider_State = STATE;
			byte Slider_Source = SOURCE;
			int slider_source_for_16Bit = slider_value;
			boolean Slider_LFlag = Listener_Flag;
			Expected_Msg_Type = b.MSG_TYPE_RESP; //Expected Message type 
			Expected_Cmd_Id = b.INTENSITY_COMMAND;
			System.out.println("Inside " + Current_Control +  " Case");
			//Expected_Msg_Type = b.MSG_TYPE_RESP; //Expected Message type 
			//Expected_Cmd_Id = b.INTENSITY_PWR_FAN; //Expected Command ID
			
			IPCPacket.add(b.MSG_TYPE_CMD);
			IPCPacket.add(b.INTENSITY_COMMAND);
			
			if (Slider_State == new bytedefinitions().INTENSITY_PWR_FAN | Slider_State == new bytedefinitions().INTENSITY_HSINK_FAN
					| Slider_State == new bytedefinitions().INTENSITY_IRIS_FAN | Slider_State == new bytedefinitions().INTENSITY_SLIDER_IRIS1
					| Slider_State == new bytedefinitions().INTENSITY_SLIDER_IRIS2) {
				payload_length[0] = (byte) (0x2 & 0xff);
				payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
				IPCPacket.add(payload_length[0]);
				IPCPacket.add(payload_length[1]);
				for (int i = 0; i < b.RESERVED_FIELDS.length; i++) 
				{
					IPCPacket.add(b.RESERVED_FIELDS[i]);
				}
				// w.add(b.TYPE_PWM);
				IPCPacket.add(Slider_State);
				IPCPacket.add(Slider_Source);
			} else {
				payload_length[0] = (byte) (0x3 & 0xff);
				payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
				IPCPacket.add(payload_length[0]);
				IPCPacket.add(payload_length[1]);
				for (int i = 0; i < b.RESERVED_FIELDS.length; i++) 
				{
					IPCPacket.add(b.RESERVED_FIELDS[i]);
				}
				// w.add(b.TYPE_PWM);
				IPCPacket.add(Slider_State);
				
				payload_byte[0] = (byte) (slider_source_for_16Bit & 0xff);
				payload_byte[1] = (byte) ((slider_source_for_16Bit >> 8) & 0xff);
				IPCPacket.add(payload_byte[1]);
				IPCPacket.add(payload_byte[0]);
			}
			//IPCPacket.add(Slider_Source);
			
			Slider_Control = Get_Slider_Name(Slider_State);
			chk_sum = b.calculateCRC(IPCPacket.subList(0, IPCPacket.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			IPCPacket.add(chksum[0]);
			IPCPacket.add(chksum[1]);
			Stuffed_Buffer.clear();
			//Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(IPCPacket.subList(0, IPCPacket.size())); //Stuff the buffer 
			//Buffer contents for stuffing - buffer without start byte and end byte
			//LOG$print("###################Stuffed Buffer###################");
			//LOG$for(byte by : Stuffed_Buffer)
			//LOG${
			//LOG$	print(Hex(by,2));
			//LOG$}
			IPCPacket.clear(); //Clear the Old ArrayList 
			IPCPacket = new ArrayList(Stuffed_Buffer); //populate it with the stuffed ArrayList
			
			IPCPacket.add(0, b.START_byte);  //add start byte at the 0'th location of the ArrayList
			IPCPacket.add(b.END_byte);
			// #print_al(w);
			SerialBytes = null;
			SerialBytes = al_to_byte(IPCPacket);
			try
			{
				System.out.println("Sending " + Slider_Control + " Intensity Command!!");
				if(serialport!=null)
				{
					//serialport.writeBytes(SerialBytes, SerialBytes.length);
				}
				Send_Cmd_and_wait_for_Timeout(Slider_Control,SerialBytes, true);
				//Thread.sleep(10);
				//Stryk_Demo.pause_flag = false;
				return Slider_Control;
				
			}catch(Exception ex)
			{
				System.out.println("Exception While Sending " + Slider_Control + " Intensity Command!!");
				ex.printStackTrace();
			}
			break;
		
		case Reset_uC1:
			String Reset1_Control = Current_Control;
			System.out.println("Inside Reset uC1 Command Case!!");
			Live RS1 = new Live("Waiting for Live Update to complete...");
			RS1.Animation.setVisible(true);
			synchronized(lockObject)
			{
				while(live_flag)
				{
					System.out.println("Waiting For Live_Flag Inside uC1 Reset Command Case...");
					lockObject.wait(10);
					
				}
				
			}
			RS1.Animation.setVisible(false);
			
			//String Reset1_Control = Current_Control;
			byte Reset1_State = STATE;
			byte Reset1_Source = SOURCE;
			//boolean Slider_LFlag = Listener_Flag;
			Expected_Msg_Type = b.EXPECT_NO_RESPONSE; //Expected Message type 
			Expected_Cmd_Id = b.EXPECT_NO_RESPONSE;
			IPCPacket.add(b.MSG_TYPE_NOTIFY);
			IPCPacket.add((byte) b.RESET_uC_COMMAND);
			payload_length[0] = (byte) (0x1 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			IPCPacket.add(payload_length[0]);
			IPCPacket.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) 
			{
				IPCPacket.add(b.RESERVED_FIELDS[i]);
			}
			
			IPCPacket.add(Reset1_State);
			//#print("Size : " + Integer.toString(w.size()));
			
			chk_sum = b.calculateCRC(IPCPacket.subList(0, IPCPacket.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			IPCPacket.add(chksum[0]);
			IPCPacket.add(chksum[1]);
			Stuffed_Buffer.clear();
			//Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(IPCPacket.subList(0, IPCPacket.size())); //Stuff the buffer 
			//Buffer contents for stuffing - buffer without start byte and end byte
			//LOG$print("###################Stuffed Buffer###################");
			//LOG$for(byte by : Stuffed_Buffer)
			//LOG${
			//LOG$	print(Hex(by,2));
			//LOG$}
			IPCPacket.clear(); //Clear the Old ArrayList 
			IPCPacket = new ArrayList(Stuffed_Buffer); //populate it with the stuffed ArrayList
			
			IPCPacket.add(0, b.START_byte);  //add start byte at the 0'th location of the ArrayList
			IPCPacket.add(b.END_byte);
			// #print_al(w);
			SerialBytes = null;
			SerialBytes = al_to_byte(IPCPacket);
			// Send data frame to serialport and wait for a response
			try
			{
				System.out.println("Sending " + Reset1_Control + " Command!!");
				publish("CHECK_ENABLE," + Reset1_Control);
				if(serialport!=null)
				{
					serialport.writeBytes(SerialBytes, SerialBytes.length);
				}
				//Send_Cmd_and_wait_for_Timeout(Reset1_Control,SerialBytes, Listener_Flag);
				Thread.sleep(10);
				Stryk_Demo.pause_flag = false;
				return Reset1_Control;
				
			}catch(Exception ex)
			{
				System.out.println("Exception While Sending " + Reset1_Control + " Command!!");
				ex.printStackTrace();
			}
			break;
			
		case Reset_uC2:
			String Reset2_Control = Current_Control;
			System.out.println("Inside Reset uC2 Command Case!!");
			Live RS2 = new Live("Waiting for Live Update to complete...");
			RS2.Animation.setVisible(true);
			synchronized(lockObject)
			{
				while(live_flag)
				{
					System.out.println("Waiting For Live_Flag Inside uC2 Reset Command Case...");
					lockObject.wait(10);
					
				}
				
			}
			RS2.Animation.setVisible(false);
			
			//String Reset2_Control = Current_Control;
			byte Reset2_State = STATE;
			byte Reset2_Source = SOURCE;
			Expected_Cmd_Id = b.EXPECT_NO_RESPONSE;
			Expected_Msg_Type = b.EXPECT_NO_RESPONSE;
			IPCPacket.add(b.MSG_TYPE_NOTIFY);
			IPCPacket.add((byte) b.RESET_uC_COMMAND);
			payload_length[0] = (byte) (0x1 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			IPCPacket.add(payload_length[0]);
			IPCPacket.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) 
			{
				IPCPacket.add(b.RESERVED_FIELDS[i]);
			}
			
			IPCPacket.add(Reset2_State);
			//#print("Size : " + Integer.toString(w.size()));
			
			chk_sum = b.calculateCRC(IPCPacket.subList(0, IPCPacket.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			IPCPacket.add(chksum[0]);
			IPCPacket.add(chksum[1]);
			Stuffed_Buffer.clear();
			//Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(IPCPacket.subList(0, IPCPacket.size())); //Stuff the buffer 
			//Buffer contents for stuffing - buffer without start byte and end byte
			//LOG$print("###################Stuffed Buffer###################");
			//LOG$for(byte by : Stuffed_Buffer)
			//LOG${
			//LOG$	print(Hex(by,2));
			//LOG$}
			IPCPacket.clear(); //Clear the Old ArrayList 
			IPCPacket = new ArrayList(Stuffed_Buffer); //populate it with the stuffed ArrayList
			
			IPCPacket.add(0, b.START_byte);  //add start byte at the 0'th location of the ArrayList
			IPCPacket.add(b.END_byte);
			// #print_al(w);
			SerialBytes = null;
			SerialBytes = al_to_byte(IPCPacket);
			// Send data frame to serialport and wait for a response
			try
			{
				System.out.println("Sending " + Reset2_Control + " Command!!");
				publish("CHECK_ENABLE," + Reset2_Control);
				if(serialport != null)
				{
					serialport.writeBytes(SerialBytes, SerialBytes.length);
				}
				//Send_Cmd_and_wait_for_Timeout(Current_Control,SerialBytes, Listener_Flag);
				Thread.sleep(10);
				Stryk_Demo.pause_flag = false;
				return Reset2_Control;
				
			}catch(Exception ex)
			{
				System.out.println("Exception While Sending " + Reset2_Control + " Command!!");
				ex.printStackTrace();
			}
			break;	
			
				
		case ENABLE_DISABLE:
			String actual_control = Current_Control;
			System.out.println("Inside Enable Disable command case!!");
			//Pakka
			Live END = new Live("Waiting for Live Update to complete...");
			END.Animation.setVisible(true);
			synchronized(lockObject)
			{
				while(live_flag)
				{
					System.out.println("Waiting For Live_Flag inside Enable Disable Command Case...");
					lockObject.wait(10);
					
				}
				
			}
			END.Animation.setVisible(false);
			
			//System.out.println("ListenerFlag : " + Listener_Flag);
			byte State = STATE;
			byte Source = SOURCE;
			
			boolean En_LFlag = Listener_Flag;
			if(Listener_Flag)
			{
				Listener_Flag = false;
			}
			System.out.println("Listener1Flag : " + Listener_Flag);
			String Control = "";
			System.out.println("ENABLE_DISABLE case");
			Expected_Cmd_Id = b.ENADIS_COMMAND;
			Expected_Msg_Type = b.MSG_TYPE_RESP;
			
			IPCPacket.add(b.MSG_TYPE_CMD);
			IPCPacket.add((byte) b.ENADIS_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			IPCPacket.add(payload_length[0]);
			IPCPacket.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) 
			{
				IPCPacket.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			System.out.println("STATE : " + State);
			IPCPacket.add(State);
			IPCPacket.add(Source);
			
			if(Source == (byte) 0x0)
			{
				Control = "DISABLE";
			}
			else if(Source == (byte) 0x1)
			{
				Control = "ENABLE";
			}
			//#print(Integer.toString(w.get(13)));
			actual_control = Get_Slider_Name(State);
			//#print("Size : " + Integer.toString(w.size()));
			
			chk_sum = b.calculateCRC(IPCPacket.subList(0, IPCPacket.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			IPCPacket.add(chksum[0]);
			IPCPacket.add(chksum[1]);
			Stuffed_Buffer.clear();
			//Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(IPCPacket.subList(0, IPCPacket.size())); //Stuff the buffer 
			//Buffer contents for stuffing - buffer without start byte and end byte
			//LOG$print("###################Stuffed Buffer###################");
			//LOG$for(byte by : Stuffed_Buffer)
			//LOG${
			//LOG$	print(Hex(by,2));
			//LOG$}
			IPCPacket.clear(); //Clear the Old ArrayList 
			IPCPacket = new ArrayList(Stuffed_Buffer); //populate it with the stuffed ArrayList
			
			IPCPacket.add(0, b.START_byte);  //add start byte at the 0'th location of the ArrayList

			IPCPacket.add(b.END_byte);
			// #print_al(w);
			SerialBytes = null;
			SerialBytes = al_to_byte(IPCPacket);
			try
			{
				System.out.println("Sending " + actual_control +  " " +Control +  " Command!!");
				publish("Sending " + Current_Control +  " " +Control +  " Command!!\r\n");
				//publish("CHECK_ENABLE," + actual_control);
				System.out.println("STATE : "  +State );
				System.out.println("SOURCE : " + Source);
				if(serialport!=null)
				{
					//serialport.writeBytes(SerialBytes, SerialBytes.length);
				}
				//Thread.sleep(10);
				
				//Stryk_Demo.pause_flag = false;
				
				Send_Cmd_and_wait_for_Timeout(actual_control,SerialBytes, true);
				return actual_control;
				
			}catch(Exception ex)
			{
				System.out.println("Exception While Sending " + actual_control +  " " +Control + " Command!!");
				ex.printStackTrace();
			}
			break;
		case IRIS_POWER_SHUTDOWN:
			String IRIS_Cmd = Current_Control;
			System.out.println("IRIS POWER SHUTDOWN case");
			Live IPWR = new Live("Waiting for Live Update to complete...");
			IPWR.Animation.setVisible(true);
			synchronized(lockObject)
			{
				while(live_flag)
				{
					System.out.println("Waiting For Live_Flag inside IRIS POWER SHUTDOWN Command Case...");
					lockObject.wait(10);
					
				}
				
			}
			IPWR.Animation.setVisible(false);
			
			
			byte Iris_Pwr_State = STATE;
			byte Iris_Pwr_Source = SOURCE;
			
			Expected_Cmd_Id = b.EXPECT_NO_RESPONSE;
			Expected_Msg_Type = b.EXPECT_NO_RESPONSE;
			
			IPCPacket.add(b.MSG_TYPE_NOTIFY);
			IPCPacket.add((byte) b.ENADIS_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff); //This Function Has Payload
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			IPCPacket.add(payload_length[0]);
			IPCPacket.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) 
			{
				IPCPacket.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			IPCPacket.add(Iris_Pwr_State);
			IPCPacket.add(Iris_Pwr_Source);
			
			
			System.out.println("STATE INSIDE IRIS_PWR_SHUTDOWN : " + Iris_Pwr_State);
			chk_sum = b.calculateCRC(IPCPacket.subList(0, IPCPacket.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			IPCPacket.add(chksum[0]);
			IPCPacket.add(chksum[1]);
			
			
			//An Empty Buffer to Store the Stuffed Version of the input buffer
			
			//Function to Stuff the Actual Buffer - Buffer that contains MSG_TYPE to CHECKSUM
			Stuffed_Buffer.clear();
			Stuffed_Buffer = Stuff_Packet( IPCPacket.subList(0, IPCPacket.size()));
			//#LOGprint("###################Stuffed Buffer###################");
			//#LOGfor(byte by : Stuffed_Buffer)
			//#LOG{
			//#LOG	print(Hex(by,2));
			//#LOG}
			IPCPacket.clear();
			IPCPacket = new ArrayList(Stuffed_Buffer);
			IPCPacket.add(0, b.START_byte);
			IPCPacket.add(b.END_byte);
			// #print_al(w);
			SerialBytes = null;
			SerialBytes = al_to_byte(IPCPacket);
			// Send data frame to serialport and wait for a response
			try
			{
				if(Iris_Pwr_Source == (byte) 0x0)
				{
					publish("DISABLE_IRIS_CONTROLS," + IRIS_Cmd);
				}
				else if(Iris_Pwr_Source == (byte) 0x1)
				{
					publish("ENABLE_IRIS_CONTROLS," + IRIS_Cmd);
				}
				System.out.println("Sending " + IRIS_Cmd + " Command!!");
				publish("Sending " + IRIS_Cmd +  " Command!!\r\n");
				Send_Cmd_and_wait_for_Timeout(IRIS_Cmd,SerialBytes, Listener_Flag);
				return IRIS_Cmd;
				
				
			}catch(Exception ex)
			{
				System.out.println("Exception While Sending " + Current_Control + " Command!!");
				ex.printStackTrace();
			}
			break;
		case INDIVIDUAL:
			//Pakka
			String Ind = Current_Control;
			Expected_Cmd_Id = b.EXPECT_NO_RESPONSE;
			Expected_Msg_Type = b.EXPECT_NO_RESPONSE;
			if(SOURCE == (byte) 0x1)
			{
				publish("INDIVIDUAL_SELECTED," + Ind);
				System.out.println("<--------------------------------SELECTED---------------------------->");
			}
			else if(SOURCE == (byte) 0x0)
			{
				publish("INDIVIDUAL_DESELECTED," + Ind);
				System.out.println("<--------------------------------DESELECTED---------------------------->");
			}
			break;
			
		case LIVE:
			//Pakka
			String Live_Control = Current_Control;
			System.out.println("<-----------############################################################--------------------------->");
			live_flag = true;
			System.out.println("Live Flag : " + live_flag);
			//System.out.println("Inside Live Command Update Case!!");
			
			boolean live_listener = Listener_Flag;
			Expected_Msg_Type = b.MSG_TYPE_RESP;
			Expected_Cmd_Id = b.LIVE_WIDTH_COMMAND;
			byte live_state = STATE;
			byte live_source = SOURCE;
			IPCPacket.add(b.MSG_TYPE_CMD);
			IPCPacket.add((byte) b.LIVE_WIDTH_COMMAND);
			//For Pay load Length Bytes
			payload_length[0] = (byte) (0x2 & 0xff); //PayLoad 2 Bytes
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			
			//Add Pay load Bytes
			IPCPacket.add(payload_length[0]); //LSB
			IPCPacket.add(payload_length[1]); //MSB
			
			//Add Reserved Field Bytes
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) 
			{
				IPCPacket.add(b.RESERVED_FIELDS[i]);
			}
			System.out.println("Control Inside Live Update : " + SOURCE);
			System.out.println("LIght Source Inside Live Update : " + STATE);
			
			IPCPacket.add(live_state);
			IPCPacket.add(live_source);
			chk_sum = b.calculateCRC(IPCPacket.subList(0, IPCPacket.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			IPCPacket.add(chksum[0]);
			IPCPacket.add(chksum[1]);
			Stuffed_Buffer.clear();
			//Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(IPCPacket.subList(0, IPCPacket.size())); //Stuff the buffer 
			//Buffer contents for stuffing - buffer without start byte and end byte
			//LOG$print("###################Stuffed Buffer###################");
			//LOG$for(byte by : Stuffed_Buffer)
			//LOG${
			//LOG$	print(Hex(by,2));
			//LOG$}
			IPCPacket.clear(); //Clear the Old ArrayList 
			IPCPacket = new ArrayList(Stuffed_Buffer); //populate it with the stuffed ArrayList
			
			IPCPacket.add(0, b.START_byte);  //add start byte at the 0'th location of the ArrayList
			IPCPacket.add(b.END_byte); //add end byte at the end of the ArrayList
			
			// #print_al(w);
			SerialBytes = null;
			SerialBytes = al_to_byte(IPCPacket);
			IPCPacket.clear();
			//System.out.println("############################LENGTH--------###################     " + SerialBytes.length);
			// 	Send data frame to serial port, trigger timer and wait for a response
			try
			{
				System.out.println("Sending " + Live_Control +  " Command!!");
				publish("Sending " + Live_Control +   " Command!!\r\n");
				Send_Cmd_and_wait_for_Timeout(Live_Control,SerialBytes, live_listener);
				return Live_Control;
				
			}catch(Exception ex)
			{
				System.out.println("Exception While Sending " + Live_Control + " Command!!");
				ex.printStackTrace();
			}
			break;
		case WL_PON:
			//Pakka
			
			String Wl_On_Ctrl = Current_Control;
			System.out.println("Inside WL_PON Case!!");
			Live WLON = new Live("Waiting for Live Update to complete...");
			WLON.Animation.setVisible(true);
			
			//wait until live flag is true
			synchronized(lockObject)
			{
				while(live_flag)
				{
					System.out.println("Waiting ");
					lockObject.wait(10);
					
				}
				
			}
			WLON.Animation.setVisible(false);
			
			
			//local copy of the global params
			byte State_Wl_on = STATE;
			byte Source_Wl_on = SOURCE;
			byte Mode_Wl_on = MODE;
			byte Pwm_Source_Wl_on = PWM_SOURCE;
			byte Pwm_Type_Wl_on = PWM_TYPE;
			byte Pwm_Val_Wl_on = TYPE_VAL;
			byte Pwm_Freq_Wl_on = PWM_FREQ;
			System.out.println("STATE : " + State_Wl_on);
			System.out.println("SOURCE : " + Source_Wl_on);
			System.out.println("MODE : " + Mode_Wl_on);
			System.out.println("PWM SOURCE : " + Pwm_Source_Wl_on);
			System.out.println("PWM TYPE : " + Pwm_Type_Wl_on);
			System.out.println("PWM VALUE : " + Pwm_Val_Wl_on);
			System.out.println("PWM FREQ : " + Pwm_Freq_Wl_on);
			//IPCPacket
			Expected_Cmd_Id = b.MODE_COMMAND;
			Expected_Msg_Type = b.MSG_TYPE_RESP;
			IPCPacket.add(b.MSG_TYPE_CMD);
			IPCPacket.add((byte) b.MODE_COMMAND);
			payload_length[0] = (byte) (0x6 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			IPCPacket.add(payload_length[0]);
			IPCPacket.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) 
			{
				IPCPacket.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			IPCPacket.add(State_Wl_on);
			IPCPacket.add(Source_Wl_on);
			
			
			IPCPacket.add(Mode_Wl_on);
			IPCPacket.add(Pwm_Source_Wl_on);
			IPCPacket.add(Pwm_Type_Wl_on);
			IPCPacket.add(Pwm_Val_Wl_on);
			IPCPacket.add(Pwm_Freq_Wl_on);
			//#print(Integer.toString(w.get(13)));
			
			chk_sum = b.calculateCRC(IPCPacket.subList(0, IPCPacket.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			IPCPacket.add(chksum[0]);
			IPCPacket.add(chksum[1]);
			
			//Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(IPCPacket.subList(0, IPCPacket.size())); //Stuff the buffer 
			//Buffer contents for stuffing - buffer without start byte and end byte
			//LOG$print("###################Stuffed Buffer###################");
			//LOG$for(byte by : Stuffed_Buffer)
			//LOG${
			//LOG$	print(Hex(by,2));
			//LOG$}
			IPCPacket.clear(); //Clear the Old ArrayList 
			IPCPacket = new ArrayList(Stuffed_Buffer); //populate it with the stuffed ArrayList
			IPCPacket.add(0, b.START_byte);
			IPCPacket.add(b.END_byte);
			//#print_al(w);
			SerialBytes = null;
			SerialBytes = al_to_byte(IPCPacket);
			// Send data frame to serialport and wait for a response
			try
			{
				System.out.println("Sending " + Wl_On_Ctrl +  " Command!!");
				publish("Sending " + Wl_On_Ctrl +   " Command!!\r\n");
				Send_Cmd_and_wait_for_Timeout(Wl_On_Ctrl,SerialBytes, Listener_Flag);
				return Wl_On_Ctrl;
				
			}catch(Exception ex)
			{
				System.out.println("Exception While Sending " + Current_Control + " Command!!");
				ex.printStackTrace();
			} 
			break;
		case WL_POFF:
			//pakka 
			String Wl_Off_Ctrl = Current_Control;
			System.out.println("Inside WL_POFF Case!!");
			Live WLOFF = new Live("Waiting for Live Update to complete...");
			WLOFF.Animation.setVisible(true);
			
			
			//wait until live flag is true
			synchronized(lockObject)
			{
				while(live_flag)
				{
					System.out.println("Waiting ");
					lockObject.wait(10);
					
				}
				
			}
			WLOFF.Animation.setVisible(false);
			
			byte State_Wl_off = STATE;
			byte Source_Wl_off = SOURCE;
			byte Mode_Wl_off = MODE;
			byte Pwm_Source_Wl_off = PWM_SOURCE;
			byte Pwm_Type_Wl_off = PWM_TYPE;
			byte Pwm_Val_Wl_off = TYPE_VAL;
			byte Pwm_Freq_Wl_off = PWM_FREQ;
			//System.out.println("SOURCE : " + State_Wl_off);
			//System.out.println("SOURCE : " + Source_Wl_off);
			//System.out.println("SOURCE : " + Mode_Wl_off);
			//System.out.println("SOURCE : " + Pwm_Source_Wl_off);
			//System.out.println("SOURCE : " + Pwm_Type_Wl_off);
			//System.out.println("SOURCE : " + Pwm_Val_Wl_off);
			//IPCPacket
			Expected_Cmd_Id = b.MODE_COMMAND;
			Expected_Msg_Type = b.MSG_TYPE_RESP;
			IPCPacket.add(b.MSG_TYPE_CMD);
			IPCPacket.add((byte) b.MODE_COMMAND);
			payload_length[0] = (byte) (0x6 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			IPCPacket.add(payload_length[0]);
			IPCPacket.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) 
			{
				IPCPacket.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			IPCPacket.add(State_Wl_off);
			IPCPacket.add(Source_Wl_off);
			
			IPCPacket.add(Mode_Wl_off);
			IPCPacket.add(Pwm_Source_Wl_off);
			IPCPacket.add(Pwm_Type_Wl_off);
			IPCPacket.add(Pwm_Val_Wl_off);
			IPCPacket.add(Pwm_Freq_Wl_off);
			//#print(Integer.toString(w.get(13)));
			
			chk_sum = b.calculateCRC(IPCPacket.subList(0, IPCPacket.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			IPCPacket.add(chksum[0]);
			IPCPacket.add(chksum[1]);
			
			//Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(IPCPacket.subList(0, IPCPacket.size())); //Stuff the buffer 
			//Buffer contents for stuffing - buffer without start byte and end byte
			//LOG$print("###################Stuffed Buffer###################");
			//LOG$for(byte by : Stuffed_Buffer)
			//LOG${
			//LOG$	print(Hex(by,2));
			//LOG$}
			IPCPacket.clear(); //Clear the Old ArrayList 
			IPCPacket = new ArrayList(Stuffed_Buffer); //populate it with the stuffed ArrayList
			IPCPacket.add(0, b.START_byte);
			IPCPacket.add(b.END_byte);
			//#print_al(w);
			SerialBytes = null;
			SerialBytes = al_to_byte(IPCPacket);
			// Send data frame to serialport and wait for a response
			try
			{
				System.out.println("Sending " + Wl_Off_Ctrl +  " Command!!");
				publish("Sending " + Wl_Off_Ctrl +   " Command!!\r\n");
				Send_Cmd_and_wait_for_Timeout(Wl_Off_Ctrl,SerialBytes, Listener_Flag);
				return Wl_Off_Ctrl;
				
			}catch(Exception ex)
			{
				System.out.println("Exception While Sending " + Current_Control + " Command!!");
				ex.printStackTrace();
			} 
			break;
		case ENV_IRIS_ON:
			//IPCPacket
			String Env_Iris_On_Control = Current_Control;
			System.out.println("Inside EN_IRIS On Case");
			
			Live ENVIRISON = new Live("Waiting for Live Update to complete...");
			ENVIRISON.Animation.setVisible(true);
			//wait until live flag is true
			synchronized(lockObject)
			{
				while(live_flag)
				{
					System.out.println("Waiting ");
					lockObject.wait(10);
					
				}
				
			}
			ENVIRISON.Animation.setVisible(false);
			
			byte State_Env_Iris_On = STATE;
			byte Source_Env__Iris_On = SOURCE;
			byte Mode_Env_Iris_On = MODE;
			byte Pwm_Source_Env_Iris_On = PWM_SOURCE;
			byte Pwm_Type_Env_Iris_On = PWM_TYPE;
			byte Pwm_Val_Env_Iris_On = TYPE_VAL;
			Expected_Cmd_Id = b.MODE_COMMAND;
			Expected_Msg_Type = b.MSG_TYPE_RESP;
			IPCPacket.add(b.MSG_TYPE_CMD);
			IPCPacket.add((byte) b.MODE_COMMAND);
			payload_length[0] = (byte) (0x6 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			IPCPacket.add(payload_length[0]);
			IPCPacket.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) 
			{
				IPCPacket.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			IPCPacket.add(State_Env_Iris_On);
			IPCPacket.add(Source_Env__Iris_On);
			
			IPCPacket.add(Mode_Env_Iris_On);
			IPCPacket.add(Pwm_Source_Env_Iris_On);
			IPCPacket.add(Pwm_Type_Env_Iris_On);
			IPCPacket.add(Pwm_Val_Env_Iris_On);
			//#print(Integer.toString(w.get(13)));
			
			chk_sum = b.calculateCRC(IPCPacket.subList(0, IPCPacket.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			IPCPacket.add(chksum[0]);
			IPCPacket.add(chksum[1]);
			
			//Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(IPCPacket.subList(0, IPCPacket.size())); //Stuff the buffer 
			//Buffer contents for stuffing - buffer without start byte and end byte
			//LOG$print("###################Stuffed Buffer###################");
			//LOG$for(byte by : Stuffed_Buffer)
			//LOG${
			//LOG$	print(Hex(by,2));
			//LOG$}
			IPCPacket.clear(); //Clear the Old ArrayList 
			IPCPacket = new ArrayList(Stuffed_Buffer); //populate it with the stuffed ArrayList
			IPCPacket.add(0, b.START_byte);
			IPCPacket.add(b.END_byte);
			//#print_al(w);
			SerialBytes = null;
			SerialBytes = al_to_byte(IPCPacket);
			// Send data frame to serialport and wait for a response
			try
			{
				System.out.println("Sending " + Current_Control +  " Command!!");
				publish("Sending " + Env_Iris_On_Control +   " Command!!\r\n");
				Send_Cmd_and_wait_for_Timeout(Env_Iris_On_Control,SerialBytes, Listener_Flag);
				return Env_Iris_On_Control;
				
			}catch(Exception ex)
			{
				System.out.println("Exception While Sending " + Current_Control + " Command!!");
				ex.printStackTrace();
			} 
			break;
		case ENV_IRIS_OFF:
			String Env_Iris_Off_Control = Current_Control;
			System.out.println("Inside ENV Iris OFF Case!!");
			//IPCPacket
			Live ENVIRISOFF = new Live("Waiting for Live Update to complete...");
			ENVIRISOFF.Animation.setVisible(true);
			
			synchronized(lockObject)
			{
				while(live_flag)
				{
					System.out.println("Waiting ");
					lockObject.wait(10);
					
				}
				
			}
			ENVIRISOFF.Animation.setVisible(false);
			
			byte State_Env_Iris_Off = STATE;
			byte Source_Env__Iris_Off = SOURCE;
			byte Mode_Env_Iris_Off = MODE;
			byte Pwm_Source_Env_Iris_Off = PWM_SOURCE;
			byte Pwm_Type_Env_Iris_Off = PWM_TYPE;
			byte Pwm_Val_Env_Iris_Off = TYPE_VAL;
			
			Expected_Cmd_Id = b.MODE_COMMAND;
			Expected_Msg_Type = b.MSG_TYPE_RESP;
			IPCPacket.add(b.MSG_TYPE_CMD);
			IPCPacket.add((byte) b.MODE_COMMAND);
			payload_length[0] = (byte) (0x6 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			IPCPacket.add(payload_length[0]);
			IPCPacket.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) 
			{
				IPCPacket.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			IPCPacket.add(State_Env_Iris_Off);
			IPCPacket.add(Source_Env__Iris_Off);
			
			IPCPacket.add(Mode_Env_Iris_Off);
			IPCPacket.add(Pwm_Source_Env_Iris_Off);
			IPCPacket.add(Pwm_Type_Env_Iris_Off);
			IPCPacket.add(Pwm_Val_Env_Iris_Off);
			//#print(Integer.toString(w.get(13)));
			
			chk_sum = b.calculateCRC(IPCPacket.subList(0, IPCPacket.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			IPCPacket.add(chksum[0]);
			IPCPacket.add(chksum[1]);
			
			//Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(IPCPacket.subList(0, IPCPacket.size())); //Stuff the buffer 
			//Buffer contents for stuffing - buffer without start byte and end byte
			//LOG$print("###################Stuffed Buffer###################");
			//LOG$for(byte by : Stuffed_Buffer)
			//LOG${
			//LOG$	print(Hex(by,2));
			//LOG$}
			IPCPacket.clear(); //Clear the Old ArrayList 
			IPCPacket = new ArrayList(Stuffed_Buffer); //populate it with the stuffed ArrayList
			IPCPacket.add(0, b.START_byte);
			IPCPacket.add(b.END_byte);
			//#print_al(w);
			SerialBytes = null;
			SerialBytes = al_to_byte(IPCPacket);
			// Send data frame to serialport and wait for a response
			try
			{
				System.out.println("Sending " + Env_Iris_Off_Control +  " Command!!");
				publish("Sending " + Env_Iris_Off_Control +   " Command!!" + LocalDateTime.now()+" \r\n");
				Send_Cmd_and_wait_for_Timeout(Env_Iris_Off_Control,SerialBytes, Listener_Flag);
				return Env_Iris_Off_Control;
				
			}catch(Exception ex)
			{
				System.out.println("Exception While Sending " + Current_Control + " Command!!");
				ex.printStackTrace();
			} 
			break;
			
		case INDIVIDUAL_CONTROL:
			String Individual_Control = Current_Control;
			publish(INDIVIDUAL_CONTROL+","+Individual_Control);
			break;
		case ENV_IRIS_SEL_FALSE:
			String Env_Iris_Sel_false = Current_Control;
			publish(ENV_IRIS_SEL_FALSE+","+Env_Iris_Sel_false);
			break;
		case WL_FAKE_ON:
			String wlfakeon = Current_Control;
			Expected_Cmd_Id = b.EXPECT_NO_RESPONSE;
			Expected_Msg_Type = b.EXPECT_NO_RESPONSE;
			publish(WL_FAKE_ON+"," + wlfakeon);
			
			break;
		case ENV_IRIS_FAKE_ON:
			String envirisfakeon = Current_Control;
			Expected_Cmd_Id = b.EXPECT_NO_RESPONSE;
			Expected_Msg_Type = b.EXPECT_NO_RESPONSE;
			publish(ENV_IRIS_FAKE_ON + "," + envirisfakeon);
			break;
			
			
		case CWM_NR_ON:
			String cwmnron = Current_Control;
			Expected_Cmd_Id = b.EXPECT_NO_RESPONSE;
			Expected_Msg_Type = b.EXPECT_NO_RESPONSE;
			publish(CWM_NR_ON + "," + cwmnron);
			
			break;
			
		case CWM_NR_OFF:
			String cwmnroff = Current_Control;
			Expected_Cmd_Id = b.EXPECT_NO_RESPONSE;
			Expected_Msg_Type = b.EXPECT_NO_RESPONSE;
			publish(CWM_NR_OFF +"," + cwmnroff);
			
			break;
			
			//Default Case
		default:
			System.out.println("DEFAULTED!!");
			break;
		}
		
		return Current_Control;
		
	}
	
	protected void process(List<String> chunks)
	
	{
		System.out.println("Inside Process Method!!");
		bytedefinitions byt = new bytedefinitions();
		
		
		for(String str : chunks)
		{
			System.out.println("String at the entry point of process Method : " + str);
			if(str.contains(","))
			{
				String main_string[] = str.split(",");
				
				String Cmd_Id = main_string[0];
				String Current_Control = main_string[1];
				
				System.out.println("Com ID : " + Cmd_Id);
			
			//System.out.println("Current Command Id to  be Processed : "  +str);
			if(Cmd_Id.equals(Integer.toString(byt.CONNECT_COMMAND)))
			{
				Stryk_Demo.connect_flag = true;
				Stryk_Demo.WlPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
				Stryk_Demo.EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
				Stryk_Demo.panel_3.setBackground(Color.GREEN);
				Stryk_Demo.comboBox.setEditable(false);
				Stryk_Demo.comboBox.setEnabled(false);
				Stryk_Demo.btnRefreshPortList.setEnabled(false);
				Stryk_Demo.chckbxReset.setEnabled(true);
				//Color sensor Text Fields
				Stryk_Demo.textField_28.setEnabled(true);
				Stryk_Demo.textField_29.setEnabled(true);
				Stryk_Demo.textField_30.setEnabled(true);
				Stryk_Demo.textField_31.setEnabled(true);
				
				Stryk_Demo.IrisVoltage.setEnabled(true);
				Stryk_Demo.IrisCurrent.setEnabled(true);
				Stryk_Demo.IrispdVoltage.setEnabled(true);
				
				Stryk_Demo.textField_32.setEnabled(true);
				Stryk_Demo.textField_33.setEnabled(true);
				Stryk_Demo.textField_34.setEnabled(true);
				Stryk_Demo.textField_35.setEnabled(true);
				
				//ADC VAlues- RGBL
				Stryk_Demo.textField_8.setEnabled(true);
				Stryk_Demo.textField_9.setEnabled(true);
				Stryk_Demo.textField_10.setEnabled(true);
				Stryk_Demo.textField_11.setEnabled(true);
				Stryk_Demo.textField_12.setEnabled(true);
				Stryk_Demo.textField_13.setEnabled(true);
				Stryk_Demo.textField_14.setEnabled(true);
				Stryk_Demo.textField_15.setEnabled(true);
				Stryk_Demo.textField_16.setEnabled(true);
				Stryk_Demo.textField_17.setEnabled(true);
				Stryk_Demo.textField_18.setEnabled(true);
				Stryk_Demo.textField_20.setEnabled(true);
				//IRIS-1 and IRIS-2
				Stryk_Demo.textField_21.setEnabled(true);
				Stryk_Demo.textField_22.setEnabled(true);
				Stryk_Demo.textField_23.setEnabled(true);
				Stryk_Demo.textField_24.setEnabled(true);
				//PD
				Stryk_Demo.textField_25.setEnabled(true);
				Stryk_Demo.textField_26.setEnabled(true);
				Stryk_Demo.textField_27.setEnabled(true);
				Stryk_Demo.timer_textField.setText("0");
				
				Stryk_Demo.btnConnect.setEnabled(false);
				Stryk_Demo.btnDisconnect.setEnabled(true);
				Stryk_Demo.chckbxReset.setEnabled(true);
				Stryk_Demo.chckbxLoadDefaults.setEnabled(true);
				Stryk_Demo.rdbtnuC1.setEnabled(true);
				Stryk_Demo.rdbtnuC2.setEnabled(true);
				Stryk_Demo.lblPowerSupply.setEnabled(true);
				Stryk_Demo.lblHeatSink.setEnabled(true);
				Stryk_Demo.lblFanFailure.setEnabled(true);
				Stryk_Demo.label_19.setEnabled(false);
				Stryk_Demo.lblIris.setEnabled(true);
				Stryk_Demo.label_5.setEnabled(true);
				Stryk_Demo.label_17.setEnabled(true);
				Stryk_Demo.label_18.setEnabled(true);
				Stryk_Demo.slider_12.setEnabled(true);
				Stryk_Demo.slider_13.setEnabled(true);
				Stryk_Demo.slider_14.setEnabled(true);
				Stryk_Demo.txtle_tach_count.setEnabled(true);
				Stryk_Demo.txths_tach_count.setEnabled(true);
				Stryk_Demo.txtiris_tach_count.setEnabled(true);
				Stryk_Demo.label_6.setEnabled(false);
				Stryk_Demo.label_7.setEnabled(false);
				Stryk_Demo.label_8.setEnabled(false);
				Stryk_Demo.label_9.setEnabled(false);
				Stryk_Demo.lblNewLabel.setEnabled(false);
				Stryk_Demo.label_3.setEnabled(false);
				Stryk_Demo.label_4.setEnabled(false);
				Stryk_Demo.lblGrp.setEnabled(false);
				Stryk_Demo.btnManufacturingTests.setEnabled(false);
				Color en = Color.decode("#000000");
				Stryk_Demo.panel_33.setBackground(en);
				Stryk_Demo.lblFibre1_Detect.setEnabled(true);
				Stryk_Demo.lblFibre2_Detect.setEnabled(true);
				Stryk_Demo.lblBeamSensor.setEnabled(true);
				Stryk_Demo.lblESST.setEnabled(true);
				Stryk_Demo.lblAUX_CAM_IN1.setEnabled(true);
				Stryk_Demo.lblAUX_CAM_In2.setEnabled(true);
				Stryk_Demo.lblIris_Pgood.setEnabled(true);
				Stryk_Demo.btnSave.setEnabled(true);
				Stryk_Demo.btnLastMode.setEnabled(true);
				//btnCalibration.setEnabled(true);
				Stryk_Demo.btnReadAll.setEnabled(true);
				Stryk_Demo.chckbxEnv.setEnabled(true);
				Stryk_Demo.chckbxIris.setEnabled(true);
				Stryk_Demo.lblCs.setEnabled(true);
				Stryk_Demo.lblCs_1.setEnabled(true);
				Stryk_Demo.lblCs_2.setEnabled(true);
				Stryk_Demo.lblCs_3.setEnabled(true);
				Stryk_Demo.lblCs_4.setEnabled(true);
				Stryk_Demo.lblCs_5.setEnabled(true);
				Stryk_Demo.lblCs_6.setEnabled(true);
				Stryk_Demo.lblCs_7.setEnabled(true);
				Stryk_Demo.lblLl.setEnabled(true);
				Stryk_Demo.lblLWl.setEnabled(true);
				Stryk_Demo.lblAdc_Red.setEnabled(true);
				Stryk_Demo.lblAdc_Green.setEnabled(true);
				Stryk_Demo.lblAdc_Blue.setEnabled(true);
				Stryk_Demo.lblAdc_Laser.setEnabled(true);
				Stryk_Demo.lblIris_2.setEnabled(true);
				Stryk_Demo.lblIris_3.setEnabled(true);
				Stryk_Demo.lblIris1pd.setEnabled(true);
				Stryk_Demo.lblIris2pd.setEnabled(true);
				Stryk_Demo.lblEnvpd.setEnabled(true);
				Stryk_Demo.lblV.setEnabled(true);
				Stryk_Demo.lblI.setEnabled(true);
				Stryk_Demo.slider_12.setValue(0);
				Stryk_Demo.chckbxRed.setSelected(false);
				Stryk_Demo.chckbxGreen.setSelected(false);
				Stryk_Demo.chckbxBlue.setSelected(false);
				Stryk_Demo.chckbxIris1.setSelected(false);
				Stryk_Demo.chckbxIris2.setSelected(false);
				Stryk_Demo.rdbtnIndividual.setSelected(false);
				
				//Disable all the absolute user input textboxes
				Stryk_Demo.RedtextField.setEditable(false);
				Stryk_Demo.RedtextField.setEnabled(false);
				
				Stryk_Demo.GreentextField.setEditable(false);
				Stryk_Demo.GreentextField.setEnabled(false);
				
				Stryk_Demo.BluetextField.setEditable(false);
				Stryk_Demo.BluetextField.setEnabled(false);
				
				
				
				
				
				//Disable the calibration button on connect
				//Stryk_Demo.btnCalibration.setEnabled(false);
				Stryk_Demo.label_5.setText(Integer.toString(Stryk_Demo.slider_12.getValue()) + " %");
				
				Stryk_Demo.slider_13.setValue(0);
				Stryk_Demo.label_17.setText(Integer.toString(Stryk_Demo.slider_13.getValue()) + " %");
				
				Stryk_Demo.slider_14.setValue(0);
				Stryk_Demo.label_18.setText(Integer.toString(Stryk_Demo.slider_14.getValue()) + " %");
				
				Stryk_Demo.slider_15.setValue(0);
				Stryk_Demo.slider_15.setEnabled(false);
				
				Stryk_Demo.GrptextField.setText(Integer.toString(Stryk_Demo.slider_15.getValue()));
				Stryk_Demo.label_20.setText(Integer.toString(Stryk_Demo.slider_15.getValue()) + " %");
				//Stryk_Demo.label_20.setText(Integer.toString(Stryk_Demo.slider_15.getValue()));
				Stryk_Demo.label_20.setEnabled(false);
				
				
				Stryk_Demo.slider_16.setValue(0);
				Stryk_Demo.RedtextField.setText(Integer.toString(Stryk_Demo.slider_16.getValue()));
				Stryk_Demo.label_21.setText(Integer.toString(Stryk_Demo.slider_16.getValue()) + " %");
				//Stryk_Demo.label_21.setText(Integer.toString(Stryk_Demo.slider_16.getValue()));
				
				Stryk_Demo.slider_17.setValue(0);
				Stryk_Demo.GreentextField.setText(Integer.toString(Stryk_Demo.slider_17.getValue()));
				Stryk_Demo.label_22.setText(Integer.toString(Stryk_Demo.slider_17.getValue()) + " %");
				//Stryk_Demo.label_22.setText(Integer.toString(Stryk_Demo.slider_17.getValue()));
				
				Stryk_Demo.slider_18.setValue(0);
				Stryk_Demo.BluetextField.setText(Integer.toString(Stryk_Demo.slider_18.getValue()));
				Stryk_Demo.label_23.setText(Integer.toString(Stryk_Demo.slider_18.getValue()) + " %");
				//Stryk_Demo.label_23.setText(Integer.toString(Stryk_Demo.slider_18.getValue()));
				
				Stryk_Demo.slider_19.setValue(0);
				Stryk_Demo.LasertextField.setText(Integer.toString(Stryk_Demo.slider_19.getValue()));
				Stryk_Demo.label_24.setText(Integer.toString(Stryk_Demo.slider_19.getValue()) + " %");
				//Stryk_Demo.label_24.setText(Integer.toString(Stryk_Demo.slider_19.getValue()));
				
				Stryk_Demo.slider_20.setValue(0);
				Stryk_Demo.Iris1TextField.setText(Integer.toString(Stryk_Demo.slider_20.getValue()));
				Stryk_Demo.label_25.setText(Integer.toString(Stryk_Demo.slider_20.getValue()) + " %");
				//Stryk_Demo.label_25.setText(Integer.toString(Stryk_Demo.slider_20.getValue()));
				
				Stryk_Demo.slider_21.setValue(0);
				Stryk_Demo.Iris2TextField.setText(Integer.toString(Stryk_Demo.slider_21.getValue()));
				Stryk_Demo.label_26.setText(Integer.toString(Stryk_Demo.slider_21.getValue()) + " %");
				//Stryk_Demo.label_26.setText(Integer.toString(Stryk_Demo.slider_21.getValue()));
				
				Stryk_Demo.reset_ld_rdbtn.clearSelection();
				Stryk_Demo.wlmode.clearSelection();
				Stryk_Demo.wlsource.clearSelection();
				//Stryk_Demo.tglbtnFixed_Wl.setSelected(false);
				Stryk_Demo.lblFixedValue_2.setText("VALUE");
				Stryk_Demo.formattedTextField_WlPwm.setText(Integer.toString(0));
				Stryk_Demo.formattedTextField_WlPwm.setEnabled(false);
				Stryk_Demo.lblFixedValue_2.setEnabled(false);
				
				Stryk_Demo.WlPowerButton.setSelected(false);
				Stryk_Demo.EnvIrisPowerButton.setSelected(false);
				Stryk_Demo.lblTemperature.setEnabled(true);
				
				Stryk_Demo.env_iris_chckbx.clearSelection();
				Stryk_Demo.env_iris_mode.clearSelection();
				Stryk_Demo.env_iris_source.clearSelection();
				Stryk_Demo.chckbxIris1.setSelected(false);
				Stryk_Demo.chckbxIris2.setSelected(false);
				
				Stryk_Demo.tglbtnFixed_Env.setSelected(false);
				Stryk_Demo.lblNewLabel_1.setText("VALUE");
				Stryk_Demo.formattedTextField_EnvPwm.setText(Integer.toString(0));
				Stryk_Demo.formattedTextField_EnvPwm.setEnabled(false);
				Stryk_Demo.lblNewLabel_1.setEnabled(false);
				Stryk_Demo.textField_28.setText("");
				Stryk_Demo.textField_29.setText("");
				Stryk_Demo.textField_30.setText("");
				Stryk_Demo.textField_31.setText("");
				Stryk_Demo.textField_32.setText("");
				Stryk_Demo.textField_33.setText("");
				Stryk_Demo.textField_34.setText("");
				Stryk_Demo.textField_35.setText("");
				
				Stryk_Demo.textField_8.setText("");
				Stryk_Demo.textField_9.setText("");
				Stryk_Demo.textField_10.setText("");
				Stryk_Demo.textField_11.setText("");
				Stryk_Demo.textField_12.setText("");
				Stryk_Demo.textField_13.setText("");
				Stryk_Demo.textField_14.setText("");
				Stryk_Demo.textField_15.setText("");
				Stryk_Demo.textField_16.setText("");
				Stryk_Demo.textField_17.setText("");
				Stryk_Demo.textField_18.setText("");
				Stryk_Demo.textField_20.setText("");
				Stryk_Demo.textField_21.setText("");
				Stryk_Demo.textField_22.setText("");
				Stryk_Demo.textField_23.setText("");
				Stryk_Demo.textField_24.setText("");
				Stryk_Demo.textField_25.setText("");
				Stryk_Demo.textField_26.setText("");
				Stryk_Demo.textField_27.setText("");
				Stryk_Demo.rdbtnLaserlight.setEnabled(true);
				Stryk_Demo.rdbtnWhitelight.setEnabled(true);
				Stryk_Demo.Live_Updates.setEnabled(true);
				
				Stryk_Demo.slider_12.setValue(100);
				Stryk_Demo.label_5.setText(Integer.toString(Stryk_Demo.slider_12.getValue())+ " %");
				
				Stryk_Demo.slider_13.setValue(100);
				Stryk_Demo.label_17.setText(Integer.toString(Stryk_Demo.slider_13.getValue())+ " %");
				
				Stryk_Demo.slider_14.setValue(100);
				Stryk_Demo.label_18.setText(Integer.toString(Stryk_Demo.slider_14.getValue())+ " %");
				
				Stryk_Demo.chckbxIrisPowerShutdown_1.setSelected(false);
				for(Component c :  Stryk_Demo.wl_mode)
				{
					c.setEnabled(true);
				}
				Stryk_Demo.textArea.append("Connected\r\n\n");
				Stryk_Demo.connect_flag = false;
			}
			else if(Cmd_Id.equals(Integer.toString(byt.LIVE_WIDTH_COMMAND)))
			{
				System.out.println("It is a Live Command!!");
				byte[] Live_Values = Arrays.copyOfRange(destuff_buffer, 7, 9);
				System.out.println(Arrays.toString(Live_Values));
				ArrayList<Integer> live = Get_Integer_From_ByteBuffer(Live_Values);
				System.out.println("Array List Size : " + live.size());
				System.out.println(Integer.toString(live.get(0)));
				Stryk_Demo.timer_textField.setText(String.format("%02d", (0xFFFF & live.get(0))));
				Stryk_Demo.textArea.append("Updated!!\r\n\n");
				//Stryk_Demo.live_flag = false;
				
			
			}
			else if(Cmd_Id.equals(Integer.toString(byt.ENADIS_COMMAND)))
			{
				System.out.println("HEHEHEHEHEHEHEHEHEHEHE");
				if(Stryk_Demo.chckbxRed.isEnabled()  && Stryk_Demo.chckbxGreen.isEnabled() && Stryk_Demo.chckbxBlue.isEnabled())
				{
					if(Stryk_Demo.chckbxRed.isSelected())
					{
						System.out.println("Red selected");
						Stryk_Demo.slider_16.setEnabled(true);
						Stryk_Demo.RedtextField.setEditable(true);
						Stryk_Demo.RedtextField.setEnabled(true);
						Stryk_Demo.label_21.setText(Integer.toString((Stryk_Demo.slider_16.getValue()*100)/4095) + " %");
						Stryk_Demo.label_21.setEnabled(true);
						
						
					}
					if(!Stryk_Demo.chckbxRed.isSelected())
					{
						System.out.println("Red Unselected");
						Stryk_Demo.RedtextField.setEditable(false);
						Stryk_Demo.RedtextField.setEnabled(false);
						Stryk_Demo.slider_16.setEnabled(false);
						Stryk_Demo.label_21.setEnabled(false);
					}
					if(Stryk_Demo.chckbxGreen.isSelected())
					{
						System.out.println("Green selected");
						Stryk_Demo.slider_17.setEnabled(true);
						Stryk_Demo.GreentextField.setEditable(true);
						Stryk_Demo.GreentextField.setEnabled(true);
						Stryk_Demo.label_22.setText(Integer.toString((Stryk_Demo.slider_17.getValue()*100)/4095) + " %");
						Stryk_Demo.label_22.setEnabled(true);
						
					}
					if(!Stryk_Demo.chckbxGreen.isSelected())
					{
						Stryk_Demo.GreentextField.setEditable(false);
						Stryk_Demo.GreentextField.setEnabled(false);
						System.out.println("Green Unselected");
						Stryk_Demo.slider_17.setEnabled(false);
						Stryk_Demo.label_22.setEnabled(false);
					}
					if(Stryk_Demo.chckbxBlue.isSelected())
					{
						Stryk_Demo.BluetextField.setEditable(true);
						Stryk_Demo.BluetextField.setEnabled(true);
						System.out.println("Blue selected");
						Stryk_Demo.slider_18.setEnabled(true);
						Stryk_Demo.label_23.setText(Integer.toString((Stryk_Demo.slider_18.getValue()*100)/4095) + " %");
						Stryk_Demo.label_23.setEnabled(true);
					}
					if(!Stryk_Demo.chckbxBlue.isSelected())
					{
						Stryk_Demo.BluetextField.setEditable(false);
						Stryk_Demo.BluetextField.setEnabled(false);
						System.out.println("Blue Unselected");
						Stryk_Demo.slider_18.setEnabled(false);
						Stryk_Demo.label_23.setEnabled(false);
					}
				}
				if(Stryk_Demo.chckbxIris1.isEnabled() && Stryk_Demo.chckbxIris2.isEnabled())
				{
					if(!Stryk_Demo.chckbxIris1.isSelected())
					{
						System.out.println("chckbx Iris1 Unselected");
						Stryk_Demo.slider_20.setEnabled(false);
						Stryk_Demo.label_25.setEnabled(false);
						Stryk_Demo.Iris1TextField.setEditable(false);
						Stryk_Demo.Iris1TextField.setEnabled(false);
						
					}
					if(Stryk_Demo.chckbxIris1.isSelected())
					{
						Stryk_Demo.slider_20.setEnabled(true);
						Stryk_Demo.label_25.setEnabled(true);
						Stryk_Demo.label_25.setText(Integer.toString((Stryk_Demo.slider_20.getValue()*100)/4095) + " %");
						Stryk_Demo.Iris1TextField.setEditable(true);
						Stryk_Demo.Iris1TextField.setEnabled(true);
						
					}
					if(!Stryk_Demo.chckbxIris2.isSelected())
					{
						System.out.println("chckbx Iris2 Unselected");
						Stryk_Demo.slider_21.setEnabled(false);
						Stryk_Demo.label_26.setEnabled(false);
						Stryk_Demo.Iris2TextField.setEditable(false);
						Stryk_Demo.Iris2TextField.setEnabled(false);
					}
					if(Stryk_Demo.chckbxIris2.isSelected())
					{
						Stryk_Demo.slider_21.setEnabled(true);
						Stryk_Demo.label_26.setEnabled(true);
						Stryk_Demo.label_26.setText(Integer.toString((Stryk_Demo.slider_21.getValue()*100)/4095) + " %");
						Stryk_Demo.Iris2TextField.setEditable(true);
						Stryk_Demo.Iris2TextField.setEnabled(true);
					}
				}
				
			}
			else if(Cmd_Id.equals(Integer.toString(byt.DISCONNECT_COMMAND)))
			{
				Stryk_Demo.WlPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
				Stryk_Demo.EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
				//WlPowerButton.setIcon(off_switch);
				
				//Disable the newly added labels
				Stryk_Demo.IrisVoltage.setEnabled(false);
				Stryk_Demo.IrisCurrent.setEnabled(false);
				Stryk_Demo.IrispdVoltage.setEnabled(false);
				
				//Disable all the absolute user input textboxes related to wl, env and iris
				Stryk_Demo.GrptextField.setEditable(false);
				Stryk_Demo.GrptextField.setEnabled(false);
				
				Stryk_Demo.RedtextField.setEditable(false);
				Stryk_Demo.RedtextField.setEnabled(false);
				
				Stryk_Demo.GreentextField.setEditable(false);
				Stryk_Demo.GreentextField.setEnabled(false);
				
				Stryk_Demo.BluetextField.setEditable(false);
				Stryk_Demo.BluetextField.setEnabled(false);
				
				Stryk_Demo.LasertextField.setEditable(false);
				Stryk_Demo.LasertextField.setEnabled(false);
				
				Stryk_Demo.Iris1TextField.setEditable(false);
				Stryk_Demo.Iris1TextField.setEnabled(false);
				
				Stryk_Demo.Iris2TextField.setEditable(false);
				Stryk_Demo.Iris2TextField.setEnabled(false);
				
				//EnvIrisPowerButton.setIcon(off_switch);
				Stryk_Demo.frmStrykerVer.repaint();
				Stryk_Demo.frmStrykerVer.revalidate();
				Stryk_Demo.reset_ld_rdbtn.clearSelection();
				Stryk_Demo.panel_3.setBackground(Color.RED);
				Stryk_Demo.comboBox.setEditable(true);
				Stryk_Demo.comboBox.setEnabled(true);
				Stryk_Demo.btnRefreshPortList.setEnabled(true);
				
				
				
				
				//Enable the Calibration Button on Disconnect
				//Stryk_Demo.btnCalibration.setEnabled(true);
				
				//Color sensor Text Fields
				Stryk_Demo.textField_28.setEnabled(false);
				Stryk_Demo.textField_29.setEnabled(false);
				Stryk_Demo.textField_30.setEnabled(false);
				Stryk_Demo.textField_31.setEnabled(false);
				
				Stryk_Demo.textField_32.setEnabled(false);
				Stryk_Demo.textField_33.setEnabled(false);
				Stryk_Demo.textField_34.setEnabled(false);
				Stryk_Demo.textField_35.setEnabled(false);
				
				//ADC VAlues- RGBL
				Stryk_Demo.textField_8.setEnabled(false);
				Stryk_Demo.textField_9.setEnabled(false);
				Stryk_Demo.textField_10.setEnabled(false);
				Stryk_Demo.textField_11.setEnabled(false);
				Stryk_Demo.textField_12.setEnabled(false);
				Stryk_Demo.textField_13.setEnabled(false);
				Stryk_Demo.textField_14.setEnabled(false);
				Stryk_Demo.textField_15.setEnabled(false);
				Stryk_Demo.textField_16.setEnabled(false);
				Stryk_Demo.textField_17.setEnabled(false);
				Stryk_Demo.textField_18.setEnabled(false);
				Stryk_Demo.textField_20.setEnabled(false);
				//IRIS-1 and IRIS-2
				Stryk_Demo.textField_21.setEnabled(false);
				Stryk_Demo.textField_22.setEnabled(false);
				Stryk_Demo.textField_23.setEnabled(false);
				Stryk_Demo.textField_24.setEnabled(false);
				//PD
				Stryk_Demo.textField_25.setEnabled(false);
				Stryk_Demo.textField_26.setEnabled(false);
				Stryk_Demo.textField_27.setEnabled(false);
				
				
				
				
				
				
				Stryk_Demo.chckbxReset.setEnabled(false);
				Stryk_Demo.chckbxLoadDefaults.setEnabled(false);
				Stryk_Demo.rdbtnuC1.setEnabled(false);
				Stryk_Demo.lblExt.setEnabled(false);
				Stryk_Demo.rdbtnExt.setEnabled(false);
				Stryk_Demo.lblUc1.setEnabled(false);
				Stryk_Demo.rdbtnuC_1.setEnabled(false);
				Stryk_Demo.lblUc2.setEnabled(false);
				Stryk_Demo.rdbtnuC_2.setEnabled(false);
				Stryk_Demo.WlPowerButton.setEnabled(false);
				Stryk_Demo.rdbtnuC2.setEnabled(false);
				Stryk_Demo.rdbtn120hz.setEnabled(false);
				Stryk_Demo.rdbtn70khz.setEnabled(false);
				Stryk_Demo.btnDisconnect.setEnabled(false);
				Stryk_Demo.btnConnect.setEnabled(true);
				Stryk_Demo.lblPowerSupply.setEnabled(false);
				Stryk_Demo.lblHeatSink.setEnabled(false);
				Stryk_Demo.lblIris.setEnabled(false);
				Stryk_Demo.label_5.setEnabled(false);
				Stryk_Demo.label_17.setEnabled(false);
				Stryk_Demo.btnManufacturingTests.setEnabled(true);
				Stryk_Demo.label_18.setEnabled(false);
				Stryk_Demo.slider_12.setEnabled(false);
				Stryk_Demo.slider_13.setEnabled(false);
				Stryk_Demo.slider_14.setEnabled(false);
				
				Stryk_Demo.txtle_tach_count.setEnabled(false);
				Stryk_Demo.txths_tach_count.setEnabled(false);
				Stryk_Demo.txtiris_tach_count.setEnabled(false);
				
				Stryk_Demo.	lblFanFailure.setEnabled(false);
				Stryk_Demo.label_19.setEnabled(false);
				Stryk_Demo.label_6.setEnabled(false);
				Stryk_Demo.label_7.setEnabled(false);
				Stryk_Demo.label_8.setEnabled(false);
				Stryk_Demo.label_9.setEnabled(false);
				
				Stryk_Demo.lblNewLabel.setEnabled(false);
				Stryk_Demo.label_3.setEnabled(false);
				Stryk_Demo.label_4.setEnabled(false);
				Stryk_Demo.lblESST.setEnabled(false);
				Stryk_Demo.lblAUX_CAM_IN1.setEnabled(false);
				Stryk_Demo.lblAUX_CAM_In2.setEnabled(false);
				
				Stryk_Demo.lblFibre1_Detect.setEnabled(false);
				Stryk_Demo.lblFibre2_Detect.setEnabled(false);
				Stryk_Demo.lblBeamSensor.setEnabled(false);
				Stryk_Demo.lblIris_Pgood.setEnabled(false);
				Stryk_Demo.btnSave.setEnabled(false);
				Stryk_Demo.btnLastMode.setEnabled(false);
				//btnCalibration.setEnabled(false);
				Stryk_Demo.btnReadAll.setEnabled(false);
				Stryk_Demo.lblPwm_4.setEnabled(false);
				Stryk_Demo.rdbtnPwm.setEnabled(false);
				Stryk_Demo.rdbtnCwm.setEnabled(false);
				Stryk_Demo.lblCwm.setEnabled(false);
				Stryk_Demo.lblStb.setEnabled(false);
				Stryk_Demo.rdbtnStb.setEnabled(false);
				Stryk_Demo.lblLc.setEnabled(false);
				Stryk_Demo.rdbtnLc.setSelected(false);
				Stryk_Demo.rdbtnLc.setEnabled(false);
				Stryk_Demo.chckbxEnv.setEnabled(false);
				Stryk_Demo.chckbxIris.setEnabled(false);
				Stryk_Demo.lblCs.setEnabled(false);
				Stryk_Demo.lblCs_1.setEnabled(false);
				Stryk_Demo.lblCs_2.setEnabled(false);
				Stryk_Demo.lblCs_3.setEnabled(false);
				Stryk_Demo.lblCs_4.setEnabled(false);
				Stryk_Demo.lblCs_5.setEnabled(false);
				Stryk_Demo.lblCs_6.setEnabled(false);
				Stryk_Demo.lblCs_7.setEnabled(false);
				Stryk_Demo.lblLl.setEnabled(false);
				Stryk_Demo.slider_15.setEnabled(false);
				Stryk_Demo.slider_16.setEnabled(false);
				Stryk_Demo.slider_17.setEnabled(false);
				Stryk_Demo.slider_18.setEnabled(false);
				Stryk_Demo.label_20.setEnabled(false);
				Stryk_Demo.label_21.setEnabled(false);
				Stryk_Demo.label_22.setEnabled(false);
				Stryk_Demo.label_23.setEnabled(false);
				Stryk_Demo.lblGrp.setEnabled(false);
				Stryk_Demo.rdbtnIndividual.setEnabled(false);
				Stryk_Demo.chckbxRed.setEnabled(false);
				Stryk_Demo.chckbxGreen.setEnabled(false);
				Stryk_Demo.chckbxBlue.setEnabled(false);
				Stryk_Demo.chckbxEnv.setEnabled(false);
				Stryk_Demo.chckbxIris.setEnabled(false);
				Stryk_Demo.EnvIrisPowerButton.setEnabled(false);
				Stryk_Demo.lblEnv.setEnabled(false);
				Stryk_Demo.slider_19.setEnabled(false);
				Stryk_Demo.label_24.setEnabled(false);
				Stryk_Demo.slider_20.setEnabled(false);
				Stryk_Demo.label_25.setEnabled(false);
				Stryk_Demo.slider_21.setEnabled(false);
				Stryk_Demo.label_26.setEnabled(false);
				Stryk_Demo.chckbxIris1.setEnabled(false);
				Stryk_Demo.chckbxIris2.setEnabled(false);
				Stryk_Demo.label_25.setEnabled(false);
				Stryk_Demo.label_26.setEnabled(false);
				Stryk_Demo.chckbxIrisPowerShutdown_1.setEnabled(false);
				Stryk_Demo.panel_33.setEnabled(false);
				Stryk_Demo.rdbtnLaserlight.setEnabled(false);
				Stryk_Demo.rdbtnWhitelight.setEnabled(false);
				Stryk_Demo.Live_Updates.setEnabled(false);
				for(Component c: Stryk_Demo.env_iris)
				{
					c.setEnabled(false);
				}
				for(Component c: Stryk_Demo.env_source)
				{
					c.setEnabled(false);
				}
				for(Component c : Stryk_Demo.env_pwm_type)
				{
					c.setEnabled(false);
				}
				
				Stryk_Demo.	lblLWl.setEnabled(false);
				Stryk_Demo.lblAdc_Red.setEnabled(false);
				Stryk_Demo.lblAdc_Green.setEnabled(false);
				Stryk_Demo.lblAdc_Blue.setEnabled(false);
				Stryk_Demo.lblAdc_Laser.setEnabled(false);
				Stryk_Demo.lblIris_2.setEnabled(false);
				Stryk_Demo.lblIris_3.setEnabled(false);
				Stryk_Demo.lblIris1pd.setEnabled(false);
				Stryk_Demo.lblIris2pd.setEnabled(false);
				Stryk_Demo.lblEnvpd.setEnabled(false);
				Stryk_Demo.lblV.setEnabled(false);
				Stryk_Demo.lblI.setEnabled(false);
				Stryk_Demo.lblTemperature.setEnabled(false);
				for (Component c: Stryk_Demo.wl_pwm_type)
				{
					c.setEnabled(false);
				}
				
				if(Stryk_Demo.live_timer_on != null)
				{
					if(Stryk_Demo.live_timer_on.isRunning())
					{
						Stryk_Demo.Live_Updates.setSelected(false);
						Stryk_Demo.rdbtnWhitelight.setEnabled(false);
						Stryk_Demo.rdbtnLaserlight.setEnabled(false);
						if(Stryk_Demo.live_timer_on != null)
						{
							Stryk_Demo.live_timer_on.stop();
						}
						
					}
				}
				Stryk_Demo.textArea.append("Disconnected\r\n\n");	
			}
			else if(Cmd_Id.equals(Integer.toString(byt.SAVE_COMMAND)))
			{
				Stryk_Demo.textArea.append("Values Saved Successfully!!\r\n\n");	
			}
			else if(Cmd_Id.equals(Integer.toString(byt.RESTORE_CMD)))
			{
				byte[] Restore_Values;
				//Contents of Restore Values
				//1. WLENABLE  2. ENVENABLE   3. IRISENABLE  4. REDENABLE
				//5. GREENENABLE   6. BLUEENABLE  7. IRIS1ENABLE  8. IRIS2ENABLE
				//9. WL_MODE   10. ENV_MODE  11. IRIS_MODE  12. WL_SRC 
				//13. ENV_SRC  14. IRIS_SRC  15. PWM_WL_TYPE  16. PWM_ENV_TYPE
				//17. PWM_WL_DUTY  18. PWM_ENV_DUTY  19. RED_INTENSITY  20. GREEN_INTENSITY
				//21. BLUE_INTENSITY  22. WHITE_INTENSITY  23. ENVINTENSITY  24. IRIS1_INTENSITY
				//25. IRIS2_INTENSITY  26. POWERSUPPLYFANSPEED  27. HEATSINKFANSPEED  
				Restore_Values = Arrays.copyOfRange(destuff_buffer, 7, destuff_buffer.length-2);
				System.out.println("Restore Buffer : "+Arrays.toString(Restore_Values));
				if(Restore_Values.length != 32)
				{
					if(Restore_Values.length == 2)
					{
						if(Restore_Values[0] == 2)
						{
							
						}
						else if(Restore_Values[0] == 9 )
						{
							
						}
						else
						{
							JOptionPane.showMessageDialog(Stryk_Demo.frmStrykerVer, "Unknown Response for ReadAll Command", "READ ALL", JOptionPane.ERROR_MESSAGE);
						}
					}
					System.out.println("Receive Buffer Doesn't meet the specific Requirements");
					JOptionPane.showMessageDialog(Stryk_Demo.frmStrykerVer, "Failed to restore Values!!", "READ ALL", JOptionPane.ERROR_MESSAGE);
					
				}
				
				else
				{
					System.out.println("Last Mode Packet Valid");
					Stryk_Demo.connect_flag = true;
					DataInputStream DIS = new DataInputStream(new ByteArrayInputStream(Restore_Values));
					publish("Restoring Previously Saved Configuration...\r\n");
					//Update_LastMode_Values(DIS);
					try
					{
						Stryk_Demo.wl_critical_flag = true;
						Stryk_Demo.env_iris_critical_flag = true;
						Stryk_Demo.connect_flag = true;
						
						System.out.println("Inside Update Restore Mode Option");
						bytedefinitions bytes = new bytedefinitions();
						LastMode_Structure LastModeValues = new LastMode_Structure(DIS);
						
						//WhiteLight Mode Switch Enable
						System.out.println("Updating Fields");
						
						//Red Slider Enable
						if(LastModeValues.RedEnable == true)
						{
							//Make the RedTextField Enabled and Editable
							//Stryk_Demo.RedtextField.setEditable(true);
							//Stryk_Demo.RedtextField.setEnabled(true);
							
							System.out.println("RED Enable");
							Stryk_Demo.chckbxRed.setSelected(true);
						}
						//Red Slider Disable
						else if(LastModeValues.RedEnable == false)
						{
							//Make the RedTextField Disabled and not Editable
							//Stryk_Demo.RedtextField.setEditable(false);
							//Stryk_Demo.RedtextField.setEnabled(false);
							
							System.out.println("Red Disable");
							Stryk_Demo.chckbxRed.setSelected(false);
						}
						else {}
						//Green Slider Enable
						if(LastModeValues.GreenEnable == true)
						{
							//Make the GreenTextField Enabled and Editable
							//Stryk_Demo.GreentextField.setEditable(true);
							//Stryk_Demo.GreentextField.setEnabled(true);
							
							System.out.println("Green Enable");
							Stryk_Demo.chckbxGreen.setSelected(true);
						}
						//Green Slider disable
						else if(LastModeValues.GreenEnable == false)
						{
							//Make the GreenTextField Disabled and not Editable
							//Stryk_Demo.GreentextField.setEditable(false);
							//Stryk_Demo.GreentextField.setEnabled(false);
							
							System.out.println("Green Disable");
							Stryk_Demo.chckbxGreen.setSelected(false);
						}
						else {}
						//Blue Slider Enable
						if(LastModeValues.BlueEnable == true)
						{
							//Make the BlueTextField Enabled and Editable
							//Stryk_Demo.BluetextField.setEditable(true);
							//Stryk_Demo.BluetextField.setEnabled(true);
							
							System.out.println("Blue Enable");
							Stryk_Demo.chckbxBlue.setSelected(true);
						}
						//Blue Slider disable
						else if(LastModeValues.BlueEnable == false)
						{
							//Make the BlueTextField Disabled and not Editable
							//Stryk_Demo.BluetextField.setEditable(false);
							//Stryk_Demo.BluetextField.setEnabled(false);
							
							System.out.println("Blue Disable");
							Stryk_Demo.chckbxBlue.setSelected(false);
						}
						else {}
					
						//WhiteLight Mode - PWM
						if(LastModeValues.WLMode == bytes.TYPE_PWM || LastModeValues.WLMode == 0)
						{
							System.out.println("WL PWM Mode");
							Stryk_Demo.rdbtnPwm.setSelected(true);
						}
						//WhiteLight Mode - CWM
						else if(LastModeValues.WLMode == bytes.TYPE_CWM)
						{
							System.out.println("WL CWM Mode");
							Stryk_Demo.rdbtnCwm.setSelected(true);
						}
						//WhiteLight Mode - STB
						else if(LastModeValues.WLMode == bytes.TYPE_STROBE)
						{
							System.out.println("WL Strobe Mode");
							Stryk_Demo.rdbtnStb.setSelected(true);
						}
						//WhiteLight Mode - LC
						else if(LastModeValues.WLMode == bytes.TYPE_LC)
						{
							System.out.println("WL LC Mode");
							Stryk_Demo.rdbtnLc.setSelected(true);
						}
						else {
							System.out.println("Not a valid WL  Mode. Choosing PWM as Default");
							Stryk_Demo.rdbtnPwm.setSelected(true);
							//Mode number at times comes as 8,9,10,11,12 from the device on P4 code. This is a fix for that 
							//Date : 29122017
							
						}
						
						
						//White Light Source Selection
						if(LastModeValues.WLSrc == bytes.SOURCE_EXTERNAL || LastModeValues.WLSrc == 0)
						{
							System.out.println("WL Ext Source");
							Stryk_Demo.rdbtnExt.setSelected(true);
						}
						else if(LastModeValues.WLSrc == bytes.SOURCE_uC1)
						{
							System.out.println("WL uC1 Source");
							Stryk_Demo.rdbtnuC_1.setSelected(true);
						}
						else if(LastModeValues.WLSrc == bytes.SOURCE_uC2)
						{
							System.out.println("WL uC2 Source");
							Stryk_Demo.rdbtnuC_2.setSelected(true);
						}
						else {
							System.out.println("Not a valid WL Source. Choosing Ext as Default!");
							Stryk_Demo.rdbtnExt.setSelected(true);
						}
						
						//PWM Type - WL
						if(LastModeValues.PWM_WL_TYPE == bytes.PWM_TYPE_FIXED || LastModeValues.PWM_WL_TYPE == 0)
						{
							//Set the duty here
							Stryk_Demo.tglbtnFixed_Wl.setSelected(false);
							Stryk_Demo.lblFixedValue_2.setText("VALUE");
							Stryk_Demo.formattedTextField_WlPwm.setValue(LastModeValues.PWM_WL_DUTY);
						}
						else if(LastModeValues.PWM_WL_TYPE == bytes.PWM_TYPE_FADE)
						{
							//Set the duration here
							Stryk_Demo.lblFixedValue_2.setText("DURATION");
							Stryk_Demo.tglbtnFixed_Wl.setSelected(true);
							Stryk_Demo.formattedTextField_WlPwm.setValue(LastModeValues.PWM_WL_DUTY);
							
						}
						else
						{
							System.out.println("Not a Valid WL PWM Type. Choosing Fixed as Default");
							Stryk_Demo.tglbtnFixed_Wl.setSelected(false);
							Stryk_Demo.lblFixedValue_2.setText("VALUE");
							Stryk_Demo.formattedTextField_WlPwm.setValue(LastModeValues.PWM_WL_DUTY);
						}
						
						if(LastModeValues.WLEnable == true)
						{
							//Enable and make the GrpTextField editable
							Stryk_Demo.GrptextField.setEditable(true);
							Stryk_Demo.GrptextField.setEnabled(true);
							
							
							System.out.println("Switch ON WhiteLight");
							
							Stryk_Demo.WlPowerButton.setSelected(true);
							Stryk_Demo.WlPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/on.jpg")));
							System.out.println("Reached WL FAKE ON");
							if(Stryk_Demo.EnvIrisPowerButton.isSelected())
							{
								Color en = Color.decode("#ccffff");
								Stryk_Demo.panel_33.setBackground(en);
							}
							
							Stryk_Demo.textArea.append("White Light Switched ON!!\r\n\n");
							Component[] p26 = Stryk_Demo.panel_26.getComponents();
							Component[] p27 = Stryk_Demo.panel_27.getComponents();
							Component[] p28 = Stryk_Demo.panel_28.getComponents();
							
							for(Component  c : p26)
							{
								c.setEnabled(false);
							}
							for(Component  c : p27)
							{
								c.setEnabled(false);
							}
							for(Component  c : p28)
							{
								c.setEnabled(false);
							}
							Stryk_Demo.slider_15.setEnabled(true);
							Stryk_Demo.lblGrp.setEnabled(true);
							Stryk_Demo.label_20.setEnabled(true);
							Stryk_Demo.rdbtnIndividual.setEnabled(true);
							Stryk_Demo.wl_onoff_flag = true;
							Stryk_Demo.label_21.setEnabled(false);
							Stryk_Demo.label_22.setEnabled(false);
							Stryk_Demo.label_23.setEnabled(false);
							Stryk_Demo.chckbxRed.setSelected(true);
							Stryk_Demo.chckbxGreen.setSelected(true);
							Stryk_Demo.chckbxBlue.setSelected(true);
							Stryk_Demo.lblLc.setEnabled(false);
							Stryk_Demo.rdbtnLc.setEnabled(false);
							Stryk_Demo.wl_onoff_flag = false;
										
						}
						//WhiteLight Mode Switch Disable
						else if(LastModeValues.WLEnable == false)
						{
							//Disable and make the GrpTextField non editable
							Stryk_Demo.GrptextField.setEditable(false);
							Stryk_Demo.GrptextField.setEnabled(false);
							
							System.out.println("Switch OFF WhiteLight");
							Stryk_Demo.WlPowerButton.setSelected(false);
							Stryk_Demo.WlPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
							Component[] c1 = Stryk_Demo.panel_26.getComponents();
							Component[] c2 = Stryk_Demo.panel_27.getComponents();
							Component[] c3 = Stryk_Demo.panel_28.getComponents();
							for (Component comp : c1)
							{
								comp.setEnabled(true);
							}
							for (Component comp : c2)
							{
								comp.setEnabled(true);
							}
							for (Component comp : c3)
							{
								//comp.setEnabled(true);
							}
							
							Stryk_Demo.lblGrp.setEnabled(false);
							Stryk_Demo.slider_15.setEnabled(false);
							Stryk_Demo.label_20.setEnabled(false);
							Stryk_Demo.rdbtnIndividual.setEnabled(false);
							
							Component[] c4 = Stryk_Demo.panel_29.getComponents();
							for(Component comp : c4)
							{
								comp.setEnabled(false);
								
							}
							
						}
						else {
							System.out.println("Not a Valid WL State. Neither ON nor OFF.");
						}
					
						//IRIS Process
						if(LastModeValues.IRISMode == bytes.TYPE_PWM || LastModeValues.IRISMode == 0)
						{
							System.out.println("IRIS PWM Mode");
							Stryk_Demo.rdbtnEnv_Pwm.setSelected(true);
						}
						//IRIS Mode CWM
						else if(LastModeValues.IRISMode == bytes.TYPE_CWM)
						{
							System.out.println("IRIS CWM Mode");
							Stryk_Demo.rdbtnEnv_Cwm.setSelected(true);
						}
						else {
							System.out.println("Not a vliad IRIS Mode. Choosing PWM by Default");
							Stryk_Demo.rdbtnEnv_Pwm.setSelected(true);
						}
						//Stryk_Demo.chckbxIris.setSelected(true);
						//IRIS Source Selection
						if(LastModeValues.IRISSrc == bytes.SOURCE_uC1)
						{
							System.out.println("IRIS uC1 Source");
							Stryk_Demo.rdbtnuC_3.setSelected(true);
						}
						else if(LastModeValues.IRISSrc == bytes.SOURCE_uC2)
						{
							System.out.println("IRIS uC2 Source");
							Stryk_Demo.rdbtnuC_4.setSelected(true);
						}
						else if(LastModeValues.IRISSrc == bytes.SOURCE_EXT_WHITELIGHT || LastModeValues.IRISSrc == 0)
						{
							System.out.println("IRIS Ext WL Source");
							Stryk_Demo.rdbtn_IrisExtWht.setSelected(true);
						}
						else if(LastModeValues.IRISSrc == bytes.SOURCE_EXT_ENV)
						{
							System.out.println("IRIS Ext Env Source");
							Stryk_Demo.rdbtn_IrisExtEnv.setSelected(true);
						}
						else {
							System.out.println("Not a valid IRIS Source. Choosing External White Light Source by Default");
							Stryk_Demo.rdbtn_IrisExtWht.setSelected(true);
						}
						//IRIS1 Slider Enable
						if(LastModeValues.IRIS1Enable == true)
						{
							//make the Iris1TextField Enabled and Editable
							Stryk_Demo.Iris1TextField.setEditable(true);
							Stryk_Demo.Iris1TextField.setEnabled(true);
							
							System.out.println("IRIS 1 Enable");
							Stryk_Demo.chckbxIris1.setSelected(true);
						}
						//IRIS1 Slider Disable
						else if(LastModeValues.IRIS1Enable == false )
						{
							//make the Iris1TextField Disabled and non Editable
							Stryk_Demo.Iris1TextField.setEditable(false);
							Stryk_Demo.Iris1TextField.setEnabled(false);
							
							System.out.println("IRIS 1 Disable");
							Stryk_Demo.chckbxIris1.setSelected(false);
						}
						else {
							System.out.println("Invalid Control - IRIS 1 ENABLE / DISABLE.");
						}
						//IRIS2 Slider Enable
						if(LastModeValues.IRIS2Enable == true)
						{
							//make the Iris2TextField Enabled and Editable
							Stryk_Demo.Iris2TextField.setEditable(true);
							Stryk_Demo.Iris2TextField.setEnabled(true);
							
							System.out.println("IRIS2 Enable");
							Stryk_Demo.chckbxIris2.setSelected(true);
						}
						//IRIS2 Slider Disable
						else if(LastModeValues.IRIS2Enable == false)
						{
							//make the Iris1TextField Enabled and Editable
							Stryk_Demo.Iris1TextField.setEditable(false);
							Stryk_Demo.Iris1TextField.setEnabled(false);
							
							System.out.println("IRIS2 Disable");
							Stryk_Demo.chckbxIris2.setSelected(false);
						}
						else
						{
							System.out.println("Invalid Control - IRIS 2 ENABLE / DISABLE.");
						}
						
						//IRIS Mode Switch Enable
						if(LastModeValues.IRISEnable == true)
						{
							//IRIS Mode PWM
							System.out.println("IRIS Power ON");
							//Stryk_Demo.EnvIrisPowerButton.setSelected(true);
							//Stryk_Demo.EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/on.jpg")));
							//Stryk_Demo.EnvIrisPowerButton.setSelected(true);
							//Stryk_Demo.EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/on.jpg")));
							Stryk_Demo.chckbxIris.setSelected(true);
							//System.out.println("IRIS Power ON");
							Stryk_Demo.EnvIrisPowerButton.setSelected(true);
							Stryk_Demo.EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/on.jpg")));
							if(LastModeValues.IRISEnable)
							{
								Stryk_Demo.chckbxIris1.setEnabled(true);
								Stryk_Demo.slider_20.setEnabled(true);
								Stryk_Demo.label_25.setEnabled(true);
								Stryk_Demo.chckbxIris2.setEnabled(true);
								Stryk_Demo.slider_21.setEnabled(true);
								Stryk_Demo.label_26.setEnabled(true);
								
								
							}
							
							
						}
						//IRIS Mode Switch Disable
						else if(LastModeValues.IRISEnable == false)
						{
							System.out.println ("IRIS POFF");
							Stryk_Demo.chckbxIris.setSelected(false);
							Stryk_Demo.EnvIrisPowerButton.setEnabled(false);
							Stryk_Demo.EnvIrisPowerButton.setSelected(false);
							Stryk_Demo.EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
							Stryk_Demo.lblCwmnr.setEnabled(false);
							Stryk_Demo.rdbtnEnv_CwmNr.setEnabled(false);
							Stryk_Demo.lblLcnr.setEnabled(false);
							Stryk_Demo.rdbtnEnv_LcNr.setEnabled(false);
							
						}
						else {}
						//PWM-Type ENV/IRIS
						if(LastModeValues.PWM_ENV_TYPE == bytes.PWM_TYPE_FIXED || LastModeValues.PWM_ENV_TYPE == 0)
						{
							//Set the duty here
							Stryk_Demo.tglbtnFixed_Env.setSelected(false);
							Stryk_Demo.lblNewLabel_1.setText("VALUE");
							Stryk_Demo.formattedTextField_EnvPwm.setValue(LastModeValues.PWM_ENV_DUTY);
						}
						else if(LastModeValues.PWM_ENV_TYPE == bytes.PWM_TYPE_FADE)
						{
							//Set the duration here
							Stryk_Demo.tglbtnFixed_Env.setSelected(true);
							Stryk_Demo.lblNewLabel_1.setText("DURATION");
							Stryk_Demo.formattedTextField_EnvPwm.setValue(LastModeValues.PWM_ENV_DUTY);
						}
						else {
							System.out.println("Not a valid ENV PWM Type. Choosing Fixed as Default");
							Stryk_Demo.tglbtnFixed_Env.setSelected(false);
							Stryk_Demo.lblNewLabel_1.setText("VALUE");
							Stryk_Demo.formattedTextField_EnvPwm.setValue(LastModeValues.PWM_ENV_DUTY);
							
						}
						//ENV Mode Switch Enable
						if(LastModeValues.ENVEnable == true)
						{
							
							
							System.out.println("Laser Switched ON!!");
							Stryk_Demo.env_iris_chckbx.clearSelection();
							Stryk_Demo.chckbxEnv.setSelected(true);
							if(LastModeValues.ENVMode == bytes.TYPE_PWM || LastModeValues.ENVMode == 0)
							{
								System.out.println("ENV PWM Mode");
								Stryk_Demo.rdbtnEnv_Pwm.setSelected(true);
							}
							//ENV Mode CWM
							else if(LastModeValues.ENVMode == bytes.TYPE_CWM)
							{
								System.out.println("ENV CWM Mode");
								Stryk_Demo.rdbtnEnv_Cwm.setSelected(true);
							}
							//ENV Mode CWM_NORED
							else if(LastModeValues.ENVMode == bytes.TYPE_CWM_NORED)
							{
								System.out.println("ENV CWMNORED Mode");
								Stryk_Demo.rdbtnEnv_CwmNr.setSelected(true);
							}
							//ENV Mode LC_NORED
							else if(LastModeValues.ENVMode == bytes.TYPE_LC_NORED)
							{
								System.out.println("ENV LC_NORED Mode");
								Stryk_Demo.rdbtnEnv_LcNr.setSelected(true);
							}
							else {}
							//Env Source Selection
							
							
							if(LastModeValues.ENVSrc == bytes.SOURCE_EXTERNAL || LastModeValues.ENVSrc == 0)
							{
								System.out.println("ENV Ext Source");
								Stryk_Demo.rdbtnEnv_Ext.setSelected(true);
								
							}
							else if(LastModeValues.ENVSrc == bytes.SOURCE_uC1)
							{
								System.out.println("ENV uC1 Source");
								Stryk_Demo.rdbtnuC_3.setSelected(true);
							}
							else if(LastModeValues.ENVSrc == bytes.SOURCE_uC2)
							{
								System.out.println("ENV uC2 Source");
								Stryk_Demo.rdbtnuC_4.setSelected(true);
							}
							else {}
							
							System.out.println("Switch ON ENV");
							
							Stryk_Demo.EnvIrisPowerButton.setSelected(true);
							Stryk_Demo.EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/on.jpg")));
							Stryk_Demo.EnvIrisPowerButton.setEnabled(true);
							
							
							//System.out.println("Serial Port Flag False");
							if(Stryk_Demo.WlPowerButton.isSelected())
							{
								Color en = Color.decode("#ccffff");
								Stryk_Demo.panel_33.setBackground(en);
							}
							if(Stryk_Demo.SOURCE == Stryk_Demo.ENV_SOURCE)
							{
								//Enable and make the LaserTextField Editable
								Stryk_Demo.LasertextField.setEditable(true);
								Stryk_Demo.LasertextField.setEnabled(true);
								
								Stryk_Demo.textArea.append("ENV Light Switched ON!!\r\n");
								Stryk_Demo.chckbxEnv.setEnabled(false);
								Stryk_Demo.chckbxIris.setEnabled(false);
								for(Component c : Stryk_Demo.env_iris)
								{
									c.setEnabled(false);	
								}
								for(Component c : Stryk_Demo.env_source)
								{
									c.setEnabled(false);
									
								}
								for(Component c: Stryk_Demo.env_pwm_type)
								{
									c.setEnabled(false);
								}
								Stryk_Demo.lblEnv.setEnabled(true);
								Stryk_Demo.slider_19.setEnabled(true);
								Stryk_Demo.label_24.setEnabled(true);
								Stryk_Demo.chckbxIris1.setEnabled(false);
								Stryk_Demo.chckbxIris2.setEnabled(false);
								Stryk_Demo.chckbxIrisPowerShutdown_1.setEnabled(false);
								Stryk_Demo.slider_20.setEnabled(false);
								Stryk_Demo.label_25.setEnabled(false);
								Stryk_Demo.slider_21.setEnabled(false);
								Stryk_Demo.label_26.setEnabled(false);
								//ENV Mode PWm
								
								
							}
							else if(Stryk_Demo.SOURCE == Stryk_Demo.IRIS_SOURCE)
							{
								Stryk_Demo.chckbxEnv.setEnabled(false);
								Stryk_Demo.chckbxIris.setEnabled(false);
								for(Component c : Stryk_Demo.env_iris)
								{
									c.setEnabled(false);	
								}
								for(Component c : Stryk_Demo.env_source)
								{
									c.setEnabled(false);
									
								}
								for(Component c: Stryk_Demo.env_pwm_type)
								{
									c.setEnabled(false);
								}
								Stryk_Demo.lblEnv.setEnabled(false);
								Stryk_Demo.slider_19.setEnabled(false);
								Stryk_Demo.label_24.setEnabled(false);
								Stryk_Demo.iris_onoff_flag = true;
								Stryk_Demo.chckbxIris1.setEnabled(true);
								Stryk_Demo.chckbxIris1.setSelected(true);
								Stryk_Demo.slider_20.setEnabled(true);
								Stryk_Demo.label_25.setEnabled(true);
								Stryk_Demo.chckbxIris2.setSelected(true);
								Stryk_Demo.chckbxIris2.setEnabled(true);
								Stryk_Demo.slider_21.setEnabled(true);
								Stryk_Demo.label_26.setEnabled(true);
								Stryk_Demo.chckbxIrisPowerShutdown_1.setEnabled(true);
								Stryk_Demo.iris_onoff_flag = false;
								
								Stryk_Demo.textArea.append("IRIS Light Switched ON!!\r\n");
							}
							
						
							Stryk_Demo.EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/on.jpg")));
						}
						//ENV Mode Switch Disable
						else if(LastModeValues.ENVEnable == false)
						{
							//Disable and make non editable the LaserTextField
							Stryk_Demo.LasertextField.setEditable(false);
							Stryk_Demo.LasertextField.setEnabled(false);
							
							Stryk_Demo.chckbxEnv.setSelected(false);
							System.out.println("Switch OFF ENV");
							System.out.println("Connect Flag is : " + Stryk_Demo.connect_flag);
							Stryk_Demo.EnvIrisPowerButton.setSelected(false);
							Stryk_Demo.EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
							//Check whether IRIS is enabled even if the env is disabled
							if(LastModeValues.IRISEnable)
							{
								
								Stryk_Demo.chckbxEnv.setEnabled(false);
								Stryk_Demo.chckbxIris.setEnabled(false);
								Stryk_Demo.chckbxIris.setSelected(true);
								Stryk_Demo.EnvIrisPowerButton.setSelected(true);
								Stryk_Demo.EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/on.jpg")));
								Stryk_Demo.chckbxIris1.setEnabled(true);
								Stryk_Demo.slider_20.setEnabled(true);
								Stryk_Demo.label_25.setEnabled(true);
								Stryk_Demo.chckbxIris2.setEnabled(true);
								Stryk_Demo.slider_21.setEnabled(true);
								Stryk_Demo.label_26.setEnabled(true);
								Stryk_Demo.chckbxIrisPowerShutdown_1.setEnabled(true);
								for(Component c : Stryk_Demo.panel_30.getComponents())
								{
									c.setEnabled(false);
								}
								for(Component c1: Stryk_Demo.panel_31.getComponents())
								{
									c1.setEnabled(false);
									
								}
								for(Component c2 : Stryk_Demo.panel_32.getComponents())
								{
									c2.setEnabled(false);
								}
							}
							else
							{
								Stryk_Demo.chckbxIris1.setEnabled(false);
								Stryk_Demo.slider_20.setEnabled(false);
								Stryk_Demo.label_25.setEnabled(false);
								Stryk_Demo.chckbxIris2.setEnabled(false);
								Stryk_Demo.slider_21.setEnabled(false);
								Stryk_Demo.label_26.setEnabled(false);
								for(Component c : Stryk_Demo.panel_30.getComponents())
								{
									//System.out.println(c.);
									c.setEnabled(true);
								}
								for(Component c1: Stryk_Demo.panel_31.getComponents())
								{
									//System.out.println(c1.getName());
									c1.setEnabled(true);
									
								}
								for(Component c2 : Stryk_Demo.panel_32.getComponents())
								{
									//System.out.println(c2.getName());
									c2.setEnabled(true);
								}
								
								//Stryk_Demo.chckbxIris.setSelected(true);
								Stryk_Demo.chckbxEnv.setSelected(false);
								Stryk_Demo.EnvIrisPowerButton.setEnabled(false);
								Stryk_Demo.textArea.append("IRIS Switched OFF!!");
							}

							
							
						}
						else {}
						int val = 0;
						val = SignedtoUnsigned.printByte(LastModeValues.WhiteIntensity);
						Stryk_Demo.slider_15.setValue(val);
						Stryk_Demo.GrptextField.setText(Integer.toString(val));
						int local = (Stryk_Demo.slider_15.getValue()*100)/4095;
						Stryk_Demo.label_20.setText(Integer.toString(local) + " %");
						//Stryk_Demo.label_20.setText(Integer.toString(Stryk_Demo.slider_15.getValue()));
						
						val = SignedtoUnsigned.printByte(LastModeValues.RedIntensity);
						Stryk_Demo.slider_16.setValue(val);
						Stryk_Demo.label_21.setText(Integer.toString((Stryk_Demo.slider_16.getValue()*100)/4095)+ " %");
						Stryk_Demo.RedtextField.setText(Integer.toString(Stryk_Demo.slider_16.getValue()));
						//Stryk_Demo.label_21.setText(Integer.toString(Stryk_Demo.slider_16.getValue()));
						
						val = SignedtoUnsigned.printByte(LastModeValues.GreenIntensity);
						Stryk_Demo.slider_17.setValue(val);
						Stryk_Demo.GreentextField.setText(Integer.toString(Stryk_Demo.slider_17.getValue()));
						Stryk_Demo.label_22.setText(Integer.toString((Stryk_Demo.slider_17.getValue()*100)/4095)+ " %");
						//Stryk_Demo.label_22.setText(Integer.toString(Stryk_Demo.slider_17.getValue()));
						
						val = SignedtoUnsigned.printByte(LastModeValues.BlueIntensity);
						Stryk_Demo.slider_18.setValue(val);
						Stryk_Demo.BluetextField.setText(Integer.toString(Stryk_Demo.slider_18.getValue()));
						Stryk_Demo.label_23.setText(Integer.toString((Stryk_Demo.slider_18.getValue()*100)/4095)+ " %");
						//Stryk_Demo.label_23.setText(Integer.toString(Stryk_Demo.slider_18.getValue()));
						
						val = SignedtoUnsigned.printByte(LastModeValues.ENVIntensity);
						Stryk_Demo.slider_19.setValue(val);
						Stryk_Demo.LasertextField.setText(Integer.toString(Stryk_Demo.slider_19.getValue()));
						Stryk_Demo.label_24.setText(Integer.toString((Stryk_Demo.slider_19.getValue()*100)/4095)+ " %");
						//Stryk_Demo.LasertextField.setText(Integer.toString(val));
						
						//Stryk_Demo.label_24.setText(Integer.toString(Stryk_Demo.slider_19.getValue()));
						
						val = SignedtoUnsigned.printByte(LastModeValues.IRIS1Intensity);
						Stryk_Demo.slider_20.setValue(val);
						Stryk_Demo.Iris1TextField.setText(Integer.toString(Stryk_Demo.slider_20.getValue()));
						Stryk_Demo.label_25.setText(Integer.toString((Stryk_Demo.slider_20.getValue()*100)/255)+ " %");
						//Stryk_Demo.label_25.setText(Integer.toString(Stryk_Demo.slider_20.getValue()));
						
						val = SignedtoUnsigned.printByte(LastModeValues.IRIS2Intensity);
						Stryk_Demo.slider_21.setValue(val);
						Stryk_Demo.Iris2TextField.setText(Integer.toString(Stryk_Demo.slider_21.getValue()));
						Stryk_Demo.label_26.setText(Integer.toString((Stryk_Demo.slider_21.getValue()*100)/255)+ " %");
						//Stryk_Demo.	label_26.setText(Integer.toString(Stryk_Demo.slider_21.getValue()));
						
						Stryk_Demo.slider_12.setValue(LastModeValues.PowerSupplyFanSpeed);
						Stryk_Demo.label_5.setText(Integer.toString(Stryk_Demo.slider_12.getValue())+ " %");
						
						Stryk_Demo.slider_13.setValue(LastModeValues.HeatSinkFanSpeed);
						Stryk_Demo.label_17.setText(Integer.toString(Stryk_Demo.slider_13.getValue())+ " %");
						
						Stryk_Demo.slider_14.setValue(LastModeValues.IRISFanSpeed);
						Stryk_Demo.label_18.setText(Integer.toString(Stryk_Demo.slider_14.getValue())+ " %");
						
						if(!Stryk_Demo.chckbxEnv.isSelected() && !Stryk_Demo.chckbxIris.isSelected())
						{
							for(Component c1 : Stryk_Demo.panel_30.getComponents())
							{
								c1.setEnabled(false);
							}
							for(Component c2 : Stryk_Demo.panel_31.getComponents())
							{
								c2.setEnabled(false);
							}
							for(Component c3 : Stryk_Demo.panel_32.getComponents())
							{
								c3.setEnabled(false);
							}
						}
						else
						{
							if(Stryk_Demo.chckbxIris.isSelected())
							{
								
							}
							else if(Stryk_Demo.chckbxEnv.isSelected())
							{
								
							}
							else
							{
								for(Component c1 : Stryk_Demo.panel_30.getComponents())
								{
									c1.setEnabled(true);
								}
								for(Component c2 : Stryk_Demo.panel_31.getComponents())
								{
									c2.setEnabled(true);
								}
								for(Component c3 : Stryk_Demo.panel_32.getComponents())
								{
									c3.setEnabled(true);
								}
							}
							
						}
						Stryk_Demo.wl_critical_flag = false;
						Stryk_Demo.env_iris_critical_flag = false;
						Stryk_Demo.connect_flag = false;
						System.out.println("Values Restored Successfully");
						System.out.println("Out of restore mode option");
						publish("Configuration Restored Successfully!!\r\n\n");
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
					
					Stryk_Demo.connect_flag = false;
					
				}
			}
			else if(Cmd_Id.equals(Integer.toString(byt.SENSING_COMMAND)))
			{
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				
				
				//Buffer that hold the complete values(101 Bytes)
				byte[] UI_Values ;
				UI_Values = Arrays.copyOfRange(destuff_buffer, 7, destuff_buffer.length-2);
				System.out.println(Arrays.toString(UI_Values));
				full_buffer.reset();
				System.out.println("Full Buffer Reset");
				for(int i = 0;i<destuff_buffer.length;i++)
				{
					destuff_buffer[i] = 0;
				}
				//Byte Buffer to hold ADC Fields(80 bytes)
				byte[] ADC_Values;
				
				//Byte Buffer to hold Color Sensor Values(16 bytes)
				byte[] Color_Sensor_Values;
				
				//Byte Buffer to hold Sense gpio Values (5 bytes)
				byte[] Sense_Gpio_Values;
				
				//Float ArrayList to hold ADC Values
				ArrayList<Float> ADC_Buffer = new ArrayList<Float> ();
				
				//Integer ArrayList to hold Color Sensor Values
				ArrayList<Integer> CS_Buffer = new ArrayList<Integer> ();
				
				
				
				Stryk_Demo.label_6.setEnabled(true);
				Stryk_Demo.label_7.setEnabled(true);
				Stryk_Demo.label_8.setEnabled(true);
				Stryk_Demo.lblNewLabel.setEnabled(true);
				Stryk_Demo.label_3.setEnabled(true);
				Stryk_Demo.label_4.setEnabled(true);
				Stryk_Demo.label_9.setEnabled(true);
				Stryk_Demo.label_19.setEnabled(true);
				
				if(UI_Values.length != 110)
				{
					System.out.println(Integer.toString(UI_Values.length));
					System.out.println("Buffer Doesn't meet Expected Count!!");
					if (Stryk_Demo.Response_Animator.Animation.isVisible()) {
						Stryk_Demo.Response_Animator.Animation.setVisible(false);
					}
				}
				else
				{
					Stryk_Demo.textArea.append("Successfully Read All Values From the Device!!\r\n\n");
					out.write(UI_Values[0]);
					out.write(UI_Values[1]);
					out.write(UI_Values[2]);
					out.write(UI_Values[3]);
					out.write(UI_Values[4]);
					out.write(UI_Values[5]);
					out.write(UI_Values[102]);
					out.write(UI_Values[103]);
					
					
					ByteBuffer bb = ByteBuffer.allocate(4);
			        // Little Endian
			        bb.order(ByteOrder.LITTLE_ENDIAN);
			        // Place the LSB and MSB of bytes into the buffer
			        bb.put(UI_Values[104]);
			        bb.put(UI_Values[105]);
			        
			        Stryk_Demo.txtle_tach_count.setText(String.valueOf(bb.getInt(0)));
			        //int shortVal = bb.getInt(0);
			        bb.clear();
			        
			        bb.put(UI_Values[106]);
			        bb.put(UI_Values[107]);
			        Stryk_Demo.txths_tach_count.setText(String.valueOf(bb.getInt(0)));
			        //int shortVal = bb.getInt(0);
			        bb.clear();
			        
			        bb.put(UI_Values[108]);
			        bb.put(UI_Values[109]);
			        Stryk_Demo.txtiris_tach_count.setText(String.valueOf(bb.getInt(0)));
			        //int shortVal = bb.getInt(0);
			        bb.clear();
					
					Sense_Gpio_Values = out.toByteArray();
					ADC_Values = Arrays.copyOfRange(UI_Values, 6, 86);
					Color_Sensor_Values = Arrays.copyOfRange(UI_Values, 86, 102);
					
					ADC_Buffer = Stryk_Demo.Get_Float_From_ByteBuffer(ADC_Values);
					CS_Buffer = Stryk_Demo.Get_Integer_From_ByteBuffer(Color_Sensor_Values);
					Stryk_Demo.Update_ADC_Fields(ADC_Buffer);
					ADC_Buffer.clear();
					Stryk_Demo.Update_Color_Sensor_Fields(CS_Buffer);
					CS_Buffer.clear();
					Stryk_Demo.Update_GPIO_Status(Sense_Gpio_Values);
					
					
				}
				/*if(UI_Values.length != 101)
				{
					System.out.println(Integer.toString(UI_Values.length));
					System.out.println("Buffer Doesn't meet Expected Count!!");
				}
				else
				{
					Stryk_Demo.textArea.append("Successfully Read All Values From the Device!!\r\n\n");
					out.write(UI_Values[0]);
					out.write(UI_Values[1]);
					out.write(UI_Values[2]);
					out.write(UI_Values[99]);
					out.write(UI_Values[100]);
					
					Sense_Gpio_Values = out.toByteArray();
					ADC_Values = Arrays.copyOfRange(UI_Values, 3, 83);
					Color_Sensor_Values = Arrays.copyOfRange(UI_Values, 83, 99);
					
					ADC_Buffer = Stryk_Demo.Get_Float_From_ByteBuffer(ADC_Values);
					CS_Buffer = Stryk_Demo.Get_Integer_From_ByteBuffer(Color_Sensor_Values);
					Stryk_Demo.Update_ADC_Fields(ADC_Buffer);
					ADC_Buffer.clear();
					Stryk_Demo.Update_Color_Sensor_Fields(CS_Buffer);
					CS_Buffer.clear();
					Stryk_Demo.Update_GPIO_Status(Sense_Gpio_Values);
					
					
				}*/
			}
			else if(Cmd_Id.equals("ENABLE_IRIS_CONTROLS"))
			{
				Stryk_Demo.chckbxIris1.setEnabled(true);
				Stryk_Demo.chckbxIris2.setEnabled(true);
				Stryk_Demo.slider_20.setEnabled(true);
				Stryk_Demo.slider_21.setEnabled(true);
				Stryk_Demo.label_25.setEnabled(true);
				Stryk_Demo.label_26.setEnabled(true);
			}
			else if(Cmd_Id.equals("DISABLE_IRIS_CONTROLS"))
			{
				Stryk_Demo.chckbxIris1.setEnabled(false);
				Stryk_Demo.chckbxIris2.setEnabled(false);
				Stryk_Demo.slider_20.setEnabled(false);
				Stryk_Demo.slider_21.setEnabled(false);
				Stryk_Demo.label_25.setEnabled(false);
				Stryk_Demo.label_26.setEnabled(false);
			}
			else if(Cmd_Id.equals("CHECK_ENABLE"))
			{
				serialport.removeDataListener();
				bytedefinitions b = new bytedefinitions();
				if (Stryk_Demo.chckbxReset.isSelected()) 
				{
					Stryk_Demo.reset_ld_chckbx.clearSelection();
				}
				if(Stryk_Demo.chckbxLoadDefaults.isSelected())
				{
					destuff_buffer = null;
					System.out.println("Buffer Nullified");
					Stryk_Demo.reset_ld_chckbx.clearSelection();
				}	
				/*if(Stryk_Demo.chckbxRed.isEnabled()  && Stryk_Demo.chckbxGreen.isEnabled() && Stryk_Demo.chckbxBlue.isEnabled())
				{
					if(Stryk_Demo.chckbxRed.isSelected())
					{
						System.out.println("Red selected");
						Stryk_Demo.slider_16.setEnabled(true);
						Stryk_Demo.label_21.setText(Integer.toString(Stryk_Demo.slider_16.getValue()) + " %");
						Stryk_Demo.label_21.setEnabled(true);
						
					}
					if(!Stryk_Demo.chckbxRed.isSelected())
					{
						System.out.println("Red Unselected");
						Stryk_Demo.slider_16.setEnabled(false);
						Stryk_Demo.label_21.setEnabled(false);
					}
					if(Stryk_Demo.chckbxGreen.isSelected())
					{
						System.out.println("Green selected");
						Stryk_Demo.slider_17.setEnabled(true);
						Stryk_Demo.label_22.setEnabled(true);
						
					}
					if(!Stryk_Demo.chckbxGreen.isSelected())
					{
						System.out.println("Green Unselected");
						Stryk_Demo.slider_17.setEnabled(false);
						Stryk_Demo.label_22.setEnabled(false);
					}
					if(Stryk_Demo.chckbxBlue.isSelected())
					{
						System.out.println("Blue selected");
						Stryk_Demo.slider_18.setEnabled(true);
						Stryk_Demo.label_23.setEnabled(true);
					}
					if(!Stryk_Demo.chckbxBlue.isSelected())
					{
						System.out.println("Blue Unselected");
						Stryk_Demo.slider_18.setEnabled(false);
						Stryk_Demo.label_23.setEnabled(false);
					}
				}
				if(Stryk_Demo.chckbxIris1.isEnabled() && Stryk_Demo.chckbxIris2.isEnabled())
				{
					if(!Stryk_Demo.chckbxIris1.isSelected())
					{
						System.out.println("chckbx Iris1 Unselected");
						Stryk_Demo.slider_20.setEnabled(false);
						Stryk_Demo.label_25.setEnabled(false);
						
					}
					if(Stryk_Demo.chckbxIris1.isSelected())
					{
						Stryk_Demo.slider_20.setEnabled(true);
						Stryk_Demo.label_25.setEnabled(true);
						
					}
					if(!Stryk_Demo.chckbxIris2.isSelected())
					{
						System.out.println("chckbx Iris2 Unselected");
						Stryk_Demo.slider_21.setEnabled(false);
						Stryk_Demo.label_26.setEnabled(false);
					}
					if(Stryk_Demo.chckbxIris2.isSelected())
					{
						Stryk_Demo.slider_21.setEnabled(true);
						Stryk_Demo.label_26.setEnabled(true);
					}
				}*/
				//Stryk_Demo.textArea.append(Current_Control + " Command Sent Successfully.\r\n\n");
				if( Current_Control.equals(Reset_uC2))
				{
					publish(Integer.toString(byt.DISCONNECT_COMMAND) + "," + Current_Control);
					//process_serialport_response(b.DISCONNECT_COMMAND);
					full_buffer.reset();
					if(destuff_buffer != null)
					{
						for(int i=0;i<destuff_buffer.length;i++)
						{
							destuff_buffer[i] = 0;
						}
					}
					if(recv_buff != null)
					{
						for(int i = 0;i<recv_buff.length;i++)
						{
							recv_buff[i] = 0;
						}
					}
				}
				else
				{
					System.out.println("#####################################################");
				}
			}
			else if(Cmd_Id.equals("INDIVIDUAL_SELECTED"))
			{
				Stryk_Demo.individual_flag = true;
				Component[] p29 = Stryk_Demo.panel_29.getComponents();
				
				Stryk_Demo.slider_15.setEnabled(false);
				Stryk_Demo.lblGrp.setEnabled(false);
				Stryk_Demo.label_20.setEnabled(false);
				for(Component c : p29)
				{
					if(c instanceof JCheckBox)
					{
						c.setEnabled(true);
					}
					
				}
				if(Stryk_Demo.EnvIrisPowerButton.isSelected() == false)
				{
					if((Stryk_Demo.rdbtnEnv_CwmNr.isSelected() == false) &&(Stryk_Demo.rdbtnEnv_LcNr.isSelected() == false))
					{
						Stryk_Demo.chckbxRed.setEnabled(true);
						Stryk_Demo.slider_16.setEnabled(true);
						Stryk_Demo.label_21.setEnabled(true);
					}
					
				}
				
				else
				{
					Stryk_Demo.chckbxRed.setEnabled(false);
					Stryk_Demo.slider_16.setEnabled(false);
					Stryk_Demo.label_21.setEnabled(false);
				}
					if(Stryk_Demo.chckbxRed.isSelected())
					{
						Stryk_Demo.slider_16.setEnabled(true);
						Stryk_Demo.connect_flag = true;
						Stryk_Demo.slider_16.setValue(Stryk_Demo.slider_15.getValue());
						Stryk_Demo.connect_flag = false;
						Stryk_Demo.label_21.setEnabled(true);
						Stryk_Demo.label_21.setText(Integer.toString(Stryk_Demo.slider_16.getValue()) + " %");
						
					}
					if(!Stryk_Demo.chckbxRed.isSelected())
					{
						Stryk_Demo.slider_16.setEnabled(false);
						Stryk_Demo.connect_flag = true;
						Stryk_Demo.slider_16.setValue(0);
						Stryk_Demo.connect_flag = false;
						Stryk_Demo.label_21.setText(Integer.toString(0) + " %");
						Stryk_Demo.label_21.setEnabled(false);
					}
					if(Stryk_Demo.chckbxGreen.isSelected())
					{
						Stryk_Demo.slider_17.setEnabled(true);
						Stryk_Demo.connect_flag = true;
						Stryk_Demo.slider_17.setValue(Stryk_Demo.slider_15.getValue());
						Stryk_Demo.connect_flag = false;
						Stryk_Demo.label_22.setText(Integer.toString(Stryk_Demo.slider_17.getValue()) + " %");
						Stryk_Demo.label_22.setEnabled(true);
						
					}
					if(!Stryk_Demo.chckbxGreen.isSelected())
					{
						Stryk_Demo.slider_17.setEnabled(false);
						Stryk_Demo.connect_flag = true;
						Stryk_Demo.slider_17.setValue(0);
						Stryk_Demo.connect_flag = false;
						Stryk_Demo.label_22.setText(Integer.toString(Stryk_Demo.slider_17.getValue()) + " %");
						Stryk_Demo.label_22.setEnabled(false);
					}
					if(Stryk_Demo.chckbxBlue.isSelected())
					{
						Stryk_Demo.slider_18.setEnabled(true);
						Stryk_Demo.connect_flag = true;
						Stryk_Demo.slider_18.setValue(Stryk_Demo.slider_15.getValue());
						Stryk_Demo.connect_flag = false;
						Stryk_Demo.label_23.setText(Integer.toString(Stryk_Demo.slider_18.getValue()) + " %");
						Stryk_Demo.label_23.setEnabled(true);
					}
					if(!Stryk_Demo.chckbxBlue.isSelected())
					{
						Stryk_Demo.slider_18.setEnabled(false);
						Stryk_Demo.connect_flag = true;
						Stryk_Demo.slider_18.setValue(0);
						Stryk_Demo.connect_flag = false;
						Stryk_Demo.label_23.setText(Integer.toString(Stryk_Demo.slider_18.getValue()) + " %");
						Stryk_Demo.label_23.setEnabled(false);
					}
					Stryk_Demo.individual_flag = false;
			}
			else if(Cmd_Id.equals("INDIVIDUAL_DESELECTED"))
			{
				Component[] p29 = Stryk_Demo.panel_29.getComponents();
				Stryk_Demo.slider_15.setEnabled(true);
				Stryk_Demo.lblGrp.setEnabled(true);
				Stryk_Demo.label_20.setEnabled(true);
				for(Component c : p29)
				{
					if(c instanceof JCheckBox)
					{
						c.setEnabled(false);
					}
					
				}
				Stryk_Demo.slider_16.setEnabled(false);
				Stryk_Demo.label_21.setEnabled(false);
				Stryk_Demo.slider_17.setEnabled(false);
				Stryk_Demo.label_22.setEnabled(false);
				Stryk_Demo.slider_18.setEnabled(false);
				Stryk_Demo.label_23.setEnabled(false);
				Stryk_Demo.chckbxRed.setEnabled(false);
				Stryk_Demo.chckbxGreen.setEnabled(false);
				Stryk_Demo.chckbxBlue.setEnabled(false);
			}
			else if(Cmd_Id.equals(WL_FAKE_ON))
			{
				System.out.println("Reached WL FAKE ON");
				if(Stryk_Demo.EnvIrisPowerButton.isSelected())
				{
					Color en = Color.decode("#ccffff");
					Stryk_Demo.panel_33.setBackground(en);
				}
				
				Stryk_Demo.textArea.append("White Light Switched ON!!\r\n\n");
				Component[] p26 = Stryk_Demo.panel_26.getComponents();
				Component[] p27 = Stryk_Demo.panel_27.getComponents();
				Component[] p28 = Stryk_Demo.panel_28.getComponents();
				
				for(Component  c : p26)
				{
					c.setEnabled(false);
				}
				for(Component  c : p27)
				{
					c.setEnabled(false);
				}
				for(Component  c : p28)
				{
					c.setEnabled(false);
				}
				Stryk_Demo.slider_15.setEnabled(true);
				Stryk_Demo.lblGrp.setEnabled(true);
				Stryk_Demo.label_20.setEnabled(true);
				Stryk_Demo.rdbtnIndividual.setEnabled(true);
				Stryk_Demo.wl_onoff_flag = true;
				Stryk_Demo.label_21.setEnabled(false);
				Stryk_Demo.label_22.setEnabled(false);
				Stryk_Demo.label_23.setEnabled(false);
				Stryk_Demo.chckbxRed.setSelected(true);
				Stryk_Demo.chckbxGreen.setSelected(true);
				Stryk_Demo.chckbxBlue.setSelected(true);
				Stryk_Demo.lblLc.setEnabled(false);
				Stryk_Demo.rdbtnLc.setEnabled(false);
				Stryk_Demo.wl_onoff_flag = false;
			}
			else if(Cmd_Id.equals(CWM_NR_ON))
			{

				Stryk_Demo.wl_onoff_flag = true;
				Stryk_Demo.connect_flag = true;
				Stryk_Demo.chckbxRed.setSelected(false);
				Stryk_Demo.chckbxRed.setEnabled(false);
				Stryk_Demo.slider_16.setEnabled(false);
				Stryk_Demo.label_21.setEnabled(false);
				Stryk_Demo.RedtextField.setEditable(false);
				Stryk_Demo.RedtextField.setEnabled(false);
				Stryk_Demo.wl_onoff_flag = false;
				Stryk_Demo.connect_flag = false;
			}
			else if(Cmd_Id.equals(CWM_NR_OFF))
			{
				Stryk_Demo.wl_onoff_flag = true;
				Stryk_Demo.connect_flag = true;
				Stryk_Demo.chckbxRed.setSelected(true);
				Stryk_Demo.chckbxRed.setEnabled(true);
				Stryk_Demo.slider_16.setEnabled(true);
				Stryk_Demo.label_21.setEnabled(true);
				Stryk_Demo.RedtextField.setEditable(true);
				Stryk_Demo.RedtextField.setEnabled(true);
				Stryk_Demo.wl_onoff_flag = false;
				Stryk_Demo.connect_flag = false;
			}
			else if(Cmd_Id.equals(ENV_IRIS_FAKE_ON))
			{
				Stryk_Demo.EnvIrisPowerButton.setEnabled(true);
				
				System.out.println("Serial Port Flag False");
				if(Stryk_Demo.WlPowerButton.isSelected())
				{
					Color en = Color.decode("#ccffff");
					Stryk_Demo.panel_33.setBackground(en);
				}
				if(Stryk_Demo.SOURCE == Stryk_Demo.ENV_SOURCE)
				{
					Stryk_Demo.textArea.append("ENV Light Switched ON!!\r\n");
					Stryk_Demo.chckbxEnv.setEnabled(false);
					Stryk_Demo.chckbxIris.setEnabled(false);
					for(Component c : Stryk_Demo.env_iris)
					{
						c.setEnabled(false);	
					}
					for(Component c : Stryk_Demo.env_source)
					{
						c.setEnabled(false);
						
					}
					for(Component c: Stryk_Demo.env_pwm_type)
					{
						c.setEnabled(false);
					}
					Stryk_Demo.lblEnv.setEnabled(true);
					Stryk_Demo.slider_19.setEnabled(true);
					Stryk_Demo.label_24.setEnabled(true);
					Stryk_Demo.chckbxIris1.setEnabled(false);
					Stryk_Demo.chckbxIris2.setEnabled(false);
					Stryk_Demo.chckbxIrisPowerShutdown_1.setEnabled(false);
					Stryk_Demo.slider_20.setEnabled(false);
					Stryk_Demo.label_25.setEnabled(false);
					Stryk_Demo.slider_21.setEnabled(false);
					Stryk_Demo.label_26.setEnabled(false);
					
				}
				else if(Stryk_Demo.SOURCE == Stryk_Demo.IRIS_SOURCE)
				{
					Stryk_Demo.chckbxEnv.setEnabled(false);
					Stryk_Demo.chckbxIris.setEnabled(false);
					for(Component c : Stryk_Demo.env_iris)
					{
						c.setEnabled(false);	
					}
					for(Component c : Stryk_Demo.env_source)
					{
						c.setEnabled(false);
						
					}
					for(Component c: Stryk_Demo.env_pwm_type)
					{
						c.setEnabled(false);
					}
					Stryk_Demo.lblEnv.setEnabled(false);
					Stryk_Demo.slider_19.setEnabled(false);
					Stryk_Demo.label_24.setEnabled(false);
					Stryk_Demo.iris_onoff_flag = true;
					Stryk_Demo.chckbxIris1.setEnabled(true);
					Stryk_Demo.chckbxIris1.setSelected(true);
					Stryk_Demo.slider_20.setEnabled(true);
					Stryk_Demo.label_25.setEnabled(true);
					Stryk_Demo.chckbxIris2.setSelected(true);
					Stryk_Demo.chckbxIris2.setEnabled(true);
					Stryk_Demo.slider_21.setEnabled(true);
					Stryk_Demo.label_26.setEnabled(true);
					Stryk_Demo.chckbxIrisPowerShutdown_1.setEnabled(true);
					Stryk_Demo.iris_onoff_flag = false;
					
					Stryk_Demo.textArea.append("IRIS Light Switched ON!!\r\n");
				}
				
			
				Stryk_Demo.EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/on.jpg")));
			}
			else if(Cmd_Id.equals(ENV_IRIS_SEL_FALSE))
			{
				Stryk_Demo.ShowMessage("Please Choose a Valid Source!!");
				Stryk_Demo.EnvIrisPowerButton.setSelected(false);
			}
			else if(Cmd_Id.equals(Integer.toString(byt.MODE_COMMAND)))
			{
				//System.out.println("Response Time : " + System.currentTimeMillis());
				if(switch_flag)
				{
					
					Stryk_Demo.wl_critical_flag = true;
					Stryk_Demo.env_iris_critical_flag = true;
					if(Stryk_Demo.Wl_Button_Flag == true)
					{
						if(Stryk_Demo.WlPowerButton.isSelected() == true)
						{
							System.out.println("WL already switched on");
							//Stryk_Demo.WlPowerButton.setSelected(false);
							Stryk_Demo.WlPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
						}
						else if(Stryk_Demo.WlPowerButton.isSelected() == false)
						{
							Stryk_Demo.WlPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
							System.out.println("WL already Switched OFF");
							//Stryk_Demo.WlPowerButton.setSelected(true);
						}
						Stryk_Demo.Wl_Button_Flag = false;
					}
					if(Stryk_Demo.ENV_Iris_Button_Flag == true)
					{
						if(Stryk_Demo.EnvIrisPowerButton.isSelected() == true)
						{
							System.out.println("ENV_IRIS ON State");
							Stryk_Demo.EnvIrisPowerButton.setSelected(false);
						}
						else if(Stryk_Demo.EnvIrisPowerButton.isSelected() == false)
						{
							System.out.println("ENV_IRIS OFF State");
							Stryk_Demo.EnvIrisPowerButton.setSelected(true);
						}
						Stryk_Demo.ENV_Iris_Button_Flag = false;
					}
					
					else
					{
						
					}
					Stryk_Demo.wl_critical_flag = false;
					Stryk_Demo.env_iris_critical_flag = false;
					switch_flag = false;
					
				}
				else
				{
					System.out.println("Response Time : " + LocalDateTime.now());
					if(Current_Control.equals(WL_PON))
					{
						//JOptionPane.showMessageDialog(Stryk_Demo.frmStrykerVer, "Correct", "TEST", JOptionPane.INFORMATION_MESSAGE);
						if(Stryk_Demo.EnvIrisPowerButton.isSelected())
						{
							Color en = Color.decode("#ccffff");
							Stryk_Demo.panel_33.setBackground(en);
						}
						Stryk_Demo.GrptextField.setEnabled(true);
						Stryk_Demo.GrptextField.setEditable(true);
						//Set the slider value in the textbox
						Stryk_Demo.GrptextField.setText(Integer.toString(Stryk_Demo.slider_15.getValue()));
						Stryk_Demo.RedtextField.setText(Integer.toString(Stryk_Demo.slider_16.getValue()));
						Stryk_Demo.GreentextField.setText(Integer.toString(Stryk_Demo.slider_17.getValue()));
						Stryk_Demo.BluetextField.setText(Integer.toString(Stryk_Demo.slider_18.getValue()));
						Stryk_Demo.textArea.append("White Light Switched ON Actual Control!!\r\n\n");
						Component[] p26 = Stryk_Demo.panel_26.getComponents();
						Component[] p27 = Stryk_Demo.panel_27.getComponents();
						Component[] p28 = Stryk_Demo.panel_28.getComponents();
						
						for(Component  c : p26)
						{
							c.setEnabled(false);
						}
						for(Component  c : p27)
						{
							c.setEnabled(false);
						}
						for(Component  c : p28)
						{
							c.setEnabled(false);
						}
						Stryk_Demo.rdbtn120hz.setEnabled(false);
						Stryk_Demo.rdbtn70khz.setEnabled(false);
						Stryk_Demo.slider_15.setEnabled(true);
						Stryk_Demo.lblGrp.setEnabled(true);
						Stryk_Demo.label_20.setEnabled(true);
						Stryk_Demo.rdbtnIndividual.setEnabled(true);
						Stryk_Demo.wl_onoff_flag = true;
						Stryk_Demo.label_21.setEnabled(false);
						Stryk_Demo.label_22.setEnabled(false);
						Stryk_Demo.label_23.setEnabled(false);
						Stryk_Demo.chckbxRed.setSelected(true);
						Stryk_Demo.chckbxGreen.setSelected(true);
						Stryk_Demo.chckbxBlue.setSelected(true);
						Stryk_Demo.lblLc.setEnabled(false);
						Stryk_Demo.rdbtnLc.setEnabled(false);
						Stryk_Demo.wl_onoff_flag = false;
						
						Stryk_Demo.WlPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/on.jpg")));
					}
					else if(Current_Control.equals(WL_POFF))
					{
						String current_rdbtn_mode = "";
						String current_rdbtn_source = "";
						Stryk_Demo.textArea.append("White Light Switched OFF Actual Control!!\r\n\n");
						
						Component[] p26 = Stryk_Demo.panel_26.getComponents();
						Component[] p27 =Stryk_Demo.panel_27.getComponents();
						
						Stryk_Demo.GrptextField.setEnabled(false);
						Stryk_Demo.GrptextField.setEditable(false);
						
						Stryk_Demo.RedtextField.setEditable(false);
						Stryk_Demo.RedtextField.setEnabled(false);
						
						Stryk_Demo.GreentextField.setEditable(false);
						Stryk_Demo.GreentextField.setEnabled(false);
						
						Stryk_Demo.BluetextField.setEditable(false);
						Stryk_Demo.BluetextField.setEnabled(false);
						
						for(Component  c : p26)
						{
							c.setEnabled(true);
							
						}
						for(Component c : p27)
						{
							c.setEnabled(true);
						}
						
						/////////////////////////////////////////////////////////////////////////
						if(Stryk_Demo.rdbtnExt.isSelected())
						{
							current_rdbtn_source = Stryk_Demo.radiobuttonExt;
						}
						else if(Stryk_Demo.rdbtnuC_1.isSelected())
						{
							current_rdbtn_source = Stryk_Demo.radiobuttonuC_1;
							
						}
						else if(Stryk_Demo.rdbtnuC_2.isSelected())
						{
							current_rdbtn_source = Stryk_Demo.radiobuttonuC_2;
						}
						else
						{
							
						}
						Stryk_Demo.wlsource.clearSelection();
						
						/////////////////////////////////////////////////////////
						/////////////////////////////////////
						if(Stryk_Demo.rdbtnPwm.isSelected())
						{
						current_rdbtn_mode = Stryk_Demo.radiobuttonPwm;
						}
						else if(Stryk_Demo.rdbtnCwm.isSelected())
						{
						current_rdbtn_mode = Stryk_Demo.radiobuttonCwm;
						}
						else if(Stryk_Demo.rdbtnStb.isSelected())
						{
						current_rdbtn_mode = Stryk_Demo.radiobuttonStb;
						}
						else if(Stryk_Demo.rdbtnLc.isSelected())
						{
						current_rdbtn_mode = Stryk_Demo.radiobuttonLc;
						}
						else
						{
						
						}
						Stryk_Demo.wlmode.clearSelection();
						if(current_rdbtn_mode == Stryk_Demo.radiobuttonPwm)
						{
							Stryk_Demo.rdbtnPwm.setSelected(true);
						}
						else if(current_rdbtn_mode == Stryk_Demo.radiobuttonCwm)
						{
							Stryk_Demo.rdbtnCwm.setSelected(true);
						
						}
						else if(current_rdbtn_mode == Stryk_Demo.radiobuttonStb)
						{
							Stryk_Demo.rdbtnStb.setSelected(true);
						}
						else if(current_rdbtn_mode == Stryk_Demo.radiobuttonLc)
						{
							Stryk_Demo.rdbtnLc.setSelected(true);
						}
						current_rdbtn_mode = "";
						if(current_rdbtn_source == Stryk_Demo.radiobuttonExt)
						{
							Stryk_Demo.rdbtnExt.setSelected(true);
						}
						else if(current_rdbtn_source == Stryk_Demo.radiobuttonuC_1)
						{
							Stryk_Demo.rdbtnuC_1.setSelected(true);
						}
						else if(current_rdbtn_source == Stryk_Demo.radiobuttonuC_2)
						{
							Stryk_Demo.rdbtnuC_2.setSelected(true);
						}
						else 
						{
							System.out.println("No valid source");
						}
						current_rdbtn_source = "";
						/////////////////////////////////////////////////////
						Stryk_Demo.wl_onoff_flag = true;
						
						Stryk_Demo.rdbtnIndividual.setSelected(false);
						Stryk_Demo.rdbtnIndividual.setEnabled(false);
						
						
						Stryk_Demo.slider_15.setEnabled(false);
						Stryk_Demo.label_20.setText(Integer.toString((Stryk_Demo.slider_15.getValue()*100)/4095) + " %");
						//Stryk_Demo.label_20.setText(Integer.toString(Stryk_Demo.slider_15.getValue()));
						Stryk_Demo.label_20.setEnabled(false);
						
						Stryk_Demo.slider_16.setEnabled(false);
						//Stryk_Demo.slider_16.setValue(0);
						Stryk_Demo.label_21.setText(Integer.toString((Stryk_Demo.slider_16.getValue()*100)/4095) + " %");
						//Stryk_Demo.label_21.setText(Integer.toString(Stryk_Demo.slider_16.getValue()));
						Stryk_Demo.label_21.setEnabled(false);
						
						
						//Stryk_Demo.slider_17.setValue(0);
						Stryk_Demo.slider_17.setEnabled(false);
						Stryk_Demo.label_22.setText(Integer.toString((Stryk_Demo.slider_17.getValue()*100)/4095) + " %");
						//Stryk_Demo.label_22.setText(Integer.toString(Stryk_Demo.slider_17.getValue()));
						Stryk_Demo.label_22.setEnabled(false);
						
						
						//Stryk_Demo.slider_18.setValue(0);
						Stryk_Demo.slider_18.setEnabled(false);
						Stryk_Demo.label_23.setText(Integer.toString((Stryk_Demo.slider_18.getValue())) + " %");
						//Stryk_Demo.label_23.setText(Integer.toString(Stryk_Demo.slider_18.getValue()));
						Stryk_Demo.label_23.setEnabled(false);
						
						Stryk_Demo.chckbxRed.setSelected(false);
						Stryk_Demo.chckbxRed.setEnabled(false);
						
						Stryk_Demo.chckbxGreen.setSelected(false);
						Stryk_Demo.chckbxGreen.setEnabled(false);
						
						Stryk_Demo.chckbxBlue.setSelected(false);
						Stryk_Demo.chckbxBlue.setEnabled(false);
						
						//Disable the controls and disable the editing of Grp, Red, Green and Blue TextFields
						Stryk_Demo.GrptextField.setEditable(false);
						Stryk_Demo.GrptextField.setEnabled(false);
						
						Stryk_Demo.RedtextField.setEditable(false);
						Stryk_Demo.RedtextField.setEnabled(false);
						
						Stryk_Demo.GreentextField.setEditable(false);
						Stryk_Demo.GreentextField.setEnabled(false);
						
						Stryk_Demo.BluetextField.setEditable(false);
						Stryk_Demo.BluetextField.setEnabled(false);
						
						Stryk_Demo.wl_onoff_flag = false;
						Stryk_Demo.lblGrp.setEnabled(false);
						Stryk_Demo.label_20.setEnabled(false);
						Stryk_Demo.lblLc.setEnabled(true);
						Stryk_Demo.rdbtnLc.setEnabled(true);
						Stryk_Demo.WlPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
						if(!Stryk_Demo.rdbtnLc.isSelected())
						{
							
							Stryk_Demo.rdbtnPwm.setEnabled(true);
							Stryk_Demo.rdbtnCwm.setEnabled(true);
							Stryk_Demo.rdbtnStb.setEnabled(true);
						}
						else
						{
							if(Stryk_Demo.rdbtnEnv_LcNr.isSelected())
							{
								Stryk_Demo.rdbtnPwm.setEnabled(false);
								Stryk_Demo.rdbtnCwm.setEnabled(false);
								Stryk_Demo.rdbtnStb.setEnabled(false);
								
								
							}
							else
							{
								Stryk_Demo.rdbtnPwm.setEnabled(true);
								Stryk_Demo.rdbtnCwm.setEnabled(true);
								Stryk_Demo.rdbtnStb.setEnabled(true);
							}
							
						}
					}
					else if(Current_Control.equals(ENV_IRIS_ON))
					{
						if(Stryk_Demo.WlPowerButton.isSelected())
						{
							Color en = Color.decode("#ccffff");
							Stryk_Demo.panel_33.setBackground(en);
						}
						if(Stryk_Demo.SOURCE == Stryk_Demo.ENV_SOURCE)
						{
							
							//Enable and make editable the lasertextfield to get absolute control input from user
							Stryk_Demo.LasertextField.setEditable(true);
							Stryk_Demo.LasertextField.setEnabled(true);
							Stryk_Demo.LasertextField.setText(Integer.toString(Stryk_Demo.slider_19.getValue()));
							
							Stryk_Demo.textArea.append("ENV Light Switched ON Actual Control!!\r\n\n");
							Stryk_Demo.chckbxEnv.setEnabled(false);
							Stryk_Demo.chckbxIris.setEnabled(false);
							for(Component c : Stryk_Demo.env_iris)
							{
								c.setEnabled(false);	
							}
							for(Component c : Stryk_Demo.env_source)
							{
								c.setEnabled(false);
								
							}
							for(Component c: Stryk_Demo.env_pwm_type)
							{
								c.setEnabled(false);
							}
							Stryk_Demo.lblEnv.setEnabled(true);
							Stryk_Demo.slider_19.setEnabled(true);
							Stryk_Demo.label_24.setEnabled(true);
							Stryk_Demo.chckbxIris1.setEnabled(false);
							Stryk_Demo.chckbxIris2.setEnabled(false);
							Stryk_Demo.chckbxIrisPowerShutdown_1.setEnabled(false);
							Stryk_Demo.slider_20.setEnabled(false);
							Stryk_Demo.label_25.setEnabled(false);
							Stryk_Demo.slider_21.setEnabled(false);
							Stryk_Demo.label_26.setEnabled(false);
							
						}
						else if(Stryk_Demo.SOURCE == Stryk_Demo.IRIS_SOURCE)
						{
							
							//Enable and set editable the iris1 and iris2 text field to get absolute input from user
							
							Stryk_Demo.Iris1TextField.setEditable(true);
							Stryk_Demo.Iris1TextField.setEnabled(true);
							Stryk_Demo.Iris1TextField.setText(Integer.toString(Stryk_Demo.slider_20.getValue()));
							
							
							Stryk_Demo.Iris2TextField.setEditable(true);
							Stryk_Demo.Iris2TextField.setEnabled(true);
							Stryk_Demo.Iris2TextField.setText(Integer.toString(Stryk_Demo.slider_21.getValue()));
							
							
							Stryk_Demo.chckbxEnv.setEnabled(false);
							Stryk_Demo.chckbxIris.setEnabled(false);
							for(Component c : Stryk_Demo.env_iris)
							{
								c.setEnabled(false);	
							}
							for(Component c : Stryk_Demo.env_source)
							{
								c.setEnabled(false);
								
							}
							for(Component c: Stryk_Demo.env_pwm_type)
							{
								c.setEnabled(false);
							}
							Stryk_Demo.lblEnv.setEnabled(false);
							Stryk_Demo.slider_19.setEnabled(false);
							Stryk_Demo.label_24.setEnabled(false);
							Stryk_Demo.iris_onoff_flag = true;
							Stryk_Demo.chckbxIris1.setEnabled(true);
							//Stryk_Demo.chckbxIris1.setSelected(false); //Change suggested by MR	
							Stryk_Demo.chckbxIris1.setSelected(true);
							Stryk_Demo.slider_20.setEnabled(true);
							Stryk_Demo.label_25.setEnabled(true);
							
							Stryk_Demo.chckbxIris2.setEnabled(true);
							Stryk_Demo.chckbxIris2.setSelected(true);
							//Stryk_Demo.chckbxIris2.setSelected(false); //Change Suggested by MR
							Stryk_Demo.slider_21.setEnabled(true);
							Stryk_Demo.label_26.setEnabled(true);
							Stryk_Demo.chckbxIrisPowerShutdown_1.setEnabled(true);
							Stryk_Demo.iris_onoff_flag = false;
							
							Stryk_Demo.textArea.append("IRIS Light Switched ON!!\r\n");
						}
						else
						{
							System.out.println("HEEEEEEEEEEEEEEE");
						}
						Stryk_Demo.EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/on.jpg")));
					}
					else if(Current_Control.equals(ENV_IRIS_OFF))
					{
						Stryk_Demo.LasertextField.setEditable(false);
						Stryk_Demo.LasertextField.setEnabled(false);
						//EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
						Color en = Color.decode("#000000");
						Stryk_Demo.panel_33.setBackground(en);
						String current_rdbtn_mode = "";
						String current_rdbtn_source = "";
						if(Stryk_Demo.SOURCE == Stryk_Demo.ENV_SOURCE)
						{
							for(Component c : Stryk_Demo.env_iris)
							{
								c.setEnabled(true);	
							}
							
							
							if(Stryk_Demo.rdbtnEnv_Ext.isSelected())
							{
								current_rdbtn_source = Stryk_Demo.radiobuttonExtEnv;
							}
							else if(Stryk_Demo.rdbtnuC_3.isSelected())
							{
								current_rdbtn_source = Stryk_Demo.radiobuttonuC_3;
							}
							else if(Stryk_Demo.rdbtnuC_4.isSelected())
							{
								current_rdbtn_source = Stryk_Demo.radiobuttonuC_4;
							}
							else if(Stryk_Demo.rdbtn_IrisExtWht.isSelected())
							{
								current_rdbtn_source = Stryk_Demo.radiobuttonIrisExtWht;
							}
							else if(Stryk_Demo.rdbtn_IrisExtEnv.isSelected())
							{
								current_rdbtn_source = Stryk_Demo.radiobuttonIrisExtEnv;
							}
							else
							{
								
							}
							Stryk_Demo.env_iris_source.clearSelection();
							if(Stryk_Demo.rdbtnEnv_Pwm.isSelected())
							{
								current_rdbtn_mode = Stryk_Demo.radiobuttonEnvPwm;
							}
							else if(Stryk_Demo.rdbtnEnv_Cwm.isSelected())
							{
								current_rdbtn_mode = Stryk_Demo.radiobuttonEnvCwm;
							}
							else if(Stryk_Demo.rdbtnEnv_CwmNr.isSelected())
							{
								current_rdbtn_mode = Stryk_Demo.radiobuttonEnvCwmNr;
							}
							else if(Stryk_Demo.rdbtnEnv_LcNr.isSelected())
							{
								current_rdbtn_mode = Stryk_Demo.radiobuttonEnvLcNr;
							}
							else
							{
								
							}
							Stryk_Demo.env_iris_mode.clearSelection();
							if(current_rdbtn_mode == Stryk_Demo.radiobuttonEnvPwm)
							{
								Stryk_Demo.rdbtnEnv_Pwm.setSelected(true);
							}
							else if(current_rdbtn_mode == Stryk_Demo.radiobuttonEnvCwm)
							{
								Stryk_Demo.rdbtnEnv_Cwm.setSelected(true);
							}
							else if(current_rdbtn_mode == Stryk_Demo.radiobuttonEnvCwmNr)
							{
								Stryk_Demo.rdbtnEnv_CwmNr.setSelected(true);
							}
							else if(current_rdbtn_mode == Stryk_Demo.radiobuttonEnvLcNr)
							{
								Stryk_Demo.rdbtnEnv_LcNr.setSelected(true);
							}
							else
							{
								
							}
							if(current_rdbtn_source == Stryk_Demo.radiobuttonExtEnv)
							{
								Stryk_Demo.rdbtnEnv_Ext.setSelected(true);
							}
							else if(current_rdbtn_source == Stryk_Demo.radiobuttonuC_3)
							{
								Stryk_Demo.rdbtnuC_3.setSelected(true);
							}
							else if(current_rdbtn_source == Stryk_Demo.radiobuttonuC_4)
							{
								Stryk_Demo.rdbtnuC_4.setSelected(true);
							}
							else if(current_rdbtn_source == Stryk_Demo.radiobuttonIrisExtWht)
							{
								Stryk_Demo.rdbtn_IrisExtWht.setSelected(true);
							}
							else if(current_rdbtn_source == Stryk_Demo.radiobuttonIrisExtEnv)
							
							{
								Stryk_Demo.rdbtn_IrisExtEnv.setSelected(true);
							}
							else
							{
								
							}
							
								
							
							Stryk_Demo.lblEnv.setEnabled(false);
							Stryk_Demo.slider_19.setEnabled(false);
							Stryk_Demo.label_24.setEnabled(false);
							Stryk_Demo.chckbxIris1.setEnabled(false);
							Stryk_Demo.chckbxIris2.setEnabled(false);
							Stryk_Demo.chckbxIrisPowerShutdown_1.setEnabled(false);
							Stryk_Demo.slider_20.setEnabled(false);
							Stryk_Demo.label_25.setEnabled(false);
							Stryk_Demo.slider_21.setEnabled(false);
							Stryk_Demo.label_26.setEnabled(false);
							if(Stryk_Demo.rdbtnEnv_LcNr.isSelected())
							{
								if(!Stryk_Demo.WlPowerButton.isSelected())
								{
									Stryk_Demo.rdbtnLc.setSelected(true);
									Stryk_Demo.rdbtnLc.setEnabled(true);
								}
								
							}
							if(Stryk_Demo.rdbtnLc.isSelected())
								
							{
								System.out.println("YES");
								Stryk_Demo.rdbtnEnv_LcNr.setEnabled(true);
								
							}
							else
							{
								Stryk_Demo.rdbtnEnv_LcNr.setEnabled(false);
							}
							Stryk_Demo.chckbxEnv.setEnabled(true);
							Stryk_Demo.chckbxIris.setEnabled(true);
							Stryk_Demo.textArea.append("ENV Light Switched OFF Actual Control\r\n\n");
						}
						else if(Stryk_Demo.SOURCE == Stryk_Demo.IRIS_SOURCE)
						{
							
							Stryk_Demo.Iris1TextField.setEditable(false);
							Stryk_Demo.Iris1TextField.setEnabled(false);
							
							Stryk_Demo.Iris2TextField.setEditable(false);
							Stryk_Demo.Iris2TextField.setEnabled(false);
							
							Stryk_Demo.chckbxEnv.setEnabled(true);
							Stryk_Demo.chckbxIris.setEnabled(true);
							
							Stryk_Demo.iris_onoff_flag = true;
							Stryk_Demo.chckbxIris1.setSelected(false);
							Stryk_Demo.chckbxIris2.setSelected(false);
							Stryk_Demo.iris_onoff_flag = false;
							if(Stryk_Demo.rdbtnEnv_Ext.isSelected())
							{
								current_rdbtn_source = Stryk_Demo.radiobuttonExtEnv;
							}
							else if(Stryk_Demo.rdbtnuC_3.isSelected())
							{
								current_rdbtn_source = Stryk_Demo.radiobuttonuC_3;
							}
							else if(Stryk_Demo.rdbtnuC_4.isSelected())
							{
								current_rdbtn_source = Stryk_Demo.radiobuttonuC_4;
							}
							else if(Stryk_Demo.rdbtn_IrisExtWht.isSelected())
							{
								current_rdbtn_source = Stryk_Demo.radiobuttonIrisExtWht;
							}
							else if(Stryk_Demo.rdbtn_IrisExtEnv.isSelected())
							{
								current_rdbtn_source = Stryk_Demo.radiobuttonIrisExtEnv;
							}
							else
							{
								
							}
							Stryk_Demo.env_iris_source.clearSelection();
							if(Stryk_Demo.rdbtnEnv_Pwm.isSelected())
							{
								current_rdbtn_mode = Stryk_Demo.radiobuttonEnvPwm;
							}
							else if(Stryk_Demo.rdbtnEnv_Cwm.isSelected())
							{
								current_rdbtn_mode = Stryk_Demo.radiobuttonEnvCwm;
							}
							else if(Stryk_Demo.rdbtnEnv_CwmNr.isSelected())
							{
								current_rdbtn_mode = Stryk_Demo.radiobuttonEnvCwmNr;
							}
							else if(Stryk_Demo.rdbtnEnv_LcNr.isSelected())
							{
								current_rdbtn_mode = Stryk_Demo.radiobuttonEnvLcNr;
							}
							else
							{
								
							}
							Stryk_Demo.env_iris_mode.clearSelection();
							if(current_rdbtn_mode == Stryk_Demo.radiobuttonEnvPwm)
							{
								
								Stryk_Demo.rdbtnEnv_Pwm.setSelected(true);
							}
							else if(current_rdbtn_mode == Stryk_Demo.radiobuttonEnvCwm)
							{
								
								Stryk_Demo.rdbtnEnv_Cwm.setSelected(true);
							}
							else if(current_rdbtn_mode == Stryk_Demo.radiobuttonEnvCwmNr)
							{
								Stryk_Demo.rdbtnEnv_CwmNr.setEnabled(true);
								Stryk_Demo.rdbtnEnv_CwmNr.setSelected(true);
							}
							else if(current_rdbtn_mode == Stryk_Demo.radiobuttonEnvLcNr)
							{
								Stryk_Demo.rdbtnEnv_LcNr.setEnabled(true);
								Stryk_Demo.rdbtnEnv_LcNr.setSelected(true);
							}
							else
							{
								
							}
							if(current_rdbtn_source == Stryk_Demo.radiobuttonExtEnv)
							{
								Stryk_Demo.rdbtnEnv_Ext.setSelected(true);
							}
							else if(current_rdbtn_source == Stryk_Demo.radiobuttonuC_3)
							{
								Stryk_Demo.rdbtnuC_3.setSelected(true);
							}
							else if(current_rdbtn_source == Stryk_Demo.radiobuttonuC_4)
							{
								Stryk_Demo.rdbtnuC_4.setSelected(true);
							}
							else if(current_rdbtn_source == Stryk_Demo.radiobuttonIrisExtWht)
							{
								Stryk_Demo.rdbtn_IrisExtWht.setSelected(true);
							}
							else if(current_rdbtn_source == Stryk_Demo.radiobuttonIrisExtEnv)
							
							{
								Stryk_Demo.rdbtn_IrisExtEnv.setSelected(true);
							}
							else
							{
								
							}
							Stryk_Demo.rdbtnEnv_Pwm.setEnabled(true);
							Stryk_Demo.lblPwm_8.setEnabled(true);
							Stryk_Demo.lblCwm_1.setEnabled(true);
							Stryk_Demo.rdbtnEnv_Cwm.setEnabled(true);
							Stryk_Demo.lblEnv.setEnabled(false);
							Stryk_Demo.slider_19.setEnabled(false);
							Stryk_Demo.label_24.setEnabled(false);
							Stryk_Demo.chckbxIris1.setEnabled(false);
							Stryk_Demo.chckbxIris2.setEnabled(false);
							Stryk_Demo.chckbxIrisPowerShutdown_1.setEnabled(false);
							Stryk_Demo.slider_20.setEnabled(false);
							Stryk_Demo.label_25.setEnabled(false);
							Stryk_Demo.slider_21.setEnabled(false);
							Stryk_Demo.label_26.setEnabled(false);
							
							Stryk_Demo.textArea.append("IRIS Light Switched OFF\r\n");
						}
						else
						{
							System.out.println("HEYEEEEEE");
						}
						Stryk_Demo.EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
						//Stryk_Demo.EnvIrisPowerButton.setEnabled(false);
						//Stryk_Demo.chckbxEnv.setEnabled(true);
						//Stryk_Demo.chckbxIris.setEnabled(true);
					}
				}
			}
			else if(Cmd_Id.equals(Integer.toString(byt.INTENSITY_COMMAND)))
			{
				Stryk_Demo.textArea.append(Current_Control + "Command Processed Successfully!!\r\n\n");
			}
			else if(Cmd_Id.equals(INDIVIDUAL_CONTROL))
			{
				Stryk_Demo.connect_flag = true;
				Stryk_Demo.wl_onoff_flag = true;
				Stryk_Demo.chckbxRed.setSelected(true);
				Stryk_Demo.chckbxRed.setEnabled(true);
				Stryk_Demo.slider_16.setEnabled(true);
				Stryk_Demo.slider_16.setValue(Stryk_Demo.slider_15.getValue());
				Stryk_Demo.label_21.setEnabled(true);
				Stryk_Demo.label_21.setText(Integer.toString(Stryk_Demo.slider_16.getValue()) + " %");
				Stryk_Demo.connect_flag = false;
				Stryk_Demo.wl_onoff_flag = false;
			}
			else if(Cmd_Id.equals("TIMEOUT"))
			{
				if(Current_Control.equals(Connect))
				{
					if(serialport != null)
					{
						try
						{
							serialport.closePort();
							System.out.println("Serialport Closed inside Connect command Timeout!!");
						}
						catch(Exception ex)
						{
							System.out.println("Could not open Serial Port inside Connect Command Timeout!!");
							System.out.println("Leaving it Opened!!");
							ex.printStackTrace();
						}
						
					}
				}
				if(Current_Control.equals(WL_PON))
				{
					Stryk_Demo.connect_flag = true;
					Stryk_Demo.WlPowerButton.setSelected(false);
					Stryk_Demo.connect_flag = false;
				}
				else if(Current_Control.equals(WL_POFF))
				{
					Stryk_Demo.connect_flag = true;
					Stryk_Demo.WlPowerButton.setSelected(true);
					Stryk_Demo.connect_flag = false;
				}
				else if(Current_Control.equals(ENV_IRIS_ON))
				{
					Stryk_Demo.connect_flag = true;
					Stryk_Demo.EnvIrisPowerButton.setSelected(false);
					Stryk_Demo.connect_flag = false;
				}
				else if(Current_Control.equals(ENV_IRIS_OFF))
				{
					Stryk_Demo.connect_flag = true;
					Stryk_Demo.EnvIrisPowerButton.setSelected(true);
					Stryk_Demo.connect_flag = false;
				}
				else if(Current_Control.equals(ReadAll))
				{
					//Clear Color Sensor Field Results on Read All Timeout
					Stryk_Demo.textField_28.setText("");
					Stryk_Demo.textField_29.setText("");
					Stryk_Demo.textField_30.setText("");
					Stryk_Demo.textField_31.setText("");
					Stryk_Demo.textField_32.setText("");
					Stryk_Demo.textField_33.setText("");
					Stryk_Demo.textField_34.setText("");
					Stryk_Demo.textField_35.setText("");
					
					//Clear ADC Field Results on Read All Timeout
					Stryk_Demo.textField_8.setText("");
					Stryk_Demo.textField_9.setText("");
					Stryk_Demo.textField_10.setText("");
					Stryk_Demo.textField_11.setText("");
					Stryk_Demo.textField_12.setText("");
					Stryk_Demo.textField_13.setText("");
					Stryk_Demo.textField_14.setText("");
					Stryk_Demo.textField_15.setText("");
					Stryk_Demo.textField_16.setText("");
					Stryk_Demo.textField_17.setText("");
					Stryk_Demo.textField_18.setText("");
					Stryk_Demo.textField_20.setText("");
					
					//Clear IRIS and PD Field results on ReadAll Timeout
					Stryk_Demo.textField_21.setText("");
					Stryk_Demo.textField_22.setText("");
					Stryk_Demo.textField_23.setText("");
					Stryk_Demo.textField_24.setText("");
					Stryk_Demo.textField_25.setText("");
					Stryk_Demo.textField_26.setText("");
					Stryk_Demo.textField_27.setText("");
					publish("Reponse Timedout for sending ReadAll Command!!");
				}
				else if(Current_Control.equals(LIVE))
				{
					//synchronized(lockObject)
				//	{
				//		if(live_flag)
				//	//	{
					//		try {
					//			Thread.sleep(10);
					//		} catch (InterruptedException e) {
					//			// TODO Auto-generated catch block
					//			e.printStackTrace();
					//		}
					//		System.out.println("Releasing Live Flag inside Timeout");
					//		live_flag = false;
					//		lockObject.notifyAll();
					//	}
					//}
					synchronized(lockObject)
					{
						live_flag = false;
						lockObject.notify();
					}
					try {
						System.out.println("Sleeping after sending Live Command Live Command!!");
						Thread.sleep(100);
						
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
				{
					System.out.println("Had to process some other command inside Timeout publish!!");
				}
			}
			if(!Current_Control.equals(LIVE)&& !Current_Control.equals(ReadAll))
			{
				System.out.println("Forcing Pause Flag to be False here...");
				Stryk_Demo.pause_flag = false;
			}
			else if(Current_Control.equals(LIVE))
			{
				
				try {
					System.out.println("Sleeping after sending Live Command !!");
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		
			
			
		}
			else
			{
				Stryk_Demo.textArea.append(str);
			}
		}
		System.out.println("******************************************  END OF PROCESS ****************************************************************");
		//Stryk_Demo.pause_flag = false;
		
	}
	public void done()
	{
		try {
			String cc = get();
			System.out.println("String received inside done method : "  + cc);
			if(!cc.equals(LIVE))
			{
				System.out.println("Current Control Inside done Method : " + cc);
				if(!live_flag)
				{
					System.out.println("PF False");
					Stryk_Demo.pause_flag = false;
				}
				else
					
				{
					
				}
				
				
				
				
			}
			//Live command
			else
			{
				System.out.println("Releasing Already caught live_flag in Done Method");
				synchronized(lockObject)
				{
					if(live_flag)
					{
						try {
							Thread.sleep(10);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						live_flag = false;
						lockObject.notifyAll();
					}
				}
				System.out.println("PF True");
				
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//destuff_buffer = null;
		//Process done method stuff here.
		//Process_Done instead of 
		//if(Current_Control.equals(LIVE))
		//{
		//	synchronized(lockObject)
		//	{
		//		if(live_flag)
		//		{
		//			try {
		//				Thread.sleep(10);
		//			} catch (InterruptedException e) {
		//				// TODO Auto-generated catch block
		//				e.printStackTrace();
		//			}
		//			System.out.println("Releasing Live Flag inside Done");
		//			live_flag = false;
		//			lockObject.notifyAll();
		//		}
		//	}
		//	try {
		//		System.out.println("Sleeping after sending Live Command Live Command!!");
		//		Thread.sleep(100);
		//	} catch (InterruptedException e) {
		//		// TODO Auto-generated catch block
		//		e.printStackTrace();
		//	}
		//}
		bytedefinitions b = new bytedefinitions();
		if(Expected_Cmd_Id == b.EXPECT_NO_RESPONSE)
		{
			//Stryk_Demo.pause_flag = false;
			//set live flag after sending a command that expects no response.
			//This makes the timer to execute the actual task
		}
		//if(!Current_Control.equals(LIVE))
		//{
		//	Stryk_Demo.pause_flag = false;
		//}
		
		//Listener_Flag = false;
		//Stryk_Demo.pause_flag = false;
		System.out.println("Done Execution. Current Control : " + Current_Control);
		if (Stryk_Demo.Response_Animator.Animation.isShowing()) {
			System.out.println("Inside animation");
			Stryk_Demo.Response_Animator.Animation.setVisible(false);
		}
		System.out.println("******************************************  END OF DONE ****************************************************************");
		
	}
	
	//Timer related functions
	
	public static ArrayList<Integer> Get_Integer_From_ByteBuffer(byte[] data_buffer)
	{
		System.out.println("Count of CS : " + Integer.toString(data_buffer.length));
		ArrayList<Integer> Integer_buffer = new ArrayList<Integer>();
		int count = 0;
		byte[] local_buffer = new byte[2];
		for(int i = 0;i<data_buffer.length;i++)
		{
			local_buffer[count] = data_buffer[i];
			
			count = count + 1;
			if(count == 2)
			{
				ByteBuffer wrapped = ByteBuffer.wrap(local_buffer).order(ByteOrder.LITTLE_ENDIAN); // big-endian by default
				int cs_value = wrapped.getShort();
				
				count = 0;
				
				Integer_buffer.add(cs_value);
			}
			
		}
		return Integer_buffer;
		
	}
	public void initialize_on_timer(final byte[] SerialBytes, final byte Expected_Cmd_Id, final byte Expected_Msg_Type)
	{
		
		publish("SELECTED");
		Main_App_Worker.Expected_Cmd_Id = Expected_Cmd_Id;
		Main_App_Worker.Expected_Msg_Type = Expected_Msg_Type;
		Send_Cmd_and_wait_for_Timeout(Current_Control,SerialBytes, Listener_Flag);
		
		//live.schedule(Send_Cmd_and_wait_for_Timeout(Current_Control,SerialBytes, Listener_Flag),1000);
		//construct_and_send_IPCPacket(Current_control, IPCPacket,OFF,which_live_mode,b.SRC_WHITELIGHT,dummy_byte,dummy_byte,dummy_byte);	
		
	}
		
	
	
	public String Get_Slider_Name(Byte Slider_Value)
	{
		System.out.println("Byte : " + Slider_Value);
		bytedefinitions byt = new bytedefinitions();
		String Slider_Name = "";
		if(Slider_Value == byt.INTENSITY_SLIDER_RED)
		{
			Slider_Name =  "RED LED";
		}
		else if(Slider_Value == byt.INTENSITY_SLIDER_GREEN)
		{
			Slider_Name =  "GREEN LED";
		}
		else if(Slider_Value == byt.INTENSITY_SLIDER_BLUE)
		{
			Slider_Name =  "BLUE LED";
		}
		else if(Slider_Value == byt.INTENSITY_SLIDER_WHITE)
		{
			Slider_Name =  "WHITE LED";
		}
		else if(Slider_Value == byt.INTENSITY_SLIDER_ENV)
		{
			Slider_Name =  "LASER LED";
		}
		else if(Slider_Value == byt.INTENSITY_SLIDER_IRIS1)
		{
			Slider_Name =  "IRIS1 LED";
		}
		else if(Slider_Value == byt.INTENSITY_SLIDER_IRIS2)
		{
			Slider_Name =  "IRIS2 LED";
		}
		else if(Slider_Value == byt.INTENSITY_PWR_FAN)
		{
			Slider_Name =  "LIGHT ENGINE FAN CONTROL";
		}
		else if(Slider_Value == byt.INTENSITY_HSINK_FAN)
		{
			Slider_Name =  "HEAT SINK FAN CONTROL";
		}
		else if(Slider_Value == byt.INTENSITY_IRIS_FAN)
		{
			Slider_Name =  "IRIS FAN CONTROL";
		}
		
		return Slider_Name;
		
	}
	//#########################################################################################################################
	public void Send_Cmd_and_wait_for_Timeout(String Current_Control,byte[] SerialBytes, boolean Listener_Flag)
	{
		System.out.println("Listener2Flag : " + Listener_Flag);
	try
	{
		if(serialport != null)
		{
			System.out.println("Inside Sending Command and receive response Function!!");
		  //Check whether the serialport is already closed. If closed,
		 //Open the port
			System.out.println(LocalDateTime.now());
			serialport.openPort();
			System.out.println(LocalDateTime.now());
			System.out.println("Serial Port is : " + serialport.isOpen());
			if(serialport.isOpen() == true)
			{
				
				byte[] temp = null ;
				int bytes_available = serialport.bytesAvailable();
				
				System.out.println("Total Bytes available at the serial buffer before sending command : " + bytes_available);
				if(bytes_available > 0)
				{
					temp = new byte[bytes_available];
					int d = serialport.readBytes(temp, bytes_available);
				}
			    try {
			    	full_buffer.reset();
					full_buffer.flush();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
		    	
		    	if(Listener_Flag)
		    	{
		    		
		    		System.out.println("Data Listener Flag : " + Listener_Flag + "Current_Command : " + Current_Control);
		    		setlistener(serialport);
		    		System.out.println("Data Listener Set!!");
		    	}
		    	else
		    	{
		    		
		    		System.out.println("Data Listener Flag : " + Listener_Flag);
		    		serialport.removeDataListener();
		    		System.out.println("Data Listener Removed!!");
		    	}
		    	
		    	
		    	serialport.writeBytes(SerialBytes, SerialBytes.length);
		    	System.out.println("Entry Time : " + LocalDateTime.now());
		    	String helloHex = DatatypeConverter.printHexBinary(SerialBytes);
				//System.out.println("Hessl : " + helloHex);
		    	System.out.println("Serial Data Sent : " + helloHex);
		    	System.out.println("Written to serial port");
		    	System.out.println(LocalDateTime.now());
		    	Write_flag  = true;
		    	//ManufacturingTests.btnStartButton.setName("PAUSE");
		    	publish(Current_Control + " Command Sent\r\n");
		    	if(Write_flag )
			    {
		    		//Wait for response if Listener_Flag is set for current IPC Command
		    		if(Listener_Flag)
		    		{
		    			publish("Waiting For Response...\r\n");
		    			if(!Current_Control.equals("LIVE"))
		    			{
		    				Stryk_Demo.pause_flag = true;
		    				System.out.println("PAUSE FLAG SET!!");
		    				Stryk_Demo.Response_Animator.Animation.setVisible(true);
		    			}
		    			
			    		System.out.println("Current_Control : " + Current_Control);
			    		int status = 0;
			    		if(live_flag && !Stryk_Demo.pause_flag)
			    		{
			    			System.out.println("######################################################");
			    			long tout = 500;
			    			status = Send_cmd_wait_for_Timeout(tout, Current_Control);
			    		}
			    		else
			    		{
			    			System.out.println("Live Flag : "+live_flag);
			    			System.out.println("Pause_Flag : " +Stryk_Demo.pause_flag);
			    			//This was the fix for the currently running live command Timer.
			    			//Previuosly while giving Live Command, the process of sending LIVE command was stopped
			    			//But the timer that was waiting for the response of lat command sent, wasn't stopped
			    			//This also changed the new timeout of 10000ms to the LIVE command's 500ms timeout
			    			if(!Stryk_Demo.pause_flag && !Current_Control.equals(Connect))
			    			{
			    				if(futuree!= null)
				    			{
			    					System.out.println("#######################FUTUREEE------------->>>>>>>>>>");
				    				futuree.cancel(true);
				    			}
				    			else
				    			{
				    				
				    			}
			    			}
			    			
			    			if(Current_Control.equals(ReadAll))
			    			{
			    				status = Send_cmd_wait_for_Timeout(5000, Current_Control);
			    			}
			    			else
			    			{
			    				status = Send_cmd_wait_for_Timeout(serial_read_cmd_timeout, Current_Control);
			    			}
			    			System.out.println("###################" + serial_read_cmd_timeout + "#####################");
			    			
			    		}
			    		
		    			//int status = 0;
			    		

				    	if(status == 1)
				    	{
				    		//JOptionPane.showMessageDialog(ManufacturingTests.frmStrykerManufacturingTests, "Reponse Timedout for sending " + Query_String +  "Command", "ManufacturingTests", JOptionPane.ERROR_MESSAGE);
				    		System.out.println("Response Timedout for sending " + Current_Control  +  " Command" + LocalDateTime.now());	
				    		//Stryk_Demo.pause_flag = false;
				    	}
				    	else
				    	{
				    		//JOptionPane.showMessageDialog(ManufacturingTests.frmStrykerManufacturingTests, "Status : " + status, "Status", JOptionPane.INFORMATION_MESSAGE);
				    		System.out.println("Status : " + status);
				    	}
		    		}
		    		else
		    		{
		    			if(future != null)
			    		{
			    			future.cancel(true);
			    		}
			    		if(futuree != null)
			    		{
			    			futuree.cancel(true);
			    		}
		    			//Stryk_Demo.pause_flag = false;
		    		}
		    		//else don't wait for a response or timeout. Just send the command
			    }
		    	 	
			} 
			else 
			{
				publish(SP_OPEN_ERROR);
				System.out.println("Could not open Serial port. Exiting Now!!");
		    	publish("Could not send " + Current_Control + " command \r\n");
		    	publish("Access to COM Port Denied!!. Port is in Access by Another Application!!\r\n\n");
		    	
				
			}
		}
    	else
    	{
    		System.out.println("Serial Port is Null");
    		publish(INVALID_PORT);
    		
    	}
	}catch(Exception ex)
	{
		ex.printStackTrace();
	}
	
	}
	//####################################Function to get the stuffed-complete IPC Frame to be sent to the device#################
	public static List<Byte> Stuff_Packet(List<Byte> buffer_to_send)
	{
		bytedefinitions b = new bytedefinitions();
		List<Byte> Stuffed_List = new ArrayList<Byte>();
		for(int i = 0;i<buffer_to_send.size();i++)
		{
			
			if(buffer_to_send.get(i) == b.START_byte || buffer_to_send.get(i) ==b.END_byte || buffer_to_send.get(i) ==b.ESC_byte)
			{
				Stuffed_List.add(b.ESC_byte);
			}
			Stuffed_List.add(buffer_to_send.get(i));
		}
		return Stuffed_List;
		
	}
	
	//####################################Function to set Incoming Data Listener for the serial Port##############################
	
	public void setlistener(final SerialPort serialport) 
	{
		System.out.println("Inside Serial Port Listener Set Function!!");
		if (serialport != null) 
		{
			serialport.addDataListener(new SerialPortDataListener() 
			{
				private int total_bytes;
				private int numRead;

				@Override
				public int getListeningEvents() 
				{
					System.out.println("Getting the list of events");
					return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
				}

				@Override
				public void serialEvent(SerialPortEvent event) 
				{
					if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE) 
					{
						Stryk_Demo.textArea.append("No response\r\n\n");
						return;
					}

					try 
					{
						//System.out.println("AA");
						byte[] newData = new byte[serialport.bytesAvailable()];
						//	System.out.println("A");
						numRead = serialport.readBytes(newData, newData.length);
						//System.out.println("B");
						total_bytes = total_bytes + numRead;
						full_buffer.write(newData, 0, numRead);
						System.out.println("Some data arrived in the Main Worker instance of the serial port");
						if(future!= null)
						{
							future.cancel(true);
						}

						future = scheduler.schedule(uart_recv_timeout_task, serial_read_byte_timeout,TimeUnit.MILLISECONDS);



						// 	#uart_response_timer.start();
					}
					catch (Exception ex)
					{
						ex.printStackTrace();
						return;
					}

				}

			});

		} 
		else 
		{
			System.out.println("No Serial ports found in the system");
		}
	}
	
	
	//####################################Uart_Recv_Timeout#######################################################
	Runnable uart_recv_timeout_task = new Runnable() {
		public void run()
		{
			System.out.println("Response Time Inside Receive Timeout : " + LocalDateTime.now());
			System.out.println("Inside UART Receive Timeout Task");
			//If No data has been collected in the full_buffer
			if (full_buffer.size() == 0) //Previously it was full buffer == null check
			
			{
				
				System.out.println("No data in the response!!");
				
				ManufacturingTests.busyflag = false;
				serialport.removeDataListener(); // Removes data listener associated with serial port Object
				totalbytes = 0;
				
			} 
			//Some datahas been collected in the full_buffer
			else 
			{
				
				
				//print("Some Response Atleast!!");
				//print(Arrays.toString(full_buffer.toByteArray()));
				System.out.println("Full Buffer size before calling function :" + full_buffer.size());
				byte[] recv_buff = full_buffer.toByteArray(); //Copy the received bytes into a new byte array
				try
				{
					String helloHex = DatatypeConverter.printHexBinary(recv_buff);
					System.out.println("Hessl : " + helloHex);
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				
				try {
					full_buffer.flush();//Flush the receive baos after getting the received values to a new buffer
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Receive Buffer size before calling function :" + recv_buff.length );
				
				serialport.removeDataListener(); //Remove the data listener after successfully receiving some response
				String result = "";
				try {
					//Result OK / Fail processed here with respect to Calibration INIT Command
					result = new String(recv_buff,"UTF-8");
					System.out.println("Response to be processed : " + result);
					if(result.startsWith(INIT_OK))
					{
						Stryk_Demo.serialport.removeDataListener();
						Stryk_Demo.serialport.closePort();
						//System.out.println("Hi");
						Write_flag = false;
						Notify_Flag = 1;
						Null_Charflag = true;
						Stryk_Demo.frmStrykerVer.setVisible(false);
						Calibration window = new Calibration();
					}
					else if(result.startsWith(INIT_FAIL))
					{
						Stryk_Demo.serialport.removeDataListener();
						Stryk_Demo.serialport.closePort();
						Write_flag = false;
						Notify_Flag = 1;
						Null_Charflag = false;
						Stryk_Demo.textArea.append("INIT FAILED\r\n");
						System.out.println("Error : Could not Switch to Calibration screen");
						JOptionPane.showMessageDialog(Stryk_Demo.frmStrykerVer, "Error Response Received for sending Init Command", "Calibration - Initialization", JOptionPane.ERROR_MESSAGE);
					}
					else
					{
						//Below check has to be validated only if the calibration_Flag is true
						//If the calibration flag is false, it means that the command is something which is one of the commands in the main app
						if(Stryk_Demo.Calibration_Flag)
						{
							Stryk_Demo.Calibration_Flag = false;
							Notify_Flag = 1;
							Null_Charflag = false;
							Write_flag = false;
							Stryk_Demo.serialport.removeDataListener();
							Stryk_Demo.serialport.closePort();
							Stryk_Demo.textArea.append("Unexpected Response Received!!\r\n\n");
							JOptionPane.showMessageDialog(Stryk_Demo.frmStrykerVer, "Unexpected Response Received!!", "CALIBRATION", JOptionPane.ERROR_MESSAGE);
						}
						else
						{
							//System.out.println("Response to be processed : " + result);
							System.out.println("Inside Main Java App response processing");
							System.out.println("Inside the function");
							
							
							
							//System.out.println("Trigger Source : " + current_control + " Command");
							System.out.println("Buffer at Entry Point : "+Arrays.toString(recv_buff));
							
							int Is_Error = 0;
							Is_Error = return_error_type(recv_buff);
							String helloHe = DatatypeConverter.printHexBinary(destuff_buffer);
							System.out.println("Hessl : " + helloHe);
							//System.out.println("Buffer After Check : " + );
							System.out.println("ERROR CODE : " + Is_Error);
							for(int i=0;i<recv_buff.length;i++) {recv_buff[i] = 0;}
							if (Is_Error == 0) 
							{
								
								//Stryk_Demo.pause_flag = false;
								//System.out.println(Stryk_Demo.pause_flag);
								synchronized(lockObject)
								{
									Null_Charflag = true;
									
									Write_flag = false;
									Notify_Flag = 1;
									lockObject.notifyAll();
								}
									
								if(serialport != null)
								{
									serialport.removeDataListener();
									//##print("Data Listener Removed!!");
								}
								    
							} 
							else if(Is_Error < 9)
							{
								//Stryk_Demo.pause_flag = false;
								String err = return_error_name(Is_Error);
								System.out.print("Error is : ");
								System.out.println(Is_Error);
								System.out.println(" , " + err);
								synchronized(lockObject)
								{
									Write_flag = false;
									Notify_Flag = 1;
									Null_Charflag = false;
									lockObject.notifyAll();
								}
								
								full_buffer.reset();
								serialport.removeDataListener();
								System.out.println("Packet Received with some Error!!");
									 
							}
							else if(Is_Error == 9)
							{
								//Stryk_Demo.pause_flag = false;
								//Response Timedout
								synchronized(lockObject)
								{
									
									Notify_Flag = 1;
									Null_Charflag = false;
									Write_flag = false;
									lockObject.notifyAll();
								}
							
								serialport.removeDataListener();
								full_buffer.reset();

							}
							else
							{
									
							}
							
							//recv_buff = null;
							//serialport.removeDataListener(); // Removes data listener associated with serial port Object
							totalbytes = 0;
						}
						
						
					}
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
		}
		}
	};
		
	//#################################################################################################################################
	/*
	 * Function Name : return_error name : A Byte representing an error_code : A String
	 * representing one of the error types in the error Struct 
	 * Description : Returns a string corresponding to the given error_code(input) 
	 *  in the given input buffer(received packet)
	 * 
	 */
	//#################################################################################################################################
		public static String return_error_name(int error_code)
		{
			System.out.print("Error Code is : ");
			System.out.print(error_code);
			String return_val = "";
			if(error_code == RespError_Flag.Response_OK.value) 						{return_val = "RESPONSE_OK";}
			else if(error_code == RespError_Flag.Unknown_Cmd_Error.value) 			{return_val = "UNKNOWN_COMMAND_ERROR";}
			else if(error_code == RespError_Flag.Unknown_MsgType_Error.value) 		{return_val = "UNKNOWN_MESSAGE_TYPE_ERROR";}
			else if(error_code == RespError_Flag.Unknown_State_Error.value) 		{return_val = "UNKNOWN_STATE_ERROR";}
			else if(error_code == RespError_Flag.Payload_Length_Error.value) 		{return_val = "PAYLOAD_LENGTH_ERROR";}
			else if(error_code == RespError_Flag.IPCMutex_Timeout_Error.value) 		{return_val = "IPCMUTEX_TIMEOUT_ERROR";}
			else if(error_code == RespError_Flag.Timeout_Error.value) 				{return_val = "TIMEOUT_ERROR";}
			else if(error_code == RespError_Flag.IPC_Error.value) 					{return_val = "IPC_ERROR";}
			else if(error_code == RespError_Flag.Cmd_Execution_Error.value) 		{return_val = "COMMAND_EXECUTION_ERROR";}
			else if(error_code == RespError_Flag.ResponseType_MismatchError.value) 	{return_val = "RESPONSE_TYPE_MISMATCH_ERROR";}
			else if(error_code == RespError_Flag.CommandMode_MismatchError.value) 	{return_val = "COMMAND_MODE_MISMATCH_ERROR";}
			else if(error_code == RespError_Flag.InvalidCommand_ModeError.value) 	{return_val = "INVALID_COMMAND_MODE_ERROR";}
		System.out.println("Value to be returned : ");
		System.out.println(return_val);
		return return_val;
		
		}
		
		//#####################################################################################################

		// Function that checks for framing error
		/*
		 * Function Name : check_framing_error Input : A Byte Array Output : An Integer
		 * representing one of the error types in the error Struct Description : checks
		 * for Framing-Error in the given input buffer(received packet)
		 * 
		 */
		//######################################################################################################
			public static int check_framing_error(byte[] rec_buff) 
			{
				
				boolean start_end_byteflag = false;
				bytedefinitions reference = new bytedefinitions();
				int end_index = rec_buff.length - 1;
				for(int i=0;i<rec_buff.length;i++)
				{
					if(rec_buff[i] == reference.START_byte)
					{
						reference_index = i;
						start_end_byteflag = false;
						break;
						
					}
					else
					{
						start_end_byteflag = true;
					}
				}
				if(start_end_byteflag == false)
				{
					if(rec_buff[end_index] == reference.END_byte)
					{
						start_end_byteflag = false;
					}
					else
					{
						start_end_byteflag = true;
					}
				}
				// #System.out.println("Framing Buffer : " + Arrays.toString(recv_buff));
				if (start_end_byteflag == false) 
				{
					// #System.out.println("No Framing Error in the received Packet");
					resp_error_type = RespError_Flag.Response_OK.value;
				}
				
			
			return resp_error_type;

		}
	//####################################################################################################
	/*
	 * Function Name : return_error_type Input : A Byte Array Output : An Integer
	 * representing one of the error types in the error Struct Description : checks
	 * the given buffer for the occurrence of one of the errors in the given input
	 * buffer(received packet)
	 * 
	 */
	//####################################################################################################
	/*Order of Error Check in the Received Packet
	 * 1. Framing Error
	 * 2. Stuffing Error
	 * 3. CheckSum Error
	 * 4. Message Type Error
	 * 5. Command Type Error
	 * 6. Response OK or Error in Response
	 * 
	 */
	public int return_error_type(byte[] recv_buff) 
	{
		try
		{
			int end_index = recv_buff.length - 1;
			
			
			
			// #################################Check Framing Error###########################################
			resp_error_type = RespError_Flag.Framing_Error.value;
			@SuppressWarnings("unused")
			bytedefinitions reference = new bytedefinitions();
			
			resp_error_type = check_framing_error(recv_buff);
			if (resp_error_type != RespError_Flag.Response_OK.value) {
				System.out.println("Framing Error in the received packet. Cannot Process it Further!!");
				System.out.println("RecvBuff : "+Arrays.toString(recv_buff));
				return resp_error_type;
			}

			// #################################Check Stuffing Error##########################################
			resp_error_type = RespError_Flag.Stuffing_Error.value;
			// strip off the start-byte and stop-byte from the original buffer
			destuff_buffer = Arrays.copyOfRange(recv_buff, reference_index+1, end_index);
			destuff_buffer = check_destuff_error(destuff_buffer);
			//print("Destuff_buffer : "+Arrays.toString(destuff_buffer));
			if (resp_error_type != RespError_Flag.Response_OK.value) {
				System.out.println("De-Stuffing-Error in the received packet. Cannot Process it Further!!");
				return resp_error_type;
			} else {
				// #System.out.println("No De-Stuffing Error in the received packet!!");
			}
			
			//##################################Check CheckSum Error##########################################
			resp_error_type = RespError_Flag.Checksum_Error.value;
			
			//for(int i=0;i<destuff_buffer.length;i++)
			//{
			//	System.out.print(Hex(destuff_buffer[i],2) + ",");
			//}
			resp_error_type = check_checksum_error(destuff_buffer);
			if (resp_error_type == RespError_Flag.Checksum_Error.value) 
			{
				System.out.println("CheckSum Error in the received Buffer. Cannot Process it Further!!");
				
				return resp_error_type;
			}


			//#################################Check Message Type##############################################
			resp_error_type = RespError_Flag.Unknown_MsgType_Error.value;
			
			if (destuff_buffer[0] == Expected_Msg_Type) 
			{
				resp_error_type = RespError_Flag.Response_OK.value;
				
			}
			
			else
			{
				System.out.println("Response Type Mismatch Error");
				System.out.println("Expected Resp type is : " + Expected_Msg_Type);
				return resp_error_type;
			}
			
			
			//#################################Check for Command Id Match######################################
			resp_error_type = RespError_Flag.Unknown_Cmd_Error.value;
			if(destuff_buffer[1] == Expected_Cmd_Id)
			{
				resp_error_type = RespError_Flag.Response_OK.value;
			}
			else
			{
				System.out.println("Command ID Mismatch in Received Packet!!");
				
				if(Stryk_Demo.WlPowerButton.isSelected())
				{
					Stryk_Demo.connect_flag = true;
					Stryk_Demo.WlPowerButton.setSelected(false);
					Stryk_Demo.connect_flag = false;
				}
				else if(!Stryk_Demo.WlPowerButton.isSelected())
				{
					Stryk_Demo.connect_flag = true;
					Stryk_Demo.WlPowerButton.setSelected(true);
					Stryk_Demo.connect_flag = false;
				}
				else
				{
					
				}
				
				if(Stryk_Demo.EnvIrisPowerButton.isSelected())
				{
					Stryk_Demo.connect_flag = true;
					Stryk_Demo.EnvIrisPowerButton.setSelected(false);
					Stryk_Demo.connect_flag = false;
				}
				else if(!Stryk_Demo.EnvIrisPowerButton.isSelected())
				{
					Stryk_Demo.connect_flag = true;
					Stryk_Demo.EnvIrisPowerButton.setSelected(true);
					Stryk_Demo.connect_flag = false;
				}
				else
				{
					
				}
				
				
				JOptionPane.showMessageDialog(Stryk_Demo.frmStrykerVer, "Command ID Mismatch in the received Packet : " + Expected_Cmd_Id, "Command ID", JOptionPane.ERROR_MESSAGE);
				
				return resp_error_type;
			}
			
			
		    //#################################Check Response_Code - Success or Error Response#####################
			
			resp_error_type = check_response_code(destuff_buffer);
			if(resp_error_type == 0x0) 
			{
				//print("Error Free Response Message or Notification Message Received!!");
			}
			else
			{
				
				System.out.println("Response Message Received with Error Response!!");
				return resp_error_type;
			}
			bytedefinitions b = new bytedefinitions();
			if(Expected_Cmd_Id != b.SENSING_COMMAND && Expected_Cmd_Id != b.RESTORE_CMD && Expected_Cmd_Id != b.LIVE_WIDTH_COMMAND)
			{
				for(int i=0;i<destuff_buffer.length;i++) {destuff_buffer[i] = 0;}
			}
			System.out.println("Actual Code : " + resp_error_type);
			System.out.println("Sending Back Response...");
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
			Stryk_Demo.pause_flag = false;
		}
		return Error_Message;
		
		
	}
	//#####################################################################################################

	/*
	 * Function Name : check_response_code Input : A Byte Array Output : An Integer
	 * representing one of the error types in the error Struct Description : checks
	 * for error in response_code in the given input buffer(received packet)
	 * 
	 */
	//######################################################################################################
		public int check_response_code(byte[]  recv_buff)
		{
			byte result = 0x0;
			bytedefinitions b = new bytedefinitions();
			//Check if it is a response message
			if(recv_buff[0] == b.MSG_TYPE_RESP) 
			{
				//#print("Response Message");
				if(recv_buff[7] == RespError_Flag.Response_OK.value) //Pay-load is the actual response
				{
					//##print("No Error in the Response Packet");
				}
				else
				{
					//Check for error response if and only if the received command id is not one of below
					if(Expected_Cmd_Id != b.SENSING_COMMAND && Expected_Cmd_Id != b.RESTORE_CMD && Expected_Cmd_Id != b.LIVE_WIDTH_COMMAND) 
					{
						byte Error_Code = recv_buff[7];
						byte Error_Source = recv_buff[8];
						String Error_Src = "";
						
						
						if(Error_Source == b.SRC_JAVA_APP) {Error_Src = "Java App";}
						else if(Error_Source == b.SRC_uC1) {Error_Src = "uC1";}
						else if(Error_Source == b.SRC_uC2) {Error_Src = "uC2";}
						else if(Error_Source == b.SRC_CONTROLLER1_RESP) {Error_Src = "uC1-Response";}
						else {}
						
						
						String Error_Name = "";
						Error_Name = return_error_name(Error_Code);
						if(Error_Name == "RESPONSE_OK") 
						{
							
							Stryk_Demo.ShowMessage("Error Code : " + Integer.toString(Error_Code) + " Error Description : RESPONSE_OK"); result = 0x0;
							return result;
						}
						else 
						{
							switch_flag = true;
							Stryk_Demo.ShowMessage("Error Code : " + Integer.toString(Error_Code) + " Error Description : " + Error_Name + " Source : " + Error_Src); result = 0x1;
							if(Stryk_Demo.WlPowerButton.isSelected())
							{
								publish("WL_ON");
								
								Stryk_Demo.WlPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/on.jpg")));
							}
							else if(!Stryk_Demo.WlPowerButton.isSelected())
							{
								publish("WL_OFF");
								Stryk_Demo.WlPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
							}
							else if(Stryk_Demo.EnvIrisPowerButton.isSelected())
							{
								publish("ENV_IRIS_ON");
								Stryk_Demo.EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/on.jpg")));
							}
							else if(!Stryk_Demo.EnvIrisPowerButton.isSelected())
							{
								publish("ENV_IRIS_OFF");
								Stryk_Demo.EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
							}
							return result;
							
						}
						
					}
					
				}
			
			}//If it is a notification message, do nothing
			else if(recv_buff[0] == b.MSG_TYPE_NOTIFY)
			{
				
				System.out.println("Notofication Message");
			}
			Stryk_Demo.pause_flag = false;
		
		//Return the value
		return result;
		
	}

	//#####################################################################################################
	/*
	 * Function Name : check_checksum_error Input : A Byte Array Output : An Integer
	 * representing one of the error types in the error Struct Description : checks
	 * for checksum_error in the given input buffer(received packet)
	 * 
	 */
	//######################################################################################################
		public static int check_checksum_error(byte[] recv_buff) 
		{
			System.out.println("Buffer Received for checksum : " + Arrays.toString(recv_buff));

		    // Parse the received buffer to concatenate two bytes into a short int. This is required as checksum should be 
			//a 16 bit (Integer) so as to satisfy the checksum addition , thus the result to be zero
			ByteBuffer bb = ByteBuffer.allocate(4);
			//Little Endian
			bb.order(ByteOrder.LITTLE_ENDIAN);
			//Place the LSB and MSB of Checksum bytes into the buffer
			bb.put(recv_buff[recv_buff.length - 2]);
			bb.put(recv_buff[recv_buff.length - 1]);
			int shortVal = bb.getInt(0);
			
			
			// Replace the checksum bytes with 0x0
			recv_buff[recv_buff.length - 2] = 0x0;
			recv_buff[recv_buff.length - 1] = 0x0;
			
			int temp = 0;
			int temp_1 = 0;
			
			//Add all the bytes in the received buffer except checksum
			for (int i = 0; i < recv_buff.length; i++) 
			{
				//A Fix for adding all the bytes in the 
				temp_1 = (int) recv_buff[i]& 0xff;
				temp = temp +  temp_1;
			}
			
			//Add the result with checksum
			temp = temp + shortVal;
			
			//Check if the result is 0
			if((temp & 0xffff) == 0)
			{
				//Return Response OK - if 0
				resp_error_type = RespError_Flag.Response_OK.value;// #System.out.println("No Checksum Error in the received // Packet!!");
			} 
			else 
			{
				//Return CheckSum Error
				resp_error_type = RespError_Flag.Checksum_Error.value;
			}

		return resp_error_type;

	}
	//#####################################################################################################
	// Function that checks for destuffing_error
	/*
	 * Function Name : check_destuff_error Input : A Byte Array Output : An Integer
	 * representing one of the error types in the error Struct Description : checks
	 * for de-stuffing error in the given input buffer(received packet)
	 * 
	 */
	//######################################################################################################
		public void get_destuffed_buffer()
		{
			
		}
		public static byte[] check_destuff_error(byte[] recv_buff) 
		{
			ArrayList<Byte> copy_buffer = new ArrayList<Byte> ();
			
			
			
			//print("Recv_buff Inside destuff function : " +Arrays.toString(recv_buff));
			
			boolean Escape_Character_Flag = false;
			bytedefinitions b = new bytedefinitions();
			// #System.out.println("Destuff Buffer : " + Arrays.toString(recv_buff));
			for (int i = 0; i < recv_buff.length; i++) 
			{
				//print("Iterating through for loop");
				// if 'i' is not the end index and element at 'i' th position is '0xEE',check
				// next byte for 0x55,0xAA,0xEE
				
				
					if(recv_buff[i] == b.ESC_byte && Escape_Character_Flag == false)
					{
						
						//print("ESC Char Received");
						Escape_Character_Flag = true;
					}
					else
					{
						if(recv_buff[i] == b.ESC_byte && Escape_Character_Flag == true)
						{
							copy_buffer.add(recv_buff[i]);
							
							
							resp_error_type = RespError_Flag.Response_OK.value;
						}
						else if(recv_buff[i] != b.START_byte && recv_buff[i] != b.END_byte && Escape_Character_Flag == true)
						{
							//print("Returning Error");
							resp_error_type = RespError_Flag.Stuffing_Error.value;
							byte[] data = new byte[copy_buffer.size()];
							for (int k = 0; k < data.length; k++) {
							    data[k] = (byte) copy_buffer.get(k);
							}
							return data;
						}
						else
						{
							//print("ESC Char is not present!!");
							copy_buffer.add(recv_buff[i]);
							
						}
						
						resp_error_type = RespError_Flag.Response_OK.value;
						Escape_Character_Flag = false;
					}
					
					
				}
				
				
				
			
			//print("Inside destuffing function : " +Arrays.toString(recv_buff));
			// Return the error type
			byte[] data = new byte[copy_buffer.size()];
			for (int k = 0; k < data.length; k++) {
			    data[k] = (byte) copy_buffer.get(k);
			}
			return data;
		

	}
	//#########################################Function to calculate CRC for current Frame###############################################
	public int calculateCRC(List<Byte> list)
	{
		int temp = 0;
		int check_value = 0x0;
		for (int i=0;i<list.size();i++)
		{
			
			temp += (list.get(i));
		}
		
		//##System.out.println("Total Sum is : " + Integer.toHexString(temp));
		//##System.out.println(temp);
		check_value = ~temp; //1's complement
		check_value = check_value+1; //2's complement
		
		//##System.out.print("Checksum is : ");
		//##System.out.println(String.format("0x%04X", check_value));
		return check_value;
	
	}
	
	
	//###################################Function to COnvert the contents of Arraylist to bytearray#######################################
	public static byte[] al_to_byte(ArrayList<Byte> al) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(baos);
		for (Byte element : al) {
			try {
			// System.out.println("Element : " + element);
				out.write(element);
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
		byte[] bytes = baos.toByteArray();
		try {
			out.flush();
			baos.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bytes;
	}
	
	//#####################################################################################################################################
	//Function to send the given IPC Frame to the device and to wait/not wait for the response to be received or timeout to happen 
	public int Send_cmd_wait_for_Timeout(long timeout, final String Current_Control)
	{
		long tout = timeout;
		
		final Runnable timeout_task_performer = new Runnable()
    	{
			@Override
			public void run() 
			{
				synchronized(lockObject)
				{	
					Timeout_Flag = 1;
					lockObject.notifyAll();
				}
				
			}
			
		};
		final Runnable update_GUI_state = new Runnable() 
		{
			@Override
			public void run() 
			{
				System.out.println("Waiting here for response or timeout here..");
				System.out.println("Pause Flag : " + Stryk_Demo.pause_flag);
				synchronized(lockObject)
				{
					
					while (Timeout_Flag == 0 && Notify_Flag == 0 )
					{
						//System.out.println("Waiting Here for Timeout or Notify Inside : " + Current_Control);
						//if(!Current_Control.equals(LIVE))
						//{
							//Stryk_Demo.pause_flag = true;
							//System.out.println("Waiting Inside RUN");
					//	}
						
						try 
						{
							lockObject.wait(10);
						} 
						catch (Exception e)
						{
							e.printStackTrace();
						}
						

					}
					if(Stryk_Demo.pause_flag)
					{
						Stryk_Demo.pause_flag = false;
					}
					Stryk_Demo.Response_Animator.Animation.setVisible(false);
					Stryk_Demo.print("exited while LooP");
					System.out.println("Timeout Flag : " + Timeout_Flag);
					System.out.println("Notify_Flag : " + Notify_Flag);
					System.out.println("Null_Charflag : " + Null_Charflag);
					if(futuree != null)
					{
						System.out.println("Futuree not null!!");
						futuree.cancel(true);
					}
					else
					{
						System.out.println("Futuree null!!");
					}
					
				}
				// While ends here
				System.out.println("Here1");
				//No Data Received for sending command to device
				if (Timeout_Flag == 1 && Notify_Flag == 0) 
				{
					
					publish("Response Received : Response Timedout\r\n\n");
					publish("TIMEOUT," + Current_Control);
					serialport.openPort();
					if(serialport.isOpen() == true)
					{
						byte[] temp = null;
						int bytes_available = serialport.bytesAvailable();
						System.out.println("Total Bytes available at timeout : " + bytes_available);
						if(bytes_available > 0)
						{
							temp = new byte[bytes_available];
							serialport.readBytes(temp, bytes_available);
						}
						serialport.removeDataListener();
						//serialport.closePort();
						
						
					}
					
					
					System.out.println("Response Timedout. No data received in the Serial Port");
					synchronized(lockObject)
					{
						
						Write_flag = false;
						lockObject.notifyAll();
					}
					if(Stryk_Demo.pause_flag)
					{
						//Stryk_Demo.pause_flag = false;
					}
					
						
					
					//Update UI here
					
					
				
					
					
				}
				//Valid Response received for sending command to the device
				else if (Notify_Flag == 1 && Null_Charflag==true) 
				{
					
					System.out.println("Error Free Data Received!!");
					Notify_Flag = 0;
					
					@SuppressWarnings("unused")
					bytedefinitions b = new bytedefinitions();
					System.out.println("Valid Response in UART");
					if(Expected_Cmd_Id == b.ENADIS_COMMAND)
					{
						publish(Current_Control + "Command Processed Successfully!!\r\n\n");
					}
					
					publish(Integer.toString(Expected_Cmd_Id) + ","+Current_Control);
					System.out.println("Current Control Inside Success Response : " + Current_Control);
					serialport.removeDataListener();
					
					//serialport.closePort();
					synchronized(lockObject)
					{
						Write_flag = false;
						lockObject.notifyAll();
					}
				} 
				//Response Received. But with some errors
				else if(Notify_Flag == 1 && Null_Charflag == false)
				{
					//Stryk_Demo.pause_flag = false;
					Notify_Flag = 0;
					Timeout_Flag = 0;
					
					try {
						full_buffer.reset();
						full_buffer.flush();
						// #scheduler.shutdownNow();
						//if(Current_Control == Connect)
						//{
						//	publish("ENABLE_CONNECT_BUTTON");
						//	btnConnect.setEnabled(true);
						//}
						//else if(Current_control == Disconnect)
						//{
						//	btnDisconnect.setEnabled(true);
						//}
						if(Current_Control.equals(WL_PON))
						{
							Stryk_Demo.wl_critical_flag = true;
							Stryk_Demo.WlPowerButton.setSelected(false);
							Stryk_Demo.wl_critical_flag = false;
						}
						else if(Current_Control.equals(WL_POFF))
						{
							Stryk_Demo.wl_critical_flag = true;
							Stryk_Demo.WlPowerButton.setSelected(true);
							Stryk_Demo.wl_critical_flag = false;
						}
						else if(Current_Control.equals(ENV_IRIS_ON))
						{
							Stryk_Demo.env_iris_critical_flag = true;
							Stryk_Demo.EnvIrisPowerButton.setSelected(false);
							Stryk_Demo.env_iris_critical_flag = false;
						}
						else if(Current_Control.equals(ENV_IRIS_OFF))
						{
							Stryk_Demo.env_iris_critical_flag = true;
							Stryk_Demo.EnvIrisPowerButton.setEnabled(true);
							Stryk_Demo.env_iris_critical_flag = false;
						}
						
						//textArea.update(textArea.getGraphics());
						serialport.removeDataListener();
						//serialport.closePort();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println("Error Response Received!!");
					synchronized(lockObject)
					{
						
						Write_flag = false;
						lockObject.notifyAll();
					}
				}
				
				else
				{
					System.out.println("Peculiar");
				}
				System.out.println("Here2");
			}
		};

		
		
		//Check WriteFlag whether the data is sent to serialport successfully
		if (Write_flag == true)
		{
			
				Timeout_Flag = 0;
				System.out.println("Write Flag Set");
				//Sets the timeout_task_performer to start exactly after serial_read_cmd_timeout milliseconds
				
				System.out.println("Triggering Timer...");
				futuree = scheduler.schedule(timeout_task_performer, timeout, TimeUnit.MILLISECONDS);
				System.out.println("Timer Triggered!!");
				
				//publish("Waiting For Response!!\r\n");
				
				
					
				Thread appThread = new Thread() {
					public void run() {
						try {
							//Entry point of the function
							scheduler.schedule(update_GUI_state, 0, TimeUnit.MILLISECONDS);
							//SwingUtilities.invokeLater(update_GUI_state);
						} catch (Exception ex) {
							System.out.println("Some Error");
							ex.printStackTrace();
						}
					}
				};
				appThread.run();
			
			
			
				
		} 
		else
		{
			System.out.println("WriteFlag is False");
		}
		while(Write_flag)
    	{
			synchronized(lockObject)
    		{
				try {
					lockObject.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    		//TEST Print $System.out.println("Write Flag Inside While Loop : " + Write_flag);
    		
    		
    	}
		System.out.println("Timeout_Flag while returning from function : " + Timeout_Flag);
		return Timeout_Flag;
		
	}
	
}
