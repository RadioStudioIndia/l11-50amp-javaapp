import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SwingWorker;

import com.fazecast.jSerialComm.SerialPort;

import javax.swing.JButton;

public class AsynchronousUIUpdate {
	public SwingWorker<Void, Void> data_sender;
	public SwingWorker<Void, String> data_receiver;
	public SerialPort serialport;
	private JFrame frame;
	public int count = 0;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AsynchronousUIUpdate window = new AsynchronousUIUpdate();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AsynchronousUIUpdate() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnStartButton = new JButton("START");
		btnStartButton.setBounds(27, 40, 89, 23);
		frame.getContentPane().add(btnStartButton);
		
		JButton btnStopButton = new JButton("STOP");
		btnStopButton.setBounds(27, 77, 89, 23);
		frame.getContentPane().add(btnStopButton);
		
		serialport = Stryk_Demo.serialport;
		setcommparams s = new setcommparams();
		s.setcommparams(serialport);
		serialport.openPort();
		
		
	}
	public void setup_data_sender()
	{
		data_sender = new SwingWorker<Void, Void>()
		{

			@Override
			protected Void doInBackground() throws Exception {
				while(true)
				{
					
					if(serialport.isOpen() == true)
					{
						count = count + 1;
						String str = "HI" + Integer.toString(count);
						byte[] serialbytes =str.getBytes() ;
						serialport.writeBytes(serialbytes, serialbytes.length);
					}
					else
					{
					
						serialport.openPort();
					}
					
					
				}
				// TODO Auto-generated method stub
				
			}
			
		};
	}

}
