import java.util.ArrayList;
import java.util.List;

public class bytedefinitions {
	
	public enum RespError_Flag {
		Response_OK						((byte)(0x0)), 
		Framing_Error 					((byte)(0x1)),
		Checksum_Error					((byte)(0x2)), 
		Stuffing_Error					((byte)(0x3)), 
		Unknown_Cmd_Error				((byte)(0x4)),
		Payload_Length_Error			((byte)(0x5)),
		IPCMutex_Timeout_Error 			((byte)(0x6)),
		Unknown_MsgType_Error			((byte)(0x7)),
		Unknown_State_Error				((byte)(0x8)),
		uC1_Timeout_Error				((byte)(0x9)),
		uC1_IPC_Error					((byte)(0xA)),
		Cmd_Execution_Error				((byte)(0xB)),
		ResponseType_MismatchError		((byte)(0x4)), 
		CommandMode_MismatchError		((byte)(0x5)), 
		InvalidCommand_ModeError		((byte)(0x6));
		
		
		
		@SuppressWarnings("unused")
		private byte value;

		private RespError_Flag(byte value) {
			this.value = value;
		}
	}
	
	
	
	
	
	public byte START_byte				= 0x55; //Start Byte
	public byte END_byte				= (byte) 0xAA; //End Byte
	public byte ESC_byte 				= (byte) 0xEE; //Escape byte
	
	
	//Command Type Definitions
	public byte CONNECT_COMMAND 		= 0x50;
	public byte DISCONNECT_COMMAND		= 0x51;
	public byte MODE_COMMAND			= 0x52;
	public byte ENADIS_COMMAND			= 0x53;
	public byte INTENSITY_COMMAND		= 0x54;
	public byte SENSING_COMMAND			= 0x55;
	public byte SAVE_COMMAND			= 0x56;
	public byte RESTORE_CMD				= 0x57;
	public byte RESET_uC_COMMAND		= 0x58;
	public byte LOAD_DEFAULT_COMMAND	= 0x59;
	public byte LIVE_WIDTH_COMMAND		= 0x5A;
	
	//Byte Definitions For Manufacturing Test
	public byte VERSION_NO_TEST_COMMAND = 0x5A;
	public byte IPC_TEST_COMMAND        = 0x5B;
	public byte EEPROM_TEST_COMMAND     = 0x5C;
	public byte FLASH_TEST_COMMAND      = 0x5D;
    public byte UART_TEST_COMMAND       = 0x5E;
    public byte FAN_TEST_COMMAND		= 0x5F;
    public byte GUI_TEST_COMMAND        = 0x60;
    public byte IRIS_TEST_COMMAND		= 0x61;
    public byte RGBL_TEST_COMMAND		= 0x62;
    public byte COLOR_SENSOR_COMMAND	= 0x63;
    public byte PD_TEST_COMMAND			= 0x64;
    
	public byte EXPECT_NO_RESPONSE      = 0x0;
		
	public int CRC = 0x0;
	
	//Message type Definitions
	public byte MSG_TYPE_CMD 			= 0x1;
	public byte MSG_TYPE_RESP 			= 0x2;
	public byte MSG_TYPE_NOTIFY 		= 0x4;
	public byte MSG_TYPE_ERROR 			= 0x8;
	
	//LightSource Command type definitions
	public byte SRC_WHITELIGHT 			= 0x1;
	public byte SRC_ENV					= 0x2;
	public byte SRC_IRIS 				= 0x3;
	public int  SRC_LAST_MODE 			= 0x4;
	public int  SRC_INIT_MODE 			= 0x5;
	
	//Mode Type Definitions
	public byte TYPE_PWM        		= 0x1;
	public byte TYPE_CWM         		= 0x2;
	public byte TYPE_STROBE       		= 0x3;
	public byte TYPE_LC        			= 0x4;
	public byte TYPE_CWM_NORED      	= 0x5;
	public byte TYPE_LC_NORED 		    = 0x6;
	
	//Light State Definitions
	public byte ON_STATE 				= 0x1;
	public byte OFF_STATE 				= 0x0;
	
	//Light Source Definitions
	public byte SOURCE_EXTERNAL 	    = 0x1;
	public byte SOURCE_uC1 			    = 0x2;
	
	public byte SOURCE_uC2 			    = 0x3;
	public byte SOURCE_EXT_WHITELIGHT 	= 0x4;
	public byte SOURCE_EXT_ENV 			= 0x5;
	
	//PWM Type Definitions
	public byte PWM_TYPE_FIXED 				= 0x1;
	public byte PWM_TYPE_FADE 				= 0x2;
	public byte PWM_TYPE_PATTERN_1�s 		= 0x3;
	public byte PWM_TYPE_PATTERN_1ms 		= 0x4;
	public byte PWM_TYPE_PATTERN_TRANSIENT 	= 0x5;
	
	//PWM Frequency'
	public byte PWM_HIGH_FREQUENCY 			= 0x1;
	public byte PWM_LOW_FREQUENCY 			= 0x0;
	
	//Error Source Definitons
	public byte SRC_uC1 = 0x1;
	public byte SRC_uC2 = 0x2;
	public byte SRC_JAVA_APP = 0x3;
	public byte SRC_CONTROLLER1_RESP = 0x4;
	
	
	//Micro-Controller Definitions
	public byte WHICH_CONTROLLER_1 		= 0x1;
	public byte WHICH_CONTROLLER_2 		= 0x2;
	//Slider Definitions newly defined for P3 and P4 on 18122017 by MGR
	public byte INTENSITY_SLIDER_RED 	= 0x1;
	public byte INTENSITY_SLIDER_GREEN 	= 0x2;
	public byte INTENSITY_SLIDER_BLUE 	= 0x3;
	public byte INTENSITY_SLIDER_WHITE  = 0x7;
	public byte INTENSITY_SLIDER_ENV	= 0x4;
	public byte INTENSITY_SLIDER_IRIS1	= 0x5;
	public byte INTENSITY_SLIDER_IRIS2 	= 0x6;
	public byte INTENSITY_PWR_FAN		= 0x8;
	public byte INTENSITY_HSINK_FAN		= 0x9;
	public byte INTENSITY_IRIS_FAN		= 0xA;
	public byte IRIS_POWER_SHUTDOWN_EN  = 0xB;
	//Slider Definitions
	//public byte INTENSITY_SLIDER_RED 	= 0x1;
	//public byte INTENSITY_SLIDER_GREEN 	= 0x2;
	//public byte INTENSITY_SLIDER_BLUE 	= 0x3;
	//public byte INTENSITY_SLIDER_WHITE  = 0x4;
	//public byte INTENSITY_SLIDER_ENV	= 0x5;
	//public byte INTENSITY_SLIDER_IRIS1	= 0x6;
	//public byte INTENSITY_SLIDER_IRIS2 	= 0x7;
	//public byte INTENSITY_PWR_FAN		= 0x8;
	//public byte INTENSITY_HSINK_FAN		= 0x9;
	//public byte INTENSITY_IRIS_FAN		= 0xA;
	//public byte IRIS_POWER_SHUTDOWN_EN  = 0xB;

	//Standard Default Packets for Construct IPCPacket Function
	public byte LIGHT_SOURCE = 0x0;
	public byte STATE = 0x0;
	public byte MODE = 0x0;
	public byte PWM_SOURCE = 0x0;
	public byte PWM_TYPE = 0x0;
	public byte TYPE_VAL = 0x0;
	public byte PWM_FREQ = 0x0;
	
	
	//Reserved Field Definition
	public byte[] RESERVED_FIELDS = {0x0,0x0,0x0};// Always add 3 reserved fields in the Tx Packet

	
	//Error returns only for JavaApp, not relevant for IPC
	
	public int uC1_CHKSUM_ERROR   		= 0x10;
	public int uC1_FRAMING_ERROR  		= 0x20;
	public int uC1_UNKNOWN_CMD_ERROR 	= 0x30;
	public int uC1_PAYLOAD_LEN_ERROR	= 0x40;
	 
	public static int check_value = 0x0;
	
	
	public ArrayList<Byte>  WL_PWM = new ArrayList<Byte>();
	

	public static void main(String[] args) {
		
		System.out.println("Inside Main Function!!");
		bytedefinitions b = new bytedefinitions();
		System.out.println((byte) 0xAA);
		b.initialize();
	}
	

	
	//Constructor of the class
	public  bytedefinitions()
	{
		
	}
	
	
	public void initialize()
	{
		
		
		/*WL_PWM.add(MSG_TYPE_CMD);
		WL_PWM.add(WL_MODE_CMD);
		WL_PWM.add(PAYLOAD_LENGTH_1BYTE);
		for(int i=0;i<RESERVED_FIELDS.length;i++) {WL_PWM.add(RESERVED_FIELDS[i]);}
		
		WL_PWM.add(TYPE_PWM);
		*/
		int temp=0;
		temp = (calculateCRC(WL_PWM));
		System.out.println(temp);
		
		if(temp == 0xfc)
		{
			System.out.println("CheckSum Verification Success");
		}
	}
	public int calculateCRC(List<Byte> list)
	{
		int temp = 0;
		int temp1 = 0;
		
		for (int i=0;i<list.size();i++)
		{
			
			temp1 = ((int) (list.get(i)))& 0xff;
			temp += temp1;
			System.out.println("Temp 1 : " + Integer.toHexString( temp1));
			System.out.println("Temp : " +  Integer.toHexString(temp));
		}
		
		
		System.out.println("Total Sum is : " + Integer.toHexString(temp));
		//##System.out.println(temp);
		check_value = ~temp; //1's complement
		check_value = check_value+1; //2's complement
		
		//##System.out.print("Checksum is : ");
		//##System.out.println(String.format("0x%04X", check_value));
		return check_value;
	
	}
	
}
