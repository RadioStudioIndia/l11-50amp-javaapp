import java.awt.*;

import com.fazecast.jSerialComm.*;
import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;
import javax.swing.Timer;
import javax.swing.event.ChangeListener;
import javax.swing.text.NumberFormatter;
import javax.xml.bind.DatatypeConverter;
import javax.swing.event.ChangeEvent;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.Dialog.ModalExclusionType;
import javax.swing.border.TitledBorder;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import javax.swing.border.Border;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.border.BevelBorder;

/**
 * 
 */

/**
 * @author LakshmiNarasimman R
 *
 */
public class Stryk_Demo {
	// Enumerator that holds different Error Flag Values
	public enum RespError_Flag {
		Response_OK((byte) (0x0)), Framing_Error((byte) (0x1)), Checksum_Error((byte) (0x2)), Stuffing_Error(
				(byte) (0x3)), Unknown_Cmd_Error((byte) (0x4)), Payload_Length_Error(
						(byte) (0x5)), IPCMutex_Timeout_Error((byte) (0x6)), Unknown_MsgType_Error(
								(byte) (0x7)), Unknown_State_Error((byte) (0x8)), Timeout_Error(
										(byte) (0x9)), IPC_Error((byte) (0xA)), Cmd_Execution_Error(
												(byte) (0xB)), ResponseType_MismatchError(
														(byte) (0x4)), CommandMode_MismatchError(
																(byte) (0x5)), InvalidCommand_ModeError((byte) (0x6));

		private byte value;

		private RespError_Flag(byte value) {
			this.value = value;
		}
	}

	public static ScheduledFuture<?> future;
	public static ScheduledFuture<?> futuree;

	public static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(3);

	@SuppressWarnings("serial")
	public static ArrayList<Byte> destuff = new ArrayList<Byte>() {
		{
			bytedefinitions b = new bytedefinitions();
			add(b.START_byte);
			add(b.END_byte);
			add(b.ESC_byte);

		}
	};
	public static Logger lg = Logger.getLogger("MyLog");
	public static JToggleButton Live_Updates;

	public static final ButtonGroup _btn_retmode_ = new ButtonGroup();
	public static final ButtonGroup Live_Mode_Light_Selection = new ButtonGroup();
	public static final ButtonGroup _radiobutton_subiris_ = new ButtonGroup();
	public static final ButtonGroup _wl_ = new ButtonGroup();
	public static final ButtonGroup _env_ = new ButtonGroup();
	public static final ButtonGroup _iris_ = new ButtonGroup();
	public static final ButtonGroup _iris_controls_ = new ButtonGroup();
	public static final ButtonGroup _init_ret_ = new ButtonGroup();
	public static final ButtonGroup _misc_ = new ButtonGroup();
	public static final ButtonGroup env_iris_chckbx = new ButtonGroup();
	public static final ButtonGroup wlmode = new ButtonGroup();
	public static final ButtonGroup wlsource = new ButtonGroup();
	public static final ButtonGroup wlpwmtype = new ButtonGroup();
	public static final ButtonGroup wlpwmfrequency = new ButtonGroup();
	public static final ButtonGroup env_iris_mode = new ButtonGroup();
	public static final ButtonGroup env_iris_source = new ButtonGroup();
	public static final ButtonGroup reset_ld_chckbx = new ButtonGroup();
	public static final ButtonGroup reset_ld_rdbtn = new ButtonGroup();

	public DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
	Date dateobj = new Date();

	public static JRadioButton rdbtnLaserlight;

	// File attributes for running only one instance of the application
	private static File f;
	public static FileChannel channel;
	public static FileLock lock;

	//Divisor value for % display
	public static int Max_Val = 4095;
	public static int Max_Val_8Bit = 255;
	public static boolean pause_flag = false;
	// Timer Definitions
	public static Timer uart_recv_timer;
	public static Timer uart_response_timer;

	// int Definitions
	public static int numRead;
	public static int total_bytes;
	public static int Error_Message = JOptionPane.ERROR_MESSAGE;
	public static int resp_error_type = 0x0;
	public static int notify_flag = 0;
	public static int timeoutflag = 0;
	public int non_zero_elements = 0;
	public static int reference_index = 0;
	public static int sp;

	public static JFrame frmStrykerVer;
	public JFrame frmStrykerVer_1;
	public static ByteArrayOutputStream full_buffer = new ByteArrayOutputStream();
	public static boolean null_charflag = false;

	@SuppressWarnings("rawtypes")
	public static JComboBox comboBox;

	// Timeouts for different events. All values in long. to be used as milliseconds
	// ####################################TIMEOUT
	// DEFINITIONS###################################################################
	public static long serial_read_cmd_timeout = 2000; // Max time in ms after which timeout is triggered if at all no
														// data is received
	public static long serial_read_byte_timeout = 50; // Max time in ms after which timeout is triggered if successive
														// byte is not received
	public static long serial_read_byte_critical_timeout = 5000; // Max time in ms after which timeout is triggered if
																	// at all no data is received for critical test cmds

	public static JCheckBox chckbxReset;
	public static JCheckBox chckbxLoadDefaults;
	public static JCheckBox chckbxEnv;
	public static JCheckBox chckbxRed;
	public static JCheckBox chckbxGreen;
	public static JCheckBox chckbxBlue;
	public static JCheckBox chckbxIris1;
	public static JCheckBox chckbxIrisPowerShutdown_1;
	public static JCheckBox chckbxIris;
	public static JCheckBox chckbxIrisPowerShutdown;
	public static JCheckBox chckbxIris2;

	public static JButton btnConnect;
	public static JButton btnDisconnect;
	public JButton btnLoadDefault;
	public static JButton btnRefreshPortList;
	public static JButton btnSave;
	public static JButton btnLastMode;
	public static JButton btnReadAll;
	public static JButton btnManufacturingTests;

	public static JTextField timer_textField;
	public static JSlider slider_15;
	public static JSlider slider_12;
	public static JSlider slider_13;
	public static JSlider slider_14;
	public static JSlider slider_19;
	public static JSlider slider_20;
	public static JSlider slider_16;
	public static JSlider slider_17;
	public static JSlider slider_18;
	public static JSlider slider_21;

	public static String Pass = "PASS";
	public static String Fail = "FAIL";
	public static String init_state = "N/A";
	public static String Timeout = "RESPONSE TIMEDOUT";

	public static Color Red = Color.red;
	public static Color Green = Color.green;

	public static String Error_Title = "Error Information";
	public static String Warning_Title = "Warning";
	public static String slider_value;
	public static String slider1_value;
	public static String slider2_value;
	public static String SOURCE = "";
	public static String radiobuttonPwm = "rdbtnPwm";
	public static String radiobuttonCwm = "rdbtnCwm";
	public static String radiobuttonStb = "rdbtnStb";
	public static String radiobuttonLc = "rdbtnLc";
	public static String radiobuttonuC_1 = "rdbtnuC_1";
	public static String radiobuttonExt = "rdbtnExt";
	public static String radiobuttonuC_2 = "rdbtnuC_2";
	public static JRadioButton rdbtnWhitelight;
	public static String Manufacturing_Test = "{MTESTSTART}";
	public static String Exit_MFTests = "{MTESTEXIT}";

	// COMMAND STRINGS FOR MANUFACTURING TESTS
	// POWER Tests
	public static String MTESTSTART = "{MTESTSTART}";
	public static String MTESTEXIT = "{MTESTEXIT}";
	public static String uC1PWRTEST = "{uC1PWRTEST}";
	public static String uC2PWRTEST = "{uC2PWRTEST}";
	// COM Tests
	public static String uC1COMTEST = "{uC1COMTEST}";
	public static String uC2COMTEST = "{uC2COMTEST}";
	// IPC Test
	public static String IPCTEST = "{IPCTEST}";
	// EEPROM Tests
	public static String uC1EEPROMTEST = "{uC1EEPROMTEST}";
	public static String uC2EEPROMTEST = "{uC2EEPROMTEST}";
	// Falsh Tests
	public static String uC1FLASHTEST = "{uC1FLASHTEST}";
	public static String uC2FLASHTEST = "{uC2FLASHTEST}";
	// UART Tests - uC1
	public static String uC1CAMUARTTEST = "{uC1UARTTEST,CAMUART}";
	public static String uC1DEBUGUARTTEST = "{uC1UARTTEST,DEBUGUART}";
	// UART Tests - uC2
	public static String uC2CAMUARTTEST = "{uC2UARTTEST,CAMUART}";
	public static String uC2DEBUGUARTTEST = "{uC2UARTTEST,DEBUGUART}";
	public static String uC2GUIUARTTEST = "{uC2UARTTEST,GUIUART}";
	public static String uC2ADDUARTTEST = "{uC2UARTTEST,ADDUART}";
	// public static String DEBUG_UART_uC1 = "uUC1UART}";
	// public static String DEBUG_UART_uC2 = "{UC2UART}";

	// TEST ITEMS - FAN TEST
	public static String LIGHTENGINEFANTEST = "{FANTEST,LIGHTENGINEFAN}";
	public static String HEATSINKFANTEST = "{FANTEST,HEATSINKFAN}";
	public static String IRISFANTEST = "{FANTEST,IRISFAN}";

	public final static Object lockObject = new Object();
	public static boolean recv_worker_flag = false;
	// TEST ITEMS - PWMMUX TEST
	// PWM MUX Tests - uC1
	public static String WLuC1PWMMUXTEST = "{PWMMUXTEST,WL,uC1}";
	public static String ENVuC1PWMMUXTEST = "{PWMMUXTEST,ENV,uC1}";
	public static String IRISuC1PWMMUXTEST = "{PWMMUXTEST,IRIS,uC1}";

	// PWM MUX Tests - uC2
	public static String WLuC2PWMMUXTEST = "{PWMMUXTEST,WL,uC2}";
	public static String ENVuC2PWMMUXTEST = "{PWMMUXTEST,ENV,uC2}";
	public static String IRISuC2PWMMUXTEST = "{PWMMUXTEST,IRIS,uC2}";

	// PWM MUX Tests - External
	public static String WLEXTPWMMUXTEST = "{PWMMUXTEST,WL,EXTERNALWL}";
	public static String ENVEXTPWMMUXTEST = "{PWMMUXTEST,ENV,EXTERNALENV}";
	public static String IRISEXTWLPWMMUXTEST = "{PWMMUXTEST,IRIS,EXTERNALWL}";
	public static String IRISEXTENVPWMMUXTEST = "{PWMMUXTEST,IRIS,EXTERNALENV}";

	// TEST ITEMS - LED TEST @ 0% NOLOAD
	public static String REDLEDTEST_ZERO_NOLOAD = "{LEDTEST,RED,ZERO,NOLOAD}";
	public static String GREENLEDTEST_ZERO_NOLOAD = "{LEDTEST,GREEN,ZERO,NOLOAD}";
	public static String BLUELEDTEST_ZERO_NOLOAD = "{LEDTEST,BLUE,ZERO,NOLOAD}";
	public static String ENVLEDTEST_ZERO_NOLOAD = "{LEDTEST,LASER,ZERO,NOLOAD}";
	public static String IRIS1LEDTEST_ZERO_NOLOAD = "{LEDTEST,IRIS1,ZERO,NOLOAD}";
	public static String IRIS2LEDTEST_ZERO_NOLOAD = "{LEDTEST,IRIS2,ZERO,NOLOAD}";

	// TEST ITEMS - LED TEST @ 0% LOAD
	public static String REDLEDTEST_ZERO_LOAD = "{LEDTEST,RED,ZERO,LOAD}";
	public static String GREENLEDTEST_ZERO_LOAD = "{LEDTEST,GREEN,ZERO,LOAD}";
	public static String BLUELEDTEST_ZERO_LOAD = "{LEDTEST,BLUE,ZERO,LOAD}";
	public static String ENVLEDTEST_ZERO_LOAD = "{LEDTEST,LASER,ZERO,LOAD}";
	public static String IRIS1LEDTEST_ZERO_LOAD = "{LEDTEST,IRIS1,ZERO,LOAD}";
	public static String IRIS2LEDTEST_ZERO_LOAD = "{LEDTEST,IRIS2,ZERO,LOAD}";

	// TEST ITEMS - LED TEST @ 50% NOLOAD
	public static String REDLEDTEST_FIFTY_NOLOAD = "{LEDTEST,RED,FIFTY,NOLOAD}";
	public static String GREENLEDTEST_FIFTY_NOLOAD = "{LEDTEST,GREEN,FIFTY,NOLOAD}";
	public static String BLUELEDTEST_FIFTY_NOLOAD = "{LEDTEST,BLUE,FIFTY,NOLOAD}";
	public static String ENVLEDTEST_FIFTY_NOLOAD = "{LEDTEST,LASER,FIFTY,NOLOAD}";
	public static String IRIS1LEDTEST_FIFTY_NOLOAD = "{LEDTEST,IRIS1,FIFTY,NOLOAD}";
	public static String IRIS2LEDTEST_FIFTY_NOLOAD = "{LEDTEST,IRIS2,FIFTY,NOLOAD}";

	// TEST ITEMS - LED TEST @ 50% LOAD
	public static String REDLEDTEST_FIFTY_LOAD = "{LEDTEST,RED,FIFTY,LOAD}";
	public static String GREENLEDTEST_FIFTY_LOAD = "{LEDTEST,GREEN,FIFTY,LOAD}";
	public static String BLUELEDTEST_FIFTY_LOAD = "{LEDTEST,BLUE,FIFTY,LOAD}";
	public static String ENVLEDTEST_FIFTY_LOAD = "{LEDTEST,LASER,FIFTY,LOAD}";
	public static String IRIS1LEDTEST_FIFTY_LOAD = "{LEDTEST,IRIS1,FIFTY,LOAD}";
	public static String IRIS2LEDTEST_FIFTY_LOAD = "{LEDTEST,IRIS2,FIFTY,LOAD}";
	
	//TEST ITEMS - LED TEST @ 75% NOLOAD
	public static String REDLEDTEST_SEVENTY_FIVE_NOLOAD 		= "{LEDTEST,RED,SEVENTYFIVE,NOLOAD}";
	public static String GREENLEDTEST_SEVENTY_FIVE_NOLOAD 		= "{LEDTEST,GREEN,SEVENTYFIVE,NOLOAD}";
	public static String BLUELEDTEST_SEVENTY_FIVE_NOLOAD 		= "{LEDTEST,BLUE,SEVENTYFIVE,NOLOAD}";
	public static String ENVLEDTEST_SEVENTY_FIVE_NOLOAD 		= "{LEDTEST,LASER,SEVENTYFIVE,NOLOAD}";
	public static String IRIS1LEDTEST_SEVENTY_FIVE_NOLOAD 		= "{LEDTEST,IRIS1,SEVENTYFIVE,NOLOAD}";
	public static String IRIS2LEDTEST_SEVENTY_FIVE_NOLOAD 		= "{LEDTEST,IRIS2,SEVENTYFIVE,NOLOAD}";

	//TEST ITEMS - LED TEST @ 75% LOAD
	public static String REDLEDTEST_SEVENTY_FIVE_LOAD 			= "{LEDTEST,RED,SEVENTYFIVE,LOAD}";
	public static String GREENLEDTEST_SEVENTY_FIVE_LOAD 		= "{LEDTEST,GREEN,SEVENTYFIVE,LOAD}";
	public static String BLUELEDTEST_SEVENTY_FIVE_LOAD 			= "{LEDTEST,BLUE,SEVENTYFIVE,LOAD}";
	public static String ENVLEDTEST_SEVENTY_FIVE_LOAD 			= "{LEDTEST,LASER,SEVENTYFIVE,LOAD}";
	public static String IRIS1LEDTEST_SEVENTY_FIVE_LOAD 		= "{LEDTEST,IRIS1,SEVENTYFIVE,LOAD}";
	public static String IRIS2LEDTEST_SEVENTY_FIVE_LOAD 		= "{LEDTEST,IRIS2,SEVENTYFIVE,LOAD}";

	// TEST ITEMS - LED TEST @ 100% NOLOAD
	public static String REDLEDTEST_HUNDRED_NOLOAD = "{LEDTEST,RED,HUNDRED,NOLOAD}";
	public static String GREENLEDTEST_HUNDRED_NOLOAD = "{LEDTEST,GREEN,HUNDRED,NOLOAD}";
	public static String BLUELEDTEST_HUNDRED_NOLOAD = "{LEDTEST,BLUE,HUNDRED,NOLOAD}";
	public static String ENVLEDTEST_HUNDRED_NOLOAD = "{LEDTEST,LASER,HUNDRED,NOLOAD}";
	public static String IRIS1LEDTEST_HUNDRED_NOLOAD = "{LEDTEST,IRIS1,HUNDRED,NOLOAD}";
	public static String IRIS2LEDTEST_HUNDRED_NOLOAD = "{LEDTEST,IRIS2,HUNDRED,NOLOAD}";

	// TEST ITEMS - LED TEST @ 100% LOAD
	public static String REDLEDTEST_HUNDRED_LOAD = "{LEDTEST,RED,HUNDRED,LOAD}";
	public static String GREENLEDTEST_HUNDRED_LOAD = "{LEDTEST,GREEN,HUNDRED,LOAD}";
	public static String BLUELEDTEST_HUNDRED_LOAD = "{LEDTEST,BLUE,HUNDRED,LOAD}";
	public static String ENVLEDTEST_HUNDRED_LOAD = "{LEDTEST,LASER,HUNDRED,LOAD}";
	public static String IRIS1LEDTEST_HUNDRED_LOAD = "{LEDTEST,IRIS1,HUNDRED,LOAD}";
	public static String IRIS2LEDTEST_HUNDRED_LOAD = "{LEDTEST,IRIS2,HUNDRED,LOAD}";
	// IRIS PGOODTESTS
	public static String IRISPGOODONTEST = "{EXTINPUTTEST,IRISPGOOD,ON}";
	public static String IRISPGOODOFFTEST = "{EXTINPUTTEST,IRISPGOOD,OFF}";

	// FRONTBOARD POWER ENABLE TESTS
	public static String FRONTBAORDPOWERENONTEST = "{EXTINPUTTEST,FBPWREN,ON}";
	public static String FRONTBOARDPOWERENOFFTEST = "{EXTINPUTTEST,FBPWREN,OFF}";
	// FIBER DETECT TESTS
	public static String FIBER1DETECTONTEST = "{EXTINPUTTEST,FIBER1,ON}";
	public static String FIBER1DETECTOFFTEST = "{EXTINPUTTEST,FIBER1,OFF}";

	public static String FIBER2DETECTONTEST = "{EXTINPUTTEST,FIBER2,ON}";
	public static String FIBER2DETECTOFFTEST = "{EXTINPUTTEST,FIBER2,OFF}";

	// SENSE GPIOs
	public static String BEAMSENSORONTEST = "{EXTINPUTTEST,BEAMSENSOR,ON}";
	public static String BEAMSENSOROFFTEST = "{EXTINPUTTEST,BEAMSENSOR,OFF}";

	public static String ESSTONTEST = "{EXTINPUTTEST,ESST,ON}";
	public static String ESSTOFFTEST = "{EXTINPUTTEST,ESST,OFF}";

	public static String ASSTONTEST = "{EXTINPUTTEST,ASST,ON}";
	public static String ASSTOFFTEST = "{EXTINPUTTEST,ASST,OFF}";

	public static String FBPWRTEST = "{EXTINPUTTEST,FBPWRENABLE}";
	public static String IRISPGOODTEST = "{EXTINPUTTEST,IRISPGOOD}";
	public static String FANFAILURETEST = "{EXTINPUTTEST,FANFAIL,ON}";

	// IRIS1 PDTESTS
	public static String PDTEST_IRIS1_ZERO = "{PDTEST,IRIS1,ZERO}";
	public static String PDTEST_IRIS1_HUNDRED = "{PDTEST,IRIS1,HUNDRED}";

	// IRIS2 PDTESTS
	public static String PDTEST_IRIS2_ZERO = "{PDTEST,IRIS2,ZERO}";
	public static String PDTEST_IRIS2_HUNDRED = "{PDTEST,IRIS2,HUNDRED}";

	// LASER PDTESTS
	public static String PDTEST_ENV_ZERO = "{PDTEST,LASER,ZERO}";
	public static String PDTEST_ENV_HUNDRED = "{PDTEST,LASER,HUNDRED}";

	// COLOR SENSOR TESTS
	public static String COLORSENSOR1TEST = "{CSTEST,CS1}";
	public static String COLORSENSOR2TEST = "{CSTEST,CS2}";

	public static Timer live_timer_on = null;
	public Timer live_timer_off = null;

	public static String GUI = "{GUITEST}";
	public static Simple Response_Animator = new Simple("Waiting For Response..."); // Class that holds the animation
																					// Image
	// public static Simple Live_Animator = new Simple("Waiting for Live Update to
	// complete...");

	public static String RESPONSE_OK = "{OK,";
	public static String RESPONSE_ERROR = "{FAIL,";

	public static String radiobuttonEnvPwm = "rdbtnEnvPwm";
	public static String radiobuttonEnvCwm = "rdbtnEnvCwm";
	public static String radiobuttonEnvCwmNr = "rdbtnEnvCwmNr";
	public static String radiobuttonEnvLcNr = "rdbtnEnvLcNr";
	public static String radiobuttonExtEnv = "rdbtnExtEnv";
	public static String radiobuttonuC_3 = "rdbtnuC_3";
	public static String radiobuttonuC_4 = "rdbtnuC_4";
	public static String radiobuttonIrisExtWht = "rdbtnIrisExtWht";
	public static String radiobuttonIrisExtEnv = "rdbtnIrisExtEnv";

	public static String ENV_SOURCE = "ENV";
	public static String IRIS_SOURCE = "IRIS";

	public static String CWM_NR_ON = "CWM_NR_ON";
	public static String CWM_NR_OFF = "CWM_NR_OFF";
	// String Definitions

	public static String[] simulation_menu = { "Fixed", "Fade-Up/Down" };
	public static String[] simulation_menu_1 = { "Fixed", "Fade-Up/Down" };
	public static String[] portlistforcombobox = new String[5];// Max 5 ComPorts can be stored in this array
	public static String[] portlistforcomboboxempty = new String[10];

	public static String portname;
	public static String Current_control = "";
	public static String Connect = "CONNECT";
	public static String Disconnect = "DISCONNECT";
	public static String LIGHT = "LIGHT";

	public static String WL_FAKE_ON = "WL_FAKE_ON";
	public static String WL_FAKE_OFF = "WL_FAKE_OFF";
	public static String ENV_IRIS_FAKE_ON = "ENV_IRIS_FAKE_ON";
	public static String ENV_IRIS_FAKE_OFF = "ENV_IRIS_FAKE_OFF";
	public static String ENV_IRIS_SEL_FALSE = "FALSE";
	public static String INDIVIDUAL_CONTROL = "IND";

	public static String WL_PON = "WL_POWER_ON";
	public static String WL_POFF = "WL_POWER_OFF";
	public static String GRP_SLIDER = "WL_SLIDER";
	public static String RED_SLIDER = "RED_SLIDER";
	public static String GREEN_SLIDER = "GREEN_SLIDER";
	public static String BLUE_SLIDER = "BLUE_SLIDER";
	public static String RED_SLIDER_EN = "RED_SLIDER_ENABLE";
	public static String RED_SLIDER_DIS = "RED_SLIDER_DISABLE";
	public static String GREEN_SLIDER_EN = "GREEN_SLIDER_ENABLE";
	public static String GREEN_SLIDER_DIS = "GREEN_SLIDER_DISABLE";
	public static String BLUE_SLIDER_EN = "BLUE_SLIDER_ENABLE";
	public static String BLUE_SLIDER_DIS = "BLUE_SLIDER_DISABLE";
	public static String RESET_uC1 = "RESET1";
	public static String RESET_uC2 = "RESET2";
	public static String LOAD_DEF1 = "LD1";
	public static String LOAD_DEF2 = "LD2";
	public static String SLIDER = "SLIDER";
	public static String ENABLE_DISABLE = "ENABLE_DISABLE";

	public static String CALIBRATION = "CALIBRATION";

	public static String LIVE = "LIVE";
	public static String LIVE_OFF = "LIVE_OFF";
	public static String INDIVIDUAL = "RDBTN_INDIVIDUAL";
	public static String POWER_SUPPLY_SLIDER = "LIGHT_ENGINE_FAN_CONTROL";
	public static String HEATSINK_SLIDER = "HEAT_SINK_FAN_CONTROL";
	public static String IRIS_FAN_SLIDER = "IRIS_FAN_CONTROL";
	public static String ENV_SLIDER = "ENV_SLIDER";
	public static String IRIS1_SLIDER = "IRIS1";
	public static String IRIS2_SLIDER = "IRIS2";
	public static String SAVE = "SAVE";
	public static String ENV_IRIS_ON = "ENV_IRIS_ON";
	public static String ENV_IRIS_OFF = "ENV_IRIS_OFF";
	public static String IRIS_1_EN = "ENABLEIRIS1";
	public static String IRIS_2_EN = "ENABLEIRIS2";
	public static String IRIS_1_DIS = "DISABLEIRIS1";
	public static String IRIS_2_DIS = "DISABLEIRIS2";

	public static String IRIS_POWER_SHUTDOWN = "IRIS_PWR_SDN";
	public static String LAST = "LAST_MODE";
	public static String READ_ALL = "READ_ALL";

	// New String Definitions for Manufacturing tests
	public static String VERSION_NUMBER_CHECK = "VERSION_NUMBER";
	public static String IPC_CHECK = "IPC_CHECK";
	public static String EEPROM_CHECK = "EEPROM_CHECK";
	public static String FLASH_CHECK = "FLASH_CHECK";
	public static String UART_CHECK = "UART_CHECK";
	public static String FAN_CHECK = "FAN_CHECK";
	public static String GUI_CHECK = "GUI_CHECK";
	public static String IRIS_CHECK = "IRIS_CHECK";
	public static String RGBL_CHECK = "RGBL_CHECK";
	public static String COLOR_SENSOR_CHECK = "COLOR_SENSOR_CHECK";
	public static String PD_CHECK = "PD_CHECK";
	// Swing Worker Definitions
	public static SwingWorker<Void, Void> bg_worker;
	public static SwingWorker<List<String>, String> response_worker;
	static Runnable response_timer;

	// RadioButton Definitions
	public static JRadioButton rdbtnPwm;
	public static JRadioButton rdbtnCwm;
	public static JRadioButton rdbtnStb;
	public static JRadioButton rdbtnLc;
	public static JRadioButton rdbtnuC1;
	public static JRadioButton rdbtnClear;
	public static JRadioButton rdbtnuC_1;
	public static JRadioButton rdbtnuC_2;
	public static JRadioButton rdbtnUc1;
	public static JRadioButton rdbtnuC2;
	public static JRadioButton rdbtnExt;
	public static JRadioButton rdbtnEnv_Cwm;
	public static JRadioButton rdbtnEnv_Pwm;
	public static JRadioButton rdbtnEnv_CwmNr;
	public static JRadioButton rdbtnEnv_LcNr;
	public static JRadioButton rdbtnEnv_Ext;
	public static JRadioButton rdbtnuC_3;
	public static JRadioButton rdbtnuC_4;
	public static JRadioButton rdbtn_IrisExtWht;
	public static JRadioButton rdbtn_IrisExtEnv;
	public static JRadioButton rdbtnIndividual;
	public static JRadioButton rdbtn120hz;
	public static JRadioButton rdbtn70khz;

	// Boolean Definitions
	public static boolean Calibration_Flag = false;
	public boolean connected_flag = false;
	public boolean LastModeFlag = true;
	public static boolean Wl_Button_Flag = false;
	public static boolean ENV_Iris_Button_Flag = false;
	public boolean slider1flag = false;
	public boolean slider2flag = false;
	public boolean slider3flag = false;
	public boolean slider4flag = false;
	public boolean savedacflag = false;
	public boolean open_flag = false;
	public static boolean iris_pgood_flag = false;
	public static boolean front_board_pwr_flag = false;
	public static boolean closed_flag = false;
	public boolean sim_flag = false;
	public static boolean writeflag = false;
	public static boolean textfield_flag = false;
	public static boolean disconnect_flag = false;
	public static boolean clearflag;
	public static boolean extractpwmflag = false;
	public static boolean connect_flag = false;
	public static boolean individual_flag = false;
	public static boolean wl_onoff_flag = false;
	public static boolean env_onoff_flag = false;
	public static boolean iris_onoff_flag = false;
	public static boolean wl_critical_flag = false;
	public static boolean env_iris_critical_flag = false;
	public static boolean switch_flag = false;
	public static boolean critical_test_flag = false;

	public static JPanel Live_Update_Panel;

	/*
	 * Definitions of hex bytes for next version - Version - 2
	 * 
	 * 
	 */

	// Byte Definitions
	public byte dummy_byte = 0x0;
	public byte[] bytes = new byte[7];
	static byte[] destuff_buffer;
	public static byte[] recv_buff;
	public byte[] temp_recv_buff;
	public static byte expected_message_type = 0x0;
	public static byte expected_cmd_id = 0x0;

	// SerialPort Definitions
	public static SerialPort serialport;
	public static SerialPort mftserialPort;
	public static SerialPort calibrationserialport;
	public static SerialPort[] portlist;
	public static SerialPort commport;

	// TextArea Definitions
	public static JTextArea textArea;

	public static boolean comportemptyflag = true;
	public static boolean serialportcommunicationflag = false;

	// ImageIcon Definitions
	public ImageIcon red_icon = new javax.swing.ImageIcon(getClass().getResource("/resources/red1.png"));
	public ImageIcon on_switch = new javax.swing.ImageIcon(getClass().getResource("/resources/on.jpg"));
	public ImageIcon off_switch = new javax.swing.ImageIcon(getClass().getResource("/resources/off.jpg"));
	public ImageIcon stryker_icon = new javax.swing.ImageIcon(getClass().getResource("/resources/Stryker.png"));
	public ImageIcon top_corner_logo = new javax.swing.ImageIcon(getClass().getResource("/resources/Logo3.png"));
	public ImageIcon rastu_logo = new javax.swing.ImageIcon(getClass().getResource("resources/Logo_1.png"));
	public static ImageIcon red_indicator = new javax.swing.ImageIcon(("resources/red2.png"));
	// public static ImageIcon green_indicator = new
	// javax.swing.ImageIcon(("resources/green2.png"));
	public ImageIcon green_indicator = new javax.swing.ImageIcon(getClass().getResource("resources/green2.png"));
	public ImageIcon green_switch = new javax.swing.ImageIcon(getClass().getResource("/resources/greenswitch.jpg"));
	public ImageIcon red_switch = new javax.swing.ImageIcon(getClass().getResource("/resources/redswitch.jpg"));
	public ImageIcon green_icon = new javax.swing.ImageIcon(getClass().getResource("/resources/green1.png"));

	// TextField Definitions
	public static JTextField textField_8;
	public static JTextField textField_9;
	public static JTextField textField_10;
	public static JTextField textField_11;
	public static JTextField textField_15;
	public static JTextField textField_14;
	public static JTextField textField_13;
	public static JTextField textField_12;
	public static JTextField textField_17;
	public static JTextField textField_18;
	public static JTextField textField_16;
	public static JTextField textField_23;
	public static JTextField textField_22;
	public static JTextField textField_21;
	public static JTextField textField_20;
	public static JTextField textField_27;
	public static JTextField textField_26;
	public static JTextField textField_25;
	public static JTextField textField_24;
	public static JTextField textField_28;
	public static JTextField textField_29;
	public static JTextField textField_30;
	public static JTextField textField_31;
	public static JTextField textField_32;
	public static JTextField textField_33;
	public static JTextField textField_34;
	public static JTextField textField_35;

	public static Component[] wl_mode;
	public static Component[] wl_source;
	public static Component[] wl_pwm_type;
	public static Component[] env_iris;
	public static Component[] env_source;
	public static Component[] env_pwm_type;
	
	//Added for UI alteration done for Absolute DAC Values
	public static JLabel IrisVoltage;
	public static JLabel IrisCurrent;
	public static JLabel IrispdVoltage;
	
	public static JLabel lblFibre1_Detect;
	public static JLabel label_20;
	public static JLabel lblFibre2_Detect;
	public static JLabel lblBeamSensor;
	public static JLabel lblIris_Pgood;
	public static JLabel label_6;
	public static JLabel label_7;
	public static JLabel label_8;
	public static JLabel label_9;
	public static JLabel lblV;
	public static JLabel lblI;
	public static JLabel lblIris_2;
	public static JLabel lblIris_3;
	public static JLabel lblIris1pd;
	public static JLabel lblIris2pd;
	public static JLabel lblGrp;
	public static JLabel label_5;
	public static JLabel label_17;
	public static JLabel label_18;
	public static JLabel label_19;
	public static JLabel lblFanFailure;
	public static JLabel lblTemperature;
	public static JLabel lblEnvpd;
	public static JLabel lblFixedValue_2;
	public static JLabel lblPwm_4;
	public static JLabel lblCwm;
	public static JLabel lblStb;
	public static JLabel lblLc;
	public static JLabel lblExt;
	public static JLabel lblUc1;
	public static JLabel lblUc2;
	public static JLabel label_21;
	public static JLabel label_22;
	public static JLabel label_23;
	public static JLabel lblPwm_8;
	public static JLabel lblCwm_1;
	public static JLabel lblCwmnr;
	public static JLabel lblLcnr;
	public static JLabel lblNewLabel_1;
	public static JLabel lblExtwht;
	public static JLabel lblExtenv;
	public static JLabel lblPowerSupply;
	public static JLabel lblHeatSink;
	public static JLabel lblIris;
	public static JLabel lblExt_1;
	public static JLabel lblUc_2;
	public static JLabel lblUc_3;
	public static JLabel lblEnv;
	public static JLabel label_24;
	public static JLabel label_25;
	public static JLabel label_26;
	public static JLabel lblCs;
	public static JLabel lblCs_1;
	public static JLabel lblCs_2;
	public static JLabel lblCs_3;
	public static JLabel lblCs_4;
	public static JLabel lblCs_5;
	public static JLabel lblCs_6;
	public static JLabel lblCs_7;
	public static JLabel lblLWl;
	public static JLabel lblLl;
	public static JLabel lblAdc_Red;
	public static JLabel lblAdc_Green;
	public static JLabel lblAdc_Blue;
	public static JLabel lblAdc_Laser;
	public static JLabel Iris1pd;
	public static JLabel Iris2pd;
	public static JLabel lblNewLabel;
	public static JLabel label_3;
	public static JLabel label_4;
	public static JLabel lblESST;
	public static JLabel lblAUX_CAM_IN1;
	public static JLabel lblAUX_CAM_In2;

	// Panel Definitions
	public JPanel panel_2;
	public JPanel panel_8;
	public JPanel panel_9;
	public JPanel panel_10;
	public JPanel panel_22;
	public JPanel panel_23;
	public JPanel panel_24;
	public static JPanel panel_26;
	public static JPanel panel_33;
	public static JPanel panel_3;
	public JPanel panel_13;
	public JPanel panel_14;
	public JPanel panel_25;
	public static JPanel panel_27;
	public static JPanel panel_28;
	public static JPanel panel_29;
	public static JPanel panel_30;
	public static JPanel panel_31;
	public static JPanel panel_32;
	public JPanel panel_36;

	// ToggleButton Definitions
	public static JToggleButton WlPowerButton;
	public static JToggleButton tglbtnFixed_Env;
	public static JToggleButton tglbtnFuncTest;
	
	public static JToggleButton EnvIrisPowerButton;
	
	public static JButton tglbtnFixed_Wl;

	// FormattedTextField Definitions
	public static JFormattedTextField formattedTextField_EnvPwm;
	public static JFormattedTextField formattedTextField_WlPwm;

	static ActionListener uart_response_timeout_task = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {

			uart_response_timer.stop();
			System.out.println("No Response from the device!!.");
		}

	};

	final static Runnable uart_recv_task = new Runnable() {
		@Override
		public void run() {
			System.out.println("Done");
		}

	};

	final static Runnable uart_recv_timeout_task = new Runnable() {

		@Override
		public void run()

		{
			SwingWorker recv_worker = new SwingWorker<List<String>, String>() {

				@Override
				protected List<String> doInBackground() throws Exception {
					if (full_buffer == null) {

						System.out.println("No data in the response!!");
						ManufacturingTests.busyflag = false;
						serialport.removeDataListener(); // Removes data listener associated with serial port Object
						total_bytes = 0;

					} else {

						// print("Some Response Atleast!!");
						// print(Arrays.toString(full_buffer.toByteArray()));
						print("Full Buffer size before calling function :" + full_buffer.size());
						byte[] recv_buff = full_buffer.toByteArray();

						try {
							full_buffer.flush();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						print("Receive Buffer size before calling function :" + recv_buff.length);

						serialport.removeDataListener();
						String result = "";
						result = new String(recv_buff, "UTF-8");
						System.out.println("Response to be processed : " + result);
						System.out.println("Inside the function");
						if (ManufacturingTests.MFTest_Flag == true) {
							ManufacturingTests.logs_logger.severe("Response Received\t" + result);

							full_buffer.reset();

							publish("Response Received : " + result + "\r\n");
							System.out.println("Receive Buffer in MTEST START: " + Arrays.toString(recv_buff));
							// System.out.println("Manufacturing Test Flag Set");
							ManufacturingTests.MFTest_Flag = false;
							try {
								String Resp = new String(recv_buff, "UTF-8");

								recv_buff = null;
								System.out.println("Response Received : " + Resp + " Of Length" + Resp.length());

								if (Resp.contains(RESPONSE_OK)) {

									try {
										String resp1 = Resp.replace("{", "");
										resp1 = resp1.replace("}", "");
										String[] resp = resp1.split(",");
										ManufacturingTests.logs_logger.severe("Response OK Received");
										// System.out.println(Arrays.deepToString(resp));
										// System.out.println("Response OK Received!!");
										recv_buff = null;
										Resp = "";

										if (ManufacturingTests.Current_Query_Number == 0) {

											System.out.println("Actual Array : " + Arrays.toString(resp));
											String Ver_No = resp[2];
											publish("VER_NO," + Ver_No);
											ManufacturingTests.Current_Query_Number = 1;
											Stryk_Demo.frmStrykerVer.setVisible(false);
											publish("SUB_PANEL");
											synchronized (ManufacturingTests.lockObject) {
												notify_flag = 1;
												ManufacturingTests.busyflag = false;
												ManufacturingTests.lockObject.notifyAll();
											}

										} else {

											String src = ManufacturingTests.get_source(resp[1]);

											// ManufacturingTests.UpdateTestResults(ManufacturingTests.Current_Query,Pass,"",src,"");

											ManufacturingTests.logs_logger.severe("Result\t\t\tPass");
											ManufacturingTests.report_logger.severe("Result\t\t\tPass");
											// LIGHT ENGINE FAN TEST
											if (ManufacturingTests.lightenginefantest
													.equals(ManufacturingTests.check_string)) {
												String Fan_Count = resp[2];
												ManufacturingTests.report_logger.severe("Expected Tach Count -->  : "
														+ ManufacturingTests.Expected_LightEngine_FanTachCount
														+ "\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Tach Count -->  : "
														+ ManufacturingTests.Expected_LightEngine_FanTachCount
														+ "\t(+/- 25%)");
												ManufacturingTests.logs_logger
														.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");
												ManufacturingTests.check_string = "";
											} // HEAT SINK FAN TEST
											else if (ManufacturingTests.heatsinkfantest
													.equals(ManufacturingTests.check_string)) {
												String Fan_Count = resp[2];
												ManufacturingTests.report_logger.severe("Expected Tach Count -->  : "
														+ ManufacturingTests.Expected_HeatSink_FanTachCount
														+ "\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Tach Count -->  : "
														+ ManufacturingTests.Expected_HeatSink_FanTachCount
														+ "\t(+/- 25%)");
												ManufacturingTests.logs_logger
														.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");
												ManufacturingTests.check_string = "";
											}
											// IRIS FAN TEST
											else if (ManufacturingTests.irisfantest
													.equals(ManufacturingTests.check_string)) {
												String Fan_Count = resp[2];
												ManufacturingTests.report_logger.severe("Expected Tach Count -->  : "
														+ ManufacturingTests.Expected_Iris_FanTachCount
														+ "\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Tach Count -->  : "
														+ ManufacturingTests.Expected_Iris_FanTachCount
														+ "\t(+/- 25%)");
												ManufacturingTests.logs_logger
														.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");
												ManufacturingTests.check_string = "";
											}

											// LED TEST RESULT : LED ZERO PERCENT
											else if (ManufacturingTests.redledtestzero
													.equals(ManufacturingTests.check_string)) {
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if (ManufacturingTests.chckbxLightEngine.isSelected()) {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_RedLed_Current_zeropercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_RedLed_Current_zeropercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												} else {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}

												ManufacturingTests.check_string = "";
											} else if (ManufacturingTests.greenledtestzero
													.equals(ManufacturingTests.check_string)) {
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if (ManufacturingTests.chckbxLightEngine.isSelected()) {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_GreenLed_Current_zeropercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_GreenLed_Current_zeropercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												} else {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												ManufacturingTests.check_string = "";
											} else if (ManufacturingTests.blueledtestzero
													.equals(ManufacturingTests.check_string)) {
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if (ManufacturingTests.chckbxLightEngine.isSelected()) {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_BlueLed_Current_zeropercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_BlueLed_Current_zeropercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												} else {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												ManufacturingTests.check_string = "";
											} else if (ManufacturingTests.laserledtestzero
													.equals(ManufacturingTests.check_string)) {
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if (ManufacturingTests.chckbxLightEngine.isSelected()) {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_LaserLed_Current_zeropercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_LaserLed_Current_zeropercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												} else {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												ManufacturingTests.check_string = "";
											} else if (ManufacturingTests.iris1ledtestzero
													.equals(ManufacturingTests.check_string)) {
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if (ManufacturingTests.chckbxLightEngine.isSelected()) {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_Iris1Led_Current_zeropercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_Iris1Led_Current_zeropercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												} else {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												ManufacturingTests.check_string = "";
											} else if (ManufacturingTests.iris2ledtestzero
													.equals(ManufacturingTests.check_string)) {
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if (ManufacturingTests.chckbxLightEngine.isSelected()) {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_Iris2Led_Current_zeropercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_Iris2Led_Current_zeropercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												} else {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												ManufacturingTests.check_string = "";
											}

											// LED TEST RESULT : LED FIFTY PERCENT

											else if (ManufacturingTests.redledtestfifty
													.equals(ManufacturingTests.check_string)) {
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if (ManufacturingTests.chckbxLightEngine.isSelected()) {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_RedLed_Voltage_fiftypercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_RedLed_Current_fiftypercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_RedLed_Voltage_fiftypercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_RedLed_Current_fiftypercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												} else {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}

												ManufacturingTests.check_string = "";
											} else if (ManufacturingTests.greenledtestfifty
													.equals(ManufacturingTests.check_string)) {
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if (ManufacturingTests.chckbxLightEngine.isSelected()) {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_GreenLed_Voltage_fiftypercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_GreenLed_Current_fiftypercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_GreenLed_Voltage_fiftypercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_GreenLed_Current_fiftypercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												} else {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}

												ManufacturingTests.check_string = "";
											} else if (ManufacturingTests.blueledtestfifty
													.equals(ManufacturingTests.check_string)) {
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if (ManufacturingTests.chckbxLightEngine.isSelected()) {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_BlueLed_Voltage_fiftypercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_BlueLed_Current_fiftypercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_BlueLed_Voltage_fiftypercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_BlueLed_Current_fiftypercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												} else {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}

												ManufacturingTests.check_string = "";
											} else if (ManufacturingTests.laserledtestfifty
													.equals(ManufacturingTests.check_string)) {
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if (ManufacturingTests.chckbxLightEngine.isSelected()) {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_LaserLed_Voltage_fiftypercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_LaserLed_Current_fiftypercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_LaserLed_Voltage_fiftypercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_LaserLed_Current_fiftypercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												} else {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}

												ManufacturingTests.check_string = "";
											} else if (ManufacturingTests.iris1ledtestfifty
													.equals(ManufacturingTests.check_string)) {
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if (ManufacturingTests.chckbxLightEngine.isSelected()) {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris1Led_Voltage_fiftypercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_Iris1Led_Current_fiftypercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris1Led_Voltage_fiftypercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_Iris1Led_Current_fiftypercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												} else {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}

												ManufacturingTests.check_string = "";
											} else if (ManufacturingTests.iris2ledtestfifty
													.equals(ManufacturingTests.check_string)) {
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if (ManufacturingTests.chckbxLightEngine.isSelected()) {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris2Led_Voltage_fiftypercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_Iris2Led_Current_fiftypercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris2Led_Voltage_fiftypercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_Iris2Led_Current_fiftypercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												} else {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}

												ManufacturingTests.check_string = "";
											}
											
											
											//LED TEST RESULT : LED SEVENTY FIVE PERCENT
											
											else if(ManufacturingTests.redledtestseventyfive.equals(ManufacturingTests.check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(ManufacturingTests.chckbxLightEngine.isSelected())
												{
													ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_seventyfivepercent + " A\t(+/- 25%)");
													ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_seventyfivepercent + " A\t(+/- 25%)");
													ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												ManufacturingTests.check_string = "";
											}
											else if(ManufacturingTests.greenledtestseventyfive.equals(ManufacturingTests.check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(ManufacturingTests.chckbxLightEngine.isSelected())
												{
													ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_seventyfivepercent + " A\t(+/- 25%)");
													ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_seventyfivepercent + " A\t(+/- 25%)");
													ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												ManufacturingTests.check_string = "";
											}
											else if(ManufacturingTests.blueledtestseventyfive.equals(ManufacturingTests.check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(ManufacturingTests.chckbxLightEngine.isSelected())
												{
													ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_seventyfivepercent + " A\t(+/- 25%)");
													ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_seventyfivepercent + " A\t(+/- 25%)");
													ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												ManufacturingTests.check_string = "";
											}
											else if(ManufacturingTests.laserledtestseventyfive.equals(ManufacturingTests.check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(ManufacturingTests.chckbxLightEngine.isSelected())
												{
													ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_seventyfivepercent + " A\t(+/- 25%)");
													ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_seventyfivepercent + " A\t(+/- 25%)");
													ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												ManufacturingTests.check_string = "";
											}
											else if(ManufacturingTests.iris1ledtestseventyfive.equals(ManufacturingTests.check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(ManufacturingTests.chckbxLightEngine.isSelected())
												{
													ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_seventyfivepercent + " A\t(+/- 25%)");
													ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_seventyfivepercent + " A\t(+/- 25%)");
													ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												ManufacturingTests.check_string = "";
											}
											else if(ManufacturingTests.iris2ledtestseventyfive.equals(ManufacturingTests.check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(ManufacturingTests.chckbxLightEngine.isSelected())
												{
													ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_seventyfivepercent + " A\t(+/- 25%)");
													ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_seventyfivepercent + " A\t(+/- 25%)");
													ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												ManufacturingTests.check_string = "";
											}
											// LED TEST RESULT : LED HUNDRED PERCENT

											else if (ManufacturingTests.redledtesthundred
													.equals(ManufacturingTests.check_string)) {
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if (ManufacturingTests.chckbxLightEngine.isSelected()) {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_RedLed_Voltage_hundredpercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_RedLed_Current_hundredpercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_RedLed_Voltage_hundredpercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_RedLed_Current_hundredpercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												} else {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}

												ManufacturingTests.check_string = "";
											} else if (ManufacturingTests.greenledtesthundred
													.equals(ManufacturingTests.check_string)) {
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if (ManufacturingTests.chckbxLightEngine.isSelected()) {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_GreenLed_Voltage_hundredpercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_GreenLed_Current_hundredpercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_GreenLed_Voltage_hundredpercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_GreenLed_Current_hundredpercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												} else {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												ManufacturingTests.check_string = "";
											} else if (ManufacturingTests.blueledtesthundred
													.equals(ManufacturingTests.check_string)) {
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if (ManufacturingTests.chckbxLightEngine.isSelected()) {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_BlueLed_Voltage_hundredpercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_BlueLed_Current_hundredpercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_BlueLed_Voltage_hundredpercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_BlueLed_Current_hundredpercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												} else {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
											} else if (ManufacturingTests.laserledtesthundred
													.equals(ManufacturingTests.check_string)) {
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if (ManufacturingTests.chckbxLightEngine.isSelected()) {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_LaserLed_Voltage_hundredpercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_LaserLed_Current_hundredpercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_LaserLed_Voltage_hundredpercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_LaserLed_Current_hundredpercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												} else {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}

												ManufacturingTests.check_string = "";
											} else if (ManufacturingTests.iris1ledtesthundred
													.equals(ManufacturingTests.check_string)) {
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if (ManufacturingTests.chckbxLightEngine.isSelected()) {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris1Led_Voltage_hundredpercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_Iris1Led_Current_hundredpercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris1Led_Voltage_hundredpercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_Iris1Led_Current_hundredpercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												} else {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												ManufacturingTests.check_string = "";
											} else if (ManufacturingTests.iris2ledtesthundred
													.equals(ManufacturingTests.check_string)) {
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if (ManufacturingTests.chckbxLightEngine.isSelected()) {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris2Led_Voltage_hundredpercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_Iris2Led_Current_hundredpercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris2Led_Voltage_hundredpercent
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.Exp_Iris2Led_Current_hundredpercent
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												} else {
													ManufacturingTests.report_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.report_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

													ManufacturingTests.logs_logger
															.severe("Expected Values --> \tVoltage : "
																	+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
																	+ " V\t\tCurrent : "
																	+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																	+ " A\t(+/- 25%)");
													ManufacturingTests.logs_logger
															.severe("Observed Values --> \tVoltage : " + LedVoltage
																	+ " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}

												ManufacturingTests.check_string = "";
											}

											// PDTESTS : IRIS1ZERO and HUNDRED
											else if (ManufacturingTests.iris1pdtestzero
													.equals(ManufacturingTests.check_string)) {
												String PhotoVoltage = resp[2];
												ManufacturingTests.report_logger
														.severe("Expected Value --> \tVoltage : "
																+ ManufacturingTests.Exp_iris1pdvoltage_zero
																+ " V\t(+/- 25%)");
												ManufacturingTests.report_logger.severe(
														"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
														+ ManufacturingTests.Exp_iris1pdvoltage_zero + " V\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe(
														"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
											} else if (ManufacturingTests.iris1pdtesthundred
													.equals(ManufacturingTests.check_string)) {
												String PhotoVoltage = resp[2];
												ManufacturingTests.report_logger
														.severe("Expected Value --> \tVoltage : "
																+ ManufacturingTests.Exp_iris1pdvoltage_hundred
																+ " V\t(+/- 25%)");
												ManufacturingTests.report_logger.severe(
														"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
														+ ManufacturingTests.Exp_iris1pdvoltage_hundred
														+ " V\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe(
														"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
											}

											// PDTESTS : IRIS2 ZERO AND HUNDRED

											else if (ManufacturingTests.iris2pdtestzero
													.equals(ManufacturingTests.check_string)) {
												String PhotoVoltage = resp[2];
												ManufacturingTests.report_logger
														.severe("Expected Value --> \tVoltage : "
																+ ManufacturingTests.Exp_iris2pdvoltage_zero
																+ " V\t(+/- 25%)");
												ManufacturingTests.report_logger.severe(
														"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
														+ ManufacturingTests.Exp_iris2pdvoltage_zero + " V\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe(
														"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
											} else if (ManufacturingTests.iris2pdtesthundred
													.equals(ManufacturingTests.check_string)) {
												String PhotoVoltage = resp[2];
												ManufacturingTests.report_logger
														.severe("Expected Value --> \tVoltage : "
																+ ManufacturingTests.Exp_iris2pdvoltage_hundred
																+ " V\t(+/- 25%)");
												ManufacturingTests.report_logger.severe(
														"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
														+ ManufacturingTests.Exp_iris2pdvoltage_hundred
														+ " V\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe(
														"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
											}
											// PDTESTS : ENV ZERO AND HUNDRED

											else if (ManufacturingTests.envpdtestzero
													.equals(ManufacturingTests.check_string)) {
												String PhotoVoltage = resp[2];
												ManufacturingTests.report_logger
														.severe("Expected Value --> \tVoltage : "
																+ ManufacturingTests.Exp_envpdvoltage_zero
																+ " V\t(+/- 25%)");
												ManufacturingTests.report_logger.severe(
														"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
														+ ManufacturingTests.Exp_envpdvoltage_zero + " V\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe(
														"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
											} else if (ManufacturingTests.envpdtesthundred
													.equals(ManufacturingTests.check_string)) {
												String PhotoVoltage = resp[2];
												ManufacturingTests.report_logger
														.severe("Expected Value --> \tVoltage : "
																+ ManufacturingTests.Exp_envpdvoltage_hundred
																+ " V\t(+/- 25%)");
												ManufacturingTests.report_logger.severe(
														"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
														+ ManufacturingTests.Exp_envpdvoltage_hundred
														+ " V\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe(
														"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
											}
											// CS TESTS : COLORSENSOR 1

											else if (ManufacturingTests.colorsensor1test
													.equals(ManufacturingTests.check_string)) {
												String ColorsensorVal_1 = resp[2];
												String ColorsensorVal_2 = resp[3];
												String ColorsensorVal_3 = resp[4];
												String ColorsensorVal_4 = resp[5];
												ManufacturingTests.report_logger
														.severe("Expected Value --> \t >100 and below \"FFFF\"");
												ManufacturingTests.report_logger.severe(
														"Observed Value --> \tVal 1 : " + ColorsensorVal_1 + " Val 2 : "
																+ ColorsensorVal_2 + " Val 3 : " + ColorsensorVal_3
																+ " Val 4 : " + ColorsensorVal_4 + "\r\n\n");

												ManufacturingTests.logs_logger
														.severe("Expected Value --> \t >100 and below \"FFFF\"");
												ManufacturingTests.logs_logger.severe(
														"Observed Value --> \tVal 1 : " + ColorsensorVal_1 + " Val 2 : "
																+ ColorsensorVal_2 + " Val 3 : " + ColorsensorVal_3
																+ " Val 4 : " + ColorsensorVal_4 + "\r\n\n");
											} else if (ManufacturingTests.colorsensor2test
													.equals(ManufacturingTests.check_string)) {
												String ColorsensorVal_1 = resp[2];
												String ColorsensorVal_2 = resp[3];
												String ColorsensorVal_3 = resp[4];
												String ColorsensorVal_4 = resp[5];
												ManufacturingTests.report_logger
														.severe("Expected Value --> \t >100 and below \"FFFF\"");
												ManufacturingTests.report_logger.severe(
														"Observed Value --> \tVal 1 : " + ColorsensorVal_1 + " Val 2 : "
																+ ColorsensorVal_2 + " Val 3 : " + ColorsensorVal_3
																+ " Val 4 : " + ColorsensorVal_4 + "\r\n\n");

												ManufacturingTests.logs_logger
														.severe("Expected Value --> \t >100 and below \"FFFF\"");
												ManufacturingTests.logs_logger.severe(
														"Observed Value --> \tVal 1 : " + ColorsensorVal_1 + " Val 2 : "
																+ ColorsensorVal_2 + " Val 3 : " + ColorsensorVal_3
																+ " Val 4 : " + ColorsensorVal_4 + "\r\n\n");
											}

											// PWM MUXTESTS
											else if (ManufacturingTests.wluc1pwmmuxtest
													.equals(ManufacturingTests.check_string)) {
												String MuxInterrupt_Count = resp[2];
												ManufacturingTests.report_logger.severe("Expected Value --> \t"
														+ ManufacturingTests.Exp_wluc1pwmmuxtest_intcount + " (+/- 3)");
												ManufacturingTests.report_logger.severe(
														"Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Value --> \t"
														+ ManufacturingTests.Exp_wluc1pwmmuxtest_intcount + " (+/- 3)");
												ManufacturingTests.logs_logger.severe(
														"Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
											} else if (ManufacturingTests.envuc1pwmmuxtest
													.equals(ManufacturingTests.check_string)) {
												String MuxInterrupt_Count = resp[2];
												ManufacturingTests.report_logger.severe("Expected Value --> \t"
														+ ManufacturingTests.Exp_envuc1pwmmuxtest_intcount
														+ " (+/- 3)");
												ManufacturingTests.report_logger.severe(
														"Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Value --> \t"
														+ ManufacturingTests.Exp_envuc1pwmmuxtest_intcount
														+ " (+/- 3)");
												ManufacturingTests.logs_logger.severe(
														"Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
											} else if (ManufacturingTests.irisuc1pwmmuxtest
													.equals(ManufacturingTests.check_string)) {
												String MuxInterrupt_Count = resp[2];
												ManufacturingTests.report_logger.severe("Expected Value --> \t"
														+ ManufacturingTests.Exp_irisuc1pwmmuxtest_intcount
														+ " (+/- 3)");
												ManufacturingTests.report_logger.severe(
														"Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Value --> \t"
														+ ManufacturingTests.Exp_irisuc1pwmmuxtest_intcount
														+ " (+/- 3)");
												ManufacturingTests.logs_logger.severe(
														"Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
											} else if (ManufacturingTests.wluc2pwmmuxtest
													.equals(ManufacturingTests.check_string)) {
												String MuxInterrupt_Count = resp[2];
												ManufacturingTests.report_logger.severe("Expected Value --> \t"
														+ ManufacturingTests.Exp_wluc2pwmmuxtest_intcount + " (+/- 3)");
												ManufacturingTests.report_logger.severe(
														"Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Value --> \t"
														+ ManufacturingTests.Exp_wluc2pwmmuxtest_intcount + " (+/- 3)");
												ManufacturingTests.logs_logger.severe(
														"Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
											} else if (ManufacturingTests.envuc2pwmmuxtest
													.equals(ManufacturingTests.check_string)) {
												String MuxInterrupt_Count = resp[2];
												ManufacturingTests.report_logger.severe("Expected Value --> \t"
														+ ManufacturingTests.Exp_envuc2pwmmuxtest_intcount
														+ " (+/- 3)");
												ManufacturingTests.report_logger.severe(
														"Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Value --> \t"
														+ ManufacturingTests.Exp_envuc2pwmmuxtest_intcount
														+ " (+/- 3)");
												ManufacturingTests.logs_logger.severe(
														"Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
											} else if (ManufacturingTests.irisuc2pwmmuxtest
													.equals(ManufacturingTests.check_string)) {
												String MuxInterrupt_Count = resp[2];
												ManufacturingTests.report_logger.severe("Expected Value --> \t"
														+ ManufacturingTests.Exp_irisuc2pwmmuxtest_intcount
														+ " (+/- 3)");
												ManufacturingTests.report_logger.severe(
														"Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Value --> \t"
														+ ManufacturingTests.Exp_irisuc2pwmmuxtest_intcount
														+ " (+/- 3)");
												ManufacturingTests.logs_logger.severe(
														"Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
											} else if (ManufacturingTests.wlextpwmmuxtest
													.equals(ManufacturingTests.check_string)) {
												String MuxInterrupt_Count = resp[2];
												ManufacturingTests.report_logger.severe("Expected Value --> \t"
														+ ManufacturingTests.Exp_wlextpwmmuxtest_intcount + " (+/- 3)");
												ManufacturingTests.report_logger.severe(
														"Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Value --> \t"
														+ ManufacturingTests.Exp_wlextpwmmuxtest_intcount + " (+/- 3)");
												ManufacturingTests.logs_logger.severe(
														"Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
											} else if (ManufacturingTests.envextpwmmuxtest
													.equals(ManufacturingTests.check_string)) {
												String MuxInterrupt_Count = resp[2];
												ManufacturingTests.report_logger.severe("Expected Value --> \t"
														+ ManufacturingTests.Exp_envextpwmmuxtest_intcount
														+ " (+/- 3)");
												ManufacturingTests.report_logger.severe(
														"Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Value --> \t"
														+ ManufacturingTests.Exp_envextpwmmuxtest_intcount
														+ " (+/- 3)");
												ManufacturingTests.logs_logger.severe(
														"Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
											} else if (ManufacturingTests.irisextwlpwmmuxtest
													.equals(ManufacturingTests.check_string)) {
												String MuxInterrupt_Count = resp[2];
												ManufacturingTests.report_logger.severe("Expected Value --> \t"
														+ ManufacturingTests.Exp_irisextwlpwmmuxtest_intcount
														+ " (+/- 3)");
												ManufacturingTests.report_logger.severe(
														"Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Value --> \t"
														+ ManufacturingTests.Exp_irisextwlpwmmuxtest_intcount
														+ " (+/- 3)");
												ManufacturingTests.logs_logger.severe(
														"Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
											} else if (ManufacturingTests.irisextenvpwmmuxtest
													.equals(ManufacturingTests.check_string)) {
												String MuxInterrupt_Count = resp[2];
												ManufacturingTests.report_logger.severe("Expected Value --> \t"
														+ ManufacturingTests.Exp_irisextenvpwmmuxtest_intcount
														+ " (+/- 3)");
												ManufacturingTests.report_logger.severe(
														"Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Value --> \t"
														+ ManufacturingTests.Exp_irisextenvpwmmuxtest_intcount
														+ " (+/- 3)");
												ManufacturingTests.logs_logger.severe(
														"Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
											}
											ManufacturingTests.report_logger.severe(ManufacturingTests.ArrowDelimitter);

											// publish("UPDATE,PASS," + ""+"," + src + "," + "");
											ManufacturingTests.UpdateTestResults(Pass, "", src, "");
											synchronized (ManufacturingTests.lockObject) {
												notify_flag = 1;
												ManufacturingTests.busyflag = false;
												ManufacturingTests.lockObject.notifyAll();
											}

										}

									} catch (Exception ex) {
										ex.printStackTrace();

									}

								} else if (Resp.contains(RESPONSE_ERROR)) {

									try {

										System.out.println("Error Response Received!!");

										String resp = Resp.replace("{", "");
										resp = resp.replace("}", "");
										String[] actual = resp.split(",");
										String reason = ManufacturingTests.get_error_source(actual[1]);
										String source = ManufacturingTests.get_source(actual[2]);
										String value = "";

										ManufacturingTests.report_logger.severe("Result\t\t\tFail");
										ManufacturingTests.logs_logger.severe("Result\t\t\tFail");
										if (ManufacturingTests.lightenginefantest
												.equals(ManufacturingTests.check_string)) {
											String Fan_Count = actual[3];
											ManufacturingTests.report_logger.severe("Expected Tach Count -->  : "
													+ ManufacturingTests.Expected_LightEngine_FanTachCount
													+ "\t(+/- 25%)");
											ManufacturingTests.report_logger
													.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");

											ManufacturingTests.logs_logger.severe("Expected Tach Count -->  : "
													+ ManufacturingTests.Expected_LightEngine_FanTachCount
													+ "\t(+/- 25%)");
											ManufacturingTests.logs_logger
													.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");
											ManufacturingTests.check_string = "";
										} // HEAT SINK FAN TEST
										else if (ManufacturingTests.heatsinkfantest
												.equals(ManufacturingTests.check_string)) {
											String Fan_Count = actual[3];
											ManufacturingTests.report_logger.severe("Expected Tach Count -->  : "
													+ ManufacturingTests.Expected_HeatSink_FanTachCount
													+ "\t(+/- 25%)");
											ManufacturingTests.report_logger
													.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");

											ManufacturingTests.logs_logger.severe("Expected Tach Count -->  : "
													+ ManufacturingTests.Expected_HeatSink_FanTachCount
													+ "\t(+/- 25%)");
											ManufacturingTests.logs_logger
													.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");
											ManufacturingTests.check_string = "";
										}
										// IRIS FAN TEST
										else if (ManufacturingTests.irisfantest
												.equals(ManufacturingTests.check_string)) {
											String Fan_Count = actual[3];
											ManufacturingTests.report_logger.severe("Expected Tach Count -->  : "
													+ ManufacturingTests.Expected_Iris_FanTachCount + "\t(+/- 25%)");
											ManufacturingTests.report_logger
													.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");

											ManufacturingTests.logs_logger.severe("Expected Tach Count -->  : "
													+ ManufacturingTests.Expected_Iris_FanTachCount + "\t(+/- 25%)");
											ManufacturingTests.logs_logger
													.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");
											ManufacturingTests.check_string = "";
										} else if (ManufacturingTests.redledtestzero
												.equals(ManufacturingTests.check_string)) {
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if (ManufacturingTests.chckbxLightEngine.isSelected()) {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent
																+ " V\t\tCurrent : "
																+ ManufacturingTests.Exp_RedLed_Current_zeropercent
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent
														+ " V\t\tCurrent : "
														+ ManufacturingTests.Exp_RedLed_Current_zeropercent
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											} else {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
																+ " V\t\tCurrent : "
																+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
														+ " V\t\tCurrent : "
														+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}

											ManufacturingTests.check_string = "";
										} else if (ManufacturingTests.greenledtestzero
												.equals(ManufacturingTests.check_string)) {
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if (ManufacturingTests.chckbxLightEngine.isSelected()) {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent
																+ " V\t\tCurrent : "
																+ ManufacturingTests.Exp_GreenLed_Current_zeropercent
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent
														+ " V\t\tCurrent : "
														+ ManufacturingTests.Exp_GreenLed_Current_zeropercent
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											} else {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
																+ " V\t\tCurrent : "
																+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
														+ " V\t\tCurrent : "
														+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											ManufacturingTests.check_string = "";
										} else if (ManufacturingTests.blueledtestzero
												.equals(ManufacturingTests.check_string)) {
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if (ManufacturingTests.chckbxLightEngine.isSelected()) {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent
																+ " V\t\tCurrent : "
																+ ManufacturingTests.Exp_BlueLed_Current_zeropercent
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent
														+ " V\t\tCurrent : "
														+ ManufacturingTests.Exp_BlueLed_Current_zeropercent
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											} else {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
																+ " V\t\tCurrent : "
																+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
														+ " V\t\tCurrent : "
														+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											ManufacturingTests.check_string = "";
										} else if (ManufacturingTests.laserledtestzero
												.equals(ManufacturingTests.check_string)) {
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if (ManufacturingTests.chckbxLightEngine.isSelected()) {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent
																+ " V\t\tCurrent : "
																+ ManufacturingTests.Exp_LaserLed_Current_zeropercent
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent
														+ " V\t\tCurrent : "
														+ ManufacturingTests.Exp_LaserLed_Current_zeropercent
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											} else {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
																+ " V\t\tCurrent : "
																+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
														+ " V\t\tCurrent : "
														+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											ManufacturingTests.check_string = "";
										} else if (ManufacturingTests.iris1ledtestzero
												.equals(ManufacturingTests.check_string)) {
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if (ManufacturingTests.chckbxLightEngine.isSelected()) {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent
																+ " V\t\tCurrent : "
																+ ManufacturingTests.Exp_Iris1Led_Current_zeropercent
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent
														+ " V\t\tCurrent : "
														+ ManufacturingTests.Exp_Iris1Led_Current_zeropercent
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											} else {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
																+ " V\t\tCurrent : "
																+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
														+ " V\t\tCurrent : "
														+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											ManufacturingTests.check_string = "";
										} else if (ManufacturingTests.iris2ledtestzero
												.equals(ManufacturingTests.check_string)) {
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if (ManufacturingTests.chckbxLightEngine.isSelected()) {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent
																+ " V\t\tCurrent : "
																+ ManufacturingTests.Exp_Iris2Led_Current_zeropercent
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent
														+ " V\t\tCurrent : "
														+ ManufacturingTests.Exp_Iris2Led_Current_zeropercent
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											} else {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
																+ " V\t\tCurrent : "
																+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
														+ " V\t\tCurrent : "
														+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											ManufacturingTests.check_string = "";
										}

										// LED TEST RESULT : LED FIFTY PERCENT

										else if (ManufacturingTests.redledtestfifty
												.equals(ManufacturingTests.check_string)) {
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if (ManufacturingTests.chckbxLightEngine.isSelected()) {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_RedLed_Voltage_fiftypercent
																+ " V\t\tCurrent : "
																+ ManufacturingTests.Exp_RedLed_Current_fiftypercent
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_RedLed_Voltage_fiftypercent
														+ " V\t\tCurrent : "
														+ ManufacturingTests.Exp_RedLed_Current_fiftypercent
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											} else {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
																+ " V\t\tCurrent : "
																+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
														+ " V\t\tCurrent : "
														+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}

											ManufacturingTests.check_string = "";
										} else if (ManufacturingTests.greenledtestfifty
												.equals(ManufacturingTests.check_string)) {
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if (ManufacturingTests.chckbxLightEngine.isSelected()) {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_GreenLed_Voltage_fiftypercent
																+ " V\t\tCurrent : "
																+ ManufacturingTests.Exp_GreenLed_Current_fiftypercent
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_GreenLed_Voltage_fiftypercent
														+ " V\t\tCurrent : "
														+ ManufacturingTests.Exp_GreenLed_Current_fiftypercent
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											} else {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
																+ " V\t\tCurrent : "
																+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
														+ " V\t\tCurrent : "
														+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}

											ManufacturingTests.check_string = "";
										} else if (ManufacturingTests.blueledtestfifty
												.equals(ManufacturingTests.check_string)) {
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if (ManufacturingTests.chckbxLightEngine.isSelected()) {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_BlueLed_Voltage_fiftypercent
																+ " V\t\tCurrent : "
																+ ManufacturingTests.Exp_BlueLed_Current_fiftypercent
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_BlueLed_Voltage_fiftypercent
														+ " V\t\tCurrent : "
														+ ManufacturingTests.Exp_BlueLed_Current_fiftypercent
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											} else {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
																+ " V\t\tCurrent : "
																+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
														+ " V\t\tCurrent : "
														+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}

											ManufacturingTests.check_string = "";
										} else if (ManufacturingTests.laserledtestfifty
												.equals(ManufacturingTests.check_string)) {
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if (ManufacturingTests.chckbxLightEngine.isSelected()) {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_LaserLed_Voltage_fiftypercent
																+ " V\t\tCurrent : "
																+ ManufacturingTests.Exp_LaserLed_Current_fiftypercent
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_LaserLed_Voltage_fiftypercent
														+ " V\t\tCurrent : "
														+ ManufacturingTests.Exp_LaserLed_Current_fiftypercent
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											} else {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
																+ " V\t\tCurrent : "
																+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
														+ " V\t\tCurrent : "
														+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}

											ManufacturingTests.check_string = "";
										} else if (ManufacturingTests.iris1ledtestfifty
												.equals(ManufacturingTests.check_string)) {
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if (ManufacturingTests.chckbxLightEngine.isSelected()) {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_Iris1Led_Voltage_fiftypercent
																+ " V\t\tCurrent : "
																+ ManufacturingTests.Exp_Iris1Led_Current_fiftypercent
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_Iris1Led_Voltage_fiftypercent
														+ " V\t\tCurrent : "
														+ ManufacturingTests.Exp_Iris1Led_Current_fiftypercent
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											} else {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
																+ " V\t\tCurrent : "
																+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
														+ " V\t\tCurrent : "
														+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}

											ManufacturingTests.check_string = "";
										} else if (ManufacturingTests.iris2ledtestfifty
												.equals(ManufacturingTests.check_string)) {
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if (ManufacturingTests.chckbxLightEngine.isSelected()) {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_Iris2Led_Voltage_fiftypercent
																+ " V\t\tCurrent : "
																+ ManufacturingTests.Exp_Iris2Led_Current_fiftypercent
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_Iris2Led_Voltage_fiftypercent
														+ " V\t\tCurrent : "
														+ ManufacturingTests.Exp_Iris2Led_Current_fiftypercent
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											} else {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
																+ " V\t\tCurrent : "
																+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
														+ " V\t\tCurrent : "
														+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}

											ManufacturingTests.check_string = "";
										}
										
										//LED TEST RESULT : LED SEVENTY FIVE PERCENT
										
										else if(ManufacturingTests.redledtestseventyfive.equals(ManufacturingTests.check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(ManufacturingTests.chckbxLightEngine.isSelected())
											{
												ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_seventyfivepercent + " A\t(+/- 25%)");
												ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_seventyfivepercent + " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											ManufacturingTests.check_string = "";
										}
										else if(ManufacturingTests.greenledtestseventyfive.equals(ManufacturingTests.check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(ManufacturingTests.chckbxLightEngine.isSelected())
											{
												ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_seventyfivepercent + " A\t(+/- 25%)");
												ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_seventyfivepercent + " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											ManufacturingTests.check_string = "";
										}
										else if(ManufacturingTests.blueledtestseventyfive.equals(ManufacturingTests.check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(ManufacturingTests.chckbxLightEngine.isSelected())
											{
												ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_seventyfivepercent + " A\t(+/- 25%)");
												ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_seventyfivepercent + " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											ManufacturingTests.check_string = "";
										}
										else if(ManufacturingTests.laserledtestseventyfive.equals(ManufacturingTests.check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(ManufacturingTests.chckbxLightEngine.isSelected())
											{
												ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_seventyfivepercent + " A\t(+/- 25%)");
												ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_seventyfivepercent + " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											ManufacturingTests.check_string = "";
										}
										else if(ManufacturingTests.iris1ledtestseventyfive.equals(ManufacturingTests.check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(ManufacturingTests.chckbxLightEngine.isSelected())
											{
												ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_seventyfivepercent + " A\t(+/- 25%)");
												ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_seventyfivepercent + " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											ManufacturingTests.check_string = "";
										}
										else if(ManufacturingTests.iris2ledtestseventyfive.equals(ManufacturingTests.check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(ManufacturingTests.chckbxLightEngine.isSelected())
											{
												ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_seventyfivepercent + " A\t(+/- 25%)");
												ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_seventyfivepercent + " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											ManufacturingTests.check_string = "";
										}
										// LED TEST RESULT : LED HUNDRED PERCENT

										else if (ManufacturingTests.redledtesthundred
												.equals(ManufacturingTests.check_string)) {
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if (ManufacturingTests.chckbxLightEngine.isSelected()) {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_RedLed_Voltage_hundredpercent
																+ " V\t\tCurrent : "
																+ ManufacturingTests.Exp_RedLed_Current_hundredpercent
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_RedLed_Voltage_hundredpercent
														+ " V\t\tCurrent : "
														+ ManufacturingTests.Exp_RedLed_Current_hundredpercent
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											} else {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
																+ " V\t\tCurrent : "
																+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
														+ " V\t\tCurrent : "
														+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}

											ManufacturingTests.check_string = "";
										} else if (ManufacturingTests.greenledtesthundred
												.equals(ManufacturingTests.check_string)) {
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if (ManufacturingTests.chckbxLightEngine.isSelected()) {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_GreenLed_Voltage_hundredpercent
																+ " V\t\tCurrent : "
																+ ManufacturingTests.Exp_GreenLed_Current_hundredpercent
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_GreenLed_Voltage_hundredpercent
														+ " V\t\tCurrent : "
														+ ManufacturingTests.Exp_GreenLed_Current_hundredpercent
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											} else {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
																+ " V\t\tCurrent : "
																+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
														+ " V\t\tCurrent : "
														+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											ManufacturingTests.check_string = "";
										} else if (ManufacturingTests.blueledtesthundred
												.equals(ManufacturingTests.check_string)) {
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if (ManufacturingTests.chckbxLightEngine.isSelected()) {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_BlueLed_Voltage_hundredpercent
																+ " V\t\tCurrent : "
																+ ManufacturingTests.Exp_BlueLed_Current_hundredpercent
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_BlueLed_Voltage_hundredpercent
														+ " V\t\tCurrent : "
														+ ManufacturingTests.Exp_BlueLed_Current_hundredpercent
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											} else {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
																+ " V\t\tCurrent : "
																+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
														+ " V\t\tCurrent : "
														+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
										} else if (ManufacturingTests.laserledtesthundred
												.equals(ManufacturingTests.check_string)) {
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if (ManufacturingTests.chckbxLightEngine.isSelected()) {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_LaserLed_Voltage_hundredpercent
																+ " V\t\tCurrent : "
																+ ManufacturingTests.Exp_LaserLed_Current_hundredpercent
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_LaserLed_Voltage_hundredpercent
														+ " V\t\tCurrent : "
														+ ManufacturingTests.Exp_LaserLed_Current_hundredpercent
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											} else {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
																+ " V\t\tCurrent : "
																+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
														+ " V\t\tCurrent : "
														+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}

											ManufacturingTests.check_string = "";
										} else if (ManufacturingTests.iris1ledtesthundred
												.equals(ManufacturingTests.check_string)) {
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if (ManufacturingTests.chckbxLightEngine.isSelected()) {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_Iris1Led_Voltage_hundredpercent
																+ " V\t\tCurrent : "
																+ ManufacturingTests.Exp_Iris1Led_Current_hundredpercent
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_Iris1Led_Voltage_hundredpercent
														+ " V\t\tCurrent : "
														+ ManufacturingTests.Exp_Iris1Led_Current_hundredpercent
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											} else {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
																+ " V\t\tCurrent : "
																+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
														+ " V\t\tCurrent : "
														+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											ManufacturingTests.check_string = "";
										} else if (ManufacturingTests.iris2ledtesthundred
												.equals(ManufacturingTests.check_string)) {
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if (ManufacturingTests.chckbxLightEngine.isSelected()) {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_Iris2Led_Voltage_hundredpercent
																+ " V\t\tCurrent : "
																+ ManufacturingTests.Exp_Iris2Led_Current_hundredpercent
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_Iris2Led_Voltage_hundredpercent
														+ " V\t\tCurrent : "
														+ ManufacturingTests.Exp_Iris2Led_Current_hundredpercent
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											} else {
												ManufacturingTests.report_logger
														.severe("Expected Values --> \tVoltage : "
																+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
																+ " V\t\tCurrent : "
																+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
																+ " A\t(+/- 25%)");
												ManufacturingTests.report_logger
														.severe("Observed Values --> \tVoltage : " + LedVoltage
																+ " V\tCurrent : " + LedCurrent + " A\r\n\n");

												ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
														+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
														+ " V\t\tCurrent : "
														+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
														+ " A\t(+/- 25%)");
												ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
														+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}

											ManufacturingTests.check_string = "";
										}

										// PDTESTS : IRIS1ZERO and HUNDRED
										else if (ManufacturingTests.iris1pdtestzero
												.equals(ManufacturingTests.check_string)) {
											String PhotoVoltage = actual[3];
											ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : "
													+ ManufacturingTests.Exp_iris1pdvoltage_zero + " V\t(+/- 25%)");
											ManufacturingTests.report_logger.severe(
													"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

											ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
													+ ManufacturingTests.Exp_iris1pdvoltage_zero + " V\t(+/- 25%");
											ManufacturingTests.logs_logger.severe(
													"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
										} else if (ManufacturingTests.iris1pdtesthundred
												.equals(ManufacturingTests.check_string)) {
											String PhotoVoltage = actual[3];
											ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : "
													+ ManufacturingTests.Exp_iris1pdvoltage_hundred + " V\t(+/- 25%)");
											ManufacturingTests.report_logger.severe(
													"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

											ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
													+ ManufacturingTests.Exp_iris1pdvoltage_hundred + " V\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe(
													"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
										}

										// PDTESTS : IRIS2 ZERO AND HUNDRED

										else if (ManufacturingTests.iris2pdtestzero
												.equals(ManufacturingTests.check_string)) {
											String PhotoVoltage = actual[3];
											ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : "
													+ ManufacturingTests.Exp_iris2pdvoltage_zero + " V\t(+/- 25%)");
											ManufacturingTests.report_logger.severe(
													"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

											ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
													+ ManufacturingTests.Exp_iris2pdvoltage_zero + " V\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe(
													"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
										} else if (ManufacturingTests.iris2pdtesthundred
												.equals(ManufacturingTests.check_string)) {
											String PhotoVoltage = actual[3];
											ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : "
													+ ManufacturingTests.Exp_iris2pdvoltage_hundred + " V\t(+/- 25%)");
											ManufacturingTests.report_logger.severe(
													"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

											ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
													+ ManufacturingTests.Exp_iris2pdvoltage_hundred + " V\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe(
													"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
										}
										// PDTESTS : ENV ZERO AND HUNDRED

										else if (ManufacturingTests.envpdtestzero
												.equals(ManufacturingTests.check_string)) {
											String PhotoVoltage = actual[3];
											ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : "
													+ ManufacturingTests.Exp_envpdvoltage_zero + " V\t(+/- 25%)");
											ManufacturingTests.report_logger.severe(
													"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

											ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
													+ ManufacturingTests.Exp_envpdvoltage_zero + " V\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe(
													"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
										} else if (ManufacturingTests.envpdtesthundred
												.equals(ManufacturingTests.check_string)) {
											String PhotoVoltage = actual[3];
											ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : "
													+ ManufacturingTests.Exp_envpdvoltage_hundred + " V\t(+/- 25%)");
											ManufacturingTests.report_logger.severe(
													"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

											ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
													+ ManufacturingTests.Exp_envpdvoltage_hundred + " V\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe(
													"Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
										}
										// CS TESTS : COLORSENSOR 1

										else if (ManufacturingTests.colorsensor1test
												.equals(ManufacturingTests.check_string)) {
											String ColorsensorVal_1 = actual[3];
											String ColorsensorVal_2 = actual[4];
											String ColorsensorVal_3 = actual[5];
											String ColorsensorVal_4 = actual[6];
											ManufacturingTests.report_logger
													.severe("Expected Value --> \t >100 and below \"FFFF\"");
											ManufacturingTests.report_logger.severe("Observed Value --> \tVal 1 : "
													+ ColorsensorVal_1 + " Val 2 : " + ColorsensorVal_2 + " Val 3 : "
													+ ColorsensorVal_3 + " Val 4 : " + ColorsensorVal_4 + "\r\n\n");

											ManufacturingTests.logs_logger
													.severe("Expected Value --> \t >100 and below \"FFFF\"");
											ManufacturingTests.logs_logger.severe("Observed Value --> \tVal 1 : "
													+ ColorsensorVal_1 + " Val 2 : " + ColorsensorVal_2 + " Val 3 : "
													+ ColorsensorVal_3 + " Val 4 : " + ColorsensorVal_4 + "\r\n\n");
										} else if (ManufacturingTests.colorsensor2test
												.equals(ManufacturingTests.check_string)) {
											String ColorsensorVal_1 = actual[3];
											String ColorsensorVal_2 = actual[4];
											String ColorsensorVal_3 = actual[5];
											String ColorsensorVal_4 = actual[6];
											ManufacturingTests.report_logger
													.severe("Expected Value --> \t >100 and below \"FFFF\"");
											ManufacturingTests.report_logger.severe("Observed Value --> \tVal 1 : "
													+ ColorsensorVal_1 + " Val 2 : " + ColorsensorVal_2 + " Val 3 : "
													+ ColorsensorVal_3 + " Val 4 : " + ColorsensorVal_4 + "\r\n\n");

											ManufacturingTests.logs_logger
													.severe("Expected Value --> \t >100 and below \"FFFF\"");
											ManufacturingTests.logs_logger.severe("Observed Value --> \tVal 1 : "
													+ ColorsensorVal_1 + " Val 2 : " + ColorsensorVal_2 + " Val 3 : "
													+ ColorsensorVal_3 + " Val 4 : " + ColorsensorVal_4 + "\r\n\n");
										}

										// PWM MUXTESTS
										else if (ManufacturingTests.wluc1pwmmuxtest
												.equals(ManufacturingTests.check_string)) {
											String MuxInterrupt_Count = actual[3];
											ManufacturingTests.report_logger.severe("Expected Value --> \t"
													+ ManufacturingTests.Exp_wluc1pwmmuxtest_intcount + " (+/- 3)");
											ManufacturingTests.report_logger
													.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

											ManufacturingTests.logs_logger.severe("Expected Value --> \t"
													+ ManufacturingTests.Exp_wluc1pwmmuxtest_intcount + " (+/- 3)");
											ManufacturingTests.logs_logger
													.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
										} else if (ManufacturingTests.envuc1pwmmuxtest
												.equals(ManufacturingTests.check_string)) {
											String MuxInterrupt_Count = actual[3];
											ManufacturingTests.report_logger.severe("Expected Value --> \t"
													+ ManufacturingTests.Exp_envuc1pwmmuxtest_intcount + " (+/- 3)");
											ManufacturingTests.report_logger
													.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

											ManufacturingTests.logs_logger.severe("Expected Value --> \t"
													+ ManufacturingTests.Exp_envuc1pwmmuxtest_intcount + " (+/- 3)");
											ManufacturingTests.logs_logger
													.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
										} else if (ManufacturingTests.irisuc1pwmmuxtest
												.equals(ManufacturingTests.check_string)) {
											String MuxInterrupt_Count = actual[3];
											ManufacturingTests.report_logger.severe("Expected Value --> \t"
													+ ManufacturingTests.Exp_irisuc1pwmmuxtest_intcount + " (+/- 3)");
											ManufacturingTests.report_logger
													.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

											ManufacturingTests.logs_logger.severe("Expected Value --> \t"
													+ ManufacturingTests.Exp_irisuc1pwmmuxtest_intcount + " (+/- 3)");
											ManufacturingTests.logs_logger
													.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
										} else if (ManufacturingTests.wluc2pwmmuxtest
												.equals(ManufacturingTests.check_string)) {
											String MuxInterrupt_Count = actual[3];
											ManufacturingTests.report_logger.severe("Expected Value --> \t"
													+ ManufacturingTests.Exp_wluc2pwmmuxtest_intcount + " (+/- 3)");
											ManufacturingTests.report_logger
													.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

											ManufacturingTests.logs_logger.severe("Expected Value --> \t"
													+ ManufacturingTests.Exp_wluc2pwmmuxtest_intcount + " (+/- 3)");
											ManufacturingTests.logs_logger
													.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
										} else if (ManufacturingTests.envuc2pwmmuxtest
												.equals(ManufacturingTests.check_string)) {
											String MuxInterrupt_Count = actual[3];
											ManufacturingTests.report_logger.severe("Expected Value --> \t"
													+ ManufacturingTests.Exp_envuc2pwmmuxtest_intcount + " (+/- 3)");
											ManufacturingTests.report_logger
													.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

											ManufacturingTests.logs_logger.severe("Expected Value --> \t"
													+ ManufacturingTests.Exp_envuc2pwmmuxtest_intcount + " (+/- 3)");
											ManufacturingTests.logs_logger
													.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
										} else if (ManufacturingTests.irisuc2pwmmuxtest
												.equals(ManufacturingTests.check_string)) {
											String MuxInterrupt_Count = actual[3];
											ManufacturingTests.report_logger.severe("Expected Value --> \t"
													+ ManufacturingTests.Exp_irisuc2pwmmuxtest_intcount + " (+/- 3)");
											ManufacturingTests.report_logger
													.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

											ManufacturingTests.logs_logger.severe("Expected Value --> \t"
													+ ManufacturingTests.Exp_irisuc2pwmmuxtest_intcount + " (+/- 3)");
											ManufacturingTests.logs_logger
													.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
										} else if (ManufacturingTests.wlextpwmmuxtest
												.equals(ManufacturingTests.check_string)) {
											String MuxInterrupt_Count = actual[3];
											ManufacturingTests.report_logger.severe("Expected Value --> \t"
													+ ManufacturingTests.Exp_wlextpwmmuxtest_intcount + " (+/- 3)");
											ManufacturingTests.report_logger
													.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

											ManufacturingTests.logs_logger.severe("Expected Value --> \t"
													+ ManufacturingTests.Exp_wlextpwmmuxtest_intcount + " (+/- 3)");
											ManufacturingTests.logs_logger
													.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
										} else if (ManufacturingTests.envextpwmmuxtest
												.equals(ManufacturingTests.check_string)) {
											String MuxInterrupt_Count = actual[3];
											ManufacturingTests.report_logger.severe("Expected Value --> \t"
													+ ManufacturingTests.Exp_envextpwmmuxtest_intcount + " (+/- 3)");
											ManufacturingTests.report_logger
													.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

											ManufacturingTests.logs_logger.severe("Expected Value --> \t"
													+ ManufacturingTests.Exp_envextpwmmuxtest_intcount + " (+/- 3)");
											ManufacturingTests.logs_logger
													.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
										} else if (ManufacturingTests.irisextwlpwmmuxtest
												.equals(ManufacturingTests.check_string)) {
											String MuxInterrupt_Count = actual[3];
											ManufacturingTests.report_logger.severe("Expected Value --> \t"
													+ ManufacturingTests.Exp_irisextwlpwmmuxtest_intcount + " (+/- 3)");
											ManufacturingTests.report_logger
													.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

											ManufacturingTests.logs_logger.severe("Expected Value --> \t"
													+ ManufacturingTests.Exp_irisextwlpwmmuxtest_intcount + " (+/- 3)");
											ManufacturingTests.logs_logger
													.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
										} else if (ManufacturingTests.irisextenvpwmmuxtest
												.equals(ManufacturingTests.check_string)) {
											String MuxInterrupt_Count = actual[3];
											ManufacturingTests.report_logger.severe("Expected Value --> \t"
													+ ManufacturingTests.Exp_irisextenvpwmmuxtest_intcount
													+ " (+/- 3)");
											ManufacturingTests.report_logger
													.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

											ManufacturingTests.logs_logger.severe("Expected Value --> \t"
													+ ManufacturingTests.Exp_irisextenvpwmmuxtest_intcount
													+ " (+/- 3)");
											ManufacturingTests.logs_logger
													.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
										}
										ManufacturingTests.UpdateTestResults(Fail, reason, source, value);
										ManufacturingTests.report_logger.severe(ManufacturingTests.ArrowDelimitter);
										synchronized (ManufacturingTests.lockObject) {
											notify_flag = 1;
											ManufacturingTests.busyflag = false;
											ManufacturingTests.lockObject.notifyAll();
										}
										// publish("UPDATE," + Fail + "," + reason + "," + source + "," + value);

										recv_buff = null;
										Resp = "";
									} catch (Exception ex) {
										ex.printStackTrace();

									}

								} else {

									synchronized (ManufacturingTests.lockObject) {
										ManufacturingTests.busyflag = false;
										ManufacturingTests.lockObject.notifyAll();
									}
									// ManufacturingTests.table.getModel().setValueAt("PASSED", 1, 4);

									System.out.println("Some Other FUCKING Response Received!!");
									ManufacturingTests.btnStartButton.setEnabled(true);
									ManufacturingTests.btnStartButton.setText("RUN");
									// ManufacturingTests.btnBack.setEnabled(true);
									recv_buff = null;
									Resp = "";
								}

							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else if (ManufacturingTests.MFTest_ExitFlag) {
							full_buffer.reset();
							ManufacturingTests.MFTest_ExitFlag = false;

						} else {
							// System.out.println("Trigger Source : " + current_control + " Command");
							print("Buffer at Entry Point : " + Arrays.toString(recv_buff));

							try {
								String helloHex = DatatypeConverter.printHexBinary(recv_buff);
								System.out.println("Hessl : " + helloHex);
							} catch (Exception ex) {
								ex.printStackTrace();
							}

							int Is_Error = 0;

							Is_Error = return_error_type(recv_buff);
							recv_buff = null;
							for (int i = 0; i < recv_buff.length; i++) {
								recv_buff[i] = 0;
							}
							if (Is_Error == 0) {
								print("Error Free Buffer Received");
								null_charflag = true;
								synchronized (lockObject) {
									notify_flag = 1;
									lockObject.notifyAll();
								}

								if (serialport != null) {
									serialport.removeDataListener();
									// ##print("Data Listener Removed!!");
								}

							} else if (Is_Error < 9) {
								String err = return_error_name(Is_Error);
								System.out.print("Error is : ");
								System.out.println(Is_Error);
								System.out.println(" , " + err);
								notify_flag = 1;
								null_charflag = false;
								full_buffer.reset();
								serialport.removeDataListener();

								System.out.println("Packet Received with some Error!!");

							} else if (Is_Error == 9) {

								// Response Timedout
								notify_flag = 0;
								null_charflag = false;
								serialport.removeDataListener();
								full_buffer.reset();

							} else {

							}
						}
						recv_buff = null;
						// serialport.removeDataListener(); // Removes data listener associated with
						// serial port Object
						total_bytes = 0;

					}
					// TODO Auto-generated method stub
					return null;
				}

				protected void process(List<String> chunks) {
					for (String str : chunks) {
						if (str.equals("")) {

						} else if (str.equals("SUB_PANEL")) {
							if (ManufacturingTests.subpanellist.size() >= 1) {
								for (int t = 0; t < ManufacturingTests.subpanellist.size(); t++) {
									Component[] comp = ManufacturingTests.subpanellist.get(t).getComponents();
									for (Component c : comp) {
										c.setEnabled(true);
									}
								}
							}
							// ManufacturingTests.btnSyncMTest.setEnabled(false);
							ManufacturingTests.textField_Totaltests.setText("");
							ManufacturingTests.textField_Passedtests.setText("");
							ManufacturingTests.textField_Failedtests.setText("");
							ManufacturingTests.chckbxLightEngine.setSelected(false);
							ManufacturingTests.chckbxIrisModule.setSelected(false);
							ManufacturingTests.chckbxFans.setSelected(false);
							ManufacturingTests.chckbxExternalCamFeed.setSelected(false);
							ManufacturingTests.chckbxUartLoopBack.setSelected(false);
							ManufacturingTests.chckbxOpticFiber.setSelected(false);
							ManufacturingTests.chckbxEsst.setSelected(false);
							ManufacturingTests.chckbxAsst.setSelected(false);
							ManufacturingTests.chckbxGroupTests.setEnabled(true);
							ManufacturingTests.chckbxGroupTests.setSelected(true);

							// ManufacturingTests.btnBack.setEnabled(true);
							// ManufacturingTests.btnSyncMTest.setText("SYNCED");
							ManufacturingTests.logs_logger.severe("Response OK :\t\tMTESTSTART ");
							// disable MTESTSTART Button once {MTESTSTART} is successful
							ManufacturingTests.btnStartButton.setEnabled(true);

						} else if (str.startsWith("VER_NO")) {
							String[] local = str.split(",");
							String Ver_No = local[1];
							ManufacturingTests.textField_FirmWareVerNo.setText(Ver_No);
						} else if (str.startsWith("UPDATE")) {
							String[] local = str.split(",");
							System.out.println(Arrays.deepToString(local));
							String result = local[1];
							String reason = local[2];
							String source = local[3];

							System.out.println("btnname to be mapped : " + ManufacturingTests.btnname_mapping_topanel);
							// No Error
							if (reason.equals("")) {
								// Timeout Response
								if (result.equals(Stryk_Demo.Timeout)) {
									ManufacturingTests.fail_count = ManufacturingTests.fail_count + 1;
									if (ManufacturingTests.result_indicator_mappings
											.get(ManufacturingTests.btnname_mapping_topanel) != null) {
										ManufacturingTests.result_indicator_mappings
												.get(ManufacturingTests.btnname_mapping_topanel)
												.setBackground(Color.MAGENTA);

									} else {
										// ManufacturingTests.btnSyncMTest.setText("SYNC");
									}

								}
								// Failure response
								else if (result.equals(Stryk_Demo.Fail)) {
									ManufacturingTests.fail_count = ManufacturingTests.fail_count + 1;
									if (ManufacturingTests.result_indicator_mappings
											.get(ManufacturingTests.btnname_mapping_topanel) != null) {
										ManufacturingTests.result_indicator_mappings
												.get(ManufacturingTests.btnname_mapping_topanel)
												.setBackground(Color.RED);

									} else {
										// ManufacturingTests.btnSyncMTest.setText("SYNC");
									}

								}
								// Success response
								else if (result.equals(Stryk_Demo.Pass)) {
									ManufacturingTests.pass_count = ManufacturingTests.pass_count + 1;
									ManufacturingTests.result_indicator_mappings
											.get(ManufacturingTests.btnname_mapping_topanel).setBackground(Color.green);

								}

								source = "Source : " + source;

							} else if (!reason.equals("")) {
								// Timeout Response
								if (result.equals(Stryk_Demo.Timeout)) {
									ManufacturingTests.fail_count = ManufacturingTests.fail_count + 1;
									if (ManufacturingTests.result_indicator_mappings
											.get(ManufacturingTests.btnname_mapping_topanel) != null) {
										ManufacturingTests.result_indicator_mappings
												.get(ManufacturingTests.btnname_mapping_topanel)
												.setBackground(Color.MAGENTA);
									} else {
										// ManufacturingTests.btnSyncMTest.setText("SYNC");
									}

								}
								// Failure response
								else if (result.equals(Stryk_Demo.Fail)) {
									ManufacturingTests.fail_count = ManufacturingTests.fail_count + 1;
									if (ManufacturingTests.result_indicator_mappings
											.get(ManufacturingTests.btnname_mapping_topanel) != null) {
										ManufacturingTests.result_indicator_mappings
												.get(ManufacturingTests.btnname_mapping_topanel)
												.setBackground(Color.RED);
									} else {
										// ManufacturingTests.btnSyncMTest.setText("SYNC");
									}

								}
								// Success response
								else if (result.equals(Stryk_Demo.Pass)) {
									ManufacturingTests.pass_count = ManufacturingTests.pass_count + 1;
									ManufacturingTests.result_indicator_mappings
											.get(ManufacturingTests.btnname_mapping_topanel).setBackground(Color.green);
									// System.out.println("Pass Count Incremented");

								}
							}
							// Error Response Received
							else {
								if (ManufacturingTests.finish_button_flag) {
									// System.out.println("Yeah"); //Just for logs
									ManufacturingTests.finish_button_flag = false;
									final Runnable my_thread = new Runnable() {
										@Override
										public void run() {
											JOptionPane.showMessageDialog(
													ManufacturingTests.frmStrykerManufacturingTests,
													"Error Response Received", "ERROR", JOptionPane.ERROR_MESSAGE);
										}
									};
									Thread appThread = new Thread() {
										@Override
										public void run() {
											try {
												SwingUtilities.invokeLater(my_thread);
											} catch (Exception ex) {

												ex.printStackTrace();
											}
										}
									};
									appThread.run();

								}

								else {

								}

							}
						} else {
							ManufacturingTests.textArea.append(str);
						}
					}
				}

			};
			recv_worker.execute();
		}
	};
	private JScrollPane scroll;
	public static JFormattedTextField GrptextField;
	public static JFormattedTextField RedtextField;
	public static JFormattedTextField GreentextField;
	public static JFormattedTextField BluetextField;
	
	public static JFormattedTextField LasertextField;
	public static JFormattedTextField Iris1TextField;
	public static JFormattedTextField Iris2TextField;
	private JLabel label_1;
	private JLabel label_2;
	public static JTextField txtle_tach_count;
	public static JTextField txths_tach_count;
	public static JTextField txtiris_tach_count;
	

	// ############################################################VARIABLES
	// DEFINITIONS ENDS
	// HERE################################################################
	/**
	 * Launch the application.
	 * 
	 * @throws UnsupportedLookAndFeelException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 */
	public static void main(String[] args) {
		try {

			// UIManager.setLookAndFeel("com.jtattoo.plaf.smart.SmartLookAndFeel");

			UIManager.LookAndFeelInfo looks[] = UIManager.getInstalledLookAndFeels();
			try {
				// 0-Swing, 1-Mac, 2-?, 3-Windows, 4-Old Windows
				// UIManager.setLookAndFeel(looks[1].getClassName());// For Windows
				UIManager.setLookAndFeel(looks[1].getClassName()); // For MAC
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			// UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			// UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); //For
			// Linux

		} catch (Exception e) {

		}
		EventQueue.invokeLater(new Runnable() {
			@SuppressWarnings({ "resource", "unused", "static-access" })
			public void run() {
				try {

					// #System.out.println("EDT Thread");
					f = new File("stryker.lock");
					if (f.createNewFile()) {
						f.deleteOnExit();

					}
					// #System.out.println("Here1");
					channel = new RandomAccessFile(f, "rw").getChannel();
					lock = channel.tryLock();
					if (lock == null) {
						channel.close();
						System.out.println("Another instance of the application is already running!!");
						return;
					}
					// #System.out.println("Here2");
					Thread Shutdown_app = new Thread(new Runnable() {

						@Override
						public void run() {

							unlock();
						}

					});
					System.out.println("Calling Function");
					Stryk_Demo window = new Stryk_Demo();

					UIManager UI = new UIManager();
					UIManager.put("OptionPane.background", Color.white);
					UIManager.put("Panel.background", Color.white);
					// System.out.println("Function Called");
					window.frmStrykerVer.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("Could not Launch the Application. Some Exception");
				}
			}
		});

	}

	// ####################################################################################################
	/*
	 * Function Name : process_recv_buffer Input : A Byte Array Output : An Integer
	 * representing one of the error types in the error Structure Description :
	 * checks for error in the given input buffer, if no error performs desired
	 * action(received packet)
	 * 
	 */
	// ####################################################################################################
	public static void process_recv_buff(byte[] recv_buff, String current_control) {
		try {
			serialport.removeDataListener();
			String result = "";
			result = new String(recv_buff, "UTF-8");
			System.out.println("Response to be processed : " + result);
			System.out.println("Inside the function");
			if (ManufacturingTests.MFTest_Flag == true) {
				ManufacturingTests.logs_logger.severe("Response Received\t" + result);

				full_buffer.reset();
				ManufacturingTests.textArea.append("Response Received : " + result + "\r\n");
				System.out.println("Receive Buffer in MTEST START: " + Arrays.toString(recv_buff));
				// System.out.println("Manufacturing Test Flag Set");
				ManufacturingTests.MFTest_Flag = false;
				try {
					String Resp = new String(recv_buff, "UTF-8");

					recv_buff = null;
					System.out.println("Response Received : " + Resp + " Of Length" + Resp.length());

					if (Resp.contains(RESPONSE_OK)) {
						notify_flag = 1;
						try {
							String resp1 = Resp.replace("{", "");
							resp1 = resp1.replace("}", "");
							String[] resp = resp1.split(",");
							ManufacturingTests.logs_logger.severe("Response OK Received");
							// System.out.println(Arrays.deepToString(resp));
							// System.out.println("Response OK Received!!");
							recv_buff = null;
							Resp = "";
							synchronized (ManufacturingTests.lockObject) {
								ManufacturingTests.busyflag = false;
								ManufacturingTests.lockObject.notifyAll();
							}

							if (ManufacturingTests.Current_Query_Number == 0) {

								System.out.println("Actual Array : " + Arrays.toString(resp));
								String Ver_No = resp[2];
								ManufacturingTests.Current_Query_Number = 1;
								Stryk_Demo.frmStrykerVer.setVisible(false);
								if (ManufacturingTests.subpanellist.size() >= 1) {
									for (int t = 0; t < ManufacturingTests.subpanellist.size(); t++) {
										Component[] comp = ManufacturingTests.subpanellist.get(t).getComponents();
										for (Component c : comp) {
											c.setEnabled(true);
										}
									}
								}
								ManufacturingTests.textField_Totaltests.setText("");
								ManufacturingTests.textField_Passedtests.setText("");
								ManufacturingTests.textField_Failedtests.setText("");
								ManufacturingTests.chckbxLightEngine.setSelected(false);
								ManufacturingTests.chckbxIrisModule.setSelected(false);
								ManufacturingTests.chckbxFans.setSelected(false);
								ManufacturingTests.chckbxExternalCamFeed.setSelected(false);
								ManufacturingTests.chckbxUartLoopBack.setSelected(false);
								ManufacturingTests.chckbxOpticFiber.setSelected(false);
								ManufacturingTests.chckbxEsst.setSelected(false);
								ManufacturingTests.chckbxAsst.setSelected(false);
								ManufacturingTests.chckbxGroupTests.setEnabled(true);
								ManufacturingTests.chckbxGroupTests.setSelected(true);
								/*
								 * for (Component cp : ManufacturingTests.SubTestPanel_1.getComponents() ){
								 * cp.setEnabled(true); } for (Component cp :
								 * ManufacturingTests.SubTestPanel_2.getComponents() ){ cp.setEnabled(true); }
								 * for (Component cp : ManufacturingTests.SubTestPanel_3.getComponents() ){
								 * cp.setEnabled(true); } for (Component cp :
								 * ManufacturingTests.SubTestPanel_4.getComponents() ){ cp.setEnabled(true); }
								 */
								// ManufacturingTests.btnBack.setEnabled(true);

								ManufacturingTests.logs_logger.severe("Response OK :\t\tMTESTSTART ");

								ManufacturingTests.btnStartButton.setEnabled(true);
								ManufacturingTests.textField_FirmWareVerNo.setText(Ver_No);
							} else {

								String src = ManufacturingTests.get_source(resp[1]);

								// ManufacturingTests.UpdateTestResults(ManufacturingTests.Current_Query,Pass,"",src,"");

								ManufacturingTests.logs_logger.severe("Result\t\t\tPass");
								ManufacturingTests.report_logger.severe("Result\t\t\tPass");
								// LIGHT ENGINE FAN TEST
								if (ManufacturingTests.lightenginefantest.equals(ManufacturingTests.check_string)) {
									String Fan_Count = resp[2];
									ManufacturingTests.report_logger.severe("Expected Tach Count -->  : "
											+ ManufacturingTests.Expected_LightEngine_FanTachCount + "\t(+/- 25%)");
									ManufacturingTests.report_logger
											.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Tach Count -->  : "
											+ ManufacturingTests.Expected_LightEngine_FanTachCount + "\t(+/- 25%)");
									ManufacturingTests.logs_logger
											.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");
									ManufacturingTests.check_string = "";
								} // HEAT SINK FAN TEST
								else if (ManufacturingTests.heatsinkfantest.equals(ManufacturingTests.check_string)) {
									String Fan_Count = resp[2];
									ManufacturingTests.report_logger.severe("Expected Tach Count -->  : "
											+ ManufacturingTests.Expected_HeatSink_FanTachCount + "\t(+/- 25%)");
									ManufacturingTests.report_logger
											.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Tach Count -->  : "
											+ ManufacturingTests.Expected_HeatSink_FanTachCount + "\t(+/- 25%)");
									ManufacturingTests.logs_logger
											.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");
									ManufacturingTests.check_string = "";
								}
								// IRIS FAN TEST
								else if (ManufacturingTests.irisfantest.equals(ManufacturingTests.check_string)) {
									String Fan_Count = resp[2];
									ManufacturingTests.report_logger.severe("Expected Tach Count -->  : "
											+ ManufacturingTests.Expected_Iris_FanTachCount + "\t(+/- 25%)");
									ManufacturingTests.report_logger
											.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Tach Count -->  : "
											+ ManufacturingTests.Expected_Iris_FanTachCount + "\t(+/- 25%)");
									ManufacturingTests.logs_logger
											.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");
									ManufacturingTests.check_string = "";
								}

								// LED TEST RESULT : LED ZERO PERCENT
								else if (ManufacturingTests.redledtestzero.equals(ManufacturingTests.check_string)) {
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if (ManufacturingTests.chckbxLightEngine.isSelected()) {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent + " V\t\tCurrent : "
												+ ManufacturingTests.Exp_RedLed_Current_zeropercent + " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent + " V\t\tCurrent : "
												+ ManufacturingTests.Exp_RedLed_Current_zeropercent + " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									} else {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}

									ManufacturingTests.check_string = "";
								} else if (ManufacturingTests.greenledtestzero
										.equals(ManufacturingTests.check_string)) {
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if (ManufacturingTests.chckbxLightEngine.isSelected()) {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_GreenLed_Current_zeropercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_GreenLed_Current_zeropercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									} else {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}
									ManufacturingTests.check_string = "";
								} else if (ManufacturingTests.blueledtestzero.equals(ManufacturingTests.check_string)) {
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if (ManufacturingTests.chckbxLightEngine.isSelected()) {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_BlueLed_Current_zeropercent + " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_BlueLed_Current_zeropercent + " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									} else {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}
									ManufacturingTests.check_string = "";
								} else if (ManufacturingTests.laserledtestzero
										.equals(ManufacturingTests.check_string)) {
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if (ManufacturingTests.chckbxLightEngine.isSelected()) {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_LaserLed_Current_zeropercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_LaserLed_Current_zeropercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									} else {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}
									ManufacturingTests.check_string = "";
								} else if (ManufacturingTests.iris1ledtestzero
										.equals(ManufacturingTests.check_string)) {
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if (ManufacturingTests.chckbxLightEngine.isSelected()) {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_Iris1Led_Current_zeropercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_Iris1Led_Current_zeropercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									} else {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}
									ManufacturingTests.check_string = "";
								} else if (ManufacturingTests.iris2ledtestzero
										.equals(ManufacturingTests.check_string)) {
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if (ManufacturingTests.chckbxLightEngine.isSelected()) {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_Iris2Led_Current_zeropercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_Iris2Led_Current_zeropercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									} else {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}
									ManufacturingTests.check_string = "";
								}

								// LED TEST RESULT : LED FIFTY PERCENT

								else if (ManufacturingTests.redledtestfifty.equals(ManufacturingTests.check_string)) {
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if (ManufacturingTests.chckbxLightEngine.isSelected()) {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_RedLed_Voltage_fiftypercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_RedLed_Current_fiftypercent + " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_RedLed_Voltage_fiftypercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_RedLed_Current_fiftypercent + " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									} else {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}

									ManufacturingTests.check_string = "";
								} else if (ManufacturingTests.greenledtestfifty
										.equals(ManufacturingTests.check_string)) {
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if (ManufacturingTests.chckbxLightEngine.isSelected()) {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_GreenLed_Voltage_fiftypercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_GreenLed_Current_fiftypercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_GreenLed_Voltage_fiftypercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_GreenLed_Current_fiftypercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									} else {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}

									ManufacturingTests.check_string = "";
								} else if (ManufacturingTests.blueledtestfifty
										.equals(ManufacturingTests.check_string)) {
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if (ManufacturingTests.chckbxLightEngine.isSelected()) {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_BlueLed_Voltage_fiftypercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_BlueLed_Current_fiftypercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_BlueLed_Voltage_fiftypercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_BlueLed_Current_fiftypercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									} else {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}

									ManufacturingTests.check_string = "";
								} else if (ManufacturingTests.laserledtestfifty
										.equals(ManufacturingTests.check_string)) {
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if (ManufacturingTests.chckbxLightEngine.isSelected()) {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_LaserLed_Voltage_fiftypercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_LaserLed_Current_fiftypercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_LaserLed_Voltage_fiftypercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_LaserLed_Current_fiftypercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									} else {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}

									ManufacturingTests.check_string = "";
								} else if (ManufacturingTests.iris1ledtestfifty
										.equals(ManufacturingTests.check_string)) {
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if (ManufacturingTests.chckbxLightEngine.isSelected()) {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris1Led_Voltage_fiftypercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_Iris1Led_Current_fiftypercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris1Led_Voltage_fiftypercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_Iris1Led_Current_fiftypercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									} else {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}

									ManufacturingTests.check_string = "";
								} else if (ManufacturingTests.iris2ledtestfifty
										.equals(ManufacturingTests.check_string)) {
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if (ManufacturingTests.chckbxLightEngine.isSelected()) {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris2Led_Voltage_fiftypercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_Iris2Led_Current_fiftypercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris2Led_Voltage_fiftypercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_Iris2Led_Current_fiftypercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									} else {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}

									ManufacturingTests.check_string = "";
								}
								
								//LED TEST RESULT : LED SEVENTY FIVE PERCENT
								
								else if(ManufacturingTests.redledtestseventyfive.equals(ManufacturingTests.check_string))
								{
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if(ManufacturingTests.chckbxLightEngine.isSelected())
									{
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_seventyfivepercent + " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_seventyfivepercent + " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}
									else
									{
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}
									
									ManufacturingTests.check_string = "";
								}
								else if(ManufacturingTests.greenledtestseventyfive.equals(ManufacturingTests.check_string))
								{
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if(ManufacturingTests.chckbxLightEngine.isSelected())
									{
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_seventyfivepercent + " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_seventyfivepercent + " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}
									else
									{
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}
									
									ManufacturingTests.check_string = "";
								}
								else if(ManufacturingTests.blueledtestseventyfive.equals(ManufacturingTests.check_string))
								{
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if(ManufacturingTests.chckbxLightEngine.isSelected())
									{
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_seventyfivepercent + " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_seventyfivepercent + " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}
									else
									{
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}
									
									ManufacturingTests.check_string = "";
								}
								else if(ManufacturingTests.laserledtestseventyfive.equals(ManufacturingTests.check_string))
								{
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if(ManufacturingTests.chckbxLightEngine.isSelected())
									{
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_seventyfivepercent + " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_seventyfivepercent + " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}
									else
									{
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}
									
									ManufacturingTests.check_string = "";
								}
								else if(ManufacturingTests.iris1ledtestseventyfive.equals(ManufacturingTests.check_string))
								{
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if(ManufacturingTests.chckbxLightEngine.isSelected())
									{
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_seventyfivepercent + " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_seventyfivepercent + " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}
									else
									{
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}
									
									ManufacturingTests.check_string = "";
								}
								else if(ManufacturingTests.iris2ledtestseventyfive.equals(ManufacturingTests.check_string))
								{
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if(ManufacturingTests.chckbxLightEngine.isSelected())
									{
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_seventyfivepercent + " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_seventyfivepercent + " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}
									else
									{
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}
									
									ManufacturingTests.check_string = "";
								}
								
								// LED TEST RESULT : LED HUNDRED PERCENT

								else if (ManufacturingTests.redledtesthundred.equals(ManufacturingTests.check_string)) {
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if (ManufacturingTests.chckbxLightEngine.isSelected()) {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_RedLed_Voltage_hundredpercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_RedLed_Current_hundredpercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_RedLed_Voltage_hundredpercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_RedLed_Current_hundredpercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									} else {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}

									ManufacturingTests.check_string = "";
								} else if (ManufacturingTests.greenledtesthundred
										.equals(ManufacturingTests.check_string)) {
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if (ManufacturingTests.chckbxLightEngine.isSelected()) {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_GreenLed_Voltage_hundredpercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_GreenLed_Current_hundredpercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_GreenLed_Voltage_hundredpercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_GreenLed_Current_hundredpercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									} else {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}
									ManufacturingTests.check_string = "";
								} else if (ManufacturingTests.blueledtesthundred
										.equals(ManufacturingTests.check_string)) {
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if (ManufacturingTests.chckbxLightEngine.isSelected()) {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_BlueLed_Voltage_hundredpercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_BlueLed_Current_hundredpercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_BlueLed_Voltage_hundredpercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_BlueLed_Current_hundredpercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									} else {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}
								} else if (ManufacturingTests.laserledtesthundred
										.equals(ManufacturingTests.check_string)) {
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if (ManufacturingTests.chckbxLightEngine.isSelected()) {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_LaserLed_Voltage_hundredpercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_LaserLed_Current_hundredpercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_LaserLed_Voltage_hundredpercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_LaserLed_Current_hundredpercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									} else {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}

									ManufacturingTests.check_string = "";
								} else if (ManufacturingTests.iris1ledtesthundred
										.equals(ManufacturingTests.check_string)) {
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if (ManufacturingTests.chckbxLightEngine.isSelected()) {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris1Led_Voltage_hundredpercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_Iris1Led_Current_hundredpercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris1Led_Voltage_hundredpercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_Iris1Led_Current_hundredpercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									} else {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}
									ManufacturingTests.check_string = "";
								} else if (ManufacturingTests.iris2ledtesthundred
										.equals(ManufacturingTests.check_string)) {
									String LedVoltage = resp[2];
									String LedCurrent = resp[3];
									if (ManufacturingTests.chckbxLightEngine.isSelected()) {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris2Led_Voltage_hundredpercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_Iris2Led_Current_hundredpercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris2Led_Voltage_hundredpercent
												+ " V\t\tCurrent : "
												+ ManufacturingTests.Exp_Iris2Led_Current_hundredpercent
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									} else {
										ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

										ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
												+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
												+ " V\t\tCurrent : "
												+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
												+ " A\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
												+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									}

									ManufacturingTests.check_string = "";
								}

								// PDTESTS : IRIS1ZERO and HUNDRED
								else if (ManufacturingTests.iris1pdtestzero.equals(ManufacturingTests.check_string)) {
									String PhotoVoltage = resp[2];
									ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : "
											+ ManufacturingTests.Exp_iris1pdvoltage_zero + " V\t(+/- 25%)");
									ManufacturingTests.report_logger
											.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
											+ ManufacturingTests.Exp_iris1pdvoltage_zero + " V\t(+/- 25%)");
									ManufacturingTests.logs_logger
											.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
								} else if (ManufacturingTests.iris1pdtesthundred
										.equals(ManufacturingTests.check_string)) {
									String PhotoVoltage = resp[2];
									ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : "
											+ ManufacturingTests.Exp_iris1pdvoltage_hundred + " V\t(+/- 25%)");
									ManufacturingTests.report_logger
											.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
											+ ManufacturingTests.Exp_iris1pdvoltage_hundred + " V\t(+/- 25%)");
									ManufacturingTests.logs_logger
											.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
								}

								// PDTESTS : IRIS2 ZERO AND HUNDRED

								else if (ManufacturingTests.iris2pdtestzero.equals(ManufacturingTests.check_string)) {
									String PhotoVoltage = resp[2];
									ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : "
											+ ManufacturingTests.Exp_iris2pdvoltage_zero + " V\t(+/- 25%)");
									ManufacturingTests.report_logger
											.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
											+ ManufacturingTests.Exp_iris2pdvoltage_zero + " V\t(+/- 25%)");
									ManufacturingTests.logs_logger
											.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
								} else if (ManufacturingTests.iris2pdtesthundred
										.equals(ManufacturingTests.check_string)) {
									String PhotoVoltage = resp[2];
									ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : "
											+ ManufacturingTests.Exp_iris2pdvoltage_hundred + " V\t(+/- 25%)");
									ManufacturingTests.report_logger
											.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
											+ ManufacturingTests.Exp_iris2pdvoltage_hundred + " V\t(+/- 25%)");
									ManufacturingTests.logs_logger
											.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
								}
								// PDTESTS : ENV ZERO AND HUNDRED

								else if (ManufacturingTests.envpdtestzero.equals(ManufacturingTests.check_string)) {
									String PhotoVoltage = resp[2];
									ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : "
											+ ManufacturingTests.Exp_envpdvoltage_zero + " V\t(+/- 25%)");
									ManufacturingTests.report_logger
											.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
											+ ManufacturingTests.Exp_envpdvoltage_zero + " V\t(+/- 25%)");
									ManufacturingTests.logs_logger
											.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
								} else if (ManufacturingTests.envpdtesthundred
										.equals(ManufacturingTests.check_string)) {
									String PhotoVoltage = resp[2];
									ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : "
											+ ManufacturingTests.Exp_envpdvoltage_hundred + " V\t(+/- 25%)");
									ManufacturingTests.report_logger
											.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
											+ ManufacturingTests.Exp_envpdvoltage_hundred + " V\t(+/- 25%)");
									ManufacturingTests.logs_logger
											.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
								}
								// CS TESTS : COLORSENSOR 1

								else if (ManufacturingTests.colorsensor1test.equals(ManufacturingTests.check_string)) {
									String ColorsensorVal_1 = resp[2];
									String ColorsensorVal_2 = resp[3];
									String ColorsensorVal_3 = resp[4];
									String ColorsensorVal_4 = resp[5];
									ManufacturingTests.report_logger
											.severe("Expected Value --> \t >100 and below \"FFFF\"");
									ManufacturingTests.report_logger.severe("Observed Value --> \tVal 1 : "
											+ ColorsensorVal_1 + " Val 2 : " + ColorsensorVal_2 + " Val 3 : "
											+ ColorsensorVal_3 + " Val 4 : " + ColorsensorVal_4 + "\r\n\n");

									ManufacturingTests.logs_logger
											.severe("Expected Value --> \t >100 and below \"FFFF\"");
									ManufacturingTests.logs_logger.severe("Observed Value --> \tVal 1 : "
											+ ColorsensorVal_1 + " Val 2 : " + ColorsensorVal_2 + " Val 3 : "
											+ ColorsensorVal_3 + " Val 4 : " + ColorsensorVal_4 + "\r\n\n");
								} else if (ManufacturingTests.colorsensor2test
										.equals(ManufacturingTests.check_string)) {
									String ColorsensorVal_1 = resp[2];
									String ColorsensorVal_2 = resp[3];
									String ColorsensorVal_3 = resp[4];
									String ColorsensorVal_4 = resp[5];
									ManufacturingTests.report_logger
											.severe("Expected Value --> \t >100 and below \"FFFF\"");
									ManufacturingTests.report_logger.severe("Observed Value --> \tVal 1 : "
											+ ColorsensorVal_1 + " Val 2 : " + ColorsensorVal_2 + " Val 3 : "
											+ ColorsensorVal_3 + " Val 4 : " + ColorsensorVal_4 + "\r\n\n");

									ManufacturingTests.logs_logger
											.severe("Expected Value --> \t >100 and below \"FFFF\"");
									ManufacturingTests.logs_logger.severe("Observed Value --> \tVal 1 : "
											+ ColorsensorVal_1 + " Val 2 : " + ColorsensorVal_2 + " Val 3 : "
											+ ColorsensorVal_3 + " Val 4 : " + ColorsensorVal_4 + "\r\n\n");
								}

								// PWM MUXTESTS
								else if (ManufacturingTests.wluc1pwmmuxtest.equals(ManufacturingTests.check_string)) {
									String MuxInterrupt_Count = resp[2];
									ManufacturingTests.report_logger.severe("Expected Value --> \t"
											+ ManufacturingTests.Exp_wluc1pwmmuxtest_intcount + " (+/- 3)");
									ManufacturingTests.report_logger
											.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Value --> \t"
											+ ManufacturingTests.Exp_wluc1pwmmuxtest_intcount + " (+/- 3)");
									ManufacturingTests.logs_logger
											.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
								} else if (ManufacturingTests.envuc1pwmmuxtest
										.equals(ManufacturingTests.check_string)) {
									String MuxInterrupt_Count = resp[2];
									ManufacturingTests.report_logger.severe("Expected Value --> \t"
											+ ManufacturingTests.Exp_envuc1pwmmuxtest_intcount + " (+/- 3)");
									ManufacturingTests.report_logger
											.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Value --> \t"
											+ ManufacturingTests.Exp_envuc1pwmmuxtest_intcount + " (+/- 3)");
									ManufacturingTests.logs_logger
											.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
								} else if (ManufacturingTests.irisuc1pwmmuxtest
										.equals(ManufacturingTests.check_string)) {
									String MuxInterrupt_Count = resp[2];
									ManufacturingTests.report_logger.severe("Expected Value --> \t"
											+ ManufacturingTests.Exp_irisuc1pwmmuxtest_intcount + " (+/- 3)");
									ManufacturingTests.report_logger
											.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Value --> \t"
											+ ManufacturingTests.Exp_irisuc1pwmmuxtest_intcount + " (+/- 3)");
									ManufacturingTests.logs_logger
											.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
								} else if (ManufacturingTests.wluc2pwmmuxtest.equals(ManufacturingTests.check_string)) {
									String MuxInterrupt_Count = resp[2];
									ManufacturingTests.report_logger.severe("Expected Value --> \t"
											+ ManufacturingTests.Exp_wluc2pwmmuxtest_intcount + " (+/- 3)");
									ManufacturingTests.report_logger
											.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Value --> \t"
											+ ManufacturingTests.Exp_wluc2pwmmuxtest_intcount + " (+/- 3)");
									ManufacturingTests.logs_logger
											.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
								} else if (ManufacturingTests.envuc2pwmmuxtest
										.equals(ManufacturingTests.check_string)) {
									String MuxInterrupt_Count = resp[2];
									ManufacturingTests.report_logger.severe("Expected Value --> \t"
											+ ManufacturingTests.Exp_envuc2pwmmuxtest_intcount + " (+/- 3)");
									ManufacturingTests.report_logger
											.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Value --> \t"
											+ ManufacturingTests.Exp_envuc2pwmmuxtest_intcount + " (+/- 3)");
									ManufacturingTests.logs_logger
											.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
								} else if (ManufacturingTests.irisuc2pwmmuxtest
										.equals(ManufacturingTests.check_string)) {
									String MuxInterrupt_Count = resp[2];
									ManufacturingTests.report_logger.severe("Expected Value --> \t"
											+ ManufacturingTests.Exp_irisuc2pwmmuxtest_intcount + " (+/- 3)");
									ManufacturingTests.report_logger
											.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Value --> \t"
											+ ManufacturingTests.Exp_irisuc2pwmmuxtest_intcount + " (+/- 3)");
									ManufacturingTests.logs_logger
											.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
								} else if (ManufacturingTests.wlextpwmmuxtest.equals(ManufacturingTests.check_string)) {
									String MuxInterrupt_Count = resp[2];
									ManufacturingTests.report_logger.severe("Expected Value --> \t"
											+ ManufacturingTests.Exp_wlextpwmmuxtest_intcount + " (+/- 3)");
									ManufacturingTests.report_logger
											.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Value --> \t"
											+ ManufacturingTests.Exp_wlextpwmmuxtest_intcount + " (+/- 3)");
									ManufacturingTests.logs_logger
											.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
								} else if (ManufacturingTests.envextpwmmuxtest
										.equals(ManufacturingTests.check_string)) {
									String MuxInterrupt_Count = resp[2];
									ManufacturingTests.report_logger.severe("Expected Value --> \t"
											+ ManufacturingTests.Exp_envextpwmmuxtest_intcount + " (+/- 3)");
									ManufacturingTests.report_logger
											.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Value --> \t"
											+ ManufacturingTests.Exp_envextpwmmuxtest_intcount + " (+/- 3)");
									ManufacturingTests.logs_logger
											.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
								} else if (ManufacturingTests.irisextwlpwmmuxtest
										.equals(ManufacturingTests.check_string)) {
									String MuxInterrupt_Count = resp[2];
									ManufacturingTests.report_logger.severe("Expected Value --> \t"
											+ ManufacturingTests.Exp_irisextwlpwmmuxtest_intcount + " (+/- 3)");
									ManufacturingTests.report_logger
											.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Value --> \t"
											+ ManufacturingTests.Exp_irisextwlpwmmuxtest_intcount + " (+/- 3)");
									ManufacturingTests.logs_logger
											.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
								} else if (ManufacturingTests.irisextenvpwmmuxtest
										.equals(ManufacturingTests.check_string)) {
									String MuxInterrupt_Count = resp[2];
									ManufacturingTests.report_logger.severe("Expected Value --> \t"
											+ ManufacturingTests.Exp_irisextenvpwmmuxtest_intcount + " (+/- 3)");
									ManufacturingTests.report_logger
											.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Value --> \t"
											+ ManufacturingTests.Exp_irisextenvpwmmuxtest_intcount + " (+/- 3)");
									ManufacturingTests.logs_logger
											.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
								}
								ManufacturingTests.report_logger.severe(ManufacturingTests.ArrowDelimitter);

								ManufacturingTests.UpdateTestResults(Pass, "", src, "");

							}

						} catch (Exception ex) {
							ex.printStackTrace();

						}

					} else if (Resp.contains(RESPONSE_ERROR)) {
						notify_flag = 1;
						try {

							System.out.println("Error Response Received!!");
							synchronized (ManufacturingTests.lockObject) {
								ManufacturingTests.busyflag = false;
								ManufacturingTests.lockObject.notifyAll();
							}
							String resp = Resp.replace("{", "");
							resp = resp.replace("}", "");
							String[] actual = resp.split(",");
							String reason = ManufacturingTests.get_error_source(actual[1]);
							String source = ManufacturingTests.get_source(actual[2]);
							String value = "";

							ManufacturingTests.report_logger.severe("Result\t\t\tFail");
							ManufacturingTests.logs_logger.severe("Result\t\t\tFail");
							if (ManufacturingTests.lightenginefantest.equals(ManufacturingTests.check_string)) {
								String Fan_Count = actual[3];
								ManufacturingTests.report_logger.severe("Expected Tach Count -->  : "
										+ ManufacturingTests.Expected_LightEngine_FanTachCount + "\t(+/- 25%)");
								ManufacturingTests.report_logger
										.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Tach Count -->  : "
										+ ManufacturingTests.Expected_LightEngine_FanTachCount + "\t(+/- 25%)");
								ManufacturingTests.logs_logger
										.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");
								ManufacturingTests.check_string = "";
							} // HEAT SINK FAN TEST
							else if (ManufacturingTests.heatsinkfantest.equals(ManufacturingTests.check_string)) {
								String Fan_Count = actual[3];
								ManufacturingTests.report_logger.severe("Expected Tach Count -->  : "
										+ ManufacturingTests.Expected_HeatSink_FanTachCount + "\t(+/- 25%)");
								ManufacturingTests.report_logger
										.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Tach Count -->  : "
										+ ManufacturingTests.Expected_HeatSink_FanTachCount + "\t(+/- 25%)");
								ManufacturingTests.logs_logger
										.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");
								ManufacturingTests.check_string = "";
							}
							// IRIS FAN TEST
							else if (ManufacturingTests.irisfantest.equals(ManufacturingTests.check_string)) {
								String Fan_Count = actual[3];
								ManufacturingTests.report_logger.severe("Expected Tach Count -->  : "
										+ ManufacturingTests.Expected_Iris_FanTachCount + "\t(+/- 25%)");
								ManufacturingTests.report_logger
										.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Tach Count -->  : "
										+ ManufacturingTests.Expected_Iris_FanTachCount + "\t(+/- 25%)");
								ManufacturingTests.logs_logger
										.severe("Observed Tach Count -->  : " + Fan_Count + "\r\n\n");
								ManufacturingTests.check_string = "";
							} else if (ManufacturingTests.redledtestzero.equals(ManufacturingTests.check_string)) {
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if (ManufacturingTests.chckbxLightEngine.isSelected()) {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_RedLed_Current_zeropercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_RedLed_Current_zeropercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								} else {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}

								ManufacturingTests.check_string = "";
							} else if (ManufacturingTests.greenledtestzero.equals(ManufacturingTests.check_string)) {
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if (ManufacturingTests.chckbxLightEngine.isSelected()) {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_GreenLed_Current_zeropercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_GreenLed_Current_zeropercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								} else {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}
								ManufacturingTests.check_string = "";
							} else if (ManufacturingTests.blueledtestzero.equals(ManufacturingTests.check_string)) {
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if (ManufacturingTests.chckbxLightEngine.isSelected()) {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_BlueLed_Current_zeropercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_BlueLed_Current_zeropercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								} else {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}
								ManufacturingTests.check_string = "";
							} else if (ManufacturingTests.laserledtestzero.equals(ManufacturingTests.check_string)) {
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if (ManufacturingTests.chckbxLightEngine.isSelected()) {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_LaserLed_Current_zeropercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_LaserLed_Current_zeropercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								} else {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}
								ManufacturingTests.check_string = "";
							} else if (ManufacturingTests.iris1ledtestzero.equals(ManufacturingTests.check_string)) {
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if (ManufacturingTests.chckbxLightEngine.isSelected()) {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_Iris1Led_Current_zeropercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_Iris1Led_Current_zeropercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								} else {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}
								ManufacturingTests.check_string = "";
							} else if (ManufacturingTests.iris2ledtestzero.equals(ManufacturingTests.check_string)) {
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if (ManufacturingTests.chckbxLightEngine.isSelected()) {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_Iris2Led_Current_zeropercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_Iris2Led_Current_zeropercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								} else {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}
								ManufacturingTests.check_string = "";
							}

							// LED TEST RESULT : LED FIFTY PERCENT

							else if (ManufacturingTests.redledtestfifty.equals(ManufacturingTests.check_string)) {
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if (ManufacturingTests.chckbxLightEngine.isSelected()) {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_RedLed_Voltage_fiftypercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_RedLed_Current_fiftypercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_RedLed_Voltage_fiftypercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_RedLed_Current_fiftypercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								} else {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}

								ManufacturingTests.check_string = "";
							} else if (ManufacturingTests.greenledtestfifty.equals(ManufacturingTests.check_string)) {
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if (ManufacturingTests.chckbxLightEngine.isSelected()) {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_GreenLed_Voltage_fiftypercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_GreenLed_Current_fiftypercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_GreenLed_Voltage_fiftypercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_GreenLed_Current_fiftypercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								} else {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}

								ManufacturingTests.check_string = "";
							} else if (ManufacturingTests.blueledtestfifty.equals(ManufacturingTests.check_string)) {
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if (ManufacturingTests.chckbxLightEngine.isSelected()) {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_BlueLed_Voltage_fiftypercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_BlueLed_Current_fiftypercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_BlueLed_Voltage_fiftypercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_BlueLed_Current_fiftypercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								} else {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}

								ManufacturingTests.check_string = "";
							} else if (ManufacturingTests.laserledtestfifty.equals(ManufacturingTests.check_string)) {
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if (ManufacturingTests.chckbxLightEngine.isSelected()) {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_LaserLed_Voltage_fiftypercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_LaserLed_Current_fiftypercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_LaserLed_Voltage_fiftypercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_LaserLed_Current_fiftypercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								} else {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}

								ManufacturingTests.check_string = "";
							} else if (ManufacturingTests.iris1ledtestfifty.equals(ManufacturingTests.check_string)) {
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if (ManufacturingTests.chckbxLightEngine.isSelected()) {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris1Led_Voltage_fiftypercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_Iris1Led_Current_fiftypercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris1Led_Voltage_fiftypercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_Iris1Led_Current_fiftypercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								} else {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}

								ManufacturingTests.check_string = "";
							} else if (ManufacturingTests.iris2ledtestfifty.equals(ManufacturingTests.check_string)) {
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if (ManufacturingTests.chckbxLightEngine.isSelected()) {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris2Led_Voltage_fiftypercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_Iris2Led_Current_fiftypercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris2Led_Voltage_fiftypercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_Iris2Led_Current_fiftypercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								} else {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}

								ManufacturingTests.check_string = "";
							}
							
							//LED TEST RESULT : LED SEVENTY FIVE PERCENT
							
							else if(ManufacturingTests.redledtestseventyfive.equals(ManufacturingTests.check_string))
							{
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if(ManufacturingTests.chckbxLightEngine.isSelected())
								{
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_seventyfivepercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									
									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_seventyfivepercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}
								else
								{
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									
									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}
								
								ManufacturingTests.check_string = "";
							}
							else if(ManufacturingTests.greenledtestseventyfive.equals(ManufacturingTests.check_string))
							{
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if(ManufacturingTests.chckbxLightEngine.isSelected())
								{
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_seventyfivepercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									
									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_seventyfivepercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}
								else
								{
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									
									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}
								
								ManufacturingTests.check_string = "";
							}
							else if(ManufacturingTests.blueledtestseventyfive.equals(ManufacturingTests.check_string))
							{
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if(ManufacturingTests.chckbxLightEngine.isSelected())
								{
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_seventyfivepercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									
									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_seventyfivepercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}
								else
								{
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									
									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}
								
								ManufacturingTests.check_string = "";
							}
							else if(ManufacturingTests.laserledtestseventyfive.equals(ManufacturingTests.check_string))
							{
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if(ManufacturingTests.chckbxLightEngine.isSelected())
								{
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_seventyfivepercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									
									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_seventyfivepercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}
								else
								{
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									
									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}
								
								ManufacturingTests.check_string = "";
							}
							else if(ManufacturingTests.iris1ledtestseventyfive.equals(ManufacturingTests.check_string))
							{
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if(ManufacturingTests.chckbxLightEngine.isSelected())
								{
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_seventyfivepercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									
									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_seventyfivepercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}
								else
								{
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									
									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}
								
								ManufacturingTests.check_string = "";
							}
							else if(ManufacturingTests.iris2ledtestseventyfive.equals(ManufacturingTests.check_string))
							{
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if(ManufacturingTests.chckbxLightEngine.isSelected())
								{
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_seventyfivepercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									
									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_seventyfivepercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}
								else
								{
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
									
									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}
								
								ManufacturingTests.check_string = "";
							}
							
							// LED TEST RESULT : LED HUNDRED PERCENT

							else if (ManufacturingTests.redledtesthundred.equals(ManufacturingTests.check_string)) {
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if (ManufacturingTests.chckbxLightEngine.isSelected()) {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_RedLed_Voltage_hundredpercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_RedLed_Current_hundredpercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_RedLed_Voltage_hundredpercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_RedLed_Current_hundredpercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								} else {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}

								ManufacturingTests.check_string = "";
							} else if (ManufacturingTests.greenledtesthundred.equals(ManufacturingTests.check_string)) {
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if (ManufacturingTests.chckbxLightEngine.isSelected()) {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_GreenLed_Voltage_hundredpercent
											+ " V\t\tCurrent : "
											+ ManufacturingTests.Exp_GreenLed_Current_hundredpercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_GreenLed_Voltage_hundredpercent
											+ " V\t\tCurrent : "
											+ ManufacturingTests.Exp_GreenLed_Current_hundredpercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								} else {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}
								ManufacturingTests.check_string = "";
							} else if (ManufacturingTests.blueledtesthundred.equals(ManufacturingTests.check_string)) {
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if (ManufacturingTests.chckbxLightEngine.isSelected()) {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_BlueLed_Voltage_hundredpercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_BlueLed_Current_hundredpercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_BlueLed_Voltage_hundredpercent + " V\t\tCurrent : "
											+ ManufacturingTests.Exp_BlueLed_Current_hundredpercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								} else {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}
							} else if (ManufacturingTests.laserledtesthundred.equals(ManufacturingTests.check_string)) {
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if (ManufacturingTests.chckbxLightEngine.isSelected()) {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_LaserLed_Voltage_hundredpercent
											+ " V\t\tCurrent : "
											+ ManufacturingTests.Exp_LaserLed_Current_hundredpercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_LaserLed_Voltage_hundredpercent
											+ " V\t\tCurrent : "
											+ ManufacturingTests.Exp_LaserLed_Current_hundredpercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								} else {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}

								ManufacturingTests.check_string = "";
							} else if (ManufacturingTests.iris1ledtesthundred.equals(ManufacturingTests.check_string)) {
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if (ManufacturingTests.chckbxLightEngine.isSelected()) {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris1Led_Voltage_hundredpercent
											+ " V\t\tCurrent : "
											+ ManufacturingTests.Exp_Iris1Led_Current_hundredpercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris1Led_Voltage_hundredpercent
											+ " V\t\tCurrent : "
											+ ManufacturingTests.Exp_Iris1Led_Current_hundredpercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								} else {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}
								ManufacturingTests.check_string = "";
							} else if (ManufacturingTests.iris2ledtesthundred.equals(ManufacturingTests.check_string)) {
								String LedVoltage = actual[3];
								String LedCurrent = actual[4];
								if (ManufacturingTests.chckbxLightEngine.isSelected()) {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris2Led_Voltage_hundredpercent
											+ " V\t\tCurrent : "
											+ ManufacturingTests.Exp_Iris2Led_Current_hundredpercent + " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris2Led_Voltage_hundredpercent
											+ " V\t\tCurrent : "
											+ ManufacturingTests.Exp_Iris2Led_Current_hundredpercent + " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								} else {
									ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");

									ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : "
											+ ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload
											+ " V\t\tCurrent : "
											+ ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload
											+ " A\t(+/- 25%)");
									ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : "
											+ LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
								}

								ManufacturingTests.check_string = "";
							}

							// PDTESTS : IRIS1ZERO and HUNDRED
							else if (ManufacturingTests.iris1pdtestzero.equals(ManufacturingTests.check_string)) {
								String PhotoVoltage = actual[3];
								ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : "
										+ ManufacturingTests.Exp_iris1pdvoltage_zero + " V\t(+/- 25%)");
								ManufacturingTests.report_logger
										.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
										+ ManufacturingTests.Exp_iris1pdvoltage_zero + " V\t(+/- 25%");
								ManufacturingTests.logs_logger
										.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
							} else if (ManufacturingTests.iris1pdtesthundred.equals(ManufacturingTests.check_string)) {
								String PhotoVoltage = actual[3];
								ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : "
										+ ManufacturingTests.Exp_iris1pdvoltage_hundred + " V\t(+/- 25%)");
								ManufacturingTests.report_logger
										.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
										+ ManufacturingTests.Exp_iris1pdvoltage_hundred + " V\t(+/- 25%)");
								ManufacturingTests.logs_logger
										.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
							}

							// PDTESTS : IRIS2 ZERO AND HUNDRED

							else if (ManufacturingTests.iris2pdtestzero.equals(ManufacturingTests.check_string)) {
								String PhotoVoltage = actual[3];
								ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : "
										+ ManufacturingTests.Exp_iris2pdvoltage_zero + " V\t(+/- 25%)");
								ManufacturingTests.report_logger
										.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
										+ ManufacturingTests.Exp_iris2pdvoltage_zero + " V\t(+/- 25%)");
								ManufacturingTests.logs_logger
										.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
							} else if (ManufacturingTests.iris2pdtesthundred.equals(ManufacturingTests.check_string)) {
								String PhotoVoltage = actual[3];
								ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : "
										+ ManufacturingTests.Exp_iris2pdvoltage_hundred + " V\t(+/- 25%)");
								ManufacturingTests.report_logger
										.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
										+ ManufacturingTests.Exp_iris2pdvoltage_hundred + " V\t(+/- 25%)");
								ManufacturingTests.logs_logger
										.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
							}
							// PDTESTS : ENV ZERO AND HUNDRED

							else if (ManufacturingTests.envpdtestzero.equals(ManufacturingTests.check_string)) {
								String PhotoVoltage = actual[3];
								ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : "
										+ ManufacturingTests.Exp_envpdvoltage_zero + " V\t(+/- 25%)");
								ManufacturingTests.report_logger
										.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
										+ ManufacturingTests.Exp_envpdvoltage_zero + " V\t(+/- 25%)");
								ManufacturingTests.logs_logger
										.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
							} else if (ManufacturingTests.envpdtesthundred.equals(ManufacturingTests.check_string)) {
								String PhotoVoltage = actual[3];
								ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : "
										+ ManufacturingTests.Exp_envpdvoltage_hundred + " V\t(+/- 25%)");
								ManufacturingTests.report_logger
										.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : "
										+ ManufacturingTests.Exp_envpdvoltage_hundred + " V\t(+/- 25%)");
								ManufacturingTests.logs_logger
										.severe("Observed Value --> \tVoltage : " + PhotoVoltage + " V\r\n\n");
							}
							// CS TESTS : COLORSENSOR 1

							else if (ManufacturingTests.colorsensor1test.equals(ManufacturingTests.check_string)) {
								String ColorsensorVal_1 = actual[3];
								String ColorsensorVal_2 = actual[4];
								String ColorsensorVal_3 = actual[5];
								String ColorsensorVal_4 = actual[6];
								ManufacturingTests.report_logger
										.severe("Expected Value --> \t >100 and below \"FFFF\"");
								ManufacturingTests.report_logger.severe("Observed Value --> \tVal 1 : "
										+ ColorsensorVal_1 + " Val 2 : " + ColorsensorVal_2 + " Val 3 : "
										+ ColorsensorVal_3 + " Val 4 : " + ColorsensorVal_4 + "\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Value --> \t >100 and below \"FFFF\"");
								ManufacturingTests.logs_logger.severe("Observed Value --> \tVal 1 : " + ColorsensorVal_1
										+ " Val 2 : " + ColorsensorVal_2 + " Val 3 : " + ColorsensorVal_3 + " Val 4 : "
										+ ColorsensorVal_4 + "\r\n\n");
							} else if (ManufacturingTests.colorsensor2test.equals(ManufacturingTests.check_string)) {
								String ColorsensorVal_1 = actual[3];
								String ColorsensorVal_2 = actual[4];
								String ColorsensorVal_3 = actual[5];
								String ColorsensorVal_4 = actual[6];
								ManufacturingTests.report_logger
										.severe("Expected Value --> \t >100 and below \"FFFF\"");
								ManufacturingTests.report_logger.severe("Observed Value --> \tVal 1 : "
										+ ColorsensorVal_1 + " Val 2 : " + ColorsensorVal_2 + " Val 3 : "
										+ ColorsensorVal_3 + " Val 4 : " + ColorsensorVal_4 + "\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Value --> \t >100 and below \"FFFF\"");
								ManufacturingTests.logs_logger.severe("Observed Value --> \tVal 1 : " + ColorsensorVal_1
										+ " Val 2 : " + ColorsensorVal_2 + " Val 3 : " + ColorsensorVal_3 + " Val 4 : "
										+ ColorsensorVal_4 + "\r\n\n");
							}

							// PWM MUXTESTS
							else if (ManufacturingTests.wluc1pwmmuxtest.equals(ManufacturingTests.check_string)) {
								String MuxInterrupt_Count = actual[3];
								ManufacturingTests.report_logger.severe("Expected Value --> \t"
										+ ManufacturingTests.Exp_wluc1pwmmuxtest_intcount + " (+/- 3)");
								ManufacturingTests.report_logger
										.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Value --> \t"
										+ ManufacturingTests.Exp_wluc1pwmmuxtest_intcount + " (+/- 3)");
								ManufacturingTests.logs_logger
										.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
							} else if (ManufacturingTests.envuc1pwmmuxtest.equals(ManufacturingTests.check_string)) {
								String MuxInterrupt_Count = actual[3];
								ManufacturingTests.report_logger.severe("Expected Value --> \t"
										+ ManufacturingTests.Exp_envuc1pwmmuxtest_intcount + " (+/- 3)");
								ManufacturingTests.report_logger
										.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Value --> \t"
										+ ManufacturingTests.Exp_envuc1pwmmuxtest_intcount + " (+/- 3)");
								ManufacturingTests.logs_logger
										.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
							} else if (ManufacturingTests.irisuc1pwmmuxtest.equals(ManufacturingTests.check_string)) {
								String MuxInterrupt_Count = actual[3];
								ManufacturingTests.report_logger.severe("Expected Value --> \t"
										+ ManufacturingTests.Exp_irisuc1pwmmuxtest_intcount + " (+/- 3)");
								ManufacturingTests.report_logger
										.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Value --> \t"
										+ ManufacturingTests.Exp_irisuc1pwmmuxtest_intcount + " (+/- 3)");
								ManufacturingTests.logs_logger
										.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
							} else if (ManufacturingTests.wluc2pwmmuxtest.equals(ManufacturingTests.check_string)) {
								String MuxInterrupt_Count = actual[3];
								ManufacturingTests.report_logger.severe("Expected Value --> \t"
										+ ManufacturingTests.Exp_wluc2pwmmuxtest_intcount + " (+/- 3)");
								ManufacturingTests.report_logger
										.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Value --> \t"
										+ ManufacturingTests.Exp_wluc2pwmmuxtest_intcount + " (+/- 3)");
								ManufacturingTests.logs_logger
										.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
							} else if (ManufacturingTests.envuc2pwmmuxtest.equals(ManufacturingTests.check_string)) {
								String MuxInterrupt_Count = actual[3];
								ManufacturingTests.report_logger.severe("Expected Value --> \t"
										+ ManufacturingTests.Exp_envuc2pwmmuxtest_intcount + " (+/- 3)");
								ManufacturingTests.report_logger
										.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Value --> \t"
										+ ManufacturingTests.Exp_envuc2pwmmuxtest_intcount + " (+/- 3)");
								ManufacturingTests.logs_logger
										.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
							} else if (ManufacturingTests.irisuc2pwmmuxtest.equals(ManufacturingTests.check_string)) {
								String MuxInterrupt_Count = actual[3];
								ManufacturingTests.report_logger.severe("Expected Value --> \t"
										+ ManufacturingTests.Exp_irisuc2pwmmuxtest_intcount + " (+/- 3)");
								ManufacturingTests.report_logger
										.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Value --> \t"
										+ ManufacturingTests.Exp_irisuc2pwmmuxtest_intcount + " (+/- 3)");
								ManufacturingTests.logs_logger
										.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
							} else if (ManufacturingTests.wlextpwmmuxtest.equals(ManufacturingTests.check_string)) {
								String MuxInterrupt_Count = actual[3];
								ManufacturingTests.report_logger.severe("Expected Value --> \t"
										+ ManufacturingTests.Exp_wlextpwmmuxtest_intcount + " (+/- 3)");
								ManufacturingTests.report_logger
										.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Value --> \t"
										+ ManufacturingTests.Exp_wlextpwmmuxtest_intcount + " (+/- 3)");
								ManufacturingTests.logs_logger
										.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
							} else if (ManufacturingTests.envextpwmmuxtest.equals(ManufacturingTests.check_string)) {
								String MuxInterrupt_Count = actual[3];
								ManufacturingTests.report_logger.severe("Expected Value --> \t"
										+ ManufacturingTests.Exp_envextpwmmuxtest_intcount + " (+/- 3)");
								ManufacturingTests.report_logger
										.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Value --> \t"
										+ ManufacturingTests.Exp_envextpwmmuxtest_intcount + " (+/- 3)");
								ManufacturingTests.logs_logger
										.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
							} else if (ManufacturingTests.irisextwlpwmmuxtest.equals(ManufacturingTests.check_string)) {
								String MuxInterrupt_Count = actual[3];
								ManufacturingTests.report_logger.severe("Expected Value --> \t"
										+ ManufacturingTests.Exp_irisextwlpwmmuxtest_intcount + " (+/- 3)");
								ManufacturingTests.report_logger
										.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Value --> \t"
										+ ManufacturingTests.Exp_irisextwlpwmmuxtest_intcount + " (+/- 3)");
								ManufacturingTests.logs_logger
										.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
							} else if (ManufacturingTests.irisextenvpwmmuxtest
									.equals(ManufacturingTests.check_string)) {
								String MuxInterrupt_Count = actual[3];
								ManufacturingTests.report_logger.severe("Expected Value --> \t"
										+ ManufacturingTests.Exp_irisextenvpwmmuxtest_intcount + " (+/- 3)");
								ManufacturingTests.report_logger
										.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");

								ManufacturingTests.logs_logger.severe("Expected Value --> \t"
										+ ManufacturingTests.Exp_irisextenvpwmmuxtest_intcount + " (+/- 3)");
								ManufacturingTests.logs_logger
										.severe("Observed Value --> \t" + MuxInterrupt_Count + "\r\n\n");
							}
							ManufacturingTests.UpdateTestResults(Fail, reason, source, value);
							ManufacturingTests.report_logger.severe(ManufacturingTests.ArrowDelimitter);

							recv_buff = null;
							Resp = "";
						} catch (Exception ex) {
							ex.printStackTrace();

						}

					} else {
						notify_flag = 1;
						synchronized (ManufacturingTests.lockObject) {
							ManufacturingTests.busyflag = false;
							ManufacturingTests.lockObject.notifyAll();
						}
						// ManufacturingTests.table.getModel().setValueAt("PASSED", 1, 4);

						ManufacturingTests.btnStartButton.setEnabled(true);
						ManufacturingTests.btnStartButton.setText("RUN");
						// ManufacturingTests.btnBack.setEnabled(true);
						recv_buff = null;
						Resp = "";
					}

				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (ManufacturingTests.MFTest_ExitFlag) {
				full_buffer.reset();
				ManufacturingTests.MFTest_ExitFlag = false;

			} else {
				// System.out.println("Trigger Source : " + current_control + " Command");
				print("Buffer at Entry Point : " + Arrays.toString(recv_buff));
				int Is_Error = 0;

				Is_Error = return_error_type(recv_buff);
				System.out.println("Error Number : " + Is_Error);
				for (int i = 0; i < recv_buff.length; i++) {
					recv_buff[i] = 0;
				}
				if (Is_Error == 0) {
					print("Error Free Buffer Received");
					null_charflag = true;

					notify_flag = 1;
					if (serialport != null) {
						serialport.removeDataListener();
						// ##print("Data Listener Removed!!");
					}

				} else if (Is_Error < 9) {
					String err = return_error_name(Is_Error);
					System.out.print("Error is : ");
					System.out.println(Is_Error);
					System.out.println(" , " + err);
					notify_flag = 1;
					null_charflag = false;
					full_buffer.reset();
					serialport.removeDataListener();

					System.out.println("Packet Received with some Error!!");

				} else if (Is_Error == 9) {

					// Response Timedout
					notify_flag = 0;
					null_charflag = false;
					serialport.removeDataListener();
					full_buffer.reset();

				} else {
					System.out.println("Error Code is  : " + Is_Error);
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	// ####################################################################################################
	/*
	 * Function Name : return_error_type Input : A Byte Array Output : An Integer
	 * representing one of the error types in the error Struct Description : checks
	 * the given buffer for the occurrence of one of the errors in the given input
	 * buffer(received packet)
	 * 
	 */
	// ####################################################################################################
	/*
	 * Order of Error Check in the Received Packet 1. Framing Error 2. Stuffing
	 * Error 3. CheckSum Error 4. Message Type Error 5. Command Type Error 6.
	 * Response OK or Error in Response
	 * 
	 */
	public static int return_error_type(byte[] recv_buff) {
		try {
			int end_index = recv_buff.length - 1;

			// #################################Check Framing
			// Error###########################################
			resp_error_type = RespError_Flag.Framing_Error.value;
			@SuppressWarnings("unused")
			bytedefinitions reference = new bytedefinitions();

			resp_error_type = check_framing_error(recv_buff);
			if (resp_error_type != RespError_Flag.Response_OK.value) {
				System.out.println("Framing Error in the received packet. Cannot Process it Further!!!");
				print("RecvBuff : " + Arrays.toString(recv_buff));
				return resp_error_type;
			}

			// #################################Check Stuffing
			// Error##########################################
			resp_error_type = RespError_Flag.Stuffing_Error.value;
			// strip off the start-byte and stop-byte from the original buffer
			destuff_buffer = Arrays.copyOfRange(recv_buff, reference_index + 1, end_index);
			destuff_buffer = check_destuff_error(destuff_buffer);
			// print("Destuff_buffer : "+Arrays.toString(destuff_buffer));
			if (resp_error_type != RespError_Flag.Response_OK.value) {
				System.out.println("De-Stuffing-Error in the received packet. Cannot Process it Further!!");
				return resp_error_type;
			} else {
				// #System.out.println("No De-Stuffing Error in the received packet!!");
			}

			// ##################################Check CheckSum
			// Error##########################################
			resp_error_type = RespError_Flag.Checksum_Error.value;

			// for(int i=0;i<destuff_buffer.length;i++)
			// {
			// System.out.print(Hex(destuff_buffer[i],2) + ",");
			// }
			resp_error_type = check_checksum_error(destuff_buffer);
			if (resp_error_type == RespError_Flag.Checksum_Error.value) {
				print("CheckSum Error in the received Buffer. Cannot Process it Further!!");

				return resp_error_type;
			}

			// #################################Check Message
			// Type##############################################
			resp_error_type = RespError_Flag.Unknown_MsgType_Error.value;

			if (destuff_buffer[0] == expected_message_type) {
				resp_error_type = RespError_Flag.Response_OK.value;

			}

			else {
				print("Response Type Mismatch Error");
				// System.out.println("Expected Resp type is : " + expected_message_type);
				return resp_error_type;
			}

			// #################################Check for Command Id
			// Match######################################
			resp_error_type = RespError_Flag.Unknown_Cmd_Error.value;
			if (destuff_buffer[1] == expected_cmd_id) {
				resp_error_type = RespError_Flag.Response_OK.value;
			} else {
				print("Command ID Mismatch in Received Packet!!");
				display("Command ID Mismatch in the received Packet\r\n");
				return resp_error_type;
			}

			// #################################Check Response_Code - Success or Error
			// Response#####################

			resp_error_type = check_response_code(destuff_buffer);
			if (resp_error_type == 0x0) {
				// print("Error Free Response Message or Notification Message Received!!");
			} else {

				print("Response Message Received with Error Response!!");
			}
			bytedefinitions b = new bytedefinitions();
			if (expected_cmd_id != b.SENSING_COMMAND && expected_cmd_id != b.RESTORE_CMD) {
				for (int i = 0; i < destuff_buffer.length; i++) {
					destuff_buffer[i] = 0;
				}
			}
			System.out.println("Actual Code : " + resp_error_type);
			System.out.println("Sending Back Response...");

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return Error_Message;

	}

	// #####################################################################################################

	// Function that checks for framing error
	/*
	 * Function Name : check_framing_error Input : A Byte Array Output : An Integer
	 * representing one of the error types in the error Struct Description : checks
	 * for Framing-Error in the given input buffer(received packet)
	 * 
	 */
	// ######################################################################################################
	public static int check_framing_error(byte[] rec_buff) {

		boolean start_end_byteflag = false;
		bytedefinitions reference = new bytedefinitions();
		int end_index = rec_buff.length - 1;
		for (int i = 0; i < rec_buff.length; i++) {
			if (rec_buff[i] == reference.START_byte) {
				reference_index = i;
				start_end_byteflag = false;
				break;

			} else {
				start_end_byteflag = true;
			}
		}
		if (start_end_byteflag == false) {
			if (rec_buff[end_index] == reference.END_byte) {
				start_end_byteflag = false;
			} else {
				start_end_byteflag = true;
			}
		}
		// #System.out.println("Framing Buffer : " + Arrays.toString(recv_buff));
		if (start_end_byteflag == false) {
			// #System.out.println("No Framing Error in the received Packet");
			resp_error_type = RespError_Flag.Response_OK.value;
		}

		return resp_error_type;

	}

	// #####################################################################################################
	// Function that checks for destuffing_error
	/*
	 * Function Name : check_destuff_error Input : A Byte Array Output : An Integer
	 * representing one of the error types in the error Struct Description : checks
	 * for de-stuffing error in the given input buffer(received packet)
	 * 
	 */
	// ######################################################################################################
	public void get_destuffed_buffer() {

	}

	public static byte[] check_destuff_error(byte[] recv_buff) {
		ArrayList<Byte> copy_buffer = new ArrayList<Byte>();

		// print("Recv_buff Inside destuff function : " +Arrays.toString(recv_buff));

		boolean Escape_Character_Flag = false;
		bytedefinitions b = new bytedefinitions();
		// #System.out.println("Destuff Buffer : " + Arrays.toString(recv_buff));
		for (int i = 0; i < recv_buff.length; i++) {
			// print("Iterating through for loop");
			// if 'i' is not the end index and element at 'i' th position is '0xEE',check
			// next byte for 0x55,0xAA,0xEE

			if (recv_buff[i] == b.ESC_byte && Escape_Character_Flag == false) {

				// print("ESC Char Received");
				Escape_Character_Flag = true;
			} else {
				if (recv_buff[i] == b.ESC_byte && Escape_Character_Flag == true) {
					copy_buffer.add(recv_buff[i]);

					resp_error_type = RespError_Flag.Response_OK.value;
				} else if (recv_buff[i] != b.START_byte && recv_buff[i] != b.END_byte
						&& Escape_Character_Flag == true) {
					// print("Returning Error");
					resp_error_type = RespError_Flag.Stuffing_Error.value;
					byte[] data = new byte[copy_buffer.size()];
					for (int k = 0; k < data.length; k++) {
						data[k] = (byte) copy_buffer.get(k);
					}
					return data;
				} else {
					// print("ESC Char is not present!!");
					copy_buffer.add(recv_buff[i]);

				}

				resp_error_type = RespError_Flag.Response_OK.value;
				Escape_Character_Flag = false;
			}

		}

		// print("Inside destuffing function : " +Arrays.toString(recv_buff));
		// Return the error type
		byte[] data = new byte[copy_buffer.size()];
		for (int k = 0; k < data.length; k++) {
			data[k] = (byte) copy_buffer.get(k);
		}
		return data;

	}

	// #####################################################################################################
	/*
	 * Function Name : check_checksum_error Input : A Byte Array Output : An Integer
	 * representing one of the error types in the error Struct Description : checks
	 * for checksum_error in the given input buffer(received packet)
	 * 
	 */
	// ######################################################################################################
	public static int check_checksum_error(byte[] recv_buff) {
		print("Buffer Received for checksum : " + Arrays.toString(recv_buff));

		// Parse the received buffer to concatenate two bytes into a short int. This is
		// required as checksum should be
		// a 16 bit (Integer) so as to satisfy the checksum addition , thus the result
		// to be zero
		ByteBuffer bb = ByteBuffer.allocate(4);
		// Little Endian
		bb.order(ByteOrder.LITTLE_ENDIAN);
		// Place the LSB and MSB of Checksum bytes into the buffer
		bb.put(recv_buff[recv_buff.length - 2]);
		bb.put(recv_buff[recv_buff.length - 1]);
		int shortVal = bb.getInt(0);

		// Replace the checksum bytes with 0x0
		recv_buff[recv_buff.length - 2] = 0x0;
		recv_buff[recv_buff.length - 1] = 0x0;

		int temp = 0;
		int temp_1 = 0;

		// Add all the bytes in the received buffer except checksum
		for (int i = 0; i < recv_buff.length; i++) {
			// A Fix for adding all the bytes in the
			temp_1 = (int) recv_buff[i] & 0xff;
			temp = temp + temp_1;
		}

		// Add the result with checksum
		temp = temp + shortVal;

		// Check if the result is 0
		if ((temp & 0xffff) == 0) {
			// Return Response OK - if 0
			resp_error_type = RespError_Flag.Response_OK.value;// #System.out.println("No Checksum Error in the received
																// // Packet!!");
		} else {
			// Return CheckSum Error
			resp_error_type = RespError_Flag.Checksum_Error.value;
		}

		return resp_error_type;

	}

	// #####################################################################################################

	/*
	 * Function Name : check_response_code Input : A Byte Array Output : An Integer
	 * representing one of the error types in the error Struct Description : checks
	 * for error in response_code in the given input buffer(received packet)
	 * 
	 */
	// ######################################################################################################
	public static int check_response_code(byte[] recv_buff) {
		byte result = 0x0;
		bytedefinitions b = new bytedefinitions();
		if (recv_buff[0] == b.MSG_TYPE_RESP) {
			// #print("Response Message");
			if (recv_buff[7] == RespError_Flag.Response_OK.value) // Pay-load is the actual response
			{
				// ##print("No Error in the Response Packet");
			} else {
				// Check for error response if and only if the received command id is not one of
				// below
				if (expected_cmd_id != b.SENSING_COMMAND && expected_cmd_id != b.RESTORE_CMD) {
					byte Error_Code = recv_buff[7];
					byte Error_Source = recv_buff[8];
					String Error_Src = "";

					if (Error_Source == b.SRC_JAVA_APP) {
						Error_Src = "Java App";
					} else if (Error_Source == b.SRC_uC1) {
						Error_Src = "uC1";
					} else if (Error_Source == b.SRC_uC2) {
						Error_Src = "uC2";
					} else if (Error_Source == b.SRC_CONTROLLER1_RESP) {
						Error_Src = "uC1-Response";
					} else {
					}

					String Error_Name = "";
					Error_Name = return_error_name(Error_Code);
					if (Error_Name == "RESPONSE_OK") {

						ShowMessage(
								"Error Code : " + Integer.toString(Error_Code) + " Error Description : RESPONSE_OK");
						result = 0x0;
						return result;
					} else {
						switch_flag = true;
						ShowMessage("Error Code : " + Integer.toString(Error_Code) + " Error Description : "
								+ Error_Name + " Source : " + Error_Src);
						result = 0x1;
						if (WlPowerButton.isSelected()) {
							WlPowerButton.setIcon(new ImageIcon("/resources/on.jpg"));
						} else if (!WlPowerButton.isSelected()) {
							WlPowerButton.setIcon(new ImageIcon("/resources/on.jpg"));
						} else if (EnvIrisPowerButton.isSelected()) {
							EnvIrisPowerButton.setIcon(new ImageIcon("/resources/on.jpg"));
						} else if (!EnvIrisPowerButton.isSelected()) {
							EnvIrisPowerButton.setIcon(new ImageIcon("/resources/off.jpg"));
						}
						return result;

					}

				}

			}

		} // If it is a notification message, do nothing
		else if (recv_buff[0] == b.MSG_TYPE_NOTIFY) {

			print("Notofication Message");
		}

		// Return the value
		return result;

	}

	// #################################################################################################################################
	/*
	 * Function Name : return_error name : A Byte representing an error_code : A
	 * String representing one of the error types in the error Struct Description :
	 * Returns a string corresponding to the given error_code(input) in the given
	 * input buffer(received packet)
	 * 
	 */
	// #################################################################################################################################
	public static String return_error_name(int error_code) {
		System.out.print("Error Code is : ");
		System.out.print(error_code);
		String return_val = "";
		if (error_code == RespError_Flag.Response_OK.value) {
			return_val = "RESPONSE_OK";
		} else if (error_code == RespError_Flag.Unknown_Cmd_Error.value) {
			return_val = "UNKNOWN_COMMAND_ERROR";
		} else if (error_code == RespError_Flag.Unknown_MsgType_Error.value) {
			return_val = "UNKNOWN_MESSAGE_TYPE_ERROR";
		} else if (error_code == RespError_Flag.Unknown_State_Error.value) {
			return_val = "UNKNOWN_STATE_ERROR";
		} else if (error_code == RespError_Flag.Payload_Length_Error.value) {
			return_val = "PAYLOAD_LENGTH_ERROR";
		} else if (error_code == RespError_Flag.IPCMutex_Timeout_Error.value) {
			return_val = "IPCMUTEX_TIMEOUT_ERROR";
		} else if (error_code == RespError_Flag.Timeout_Error.value) {
			return_val = "TIMEOUT_ERROR";
		} else if (error_code == RespError_Flag.IPC_Error.value) {
			return_val = "IPC_ERROR";
		} else if (error_code == RespError_Flag.Cmd_Execution_Error.value) {
			return_val = "COMMAND_EXECUTION_ERROR";
		} else if (error_code == RespError_Flag.ResponseType_MismatchError.value) {
			return_val = "RESPONSE_TYPE_MISMATCH_ERROR";
		} else if (error_code == RespError_Flag.CommandMode_MismatchError.value) {
			return_val = "COMMAND_MODE_MISMATCH_ERROR";
		} else if (error_code == RespError_Flag.InvalidCommand_ModeError.value) {
			return_val = "INVALID_COMMAND_MODE_ERROR";
		}
		print("Value to be returned : ");
		print(return_val);
		return return_val;

	}

	// ################################################################################################################################
	/*
	 * Function Name : check_responsetype_error Input : A Byte Array, A string
	 * representing the control/JCompoenent Output : An Integer representing one of
	 * the error types in the error Struct Description : checks for response type
	 * mismatch error in the given input buffer (received packet)
	 * 
	 */
	// ################################################################################################################################
	public static int check_reponsetype_mismatcherror(byte[] recv_buff) {
		@SuppressWarnings("unused")
		bytedefinitions b = new bytedefinitions();
		/*
		 * if((source_control == reset_uc1)||(source_control == reset_uc2)) {
		 * if(recv_buff[0] == b.MSG_TYPE_NOTIFY) { resp_error_type =
		 * RespError_Flag.No_Error.value; }
		 * 
		 * }
		 */
		if (recv_buff[0] == expected_message_type) {
			resp_error_type = RespError_Flag.Response_OK.value;
		}

		return resp_error_type;

	}

	/**
	 * Create the application.
	 * 
	 * @throws IOException
	 */
	// Entry point of the application
	public Stryk_Demo() {
		try {
			@SuppressWarnings("unused")
			bytedefinitions b = new bytedefinitions();
			// Function that initializes the Users Interface
			initialize();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception at initialize() function : " + e.getLocalizedMessage());
		}

	}

	public static void unlock() {
		try {
			if (lock != null) {
				lock.release();
				channel.close();
				f.delete();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void GoToConnectedState() throws Exception {

	}

	public void clear_portlist() {
		for (int pl = 0; pl < portlist.length; pl++) {
			portlist[pl] = null;
		}
	}

	public void clear_portlistforcombobox() {

		for (int plfcp = 0; plfcp < portlistforcombobox.length; plfcp++) {
			portlistforcombobox[plfcp] = null;
		}

	}

	// Function that adds the available serial ports as a list to the comboBox

	/**
	 * Initialize the contents of the frame. The first function that is called by
	 * the main class()
	 */
	@SuppressWarnings({ "static-access", "unchecked", "rawtypes" })
	public void initialize() throws Exception {
		System.out.println(LocalDateTime.now());
		System.out.println("<-<-<-<-<-<-<-<-<-<-<-<-<STRYKER>->->->->->->->->->->->->");
		System.out.println("Initializing...");
		// ##System.out.println(Integer.toString(start));
		frmStrykerVer = new JFrame();
		frmStrykerVer.setAlwaysOnTop(false);
		frmStrykerVer.setResizable(false);
		frmStrykerVer.setForeground(UIManager.getColor("InternalFrame.borderColor"));

		frmStrykerVer.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		frmStrykerVer.getContentPane().setForeground(SystemColor.window);
		frmStrykerVer.getContentPane().setLocation(-12, -319);
		frmStrykerVer.setTitle("L11 Java Application");
		frmStrykerVer.getContentPane().setPreferredSize(new Dimension(200, 200));
		frmStrykerVer.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
		@SuppressWarnings("unused")
		Color c = Color.decode("#eeeeee");

		@SuppressWarnings("unused")
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		// frmStrykerVer.setLocation(dim.width/2-frmStrykerVer.getSize().width/2,
		// dim.height/2-frmStrykerVer.getSize().height/2);

		frmStrykerVer.getContentPane().setBackground(SystemColor.textHighlightText);

		@SuppressWarnings("unused")
		final Toolkit toolkit = frmStrykerVer.getToolkit();

		frmStrykerVer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// frame.setDefaultLookAndFeelDecorated(true);

		frmStrykerVer.setBounds(50, 100, 1361, 665);

		frmStrykerVer.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {

				if ((comboBox.getSelectedItem() != null) && (comboBox.getSelectedItem() != "")) {
					writestringasbytestoserialport("");
				} else {

				}
				frmStrykerVer.setVisible(false);

				// System.exit(0);
			}
		});
		this.frmStrykerVer.getContentPane().setLayout(null);
		frmStrykerVer.setLocationByPlatform(false);
		portlistforcombobox = null;

		// Call scan_serialport function here
		// ##System.out.println("Calling Scan_serialports function");
		scan_serialports ssp = new scan_serialports();
		comportemptyflag = ssp.scan_serialport(comportemptyflag);

		// scan_serialports(); // portlistforcombobox is a string array that
		portlist = ssp.get_serialports();// contains the portnames.
		portlistforcombobox = ssp.get_portlist();
		// ##System.out.println("Exiting from the scan_serialport() class");

		if (comportemptyflag == true) {
			// ##System.out.println("Com Port Empty Flag : True");
			System.out.println("Nothing to update with the Combobox");
			clear_portlistforcombobox();
		}
		updateportlist(comportemptyflag);
		// System.out.println("Checking ComPort Flag");

		if (comportemptyflag == false) {
			// System.out.println("Some ports detected...");
			if (comboBox.getSelectedItem().toString() != null) {
				if (portlist != null) {
					// serialport = portlist[comboBox.getSelectedIndex()]; // Describe
					mftserialPort = portlist[comboBox.getSelectedIndex()];
					setcommparams sc = new setcommparams();
					// sc.setcommparams(serialport); // call this function to
					sc.setcommparams(mftserialPort);

					calibrationserialport = portlist[comboBox.getSelectedIndex()];
					sc.setcommparams(calibrationserialport);
					// configure the serial port
					// parameters
					// setlistener();
				}
			}

		}

		@SuppressWarnings("unused")
		Color c1 = Color.decode("#ff9999");
		@SuppressWarnings("unused")
		Color c2 = Color.decode("#99ff99");
		@SuppressWarnings("unused")
		Color c3 = Color.decode("#99ccff");
		// Color c4 = Color.decode("#e6ecff");
		@SuppressWarnings("unused")
		Color c4 = Color.decode("#e6ecff");

		NumberFormat format = NumberFormat.getInstance();
		NumberFormatter formatter = new NumberFormatter(format);
		formatter.setValueClass(Integer.class);
		formatter.setMinimum(0);
		formatter.setMaximum(100);
		formatter.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of
		// focus lost
		formatter.setCommitsOnValidEdit(true);

		NumberFormatter rinc_formatter = new NumberFormatter(format);
		rinc_formatter.setValueClass(Integer.class);
		rinc_formatter.setMinimum(0);
		rinc_formatter.setMaximum(100);
		rinc_formatter.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of
		// focus lost
		rinc_formatter.setCommitsOnValidEdit(true);

		NumberFormatter iris1_formatter = new NumberFormatter(format);
		iris1_formatter.setValueClass(Integer.class);
		iris1_formatter.setMinimum(0);
		iris1_formatter.setMaximum(100);
		iris1_formatter.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of
		// focus lost
		iris1_formatter.setCommitsOnValidEdit(true);

		NumberFormatter iris2_formatter = new NumberFormatter(format);
		iris2_formatter.setValueClass(Integer.class);
		iris2_formatter.setMinimum(0);
		iris2_formatter.setMaximum(100);
		iris2_formatter.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of
		// focus lost
		iris2_formatter.setCommitsOnValidEdit(true);

		NumberFormat format1 = NumberFormat.getInstance();
		NumberFormatter formatter1 = new NumberFormatter(format1);
		formatter1.setValueClass(Integer.class);
		formatter1.setMinimum(0);
		formatter1.setMaximum(100);
		formatter1.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of
		// focus lost
		formatter1.setCommitsOnValidEdit(true);

		// Fromatter for textfield
		NumberFormat formattf = NumberFormat.getInstance();
		formattf.setGroupingUsed(false);
		NumberFormatter formattertf = new NumberFormatter(formattf);
		formattertf.setValueClass(Integer.class);
		formattertf.setMinimum(0);
		formattertf.setMaximum(4095);
		formattertf.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of
		// focus lost
		formattertf.setCommitsOnValidEdit(true);
		
		// Fromatter for textfield
		NumberFormat formattf1 = NumberFormat.getInstance();
		formattf1.setGroupingUsed(false);
		NumberFormatter formattertf1 = new NumberFormatter(formattf1);
		formattertf1.setValueClass(Integer.class);
		formattertf1.setMinimum(0);
		formattertf1.setMaximum(255);
		formattertf1.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of
		// focus lost
		formattertf1.setCommitsOnValidEdit(true);

		NumberFormat format2 = NumberFormat.getInstance();
		NumberFormatter formatter2 = new NumberFormatter(format2);
		formatter2.setValueClass(Integer.class);
		formatter2.setMinimum(0);
		formatter2.setMaximum(100);
		formatter2.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of
		// focus lost
		formatter2.setCommitsOnValidEdit(true);

		NumberFormat format3 = NumberFormat.getInstance();
		NumberFormatter formatter3 = new NumberFormatter(format3);
		formatter3.setValueClass(Integer.class);
		formatter3.setMinimum(0);
		formatter3.setMaximum(100);
		formatter3.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of
		// focus lost
		formatter3.setCommitsOnValidEdit(true);
		// #System.out.println("Components Initialized");

		NumberFormat format4 = NumberFormat.getInstance();
		NumberFormatter formatter4 = new NumberFormatter(format4);
		formatter4.setValueClass(Integer.class);
		formatter4.setMinimum(0);
		formatter4.setMaximum(100);
		formatter4.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of
		// focus lost
		formatter4.setCommitsOnValidEdit(true);

		NumberFormat format5 = NumberFormat.getInstance();
		NumberFormatter formatter5 = new NumberFormatter(format5);
		formatter5.setValueClass(Integer.class);
		formatter5.setMinimum(0);
		formatter5.setMaximum(100);
		formatter5.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of
		// focus lost
		formatter5.setCommitsOnValidEdit(true);

		NumberFormat format6 = NumberFormat.getInstance();
		NumberFormatter formatter6 = new NumberFormatter(format6);
		formatter6.setValueClass(Integer.class);
		formatter6.setMinimum(0);
		formatter6.setMaximum(100);
		formatter6.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of
		// focus lost
		formatter6.setCommitsOnValidEdit(true);

		NumberFormat format7 = NumberFormat.getInstance();
		NumberFormatter formatter7 = new NumberFormatter(format7);
		formatter7.setValueClass(Integer.class);
		formatter7.setMinimum(0);
		formatter7.setMaximum(100);
		formatter7.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of
		// focus lost
		formatter7.setCommitsOnValidEdit(true);

		NumberFormat format8 = NumberFormat.getInstance();
		NumberFormatter formatter8 = new NumberFormatter(format8);
		formatter8.setValueClass(Integer.class);
		formatter8.setMinimum(0);
		formatter8.setMaximum(100);
		formatter8.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of
		// focus lost
		formatter8.setCommitsOnValidEdit(true);

		NumberFormat format9 = NumberFormat.getInstance();
		NumberFormatter formatter9 = new NumberFormatter(format9);
		formatter9.setValueClass(Integer.class);
		formatter9.setMinimum(0);
		formatter9.setMaximum(100);
		formatter9.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of
		// focus lost
		formatter9.setCommitsOnValidEdit(true);

		NumberFormat format10 = NumberFormat.getInstance();
		NumberFormatter formatter10 = new NumberFormatter(format10);
		formatter10.setValueClass(Integer.class);
		formatter10.setMinimum(0);
		formatter10.setMaximum(100);
		formatter10.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of
		// focus lost
		formatter10.setCommitsOnValidEdit(true);

		NumberFormat format11 = NumberFormat.getInstance();
		NumberFormatter formatter11 = new NumberFormatter(format11);
		formatter11.setValueClass(Integer.class);
		formatter11.setMinimum(0);
		formatter11.setMaximum(100);
		formatter11.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of
		// focus lost
		formatter11.setCommitsOnValidEdit(true);

		NumberFormat format12 = NumberFormat.getInstance();
		NumberFormatter formatter12 = new NumberFormatter(format12);
		formatter12.setValueClass(Integer.class);
		formatter12.setMinimum(0);
		formatter12.setMaximum(100);
		formatter12.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of
		// focus lost
		formatter12.setCommitsOnValidEdit(true);

		panel_2 = new JPanel();
		panel_2.setForeground(new Color(255, 255, 255));
		panel_2.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("SplitPane.border"), "COM Port", TitledBorder.CENTER,
				TitledBorder.TOP, null, new Color(0, 0, 0)));
		((TitledBorder) panel_2.getBorder()).setTitleFont(new Font("Tahoma", Font.BOLD, 10));
		panel_2.setBounds(3, 11, 238, 101);
		frmStrykerVer.getContentPane().add(panel_2);
		panel_2.setLayout(null);
		this.btnConnect = new JButton("CONNECT");
		btnConnect.setBounds(9, 55, 98, 23);
		btnConnect.setFont(new Font("Calibri", Font.PLAIN, 13));
		panel_2.add(btnConnect);
		btnConnect.setIcon(null);
		btnConnect.setMultiClickThreshhold(1000L);
		this.btnConnect.setToolTipText("Click this Button to connect with the controller ");

		// Disconnect Button action Handler
		this.btnDisconnect = new JButton("DISCONNECT");
		btnDisconnect.setBounds(108, 55, 119, 23);
		btnDisconnect.setFont(new Font("Calibri", Font.PLAIN, 13));
		panel_2.add(btnDisconnect);
		this.btnDisconnect.setMultiClickThreshhold(1000);
		this.btnDisconnect.setToolTipText("Click this Button to Disconnect from the controller ");

		btnDisconnect.setEnabled(false); // By default disconnect button should
		this.btnRefreshPortList = new JButton("Scan Ports");
		btnRefreshPortList.setBounds(108, 21, 119, 23);
		btnRefreshPortList.setFont(new Font("Calibri", Font.PLAIN, 13));
		panel_2.add(btnRefreshPortList);

		// Refresh Port List button
		btnRefreshPortList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("*************************Refreshing Port List*************************");
				try {
					comboBox.removeAllItems();
					// Wherever be used , follow this order of calling the class
					// and it's associated functions
					scan_serialports ssp = new scan_serialports();
					comportemptyflag = ssp.scan_serialport(comportemptyflag);
					portlist = ssp.get_serialports();
					portlistforcombobox = ssp.get_portlist();

					int cbcount = comboBox.getItemCount();
					if (cbcount > portlist.length) {
						System.out.print("The Number of elements in the combobox is : ");
						System.out.println(Integer.toString(cbcount));
						System.out.print("The number of items in the port list is : ");
						System.out.println(Integer.toString(portlist.length));
						System.out.println("comboBox has more items than the actual serial ports...");
					}

					@SuppressWarnings({})
					DefaultComboBoxModel model = new DefaultComboBoxModel(portlistforcombobox);
					comboBox.setModel(model);
					clear_portlistforcombobox();// This helped to prevent
					// duplication in combobox

					// #System.out.println(Integer.toString(comboBox.getSelectedIndex()));
					if (comboBox.getSelectedItem() != null && portlist.length > 0) {
						System.out.println("Current Serial Port : " + comboBox.getSelectedItem());

						serialport = portlist[comboBox.getSelectedIndex()];
						setcommparams sc = new setcommparams();
						sc.setcommparams(serialport);
						// setlistener(serialport);
						mftserialPort = serialport;
						calibrationserialport = portlist[comboBox.getSelectedIndex()];
						sc.setcommparams(calibrationserialport);
						// sc.setcommparams(mftserialPort);

						System.out.println("List Refreshed");
					} else {
						System.out.println("No Ports found in the system");
					}

				} catch (Exception e1) {

					e1.printStackTrace();
					JOptionPane.showMessageDialog(frmStrykerVer, "Exception Occurred while refreshing COM Port List",
							"Error Message", JOptionPane.ERROR_MESSAGE);
				}
				btnRefreshPortList.setVisible(true);
			}
		});
		this.btnRefreshPortList.setToolTipText("Click to update the currently available ports");

		this.comboBox = new JComboBox(portlistforcombobox);
		comboBox.setBounds(9, 21, 98, 23);
		comboBox.setFont(new Font("Calibri", Font.PLAIN, 13));
		panel_2.add(comboBox);

		this.panel_3 = new JPanel();
		panel_3.setBounds(13, 48, 213, 3);
		panel_2.add(panel_3);
		panel_3.setBackground(Color.RED);

		// System.out.println("Inside Initialize function");
		comboBox.addItemListener(new ItemListener() {
			@SuppressWarnings("unused")
			public void itemStateChanged(ItemEvent e) {
				System.out.println("Inside state change listener...");
				boolean trueportflag = false;
				if (e.getStateChange() == ItemEvent.SELECTED) // an item Selected
				{
					if (comboBox.getSelectedItem() != null) {
						// ##System.out.println("Inside item listener...");
						System.out.println(e.getStateChange());
						int cbcount = 0;

						for (int k = 0; k < comboBox.getItemCount(); k++) {
							if (comboBox.getItemAt(k) != null) {
								cbcount++;
							}
						}

						if (comboBox.getSelectedItem() != null) {
							System.out.println(comboBox.getSelectedItem());
							if (cbcount == portlist.length) {
								serialport = SerialPort.getCommPort((String) comboBox.getSelectedItem());// Helped to
																											// assign a
																											// serialport
																											// object to
																											// currently
																											// selected
																											// item in
																											// the
								calibrationserialport = SerialPort.getCommPort((String) comboBox.getSelectedItem());
								System.out.println("The Selected Port is Valid");

								try {
									setcommparams sc = new setcommparams();
									sc.setcommparams(serialport);
									// setlistener(serialport);

									// mftserialPort = serialport;
									// calibrationserialport = serialport;//Just define the serialport. No need to
									// set the datalistener here.
									// Data Listener can be set on the calibration.java as and when required
									// call this function to
									// sc.setcommparams(mftserialPort);
									sc.setcommparams(calibrationserialport);
								} catch (Exception e2) {
									e2.printStackTrace();
								}

							}
						} else {
							System.out.println(comboBox.getSelectedItem());
							System.out.println("You have selected null");
						}
					}

				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {

					if (comboBox.getSelectedItem() != null) {
						// #System.out.println("Deselected");

					} else {
						// serialport = null;

					}

				}
			}

		});

		String current_port = comboBox.getSelectedItem().toString();
		System.out.println("Current Port : " + current_port);

		// Disconnect Button Press Action Handler
		btnDisconnect.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				try {
					if (!Response_Animator.Animation.isShowing()) {
						if (serialport != null) {
							pause_flag = true;
							bytedefinitions b = new bytedefinitions();
							ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
							Main_App_Worker my_worker = new Main_App_Worker(btnDisconnect, Disconnect, IPCPacket,
									dummy_byte, dummy_byte, dummy_byte, dummy_byte, dummy_byte, dummy_byte, true,
									serialport, 0, b.PWM_FREQ);
							my_worker.execute();
						} else {
							Date dateobj = new Date();
							textArea.append("COM Port Empty!!\r\n\n" + df.format(dateobj));
							ShowMessage("COM Port Cannot be Empty!!");
						}
					}

				} catch (Exception ex) {
					Date dateobj = new Date();
					textArea.append("Could not connect." + ex.toString() + "\r\n\n" + df.format(dateobj));
				}
			}
		});

		// ######################################################CONNECT BUTTON EVENT
		// HANDLER#####################################
		// Connect Button Action Handler
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					serialport = portlist[comboBox.getSelectedIndex()];

					if (serialport != null) {
						bytedefinitions b = new bytedefinitions();
						setcommparams sc = new setcommparams();
						sc.setcommparams(serialport); // call this function to
						ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
						Main_App_Worker my_worker = new Main_App_Worker(btnConnect, Connect, IPCPacket, dummy_byte,
								dummy_byte, dummy_byte, dummy_byte, dummy_byte, dummy_byte, true, serialport, 0, b.PWM_FREQ);
						my_worker.execute();
						// bytedefinitions b = new bytedefinitions();
						// expected_message_type = b.MSG_TYPE_RESP;
						// expected_cmd_id = b.CONNECT_COMMAND;
						// Current_control = Connect;

						// construct_and_send_IPCPacket(Current_control,
						// IPCPacket,dummy_byte,dummy_byte,dummy_byte,dummy_byte,dummy_byte,dummy_byte);
					} else {
						textArea.append("COM Port Empty!!\r\n\n");
						ShowMessage("COM Port Cannot be Empty!!");
					}

				} catch (java.lang.ArrayIndexOutOfBoundsException ex) {

					textArea.append("Could not connect.\r\n No COM Port Found in the system!!\r\n\n");
				} catch (Exception ex) {
					textArea.append("Could not Connect\r\n" + ex.toString() + "\r\n\n");
				}
			}
		});

		this.panel_8 = new JPanel();
		panel_8.setForeground(new Color(255, 255, 255));
		panel_8.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		panel_8.setBorder(
				new TitledBorder(null, "ADC VALUES", TitledBorder.LEFT, TitledBorder.TOP, null, new Color(51, 51, 51)));
		((TitledBorder) panel_8.getBorder()).setTitleFont(new Font("Tahoma", Font.BOLD, 10));
		panel_8.setBounds(856, 180, 497, 156);
		frmStrykerVer.getContentPane().add(panel_8);
		panel_8.setLayout(null);

		this.textField_15 = new JTextField();
		textField_15.setBounds(92, 88, 58, 22);
		panel_8.add(textField_15);
		textField_15.setEnabled(false);
		textField_15.setEditable(false);
		textField_15.setColumns(10);

		this.textField_14 = new JTextField();
		textField_14.setBounds(28, 88, 58, 22);
		panel_8.add(textField_14);
		textField_14.setEnabled(false);
		textField_14.setEditable(false);
		textField_14.setColumns(10);

		this.textField_13 = new JTextField();
		textField_13.setBounds(156, 63, 58, 22);
		panel_8.add(textField_13);
		textField_13.setEnabled(false);
		textField_13.setEditable(false);
		textField_13.setColumns(10);

		this.textField_12 = new JTextField();
		textField_12.setBounds(92, 63, 58, 22);
		panel_8.add(textField_12);
		textField_12.setEnabled(false);
		textField_12.setEditable(false);
		textField_12.setColumns(10);

		this.lblAdc_Red = new JLabel("R");
		lblAdc_Red.setEnabled(false);
		lblAdc_Red.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblAdc_Red.setBounds(17, 43, 19, 14);
		panel_8.add(lblAdc_Red);

		this.lblAdc_Green = new JLabel("G");
		lblAdc_Green.setEnabled(false);
		lblAdc_Green.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblAdc_Green.setBounds(17, 68, 19, 14);
		panel_8.add(lblAdc_Green);

		this.lblAdc_Blue = new JLabel("B");
		lblAdc_Blue.setEnabled(false);
		lblAdc_Blue.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblAdc_Blue.setBounds(17, 93, 19, 14);
		panel_8.add(lblAdc_Blue);

		this.lblAdc_Laser = new JLabel("L");
		lblAdc_Laser.setEnabled(false);
		lblAdc_Laser.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblAdc_Laser.setBounds(17, 118, 19, 14);
		panel_8.add(lblAdc_Laser);

		this.textField_11 = new JTextField();
		textField_11.setBounds(28, 63, 58, 22);
		panel_8.add(textField_11);
		textField_11.setEditable(false);
		textField_11.setEnabled(false);
		textField_11.setColumns(10);

		this.textField_10 = new JTextField();
		textField_10.setBounds(156, 38, 58, 22);
		panel_8.add(textField_10);
		textField_10.setEditable(false);
		textField_10.setEnabled(false);
		textField_10.setColumns(10);

		this.textField_9 = new JTextField();
		textField_9.setBounds(92, 38, 58, 22);
		panel_8.add(textField_9);
		textField_9.setEditable(false);
		textField_9.setEnabled(false);
		textField_9.setColumns(10);

		this.textField_8 = new JTextField();
		textField_8.setBounds(28, 38, 58, 22);
		panel_8.add(textField_8);
		textField_8.setEditable(false);
		textField_8.setEnabled(false);
		textField_8.setColumns(10);

		this.textField_17 = new JTextField();
		textField_17.setBounds(28, 114, 58, 22);
		panel_8.add(textField_17);
		textField_17.setEnabled(false);
		textField_17.setEditable(false);
		textField_17.setColumns(10);

		this.textField_16 = new JTextField();
		textField_16.setBounds(156, 88, 58, 22);
		panel_8.add(textField_16);
		textField_16.setEnabled(false);
		textField_16.setEditable(false);
		textField_16.setColumns(10);

		this.textField_23 = new JTextField();
		textField_23.setBounds(256, 63, 58, 22);
		panel_8.add(textField_23);
		textField_23.setEnabled(false);
		textField_23.setEditable(false);
		textField_23.setColumns(10);

		this.textField_22 = new JTextField();
		textField_22.setBounds(320, 38, 58, 22);
		panel_8.add(textField_22);
		textField_22.setEnabled(false);
		textField_22.setEditable(false);
		textField_22.setColumns(10);

		this.textField_21 = new JTextField();
		textField_21.setBounds(256, 38, 58, 22);
		panel_8.add(textField_21);
		textField_21.setEnabled(false);
		textField_21.setEditable(false);
		textField_21.setColumns(10);

		this.textField_20 = new JTextField();
		textField_20.setBounds(156, 114, 58, 22);
		panel_8.add(textField_20);
		textField_20.setEnabled(false);
		textField_20.setEditable(false);
		textField_20.setColumns(10);

		this.textField_27 = new JTextField();
		textField_27.setBounds(425, 88, 58, 22);
		panel_8.add(textField_27);
		textField_27.setEnabled(false);
		textField_27.setEditable(false);
		textField_27.setColumns(10);

		this.textField_26 = new JTextField();
		textField_26.setBounds(425, 38, 58, 22);
		panel_8.add(textField_26);
		textField_26.setEnabled(false);
		textField_26.setEditable(false);
		textField_26.setColumns(10);

		this.textField_25 = new JTextField();
		textField_25.setBounds(425, 63, 58, 22);
		panel_8.add(textField_25);
		textField_25.setEnabled(false);
		textField_25.setEditable(false);
		textField_25.setColumns(10);

		this.textField_24 = new JTextField();
		textField_24.setBounds(320, 63, 58, 22);
		panel_8.add(textField_24);
		textField_24.setEnabled(false);
		textField_24.setEditable(false);
		textField_24.setColumns(10);

		this.lblTemperature = new JLabel("TEMP");
		lblTemperature.setEnabled(false);
		lblTemperature.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblTemperature.setHorizontalAlignment(SwingConstants.CENTER);
		lblTemperature.setBounds(160, 20, 43, 14);
		panel_8.add(lblTemperature);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(93, 141, 2, 2);
		panel_8.add(scrollPane);

		this.lblV = new JLabel("VOLTAGE");
		lblV.setEnabled(false);
		lblV.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblV.setBounds(38, 20, 43, 16);
		panel_8.add(lblV);

		this.lblI = new JLabel("CURRENT");
		lblI.setEnabled(false);
		lblI.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblI.setBounds(100, 20, 46, 16);
		panel_8.add(lblI);

		this.lblIris_2 = new JLabel("IRIS1");
		lblIris_2.setEnabled(false);
		lblIris_2.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblIris_2.setBounds(226, 43, 34, 16);
		panel_8.add(lblIris_2);

		this.lblIris_3 = new JLabel("IRIS2");
		lblIris_3.setEnabled(false);
		lblIris_3.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblIris_3.setBounds(226, 68, 34, 16);
		panel_8.add(lblIris_3);

		this.lblIris1pd = new JLabel("IRIS1PD");
		lblIris1pd.setEnabled(false);
		lblIris1pd.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblIris1pd.setBounds(385, 43, 43, 16);
		panel_8.add(lblIris1pd);

		this.lblIris2pd = new JLabel("IRIS2PD");
		lblIris2pd.setEnabled(false);
		lblIris2pd.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblIris2pd.setBounds(385, 68, 43, 16);
		panel_8.add(lblIris2pd);

		this.lblEnvpd = new JLabel("ENVPD");
		lblEnvpd.setEnabled(false);
		lblEnvpd.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblEnvpd.setBounds(385, 93, 43, 16);
		panel_8.add(lblEnvpd);

		this.textField_18 = new JTextField();
		textField_18.setEnabled(false);
		textField_18.setEditable(false);
		textField_18.setColumns(10);
		textField_18.setBounds(92, 114, 58, 22);
		panel_8.add(textField_18);
		
		this.IrisVoltage = new JLabel("VOLTAGE");
		IrisVoltage.setFont(new Font("Calibri", Font.PLAIN, 11));
		IrisVoltage.setEnabled(false);
		IrisVoltage.setBounds(262, 17, 43, 16);
		panel_8.add(IrisVoltage);
		
		this.IrisCurrent = new JLabel("CURRENT");
		IrisCurrent.setFont(new Font("Calibri", Font.PLAIN, 11));
		IrisCurrent.setEnabled(false);
		IrisCurrent.setBounds(325, 17, 46, 16);
		panel_8.add(IrisCurrent);
		
		this.IrispdVoltage = new JLabel("VOLTAGE");
		IrispdVoltage.setFont(new Font("Calibri", Font.PLAIN, 11));
		IrispdVoltage.setEnabled(false);
		IrispdVoltage.setBounds(430, 17, 43, 16);
		panel_8.add(IrispdVoltage);

		this.panel_9 = new JPanel();
		panel_9.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		panel_9.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "COLOR SENSOR(CS)",
				TitledBorder.CENTER, TitledBorder.TOP, null, new Color(51, 51, 51)));
		((TitledBorder) panel_9.getBorder()).setTitleFont(new Font("Tahoma", Font.BOLD, 10));
		panel_9.setBounds(856, 11, 230, 168);
		frmStrykerVer.getContentPane().add(panel_9);
		panel_9.setLayout(null);

		this.lblCs_3 = new JLabel("W");
		lblCs_3.setEnabled(false);
		lblCs_3.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblCs_3.setBounds(30, 127, 18, 14);
		panel_9.add(lblCs_3);

		this.lblCs_2 = new JLabel("B");
		lblCs_2.setEnabled(false);
		lblCs_2.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblCs_2.setBounds(30, 97, 18, 14);
		panel_9.add(lblCs_2);

		this.lblCs_1 = new JLabel("G");
		lblCs_1.setEnabled(false);
		lblCs_1.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblCs_1.setBounds(30, 67, 18, 14);
		panel_9.add(lblCs_1);

		this.lblCs = new JLabel("R");
		lblCs.setEnabled(false);
		lblCs.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblCs.setBounds(30, 37, 18, 14);
		panel_9.add(lblCs);

		this.textField_28 = new JTextField();
		textField_28.setBounds(48, 31, 58, 22);
		panel_9.add(textField_28);
		textField_28.setEnabled(false);
		textField_28.setEditable(false);
		textField_28.setColumns(10);

		this.lblCs_4 = new JLabel("R");
		lblCs_4.setEnabled(false);
		lblCs_4.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblCs_4.setBounds(130, 37, 18, 14);
		panel_9.add(lblCs_4);

		this.textField_32 = new JTextField();
		textField_32.setBounds(145, 31, 58, 22);
		panel_9.add(textField_32);
		textField_32.setEnabled(false);
		textField_32.setEditable(false);
		textField_32.setColumns(10);

		this.textField_33 = new JTextField();
		textField_33.setBounds(145, 61, 58, 22);
		panel_9.add(textField_33);
		textField_33.setEnabled(false);
		textField_33.setEditable(false);
		textField_33.setColumns(10);

		this.lblCs_5 = new JLabel("G");
		lblCs_5.setEnabled(false);
		lblCs_5.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblCs_5.setBounds(130, 67, 18, 14);
		panel_9.add(lblCs_5);

		this.textField_29 = new JTextField();
		textField_29.setBounds(48, 61, 58, 22);
		panel_9.add(textField_29);
		textField_29.setEnabled(false);
		textField_29.setEditable(false);
		textField_29.setColumns(10);

		this.textField_30 = new JTextField();
		textField_30.setBounds(48, 91, 58, 22);
		panel_9.add(textField_30);
		textField_30.setEnabled(false);
		textField_30.setEditable(false);
		textField_30.setColumns(10);

		this.lblCs_6 = new JLabel("B");
		lblCs_6.setEnabled(false);
		lblCs_6.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblCs_6.setBounds(130, 97, 18, 14);
		panel_9.add(lblCs_6);

		this.textField_34 = new JTextField();
		textField_34.setBounds(145, 91, 58, 22);
		panel_9.add(textField_34);
		textField_34.setEnabled(false);
		textField_34.setEditable(false);
		textField_34.setColumns(10);

		this.lblCs_7 = new JLabel("W");
		lblCs_7.setEnabled(false);
		lblCs_7.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblCs_7.setBounds(130, 127, 18, 14);
		panel_9.add(lblCs_7);

		this.lblLl = new JLabel("LL");
		lblLl.setEnabled(false);
		lblLl.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblLl.setBounds(67, 15, 18, 16);
		panel_9.add(lblLl);

		this.lblLWl = new JLabel("WL");
		lblLWl.setEnabled(false);
		lblLWl.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblLWl.setBounds(160, 15, 18, 16);
		panel_9.add(lblLWl);

		this.textField_31 = new JTextField();
		textField_31.setBounds(48, 121, 58, 22);
		panel_9.add(textField_31);
		textField_31.setEnabled(false);
		textField_31.setEditable(false);
		textField_31.setColumns(10);

		textField_35 = new JTextField();
		textField_35.setBounds(145, 121, 58, 22);
		panel_9.add(textField_35);
		textField_35.setEnabled(false);
		textField_35.setEditable(false);
		textField_35.setColumns(10);

		this.panel_14 = new JPanel();
		panel_14.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		panel_14.setBorder(new TitledBorder(
				new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.CENTER, TitledBorder.TOP,
						null, new Color(0, 0, 0)),
				"MISC CONTROLS", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(51, 51, 51)));
		((TitledBorder) panel_14.getBorder()).setTitleFont(new Font("Tahoma", Font.BOLD, 10));
		panel_14.setBounds(3, 114, 238, 101);
		frmStrykerVer.getContentPane().add(panel_14);

		this.chckbxLoadDefaults = new JCheckBox("LOAD DEFAULTS");
		chckbxLoadDefaults.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxLoadDefaults.setBounds(85, 19, 113, 23);
		this.chckbxLoadDefaults.setMultiClickThreshhold(1000);
		chckbxLoadDefaults.addItemListener(new ItemListener() {
			// Task to be run by the scheduler
			public void itemStateChanged(ItemEvent e) {
				bytedefinitions b = new bytedefinitions();
				expected_message_type = b.EXPECT_NO_RESPONSE;
				expected_cmd_id = b.EXPECT_NO_RESPONSE;

				if (e.getStateChange() == ItemEvent.SELECTED) {
					try {
						
						if (rdbtnuC1.isSelected()) {
							if (!Response_Animator.Animation.isShowing()) {
								pause_flag = true;
								Current_control = LOAD_DEF1;
								ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
								Main_App_Worker my_worker = new Main_App_Worker(chckbxLoadDefaults, Current_control,
										IPCPacket, b.WHICH_CONTROLLER_1, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
										dummy_byte, false, serialport, 0, b.PWM_FREQ);
								my_worker.execute();

								// construct_and_send_IPCPacket(Current_control,
								// IPCPacket,b.WHICH_CONTROLLER_1,dummy_byte,dummy_byte,dummy_byte,dummy_byte,dummy_byte);

							}

						} else if (rdbtnuC2.isSelected()) {
							if (!Response_Animator.Animation.isShowing()) {
								Current_control = LOAD_DEF2;
								ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
								Main_App_Worker my_worker = new Main_App_Worker(chckbxLoadDefaults, Current_control,
										IPCPacket, b.WHICH_CONTROLLER_2, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
										dummy_byte, false, serialport, 0, b.PWM_FREQ);
								my_worker.execute();
								// construct_and_send_IPCPacket(Current_control,
								// IPCPacket,b.WHICH_CONTROLLER_2,dummy_byte,dummy_byte,dummy_byte,dummy_byte,dummy_byte);

							}

						} else {
							ShowMessage("Option Should be either RESET or loaddefault");
							reset_ld_chckbx.clearSelection();
						}

					} catch (Exception ex) {
						textArea.append("Could not send " + Current_control + " Command" + ex.toString() + "\r\n\n");
					}

				}
			}
		});
		panel_14.setLayout(null);
		chckbxLoadDefaults.setEnabled(false);
		reset_ld_chckbx.add(chckbxLoadDefaults);
		panel_14.add(chckbxLoadDefaults);

		this.chckbxReset = new JCheckBox("RESET");
		chckbxReset.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxReset.setBounds(19, 19, 65, 23);
		chckbxReset.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				bytedefinitions b = new bytedefinitions();
				expected_message_type = b.EXPECT_NO_RESPONSE;
				expected_cmd_id = b.EXPECT_NO_RESPONSE;

				if (e.getStateChange() == ItemEvent.SELECTED) {
					try {
						//bytedefinitions b = new bytedefinitions();
						if (rdbtnuC1.isSelected()) {
							if (!Response_Animator.Animation.isShowing()) {
								pause_flag = true;
								Current_control = RESET_uC1;

								ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
								Main_App_Worker my_worker = new Main_App_Worker(chckbxReset, Current_control, IPCPacket,
										b.WHICH_CONTROLLER_1, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
										dummy_byte, false, serialport, 0, b.PWM_FREQ);
								my_worker.execute();
								// construct_and_send_IPCPacket(Current_control,
								// IPCPacket,b.WHICH_CONTROLLER_1,dummy_byte,dummy_byte,dummy_byte,dummy_byte,dummy_byte);

							}

						} else if (rdbtnuC2.isSelected()) {
							if (!Response_Animator.Animation.isShowing()) {
								pause_flag = true;
								Current_control = RESET_uC2;
								ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
								Main_App_Worker my_worker = new Main_App_Worker(chckbxReset, Current_control, IPCPacket,
										b.WHICH_CONTROLLER_2, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
										dummy_byte, false, serialport, 0, b.PWM_FREQ);
								my_worker.execute();
								// construct_and_send_IPCPacket(Current_control,
								// IPCPacket,b.WHICH_CONTROLLER_2,dummy_byte,dummy_byte,dummy_byte,dummy_byte,dummy_byte);

							}

						} else {
							ShowMessage("Option Should be either uC1 or uC2");
							reset_ld_chckbx.clearSelection();
						}

					} catch (Exception ex) {
						textArea.append("Could not send " + Current_control + " Command" + ex.toString() + "\r\n\n");
					}
				}
			}
		});
		reset_ld_chckbx.add(chckbxReset);
		chckbxReset.setEnabled(false);

		panel_14.add(chckbxReset);

		this.rdbtnuC1 = new JRadioButton("uC1");
		rdbtnuC1.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		rdbtnuC1.setEnabled(false);
		rdbtnuC1.setBounds(19, 43, 54, 18);
		reset_ld_rdbtn.add(rdbtnuC1);
		panel_14.add(rdbtnuC1);

		this.rdbtnuC2 = new JRadioButton("uC2");
		rdbtnuC2.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		rdbtnuC2.setEnabled(false);
		rdbtnuC2.setBounds(19, 62, 125, 18);
		reset_ld_rdbtn.add(rdbtnuC2);
		panel_14.add(rdbtnuC2);

		this.panel_10 = new JPanel();
		panel_10.setBounds(3, 361, 238, 168);
		frmStrykerVer.getContentPane().add(panel_10);
		panel_10.setForeground(Color.WHITE);
		panel_10.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		panel_10.setBorder(
				new TitledBorder(
						new TitledBorder(
								new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null,
										new Color(59, 59, 59)),
								"", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)),
						"GPIO SENSING", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(51, 51, 51)));
		((TitledBorder) panel_10.getBorder()).setTitleFont(new Font("Tahoma", Font.BOLD, 10));
		panel_10.setLayout(null);

		this.label_9 = new JLabel("");
		label_9.setBounds(42, 140, 18, 14);
		label_9.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/red2.png")));
		label_9.setEnabled(false);
		panel_10.add(label_9);

		this.label_8 = new JLabel("");
		label_8.setBackground(Color.BLACK);
		label_8.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/red2.png")));
		label_8.setEnabled(false);
		label_8.setBounds(42, 61, 18, 14);
		panel_10.add(label_8);

		this.label_7 = new JLabel("");
		label_7.setEnabled(false);
		label_7.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/red2.png")));
		label_7.setBounds(42, 41, 18, 14);
		panel_10.add(label_7);

		this.label_6 = new JLabel("");
		label_6.setEnabled(false);
		label_6.setBounds(42, 22, 18, 14);
		label_6.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/red2.png")));
		panel_10.add(label_6);

		lblBeamSensor = new JLabel("BEAM_SENSOR");
		lblBeamSensor.setBounds(78, 63, 83, 14);
		lblBeamSensor.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		panel_10.add(lblBeamSensor);
		lblBeamSensor.setEnabled(false);

		lblIris_Pgood = new JLabel("IRIS_PGOOD");
		lblIris_Pgood.setBounds(78, 140, 83, 14);
		lblIris_Pgood.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		panel_10.add(lblIris_Pgood);
		lblIris_Pgood.setEnabled(false);

		lblFibre1_Detect = new JLabel("FIBRE1_DETECT");
		lblFibre1_Detect.setBounds(78, 22, 83, 14);
		panel_10.add(lblFibre1_Detect);
		lblFibre1_Detect.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		lblFibre1_Detect.setEnabled(false);

		lblFibre2_Detect = new JLabel("FIBRE2_DETECT");
		lblFibre2_Detect.setBounds(78, 43, 83, 14);
		panel_10.add(lblFibre2_Detect);
		lblFibre2_Detect.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		lblFibre2_Detect.setEnabled(false);
		
		lblNewLabel = new JLabel("");
		lblNewLabel.setEnabled(false);
		lblNewLabel.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/red2.png")));
		lblNewLabel.setBounds(42, 81, 18, 15);
		panel_10.add(lblNewLabel);
		
		label_3 = new JLabel("");
		label_3.setEnabled(false);
		label_3.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/red2.png")));
		label_3.setBounds(42, 101, 18, 15);
		panel_10.add(label_3);
		
		label_4 = new JLabel("");
		label_4.setEnabled(false);
		label_4.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/red2.png")));
		label_4.setBounds(42, 121, 18, 15);
		panel_10.add(label_4);
		
		lblESST = new JLabel("ESST");
		lblESST.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		lblESST.setEnabled(false);
		lblESST.setBounds(78, 81, 60, 15);
		panel_10.add(lblESST);
		
		lblAUX_CAM_IN1 = new JLabel("AUX_CAM_IN1");
		lblAUX_CAM_IN1.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		lblAUX_CAM_IN1.setEnabled(false);
		lblAUX_CAM_IN1.setBounds(78, 101, 85, 15);
		panel_10.add(lblAUX_CAM_IN1);
		
		lblAUX_CAM_In2 = new JLabel("AUX_CAM_IN2");
		lblAUX_CAM_In2.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		lblAUX_CAM_In2.setEnabled(false);
		lblAUX_CAM_In2.setBounds(78, 121, 85, 15);
		panel_10.add(lblAUX_CAM_In2);

		this.panel_13 = new JPanel();

		panel_13.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		panel_13.setBorder(new TitledBorder(
				new TitledBorder(null, "LOGS", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)), "",
				TitledBorder.CENTER, TitledBorder.TOP, null, new Color(51, 51, 51)));

		((TitledBorder) panel_13.getBorder()).setTitleFont(new Font("Tahoma", Font.BOLD, 10));
		panel_13.setBounds(856, 335, 497, 263);
		frmStrykerVer.getContentPane().add(panel_13);
		panel_13.setLayout(null);

		this.rdbtnClear = new JRadioButton("Clear Logs");
		rdbtnClear.setBounds(399, 6, 83, 23);
		panel_13.add(rdbtnClear);
		this.textArea = new JTextArea();
		textArea.setEditable(false);
		scroll = new JScrollPane(textArea, // Create a ScrollPane and Add it to the TextArea
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scroll.setBounds(15, 25, 467, 217);
		textArea.setBounds(23, 28, 349, 537);
		panel_13.add(scroll); // Add the ScrollPane to the panel instead of adding the textArea
		chckbxIrisPowerShutdown = new JCheckBox("IRIS POWER SHUTDOWN");
		chckbxIrisPowerShutdown.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxIrisPowerShutdown.setBounds(17, 134, 132, 18);

		this.panel_22 = new JPanel();
		panel_22.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		panel_22.setBorder(new TitledBorder(
				new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.CENTER, TitledBorder.TOP,
						null, new Color(0, 0, 0)),
				"FAN CONTROL", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(51, 51, 51)));
		((TitledBorder) panel_22.getBorder()).setTitleFont(new Font("Tahoma", Font.BOLD, 10));
		panel_22.setBounds(3, 220, 238, 135);
		frmStrykerVer.getContentPane().add(panel_22);
		panel_22.setLayout(null);

		this.slider_12 = new JSlider();
		slider_12.addChangeListener(new ChangeListener() {
			int slider_value = 0;
			bytedefinitions b = new bytedefinitions();

			public void stateChanged(ChangeEvent e) {
				bytedefinitions b = new bytedefinitions();
				if (slider_12.isEnabled()) {

					JSlider source = (JSlider) e.getSource();
					if (connect_flag == false) {
						if (!source.getValueIsAdjusting()) {

							if (!Response_Animator.Animation.isShowing()) {
								// Determines whether the user gesture to move the slider's knob is complete
								pause_flag = true;
								byte value = (byte) slider_value;
								ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
								Main_App_Worker my_worker = new Main_App_Worker(slider_12, SLIDER, IPCPacket,
										b.INTENSITY_PWR_FAN, value, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
										false, serialport, 0, b.PWM_FREQ);
								my_worker.execute();
								// expected_message_type = b.EXPECT_NO_RESPONSE;
								// expected_cmd_id = b.INTENSITY_COMMAND;
								// Current_control = POWER_SUPPLY_SLIDER;
								// ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
								// construct_and_send_IPCPacket(Current_control,
								// IPCPacket,b.INTENSITY_PWR_FAN,value,dummy_byte,dummy_byte,dummy_byte,dummy_byte);

							}

						} else {
							slider_value = slider_12.getValue();
							String slider_2 = Integer.toString(slider_value);
							label_5.setText((slider_2) + " %");
						}
					}

				}
			}
		});
		slider_12.setEnabled(false);
		slider_12.setValue(0);
		slider_12.setBounds(50, 26, 100, 21);
		panel_22.add(slider_12);

		this.lblPowerSupply = new JLabel("LE");
		lblPowerSupply.setEnabled(false);
		lblPowerSupply.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		lblPowerSupply.setBounds(16, 30, 29, 16);
		panel_22.add(lblPowerSupply);

		this.lblHeatSink = new JLabel("HS");
		lblHeatSink.setEnabled(false);
		lblHeatSink.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		lblHeatSink.setBounds(16, 54, 29, 16);
		panel_22.add(lblHeatSink);

		this.slider_13 = new JSlider();
		slider_13.addChangeListener(new ChangeListener() {
			int slider_value = 0;
			bytedefinitions b = new bytedefinitions();

			public void stateChanged(ChangeEvent e) {
				if (slider_13.isEnabled()) {
					bytedefinitions b = new bytedefinitions();
					JSlider source = (JSlider) e.getSource();
					if (connect_flag == false) {
						if (!source.getValueIsAdjusting()) {
							if (!Response_Animator.Animation.isShowing()) {
								pause_flag = true;
								System.out.println("INSIDE HSFAN CONTROL");
								byte value = (byte) slider_value;
								ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
								Main_App_Worker my_worker = new Main_App_Worker(slider_13, SLIDER, IPCPacket,
										b.INTENSITY_HSINK_FAN, value, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
										false, serialport, 0, b.PWM_FREQ);
								my_worker.execute();

								// Determines whether the user gesture to move the slider's knob is complete
								/*
								 * print(Integer.toString(slider_value)); byte value = (byte) slider_value;
								 * expected_message_type = b.EXPECT_NO_RESPONSE; expected_cmd_id =
								 * b.EXPECT_NO_RESPONSE; Current_control = HEATSINK_SLIDER; ArrayList<Byte>
								 * IPCPacket = new ArrayList<Byte>();
								 * construct_and_send_IPCPacket(Current_control,
								 * IPCPacket,b.INTENSITY_HSINK_FAN,value,dummy_byte,dummy_byte,dummy_byte,
								 * dummy_byte);
								 */

							}

						} else {
							slider_value = slider_13.getValue();
							String slider_2 = Integer.toString(slider_value);
							label_17.setText((slider_2) + " %");
						}
					}

				}
			}
		});
		slider_13.setEnabled(false);
		slider_13.setValue(0);
		slider_13.setBounds(50, 50, 100, 21);
		panel_22.add(slider_13);

		this.lblIris = new JLabel("IRIS");
		lblIris.setEnabled(false);
		lblIris.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		lblIris.setBounds(16, 78, 29, 16);
		panel_22.add(lblIris);

		this.slider_14 = new JSlider();
		slider_14.addChangeListener(new ChangeListener() {
			int slider_value = 0;
			bytedefinitions b = new bytedefinitions();

			public void stateChanged(ChangeEvent e) {
				if (slider_14.isEnabled()) {

					JSlider source = (JSlider) e.getSource();
					bytedefinitions b = new bytedefinitions();
					if (connect_flag == false) {
						if (!source.getValueIsAdjusting()) {
							if (!Response_Animator.Animation.isShowing()) {
								pause_flag = true;
								System.out.println("INSIDE HSFAN CONTROL");
								byte value = (byte) slider_value;
								ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
								Main_App_Worker my_worker = new Main_App_Worker(slider_14, SLIDER, IPCPacket,
										b.INTENSITY_IRIS_FAN, value, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
										false, serialport, 0, b.PWM_FREQ);
								my_worker.execute();

								// Determines whether the user gesture to move the slider's knob is complete
								/*
								 * print(Integer.toString(slider_value)); byte value = (byte) slider_value;
								 * expected_message_type = b.EXPECT_NO_RESPONSE; expected_cmd_id =
								 * b.EXPECT_NO_RESPONSE; Current_control = IRIS_FAN_SLIDER; ArrayList<Byte>
								 * IPCPacket = new ArrayList<Byte>();
								 * construct_and_send_IPCPacket(Current_control,
								 * IPCPacket,b.IRIS_FAN,value,dummy_byte,dummy_byte,dummy_byte,dummy_byte);
								 */

							}

						} else {
							slider_value = slider_14.getValue();
							String slider_2 = Integer.toString(slider_value);
							label_18.setText((slider_2) + " %");
						}

					}

				}
			}
		});
		slider_14.setEnabled(false);
		slider_14.setValue(0);
		slider_14.setBounds(50, 74, 100, 21);
		panel_22.add(slider_14);

		this.label_5 = new JLabel("0 %");
		label_5.setEnabled(false);
		label_5.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		label_5.setBounds(200, 29, 29, 16);
		panel_22.add(label_5);

		this.label_17 = new JLabel("0 %");
		label_17.setEnabled(false);
		label_17.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		label_17.setBounds(200, 53, 29, 16);
		panel_22.add(label_17);

		this.label_18 = new JLabel("0 %");
		label_18.setEnabled(false);
		label_18.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		label_18.setBounds(200, 77, 29, 16);
		panel_22.add(label_18);

		this.lblFanFailure = new JLabel("FAN FAILURE");
		lblFanFailure.setEnabled(false);
		lblFanFailure.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		lblFanFailure.setBounds(16, 102, 67, 16);
		panel_22.add(lblFanFailure);

		this.label_19 = new JLabel("");
		label_19.setBounds(94, 102, 20, 16);
		label_19.setEnabled(false);
		label_19.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/red2.png")));
		panel_22.add(label_19);
		
		txtle_tach_count = new JTextField();
		txtle_tach_count.setEditable(false);
		txtle_tach_count.setEnabled(false);
		txtle_tach_count.setBounds(155, 23, 45, 27);
		panel_22.add(txtle_tach_count);
		txtle_tach_count.setColumns(10);
		
		txths_tach_count = new JTextField();
		txths_tach_count.setEditable(false);
		txths_tach_count.setEnabled(false);
		txths_tach_count.setBounds(155, 48, 45, 27);
		panel_22.add(txths_tach_count);
		txths_tach_count.setColumns(10);
		
		txtiris_tach_count = new JTextField();
		txtiris_tach_count.setEditable(false);
		txtiris_tach_count.setEnabled(false);
		txtiris_tach_count.setBounds(155, 72, 45, 27);
		panel_22.add(txtiris_tach_count);
		txtiris_tach_count.setColumns(10);

		this.panel_23 = new JPanel();
		panel_23.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		panel_23.setBorder(new TitledBorder(
				new TitledBorder(null, "", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)),
				"WHITE LIGHT", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(51, 51, 51)));
		((TitledBorder) panel_23.getBorder()).setTitleFont(new Font("Tahoma", Font.BOLD, 10));
		panel_23.setBounds(244, 11, 609, 204);
		frmStrykerVer.getContentPane().add(panel_23);
		panel_23.setLayout(null);

		this.slider_15 = new JSlider();
		slider_15.setEnabled(false);
		slider_15.setMaximum(4095);
		slider_15.addChangeListener(new ChangeListener() {
			int slider_value = 0;
			bytedefinitions b = new bytedefinitions();

			public void stateChanged(ChangeEvent e) {
				bytedefinitions b = new bytedefinitions();
				if (slider_15.isEnabled()) {

					JSlider source = (JSlider) e.getSource();
					if (wl_onoff_flag == false && connect_flag == false) {
						if (!source.getValueIsAdjusting()) {
							//Slider is released by user after sliding. Do the relevant action
							if (!Response_Animator.Animation.isShowing()) {
								pause_flag = true;
								System.out.println("Inside Group Slider Control");
								byte value = (byte) slider_value;
								System.out.println("Slider value is : " + String.valueOf(slider_value));
								ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
								Main_App_Worker my_worker = new Main_App_Worker(slider_15, SLIDER, IPCPacket,
										b.INTENSITY_SLIDER_WHITE, value, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
										false, serialport, slider_value, b.PWM_FREQ);
								my_worker.execute();

								/*
								 * // Determines whether the user gesture to move the slider's knob is complete
								 * print(Integer.toString(slider_value)); byte value = (byte) slider_value;
								 * expected_message_type = b.MSG_TYPE_NOTIFY; expected_cmd_id =
								 * b.EXPECT_NO_RESPONSE; Current_control = GRP_SLIDER; ArrayList<Byte> IPCPacket
								 * = new ArrayList<Byte>(); construct_and_send_IPCPacket(Current_control,
								 * IPCPacket,b.INTENSITY_SLIDER_WHITE,value,dummy_byte,dummy_byte,dummy_byte,
								 * dummy_byte);
								 */

							}

						} else {
							//Update textbox and slider until it is fixed in a place
							slider_value = slider_15.getValue();
							String slider_2 = Integer.toString(slider_value);
							GrptextField.setText(slider_2);
							String local = Integer.toString((slider_value * 100)/Max_Val);
							System.out.println(local);
							label_20.setText(local +" %");
							// label_20.setText((slider_2));
						}
					}
					else
					{
						//When slider value is set on last mode update
						slider_value = slider_15.getValue();
						GrptextField.setText(Integer.toString(slider_value));
						String local = Integer.toString((slider_value * 100)/Max_Val);
						System.out.println(local);
						label_20.setText(local +" %");
					}

				}

			}
		});
		slider_15.setValue(0);
		slider_15.setBounds(252, 63, 255, 21);
		panel_23.add(slider_15);

		this.panel_26 = new JPanel();
		panel_26.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		panel_26.setBorder(
				new TitledBorder(null, "MODE", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
		((TitledBorder) panel_26.getBorder()).setTitleFont(new Font("Tahoma", Font.BOLD, 10));
		panel_26.setBounds(8, 13, 188, 55);
		panel_23.add(panel_26);
		panel_26.setLayout(null);

		this.rdbtnPwm = new JRadioButton("");
		rdbtnPwm.setEnabled(false);
		rdbtnPwm.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					WlPowerButton.setEnabled(false);
					for (Component c : wl_source) {
						c.setEnabled(true);
					}
					rdbtnEnv_LcNr.setEnabled(false);
					lblLcnr.setEnabled(false);
					if (!EnvIrisPowerButton.isSelected()) {
						if (chckbxEnv.isSelected() || chckbxIris.isSelected()) {
							rdbtnEnv_Pwm.setSelected(true);
						} else {
							rdbtnEnv_Pwm.setSelected(false);
						}
					}
					
				} else {
					wlsource.clearSelection();
					for (Component c : wl_source) {
						c.setEnabled(false);
					}
					

				}

			}
		});
		rdbtnPwm.setBounds(18, 25, 18, 18);
		panel_26.add(rdbtnPwm);
		rdbtnPwm.setFont(new Font("Calibri Light", Font.PLAIN, 11));

		this.lblPwm_4 = new JLabel("PWM");
		lblPwm_4.setEnabled(false);
		lblPwm_4.setBounds(16, 13, 33, 16);
		panel_26.add(lblPwm_4);
		lblPwm_4.setFont(new Font("Calibri Light", Font.PLAIN, 10));

		this.lblCwm = new JLabel("CWM");
		lblCwm.setEnabled(false);
		lblCwm.setBounds(61, 13, 33, 16);
		panel_26.add(lblCwm);
		lblCwm.setFont(new Font("Calibri Light", Font.PLAIN, 10));

		this.rdbtnCwm = new JRadioButton("");
		rdbtnCwm.setEnabled(false);
		rdbtnCwm.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					rdbtnEnv_LcNr.setEnabled(false);
					lblLcnr.setEnabled(false);
					WlPowerButton.setEnabled(false);
					for (Component c : wl_source) {
						if ((c != lblExt) && (c != rdbtnExt)) {
							c.setEnabled(true);
						} else {
							c.setEnabled(false);
						}
					}

				} else {
					wlsource.clearSelection();
					for (Component c : wl_source) {
						c.setEnabled(false);
					}

				}
			}
		});
		rdbtnCwm.setBounds(64, 25, 18, 18);
		panel_26.add(rdbtnCwm);
		rdbtnCwm.setFont(new Font("Calibri Light", Font.PLAIN, 11));

		this.lblStb = new JLabel("STB");
		lblStb.setEnabled(false);
		lblStb.setBounds(111, 13, 18, 16);
		panel_26.add(lblStb);
		lblStb.setFont(new Font("Calibri Light", Font.PLAIN, 10));

		this.rdbtnStb = new JRadioButton("");
		rdbtnStb.setEnabled(false);
		rdbtnStb.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					rdbtnEnv_LcNr.setEnabled(false);
					lblLcnr.setEnabled(false);
					WlPowerButton.setEnabled(false);
					for (Component c : wl_source) {
						if ((c != lblExt) && (c != rdbtnExt)) {
							c.setEnabled(true);
						} else {
							c.setEnabled(false);
						}
					}

				} else {
					wlsource.clearSelection();
					for (Component c : wl_source) {
						c.setEnabled(false);
					}

				}
			}
		});
		rdbtnStb.setBounds(110, 25, 18, 18);
		panel_26.add(rdbtnStb);
		rdbtnStb.setFont(new Font("Calibri Light", Font.PLAIN, 11));

		this.rdbtnLc = new JRadioButton("");
		rdbtnLc.setEnabled(false);
		rdbtnLc.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					for (Component c : wl_source) {
						c.setEnabled(false);
					}
					WlPowerButton.setEnabled(true);
					if ((chckbxEnv.isSelected() == true) && EnvIrisPowerButton.isSelected() == false) {
						lblLcnr.setEnabled(true);
						rdbtnEnv_LcNr.setEnabled(true);
					} else {
						lblLcnr.setEnabled(false);
						rdbtnEnv_LcNr.setEnabled(false);
					}

				} else if (e.getStateChange() == ItemEvent.DESELECTED) {
					wlsource.clearSelection();
					for (Component c : wl_source) {
						c.setEnabled(false);
					}
					lblLcnr.setEnabled(true);
					rdbtnEnv_LcNr.setEnabled(true);

				}

			}
		});
		rdbtnLc.setBounds(156, 25, 18, 18);
		panel_26.add(rdbtnLc);
		rdbtnLc.setFont(new Font("Calibri Light", Font.PLAIN, 11));

		this.lblLc = new JLabel("LC");
		lblLc.setEnabled(false);
		lblLc.setBounds(159, 13, 18, 16);
		panel_26.add(lblLc);
		lblLc.setFont(new Font("Calibri Light", Font.PLAIN, 10));

		this.panel_27 = new JPanel();
		panel_27.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		panel_27.setBorder(
				new TitledBorder(null, "SOURCE", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
		((TitledBorder) panel_27.getBorder()).setTitleFont(new Font("Tahoma", Font.BOLD, 10));
		panel_27.setBounds(8, 76, 188, 55);
		panel_23.add(panel_27);
		panel_27.setLayout(null);

		this.lblExt = new JLabel("EXT");
		lblExt.setEnabled(false);
		lblExt.setBounds(42, 13, 18, 16);
		panel_27.add(lblExt);
		lblExt.setFont(new Font("Calibri Light", Font.PLAIN, 10));

		this.rdbtnExt = new JRadioButton("");
		rdbtnExt.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					WlPowerButton.setEnabled(true);
					for (Component c : wl_pwm_type) {
						c.setEnabled(false);
					}
				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					WlPowerButton.setEnabled(false);
				}
			}
		});
		rdbtnExt.setEnabled(false);
		rdbtnExt.setBounds(40, 25, 18, 18);
		panel_27.add(rdbtnExt);
		wlsource.add(rdbtnExt);

		rdbtnExt.setFont(new Font("Calibri Light", Font.PLAIN, 11));

		this.lblUc1 = new JLabel("uC1");
		lblUc1.setEnabled(false);
		lblUc1.setBounds(87, 13, 18, 16);
		panel_27.add(lblUc1);
		lblUc1.setFont(new Font("Calibri Light", Font.PLAIN, 11));

		this.rdbtnuC_1 = new JRadioButton("");
		rdbtnuC_1.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					WlPowerButton.setEnabled(true);
					if (rdbtnPwm.isSelected()) {
						for (Component c : wl_pwm_type) {
							
							c.setEnabled(true);
						}
					}
					if(rdbtnPwm.isSelected()) {
						rdbtn120hz.setEnabled(true);
						rdbtn120hz.setSelected(true);
						rdbtn70khz.setEnabled(true);
					}
					
					if(tglbtnFixed_Wl.getText().toString().equals("PATTERN 10�s") || tglbtnFixed_Wl.getText().toString().equals("PATTERN 1ms")
							|| tglbtnFixed_Wl.getText().toString().equals("PATTERN TRANSIENT")) {
						lblFixedValue_2.setEnabled(false);
						formattedTextField_WlPwm.setEnabled(false);
					}
				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					WlPowerButton.setEnabled(false);
					for (Component c : wl_pwm_type) {
						c.setEnabled(false);
					}
					rdbtn120hz.setSelected(false);
					rdbtn70khz.setSelected(false);
					rdbtn120hz.setEnabled(false);
					rdbtn70khz.setEnabled(false);
				}

			}
		});
		rdbtnuC_1.setEnabled(false);
		rdbtnuC_1.setBounds(87, 25, 18, 18);
		panel_27.add(rdbtnuC_1);
		wlsource.add(rdbtnuC_1);

		rdbtnuC_1.setFont(new Font("Calibri Light", Font.PLAIN, 11));

		this.rdbtnuC_2 = new JRadioButton("");
		rdbtnuC_2.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					WlPowerButton.setEnabled(true);
					if (rdbtnPwm.isSelected()) {
						for (Component c : wl_pwm_type) {
							c.setEnabled(true);
						}
					}

				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					WlPowerButton.setEnabled(false);
					for (Component c : wl_pwm_type) {
						c.setEnabled(false);
					}
				}
			}

		});
		rdbtnuC_2.setEnabled(false);
		rdbtnuC_2.setBounds(133, 25, 18, 18);
		panel_27.add(rdbtnuC_2);
		wlsource.add(rdbtnuC_2);
		rdbtnuC_2.setFont(new Font("Calibri Light", Font.PLAIN, 11));

		this.lblUc2 = new JLabel("uC2");
		lblUc2.setEnabled(false);
		lblUc2.setBounds(133, 13, 28, 16);
		panel_27.add(lblUc2);
		lblUc2.setFont(new Font("Calibri Light", Font.PLAIN, 11));

		this.panel_29 = new JPanel();
		panel_29.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		panel_29.setBorder(new TitledBorder(
				new TitledBorder(null, "", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)), "",
				TitledBorder.CENTER, TitledBorder.TOP, null, new Color(51, 51, 51)));
		((TitledBorder) panel_29.getBorder()).setTitleFont(new Font("Tahoma", Font.BOLD, 10));
		panel_29.setBounds(197, 103, 404, 89);
		panel_23.add(panel_29);
		panel_29.setLayout(null);

		this.chckbxRed = new JCheckBox("RED");
		chckbxRed.setEnabled(false);
		chckbxRed.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				bytedefinitions b = new bytedefinitions();

				if (wl_onoff_flag == false && connect_flag == false) {
					if (e.getStateChange() == ItemEvent.SELECTED) {
						if (!Response_Animator.Animation.isShowing()) {
							System.out.println("@@@@@@@@@@@@@@@@@@@@@@");
							pause_flag = true;
							byte value = (byte) 0x1;
							ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
							Main_App_Worker my_worker = new Main_App_Worker(chckbxRed, ENABLE_DISABLE, IPCPacket,
									b.INTENSITY_SLIDER_RED, value, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
									false, serialport, 0, b.PWM_FREQ);
							my_worker.execute();
						}

					}
					if (e.getStateChange() == ItemEvent.DESELECTED) {
						if (!Response_Animator.Animation.isShowing()) {
							System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%");
							pause_flag = true;
							byte value = (byte) 0x0;
							ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
							Main_App_Worker my_worker = new Main_App_Worker(chckbxRed, ENABLE_DISABLE, IPCPacket,
									b.INTENSITY_SLIDER_RED, value, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
									false, serialport, 0, b.PWM_FREQ);
							my_worker.execute();
						}

					}

					/*
					 * 
					 * bytedefinitions b = new bytedefinitions(); expected_message_type =
					 * b.EXPECT_NO_RESPONSE; expected_cmd_id = b.EXPECT_NO_RESPONSE;
					 * 
					 * ArrayList<Byte> IPCPacket = new ArrayList<Byte>(); if(e.getStateChange() ==
					 * ItemEvent.SELECTED) { Current_control = RED_SLIDER_EN;
					 * construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,b.INTENSITY_SLIDER_RED,(byte)
					 * 0x1,dummy_byte,dummy_byte,dummy_byte,dummy_byte); } else
					 * if(e.getStateChange() == ItemEvent.DESELECTED) { Current_control =
					 * RED_SLIDER_DIS; construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,b.INTENSITY_SLIDER_RED,(byte)
					 * 0x0,dummy_byte,dummy_byte,dummy_byte,dummy_byte); } else {
					 * 
					 * }
					 */

				}

			}
		});
		chckbxRed.setBounds(8, 8, 47, 18);
		panel_29.add(chckbxRed);
		chckbxRed.setFont(new Font("Calibri Light", Font.PLAIN, 11));

		this.slider_16 = new JSlider();
		slider_16.setEnabled(false);
		slider_16.setMaximum(4095);
		slider_16.addChangeListener(new ChangeListener() {
			int slider_value = 0;
			bytedefinitions b = new bytedefinitions();

			public void stateChanged(ChangeEvent e) {
				{
					if (slider_16.isEnabled()) {
						JSlider source = (JSlider) e.getSource();
						if (wl_onoff_flag == false && connect_flag == false) {
							if (!source.getValueIsAdjusting()) {
								if (!Response_Animator.Animation.isShowing()) {
									pause_flag = true;
									System.out.println("Inside Red Slider Control");
									byte value = (byte) slider_value;
									ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
									Main_App_Worker my_worker = new Main_App_Worker(slider_16, SLIDER, IPCPacket,
											b.INTENSITY_SLIDER_RED, value, dummy_byte, dummy_byte, dummy_byte,
											dummy_byte, false, serialport, slider_value, b.PWM_FREQ);
									my_worker.execute();
									// Determines whether the user gesture to move the slider's knob is complete
									/*
									 * print(Integer.toString(slider_value)); byte value = (byte) slider_value;
									 * expected_message_type = b.MSG_TYPE_NOTIFY; expected_cmd_id =
									 * b.EXPECT_NO_RESPONSE; Current_control = RED_SLIDER; ArrayList<Byte> IPCPacket
									 * = new ArrayList<Byte>(); construct_and_send_IPCPacket(Current_control,
									 * IPCPacket,b.INTENSITY_SLIDER_RED,value,dummy_byte,dummy_byte,dummy_byte,
									 * dummy_byte);
									 */

								}

							} else {
								slider_value = slider_16.getValue();
								String slider_2 = Integer.toString(slider_value);
								RedtextField.setText(slider_2);
								String local = Integer.toString((slider_value * 100)/Max_Val);
								label_21.setText(local + " %");
							}
						}
						else
						{
							//When slider value is set on last mode update
							slider_value = slider_16.getValue();
							String local = Integer.toString((slider_value * 100)/Max_Val);
							RedtextField.setText(Integer.toString(slider_value));
							label_21.setText(local +" %");
						}
					}

				}
			}
		});
		slider_16.setBounds(52, 6, 255, 21);
		panel_29.add(slider_16);
		slider_16.setValue(0);

		this.slider_17 = new JSlider();
		slider_17.setEnabled(false);
		slider_17.setMaximum(4095);
		slider_17.addChangeListener(new ChangeListener() {
			int slider_value = 0;
			bytedefinitions b = new bytedefinitions();

			public void stateChanged(ChangeEvent e) {
				{
					if (slider_17.isEnabled()) {
						JSlider source = (JSlider) e.getSource();
						if (wl_onoff_flag == false && connect_flag == false) {
							if (!source.getValueIsAdjusting()) {
								if (!Response_Animator.Animation.isShowing()) {
									pause_flag = true;
									System.out.println("Inside Green Slider Control");
									byte value = (byte) slider_value;
									ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
									Main_App_Worker my_worker = new Main_App_Worker(slider_17, SLIDER, IPCPacket,
											b.INTENSITY_SLIDER_GREEN, value, dummy_byte, dummy_byte, dummy_byte,
											dummy_byte, false, serialport, slider_value, b.PWM_FREQ);
									my_worker.execute();
									/*
									 * // Determines whether the user gesture to move the slider's knob is complete
									 * print(Integer.toString(slider_value)); byte value = (byte) slider_value;
									 * expected_message_type = b.MSG_TYPE_NOTIFY; expected_cmd_id =
									 * b.EXPECT_NO_RESPONSE; Current_control = GREEN_SLIDER; ArrayList<Byte>
									 * IPCPacket = new ArrayList<Byte>();
									 * construct_and_send_IPCPacket(Current_control,
									 * IPCPacket,b.INTENSITY_SLIDER_GREEN,value,dummy_byte,dummy_byte,dummy_byte,
									 * dummy_byte);
									 */

								}

							} else {
								slider_value = slider_17.getValue();
								String slider_2 = Integer.toString(slider_value);
								// label_22.setText((slider_2)+" %");
								GreentextField.setText(slider_2);
								String local = Integer.toString((slider_value * 100)/Max_Val);
								
								label_22.setText(local + " %");
							}
						}
						else
						{
							slider_value = slider_17.getValue();
							GreentextField.setText(Integer.toString(slider_value));
							String local = Integer.toString((slider_value*100)/Max_Val);
							label_22.setText(local + " %");
						}
					}

				}
			}
		});
		slider_17.setBounds(52, 32, 255, 21);
		panel_29.add(slider_17);
		slider_17.setValue(0);

		this.chckbxGreen = new JCheckBox("GRN");
		chckbxGreen.setEnabled(false);
		chckbxGreen.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				if (wl_onoff_flag == false && connect_flag == false) {
					bytedefinitions b = new bytedefinitions();

					if (e.getStateChange() == ItemEvent.SELECTED) {
						if (!Response_Animator.Animation.isShowing()) {
							pause_flag = true;
							byte value = (byte) 0x1;
							ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
							Main_App_Worker my_worker = new Main_App_Worker(chckbxGreen, ENABLE_DISABLE, IPCPacket,
									b.INTENSITY_SLIDER_GREEN, value, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
									false, serialport, 0, b.PWM_FREQ);
							my_worker.execute();
						}

					}
					if (e.getStateChange() == ItemEvent.DESELECTED) {
						if (!Response_Animator.Animation.isShowing()) {
							pause_flag = true;
							byte value = (byte) 0x0;
							ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
							Main_App_Worker my_worker = new Main_App_Worker(chckbxGreen, ENABLE_DISABLE, IPCPacket,
									b.INTENSITY_SLIDER_GREEN, value, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
									false, serialport, 0, b.PWM_FREQ);
							my_worker.execute();
						}

					}

					/*
					 * expected_message_type = b.EXPECT_NO_RESPONSE; expected_cmd_id =
					 * b.EXPECT_NO_RESPONSE;
					 * 
					 * 
					 * ArrayList<Byte> IPCPacket = new ArrayList<Byte>(); if(e.getStateChange() ==
					 * ItemEvent.SELECTED) { Current_control = GREEN_SLIDER_EN;
					 * construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,b.INTENSITY_SLIDER_GREEN,(byte)
					 * 0x1,dummy_byte,dummy_byte,dummy_byte,dummy_byte); } if(e.getStateChange() ==
					 * ItemEvent.DESELECTED) { Current_control = GREEN_SLIDER_DIS;
					 * construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,b.INTENSITY_SLIDER_GREEN,(byte)
					 * 0x0,dummy_byte,dummy_byte,dummy_byte,dummy_byte); }
					 */
				}

			}
		});
		chckbxGreen.setBounds(8, 34, 55, 18);
		panel_29.add(chckbxGreen);
		chckbxGreen.setFont(new Font("Calibri Light", Font.PLAIN, 11));

		this.chckbxBlue = new JCheckBox("BLU");
		chckbxBlue.setEnabled(false);
		chckbxBlue.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				if (wl_onoff_flag == false && connect_flag == false) {
					bytedefinitions b = new bytedefinitions();

					if (e.getStateChange() == ItemEvent.SELECTED) {
						if (!Response_Animator.Animation.isShowing()) {
							pause_flag = true;
							byte value = (byte) 0x1;
							ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
							Main_App_Worker my_worker = new Main_App_Worker(chckbxBlue, ENABLE_DISABLE, IPCPacket,
									b.INTENSITY_SLIDER_BLUE, value, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
									false, serialport, 0, b.PWM_FREQ);
							my_worker.execute();
						}

					}
					if (e.getStateChange() == ItemEvent.DESELECTED) {
						if (!Response_Animator.Animation.isShowing()) {
							pause_flag = true;
							byte value = (byte) 0x0;
							ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
							Main_App_Worker my_worker = new Main_App_Worker(chckbxBlue, ENABLE_DISABLE, IPCPacket,
									b.INTENSITY_SLIDER_BLUE, value, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
									false, serialport, 0, b.PWM_FREQ);
							my_worker.execute();
						}

					}
					/*
					 * expected_message_type = b.EXPECT_NO_RESPONSE; expected_cmd_id =
					 * b.EXPECT_NO_RESPONSE;
					 * 
					 * ArrayList<Byte> IPCPacket = new ArrayList<Byte>(); if(e.getStateChange() ==
					 * ItemEvent.SELECTED) { Current_control = BLUE_SLIDER_EN;
					 * construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,b.INTENSITY_SLIDER_BLUE,(byte)
					 * 0x1,dummy_byte,dummy_byte,dummy_byte,dummy_byte); } if(e.getStateChange() ==
					 * ItemEvent.DESELECTED) { Current_control = BLUE_SLIDER_DIS;
					 * construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,b.INTENSITY_SLIDER_BLUE,(byte)
					 * 0x0,dummy_byte,dummy_byte,dummy_byte,dummy_byte); }
					 */
				}

			}
		});
		chckbxBlue.setBounds(8, 60, 47, 18);
		panel_29.add(chckbxBlue);
		chckbxBlue.setFont(new Font("Calibri Light", Font.PLAIN, 11));

		this.slider_18 = new JSlider();
		slider_18.setEnabled(false);
		slider_18.setMaximum(4095);
		slider_18.addChangeListener(new ChangeListener() {
			int slider_value = 0;
			bytedefinitions b = new bytedefinitions();

			public void stateChanged(ChangeEvent e) {
				{
					if (slider_18.isEnabled()) {
						JSlider source = (JSlider) e.getSource();
						if (wl_onoff_flag == false && connect_flag == false) {
							if (!source.getValueIsAdjusting()) {
								if (!Response_Animator.Animation.isShowing()) {
									pause_flag = true;
									System.out.println("Inside Blue Slider Control");
									byte value = (byte) slider_value;
									ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
									Main_App_Worker my_worker = new Main_App_Worker(slider_18, SLIDER, IPCPacket,
											b.INTENSITY_SLIDER_BLUE, value, dummy_byte, dummy_byte, dummy_byte,
											dummy_byte, false, serialport, slider_value, b.PWM_FREQ);
									my_worker.execute();
									/*
									 * // Determines whether the user gesture to move the slider's knob is complete
									 * print(Integer.toString(slider_value)); byte value = (byte) slider_value;
									 * expected_message_type = b.MSG_TYPE_NOTIFY; expected_cmd_id =
									 * b.EXPECT_NO_RESPONSE; Current_control = BLUE_SLIDER; ArrayList<Byte>
									 * IPCPacket = new ArrayList<Byte>();
									 * construct_and_send_IPCPacket(Current_control,
									 * IPCPacket,b.INTENSITY_SLIDER_BLUE,value,dummy_byte,dummy_byte,dummy_byte,
									 * dummy_byte);
									 */

								}

							} else {
								slider_value = slider_18.getValue();
								String slider_2 = Integer.toString(slider_value);
								BluetextField.setText(slider_2);
								String local = Integer.toString((slider_value *100)/Max_Val);
								label_23.setText(local + " %"); 
								// label_23.setText((slider_2) + " %");
								
							}
						}
						else
						{
							slider_value = slider_18.getValue();
							BluetextField.setText(Integer.toString(slider_value));
							String local = Integer.toString((slider_value*100)/Max_Val);
							label_23.setText(local + " %");
						}
					}

				}
			}
		});
		slider_18.setBounds(52, 59, 255, 21);
		panel_29.add(slider_18);
		slider_18.setValue(0);

		this.RedtextField = new JFormattedTextField(formattertf);
		RedtextField.setEditable(false);
		RedtextField.setEnabled(false);
		RedtextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					
				
				int val = 0;
				String val1 = RedtextField.getText();
				byte value = (byte) Integer.parseInt(val1);
				val = SignedtoUnsigned.printByte(value);
				// JOptionPane.showMessageDialog(frmStrykerVer, val, "Value Entered",
				// JOptionPane.ERROR_MESSAGE);
				bytedefinitions b = new bytedefinitions();
				if (!Response_Animator.Animation.isShowing()) {
					pause_flag = true;
					System.out.println("RedLight Absolute Control!!");

					ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
					Main_App_Worker my_worker = new Main_App_Worker(slider_16, SLIDER, IPCPacket,
							b.INTENSITY_SLIDER_RED, (byte) val, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
							false, serialport, Integer.parseInt(val1), b.PWM_FREQ);
					my_worker.execute();
					connect_flag = true;
					slider_16.setValue(Integer.parseInt(val1));
					//label_20.setText(Integer.toString(val));
					connect_flag = false;
					// Determines whether the user gesture to move the slider's knob is complete
					/*
					 * print(Integer.toString(slider_value)); byte value = (byte) slider_value;
					 * expected_message_type = b.MSG_TYPE_NOTIFY; expected_cmd_id =
					 * b.EXPECT_NO_RESPONSE; Current_control = RED_SLIDER; ArrayList<Byte> IPCPacket
					 * = new ArrayList<Byte>(); construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,b.INTENSITY_SLIDER_RED,value,dummy_byte,dummy_byte,dummy_byte,
					 * dummy_byte);
					 */

				}
			}
			}
		});
		RedtextField.setColumns(10);
		RedtextField.setBounds(320, 3, 42, 23);
		panel_29.add(RedtextField);
		
		
		
		this.label_21 = new JLabel("0 %");
		label_21.setBounds(367, 5, 33, 16);
		panel_29.add(label_21);
		label_21.setEnabled(false);
		label_21.setFont(new Font("Calibri Light", Font.PLAIN, 11));

		this.label_22 = new JLabel("0 %");
		label_22.setBounds(367, 33, 33, 16);
		panel_29.add(label_22);
		label_22.setEnabled(false);
		label_22.setFont(new Font("Calibri Light", Font.PLAIN, 11));

		this.label_23 = new JLabel("0 %");
		label_23.setBounds(367, 60, 33, 16);
		panel_29.add(label_23);
		label_23.setEnabled(false);
		label_23.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		
		this.GreentextField = new JFormattedTextField(formattertf);
		GreentextField.setEditable(false);
		GreentextField.setEnabled(false);
		GreentextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				
				int val = 0;
				String val1 = GreentextField.getText();
				byte value = (byte) Integer.parseInt(val1);
				val = SignedtoUnsigned.printByte(value);
				// JOptionPane.showMessageDialog(frmStrykerVer, val, "Value Entered",
				// JOptionPane.ERROR_MESSAGE);
				bytedefinitions b = new bytedefinitions();
				if (!Response_Animator.Animation.isShowing()) {
					pause_flag = true;
					System.out.println("GreenLight Absolute Control!!");

					ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
					Main_App_Worker my_worker = new Main_App_Worker(slider_17, SLIDER, IPCPacket,
							b.INTENSITY_SLIDER_GREEN, (byte) val, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
							false, serialport, Integer.parseInt(val1), b.PWM_FREQ);
					my_worker.execute();
					connect_flag = true;
					slider_17.setValue(Integer.parseInt(val1));
					//label_20.setText(Integer.toString(val));
					connect_flag = false;
					// Determines whether the user gesture to move the slider's knob is complete
					/*
					 * print(Integer.toString(slider_value)); byte value = (byte) slider_value;
					 * expected_message_type = b.MSG_TYPE_NOTIFY; expected_cmd_id =
					 * b.EXPECT_NO_RESPONSE; Current_control = RED_SLIDER; ArrayList<Byte> IPCPacket
					 * = new ArrayList<Byte>(); construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,b.INTENSITY_SLIDER_RED,value,dummy_byte,dummy_byte,dummy_byte,
					 * dummy_byte);
					 */

				}
				
			}
			}
		});
		GreentextField.setColumns(10);
		GreentextField.setBounds(320, 30, 42, 23);
		panel_29.add(GreentextField);
		
		this.BluetextField = new JFormattedTextField(formattertf);
		BluetextField.setEditable(false);
		BluetextField.setEnabled(false);
		BluetextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				int val = 0;
				String val1 = BluetextField.getText();
				byte value = (byte) Integer.parseInt(val1);
				val = SignedtoUnsigned.printByte(value);
				// JOptionPane.showMessageDialog(frmStrykerVer, val, "Value Entered",
				// JOptionPane.ERROR_MESSAGE);
				bytedefinitions b = new bytedefinitions();
				if (!Response_Animator.Animation.isShowing()) {
					pause_flag = true;
					System.out.println("BlueLight Absolute Control!!");

					ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
					Main_App_Worker my_worker = new Main_App_Worker(slider_18, SLIDER, IPCPacket,
							b.INTENSITY_SLIDER_BLUE, (byte) val, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
							false, serialport, Integer.parseInt(val1), b.PWM_FREQ);
					my_worker.execute();
					connect_flag = true;
					slider_18.setValue(Integer.parseInt(val1));
					//label_20.setText(Integer.toString(val));
					connect_flag = false;
					// Determines whether the user gesture to move the slider's knob is complete
					/*
					 * print(Integer.toString(slider_value)); byte value = (byte) slider_value;
					 * expected_message_type = b.MSG_TYPE_NOTIFY; expected_cmd_id =
					 * b.EXPECT_NO_RESPONSE; Current_control = RED_SLIDER; ArrayList<Byte> IPCPacket
					 * = new ArrayList<Byte>(); construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,b.INTENSITY_SLIDER_RED,value,dummy_byte,dummy_byte,dummy_byte,
					 * dummy_byte);
					 */

				}
			}
			}
		});
		BluetextField.setColumns(10);
		BluetextField.setBounds(320, 55, 42, 23);
		panel_29.add(BluetextField);

		this.WlPowerButton = new JToggleButton("");
		WlPowerButton.setEnabled(false);
		WlPowerButton.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (!Response_Animator.Animation.isShowing()) {
					bytedefinitions b = new bytedefinitions();
					b.LIGHT_SOURCE = b.SRC_WHITELIGHT;
					System.out.println("LIGHT SOURCE : " + b.LIGHT_SOURCE);
					Wl_Button_Flag = true;
					if (e.getStateChange() == ItemEvent.SELECTED) {
						System.out.println();
						pause_flag = true;
						if (connect_flag == false && wl_critical_flag == false) {
							// WlPowerButton.setIcon(new
							// ImageIcon(Stryk_Demo.class.getResource("/resources/on.jpg")));
							// btnLastMode.setEnabled(false);
							b.STATE = b.ON_STATE;

							int status = check_for_unselected_radiobuttons(wlsource);
							if (rdbtnLc.isSelected()) {
								status = 0x0;
							}
							if (status == 0x0) {

								if (rdbtnPwm.isSelected()) {
									b.MODE = b.TYPE_PWM;
								} else if (rdbtnCwm.isSelected()) {
									b.MODE = b.TYPE_CWM;
								} else if (rdbtnStb.isSelected()) {
									b.MODE = b.TYPE_STROBE;
								} else if (rdbtnLc.isSelected()) {
									b.MODE = b.TYPE_LC;
								} else {

								}

								if (rdbtnExt.isSelected()) {
									b.PWM_SOURCE = b.SOURCE_EXTERNAL;
								} else if (rdbtnuC_1.isSelected()) {
									b.PWM_SOURCE = b.SOURCE_uC1;
								} else if (rdbtnuC_2.isSelected()) {
									b.PWM_SOURCE = b.SOURCE_uC2;
								} else {

								}

								if (b.PWM_SOURCE == b.SOURCE_EXTERNAL) {
									b.PWM_TYPE = b.PWM_TYPE_FIXED;
									b.TYPE_VAL = 0x0;
								} else {
									int temp = 0;
									if(tglbtnFixed_Wl.getText().equals("FIXED")) {
										b.PWM_TYPE = b.PWM_TYPE_FIXED;
										temp = Integer.parseInt(formattedTextField_WlPwm.getText());
										b.TYPE_VAL = (byte) temp;
									} else if(tglbtnFixed_Wl.getText().equals("FADE")) {
										b.PWM_TYPE = b.PWM_TYPE_FADE;
										temp = Integer.parseInt(formattedTextField_WlPwm.getText());
										b.TYPE_VAL = (byte) temp;
									}else if(tglbtnFixed_Wl.getText().equals("PATTERN 10�s")) {
										b.PWM_TYPE = b.PWM_TYPE_PATTERN_1�s;
										temp = 0;
										b.TYPE_VAL = (byte) temp;
									} else if(tglbtnFixed_Wl.getText().equals("PATTERN 1ms")) {
										b.PWM_TYPE = b.PWM_TYPE_PATTERN_1ms;
										temp = 0;
										b.TYPE_VAL = (byte) temp;
									} else if(tglbtnFixed_Wl.getText().equals("PATTERN TRANSIENT")) {
										b.PWM_TYPE = b.PWM_TYPE_PATTERN_TRANSIENT;
										temp = 0;
										b.TYPE_VAL = (byte) temp;
									}
									/*if (tglbtnFixed_Wl.isSelected() == true) {

										b.PWM_TYPE = b.PWM_TYPE_FADE;
										temp = Integer.parseInt(formattedTextField_WlPwm.getText());
										b.TYPE_VAL = (byte) temp;

									} else {

										b.PWM_TYPE = b.PWM_TYPE_FIXED;
										temp = Integer.parseInt(formattedTextField_WlPwm.getText());
										b.TYPE_VAL = (byte) temp;
									}*/

								}
								
								if(rdbtnPwm.isSelected() && rdbtnuC_1.isSelected() && rdbtn120hz.isSelected()) {
									b.PWM_FREQ = b.PWM_LOW_FREQUENCY;
								} else if(rdbtnPwm.isSelected() && rdbtnuC_1.isSelected() && rdbtn70khz.isSelected()) {
									b.PWM_FREQ = b.PWM_HIGH_FREQUENCY;
								}

								Current_control = WL_PON;
								ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
								Main_App_Worker my_worker = new Main_App_Worker(WlPowerButton, Current_control,
										IPCPacket, b.LIGHT_SOURCE, b.STATE, b.MODE, b.PWM_SOURCE, b.PWM_TYPE,
										b.TYPE_VAL, true, serialport, 0, b.PWM_FREQ);
								my_worker.execute();
								// construct_and_send_IPCPacket(Current_control,
								// IPCPacket,b.LIGHT_SOURCE,b.STATE,b.MODE,b.PWM_SOURCE,b.PWM_TYPE,b.TYPE_VAL);
							} else {
								ShowMessage("Please Choose a Valid Source!!");

							}

						} else {
							// Current_control = WL_FAKE_ON;
							/// ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
							// Main_App_Worker my_worker = new Main_App_Worker(tglbtnFixed_Wl,
							// Current_control, IPCPacket, b.LIGHT_SOURCE, b.STATE, b.MODE, b.PWM_SOURCE,
							// b.PWM_TYPE, b.TYPE_VAL, true, serialport);
							// my_worker.execute();

							/*
							 * if(EnvIrisPowerButton.isSelected()) { Color en = Color.decode("#ccffff");
							 * panel_33.setBackground(en); }
							 * 
							 * textArea.append("White Light Switched ON!!\r\n\n"); Component[] p26 =
							 * panel_26.getComponents(); Component[] p27 = panel_27.getComponents();
							 * Component[] p28 = panel_28.getComponents();
							 * 
							 * for(Component c : p26) { c.setEnabled(false); } for(Component c : p27) {
							 * c.setEnabled(false); } for(Component c : p28) { c.setEnabled(false); }
							 * slider_15.setEnabled(true); lblGrp.setEnabled(true);
							 * label_20.setEnabled(true); rdbtnIndividual.setEnabled(true); wl_onoff_flag =
							 * true; label_21.setEnabled(false); label_22.setEnabled(false);
							 * label_23.setEnabled(false); chckbxRed.setSelected(true);
							 * chckbxGreen.setSelected(true); chckbxBlue.setSelected(true);
							 * lblLc.setEnabled(false); rdbtnLc.setEnabled(false); wl_onoff_flag = false;
							 */
						}

					}
					if (e.getStateChange() == ItemEvent.DESELECTED) {

						pause_flag = true;
						// WlPowerButton.setIcon(new
						// ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
						Color en = Color.decode("#000000");
						panel_33.setBackground(en);
						b.LIGHT_SOURCE = b.SRC_WHITELIGHT;
						b.STATE = b.OFF_STATE;

						System.out.println("INSIDE WLPOFF");
						System.out.println(b.LIGHT_SOURCE);
						if (connect_flag == false && wl_critical_flag == false) {
							Current_control = WL_POFF;
							ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
							Main_App_Worker my_worker = new Main_App_Worker(WlPowerButton, Current_control, IPCPacket,
									b.LIGHT_SOURCE, b.STATE, b.MODE, b.PWM_SOURCE, b.PWM_TYPE, b.TYPE_VAL, true,
									serialport, 0, b.PWM_FREQ);
							my_worker.execute();
							// construct_and_send_IPCPacket(Current_control,
							// IPCPacket,b.LIGHT_SOURCE,b.STATE,dummy_byte,dummy_byte,dummy_byte,dummy_byte);
						}

						// WlPowerButton.setIcon(new
						// ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));

					}
				}

			}
		});
		WlPowerButton.setBounds(370, 17, 42, 42);
		panel_23.add(WlPowerButton);
		WlPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
		WlPowerButton.setBackground(Color.LIGHT_GRAY);
		// WlPowerButton.setMultiClickThreshhold(500);
		WlPowerButton.setForeground(new Color(240, 255, 240));

		this.panel_28 = new JPanel();
		panel_28.setBounds(8, 139, 188, 55);
		panel_23.add(panel_28);
		panel_28.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		panel_28.setBorder(
				new TitledBorder(null, "PWM TYPE", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
		((TitledBorder) panel_28.getBorder()).setTitleFont(new Font("Tahoma", Font.BOLD, 10));
		panel_28.setLayout(null);

		this.formattedTextField_WlPwm = new JFormattedTextField(formatter1);
		formattedTextField_WlPwm.setFont(new Font("SansSerif", Font.PLAIN, 11));
		formattedTextField_WlPwm.setEnabled(false);
		formattedTextField_WlPwm.setText("0");
		formattedTextField_WlPwm.setBounds(131, 18, 43, 20);
		panel_28.add(formattedTextField_WlPwm);

		this.tglbtnFixed_Wl = new JButton("FIXED");
		tglbtnFixed_Wl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(rdbtnPwm.isSelected() && rdbtnuC_1.isSelected() && rdbtn120hz.isSelected()) {
					if(tglbtnFixed_Wl.getText().toString().equals("FIXED")) {
						tglbtnFixed_Wl.setText("FADE");
						lblFixedValue_2.setText("DURATION");
					} else if(tglbtnFixed_Wl.getText().toString().equals("FADE")) {
						tglbtnFixed_Wl.setText("PATTERN 10�s");
						lblFixedValue_2.setText("DURATION");
						formattedTextField_WlPwm.setEnabled(false);
						lblFixedValue_2.setEnabled(false);
					} else if(tglbtnFixed_Wl.getText().toString().equals("PATTERN 10�s")) {
						formattedTextField_WlPwm.setEnabled(false);
						tglbtnFixed_Wl.setText("PATTERN 1ms");
						lblFixedValue_2.setText("DURATION");
						lblFixedValue_2.setEnabled(false);
					} else if(tglbtnFixed_Wl.getText().toString().equals("PATTERN 1ms")) {
						tglbtnFixed_Wl.setText("PATTERN TRANSIENT");
						lblFixedValue_2.setText("DURATION");
						formattedTextField_WlPwm.setEnabled(false);
						lblFixedValue_2.setEnabled(false);
					} else if(tglbtnFixed_Wl.getText().toString().equals("PATTERN TRANSIENT")) {
						tglbtnFixed_Wl.setText("FIXED");
						lblFixedValue_2.setText("VALUE");
						formattedTextField_WlPwm.setEnabled(true);
						lblFixedValue_2.setEnabled(true);
					}
				} else {
					if(tglbtnFixed_Wl.getText().toString().equals("FIXED")) {
						tglbtnFixed_Wl.setText("FADE");
						lblFixedValue_2.setText("DURATION");
					}else if(tglbtnFixed_Wl.getText().toString().equals("FADE")) {
						tglbtnFixed_Wl.setText("FIXED");
						lblFixedValue_2.setText("VALUE");
					}
				}
				
			}
		});
		/*tglbtnFixed_Wl.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {

					tglbtnFixed_Wl.setText("FADE");
					lblFixedValue_2.setText("DURATION");

				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					tglbtnFixed_Wl.setText("FIXED");
					lblFixedValue_2.setText("VALUE");
				}
			}
		});*/
		tglbtnFixed_Wl.setEnabled(false);
		tglbtnFixed_Wl.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		tglbtnFixed_Wl.setBounds(12, 17, 118, 22);
		panel_28.add(tglbtnFixed_Wl);

		this.lblFixedValue_2 = new JLabel("VALUE");
		lblFixedValue_2.setEnabled(false);
		lblFixedValue_2.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		lblFixedValue_2.setBounds(133, 33, 43, 16);
		panel_28.add(lblFixedValue_2);

		this.lblGrp = new JLabel("GRP");
		lblGrp.setEnabled(false);
		lblGrp.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		lblGrp.setBounds(222, 68, 26, 16);
		panel_23.add(lblGrp);

		this.panel_24 = new JPanel();
		panel_24.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		panel_24.setBorder(new TitledBorder(
				new TitledBorder(null, "", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)),
				"ENV & IRIS", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(51, 51, 51)));
		((TitledBorder) panel_24.getBorder()).setTitleFont(new Font("Tahoma", Font.BOLD, 10));
		panel_24.setBounds(244, 220, 609, 309);
		frmStrykerVer.getContentPane().add(panel_24);
		panel_24.setLayout(null);

		panel_30 = new JPanel();
		panel_30.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		panel_30.setBorder(
				new TitledBorder(null, "MODE", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
		((TitledBorder) panel_30.getBorder()).setTitleFont(new Font("Tahoma", Font.BOLD, 10));
		panel_30.setBounds(13, 59, 188, 55);
		panel_24.add(panel_30);
		panel_30.setLayout(null);
		this.rdbtnIndividual = new JRadioButton("INDIVIDUAL");
		rdbtnIndividual.setEnabled(false);
		rdbtnIndividual.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				if (wl_onoff_flag == false && connect_flag == false) {
					bytedefinitions b = new bytedefinitions();
					// if(e.getStateChange() == ItemEvent.SELECTED)
					// {

					// byte value = (byte) 0x1;
					// ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
					// Main_App_Worker my_worker = new Main_App_Worker(rdbtnIndividual, INDIVIDUAL,
					// IPCPacket, dummy_byte,dummy_byte, dummy_byte, dummy_byte, dummy_byte,
					// dummy_byte, false, serialport);
					// my_worker.execute();
					// }
					// if(e.getStateChange() == ItemEvent.DESELECTED)
					// {
					// byte value = (byte) 0x0;
					// ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
					// Main_App_Worker my_worker = new Main_App_Worker(rdbtnIndividual, INDIVIDUAL,
					// IPCPacket, dummy_byte,dummy_byte, dummy_byte, dummy_byte, dummy_byte,
					// dummy_byte, false, serialport);
					// //my_worker.execute();
					// }

					individual_flag = true;
					Component[] p29 = panel_29.getComponents();
					if (e.getStateChange() == ItemEvent.SELECTED) {
						slider_15.setEnabled(false);
						lblGrp.setEnabled(false);
						//Disable the group field text field
						
						
						
						label_20.setEnabled(false);
						for (Component c : p29) {
							if (c instanceof JCheckBox) {
								c.setEnabled(true);
							}

						}
						if (EnvIrisPowerButton.isSelected() == false) {
							System.out.println("Not Selected");
							chckbxRed.setEnabled(true);
							slider_16.setEnabled(true);
							label_21.setEnabled(true);
							

						}

						else {
							System.out.println("ENV Selected");
							if ((rdbtnEnv_CwmNr.isSelected() == false) && (rdbtnEnv_LcNr.isSelected() == false)) {
								chckbxRed.setEnabled(true);
								slider_16.setEnabled(true);
								label_21.setEnabled(true);
								RedtextField.setEditable(true);
								RedtextField.setEnabled(true);
							} else {
								connect_flag = true;
								chckbxRed.setSelected(false);
								connect_flag = false;
								chckbxRed.setEnabled(false);
								slider_16.setEnabled(false);
								label_21.setEnabled(false);
								RedtextField.setText("0");
								RedtextField.setEditable(false);
								RedtextField.setEnabled(false);
							}

						}
						if (chckbxRed.isSelected()) {
							slider_16.setEnabled(true);
							connect_flag = true;
							slider_16.setValue(slider_15.getValue());
							connect_flag = false;
							label_21.setEnabled(true);
							label_21.setText(Integer.toString((slider_16.getValue()*100)/255) + " %");
							//label_21.setText(Integer.toString(slider_16.getValue()));

						}
						if (!chckbxRed.isSelected()) {
							slider_16.setEnabled(false);
							connect_flag = true;
							slider_16.setValue(0);
							connect_flag = false;
							label_21.setText(Integer.toString(0) + " %");
							//label_21.setText(Integer.toString(0));
							label_21.setEnabled(false);
						}
						if (chckbxGreen.isSelected()) {
							slider_17.setEnabled(true);
							connect_flag = true;
							slider_17.setValue(slider_15.getValue());
							connect_flag = false;
							label_22.setText(Integer.toString((slider_17.getValue()*100)/255) + " %");
							//label_22.setText(Integer.toString(slider_17.getValue()));
							label_22.setEnabled(true);

						}
						if (!chckbxGreen.isSelected()) {
							slider_17.setEnabled(false);
							connect_flag = true;
							slider_17.setValue(0);
							connect_flag = false;
							label_22.setText(Integer.toString(0) + " %");
							//label_22.setText(Integer.toString(slider_17.getValue()));
							label_22.setEnabled(false);
						}
						if (chckbxBlue.isSelected()) {
							slider_18.setEnabled(true);
							connect_flag = true;
							slider_18.setValue(slider_15.getValue());
							connect_flag = false;
							label_23.setText(Integer.toString((slider_18.getValue()*100)/255) + " %");
							//label_23.setText(Integer.toString(slider_18.getValue()));
							label_23.setEnabled(true);
						}
						if (!chckbxBlue.isSelected()) {
							slider_18.setEnabled(false);
							connect_flag = true;
							slider_18.setValue(0);
							connect_flag = false;
							label_23.setText(Integer.toString(slider_18.getValue()) + " %");
							//label_23.setText(Integer.toString(slider_18.getValue()));
							label_23.setEnabled(false);
						}
						GrptextField.setEnabled(false);
						GrptextField.setEditable(false);
						if(chckbxRed.isSelected())
						{
							//JOptionPane.showMessageDialog(frmStrykerVer, "SELECTED", "chckbxred", JOptionPane.ERROR_MESSAGE);
							//Enable R,G and B TextFields
							RedtextField.setEnabled(true);
							RedtextField.setEditable(true);
						}
						else
						{
							//JOptionPane.showMessageDialog(frmStrykerVer, "DESELECTED", "chckbxred", JOptionPane.ERROR_MESSAGE);
							//Disable R,G and B TextFields
							RedtextField.setEnabled(false);
							RedtextField.setEditable(false);
						}
						
						if(chckbxGreen.isSelected())
						{
							GreentextField.setEnabled(true);
							GreentextField.setEditable(true);
						}
						else
						{
							GreentextField.setEnabled(false);
							GreentextField.setEditable(false);
						}
						
						if(chckbxBlue.isSelected())
						{
							BluetextField.setEnabled(true);
							BluetextField.setEditable(true);
						}
						else
						{
							BluetextField.setEnabled(false);
							BluetextField.setEditable(false);
						}

					}
					if (e.getStateChange() == ItemEvent.DESELECTED) {
						slider_15.setEnabled(true);
						lblGrp.setEnabled(true);
						label_20.setEnabled(true);
						//Enable the GroupSlider TextBox 
						GrptextField.setEnabled(true);
						GrptextField.setEditable(true);
						
						//Disable R,G and B TextFields
						RedtextField.setEnabled(false);
						RedtextField.setEditable(false);
						
						GreentextField.setEnabled(false);
						GreentextField.setEditable(false);
						
						BluetextField.setEnabled(false);
						BluetextField.setEditable(false);
						for (Component c : p29) {
							if (c instanceof JCheckBox) {
								c.setEnabled(false);
							}

						}
						slider_16.setEnabled(false);
						label_21.setEnabled(false);
						slider_17.setEnabled(false);
						label_22.setEnabled(false);
						slider_18.setEnabled(false);
						label_23.setEnabled(false);
						chckbxRed.setEnabled(false);
						chckbxGreen.setEnabled(false);
						chckbxBlue.setEnabled(false);
					}
				}

			}
		});
		rdbtnIndividual.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		rdbtnIndividual.setBounds(207, 80, 81, 18);
		panel_23.add(rdbtnIndividual);
		this.rdbtnEnv_Pwm = new JRadioButton("");
		rdbtnEnv_Pwm.setEnabled(false);
		rdbtnEnv_Pwm.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					// env_iris_source.clearSelection();
					if (chckbxEnv.isSelected()) {
						rdbtnEnv_Ext.setEnabled(true);
						rdbtnuC_3.setEnabled(true);
						rdbtnuC_4.setEnabled(true);
						rdbtn_IrisExtEnv.setEnabled(false);
						rdbtn_IrisExtWht.setEnabled(false);
					} else if (chckbxIris.isSelected()) {
						rdbtnEnv_Ext.setEnabled(false);
						rdbtnuC_3.setEnabled(true);
						rdbtnuC_4.setEnabled(true);
						rdbtn_IrisExtEnv.setEnabled(true);
						rdbtn_IrisExtWht.setEnabled(true);
						_env_.clearSelection();

					}

				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					env_iris_source.clearSelection();
					rdbtnEnv_Ext.setEnabled(false);
					rdbtnuC_3.setEnabled(false);
					rdbtnuC_4.setEnabled(false);
					rdbtn_IrisExtEnv.setEnabled(false);
					rdbtn_IrisExtWht.setEnabled(false);

					for (Component c : env_pwm_type) {
						c.setEnabled(false);
					}

				}
			}
		});
		rdbtnEnv_Pwm.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		rdbtnEnv_Pwm.setBounds(18, 25, 18, 18);
		env_iris_mode.add(rdbtnEnv_Pwm);
		panel_30.add(rdbtnEnv_Pwm);

		this.rdbtnEnv_Cwm = new JRadioButton("");
		rdbtnEnv_Cwm.setEnabled(false);
		rdbtnEnv_Cwm.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					rdbtnuC_3.setEnabled(true);
					rdbtnuC_4.setEnabled(true);
				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					rdbtnuC_3.setEnabled(false);
					rdbtnuC_4.setEnabled(false);
					env_iris_source.clearSelection();
				}
			}
		});
		rdbtnEnv_Cwm.setBounds(64, 25, 18, 18);
		env_iris_mode.add(rdbtnEnv_Cwm);
		panel_30.add(rdbtnEnv_Cwm);

		this.rdbtnEnv_CwmNr = new JRadioButton("");
		rdbtnEnv_CwmNr.setEnabled(false);
		rdbtnEnv_CwmNr.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					rdbtnuC_3.setEnabled(true);
					rdbtnuC_4.setEnabled(true);
				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					rdbtnuC_3.setEnabled(false);
					rdbtnuC_4.setEnabled(false);
					env_iris_source.clearSelection();
				}

			}
		});
		rdbtnEnv_CwmNr.setBounds(110, 25, 18, 18);
		env_iris_mode.add(rdbtnEnv_CwmNr);
		panel_30.add(rdbtnEnv_CwmNr);

		this.rdbtnEnv_LcNr = new JRadioButton("");
		rdbtnEnv_LcNr.setEnabled(false);
		rdbtnEnv_LcNr.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					if ((rdbtnPwm.isSelected() == false) && (rdbtnCwm.isSelected() == false)
							&& (rdbtnStb.isSelected() == false)) {
						rdbtnEnv_Ext.setEnabled(false);
						rdbtnuC_3.setEnabled(true);
						rdbtnuC_4.setEnabled(true);
						rdbtn_IrisExtWht.setEnabled(false);
						rdbtn_IrisExtEnv.setEnabled(false);
						rdbtnPwm.setEnabled(false);
						rdbtnCwm.setEnabled(false);
						rdbtnStb.setEnabled(false);
						if (WlPowerButton.isSelected()) {
							rdbtnLc.setEnabled(false);
						} else {
							rdbtnLc.setEnabled(true);
						}

						rdbtnLc.setSelected(true);
						env_iris_source.clearSelection();
					} else {

					}

				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					if (WlPowerButton.isSelected() == false) {
						rdbtnPwm.setEnabled(true);
						rdbtnCwm.setEnabled(true);
						rdbtnStb.setEnabled(true);
						rdbtnLc.setEnabled(true);
						rdbtnLc.setSelected(false);
						if (!rdbtnLc.isSelected()) {
							rdbtnPwm.setSelected(true);
						}

						// rdbtnuC_3.setEnabled(false);
						// rdbtnuC_4.setEnabled(false);
						env_iris_source.clearSelection();
					}

				}
			}
		});
		rdbtnEnv_LcNr.setBounds(151, 25, 18, 18);
		env_iris_mode.add(rdbtnEnv_LcNr);
		panel_30.add(rdbtnEnv_LcNr);

		this.lblPwm_8 = new JLabel("PWM");
		lblPwm_8.setEnabled(false);
		lblPwm_8.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		lblPwm_8.setBounds(16, 13, 28, 16);
		panel_30.add(lblPwm_8);

		this.lblCwm_1 = new JLabel("CWM");
		lblCwm_1.setEnabled(false);
		lblCwm_1.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		lblCwm_1.setBounds(61, 13, 28, 16);
		panel_30.add(lblCwm_1);

		this.lblCwmnr = new JLabel("CWM_NR");
		lblCwmnr.setEnabled(false);
		lblCwmnr.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		lblCwmnr.setBounds(100, 13, 43, 16);
		panel_30.add(lblCwmnr);

		this.lblLcnr = new JLabel("LC_\r\n\r\nNR");
		lblLcnr.setEnabled(false);
		lblLcnr.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		lblLcnr.setBounds(150, 13, 30, 16);
		panel_30.add(lblLcnr);

		panel_31 = new JPanel();
		panel_31.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		panel_31.setBorder(
				new TitledBorder(null, "SOURCE", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
		((TitledBorder) panel_31.getBorder()).setTitleFont(new Font("Tahoma", Font.BOLD, 10));
		panel_31.setBounds(13, 118, 188, 110);
		panel_24.add(panel_31);
		panel_31.setLayout(null);

		this.rdbtnEnv_Ext = new JRadioButton("");
		rdbtnEnv_Ext.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					if ((chckbxEnv.isSelected() == true) || (chckbxIris.isSelected() == true)) {
						EnvIrisPowerButton.setEnabled(true);
						for (Component c : env_pwm_type) {
							c.setEnabled(false);
						}
					}

				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					EnvIrisPowerButton.setEnabled(false);
				}
			}
		});
		rdbtnEnv_Ext.setEnabled(false);
		rdbtnEnv_Ext.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		rdbtnEnv_Ext.setBounds(40, 25, 18, 18);
		env_iris_source.add(rdbtnEnv_Ext);
		panel_31.add(rdbtnEnv_Ext);

		this.lblExt_1 = new JLabel("EXT");
		lblExt_1.setEnabled(false);
		lblExt_1.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		lblExt_1.setBounds(42, 13, 18, 16);
		panel_31.add(lblExt_1);

		this.rdbtnuC_3 = new JRadioButton("");
		rdbtnuC_3.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {

					EnvIrisPowerButton.setEnabled(true);
					if (rdbtnEnv_Pwm.isSelected()) {
						for (Component c : env_pwm_type) {
							c.setEnabled(true);
						}
					} else {
						for (Component c : env_pwm_type) {
							c.setEnabled(false);
						}
					}

				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {

					EnvIrisPowerButton.setEnabled(false);
					for (Component c : env_pwm_type) {
						c.setEnabled(false);
					}
				}

			}
		});
		rdbtnuC_3.setEnabled(false);
		rdbtnuC_3.setBounds(87, 25, 18, 18);
		env_iris_source.add(rdbtnuC_3);
		panel_31.add(rdbtnuC_3);

		this.lblUc_2 = new JLabel("uC1");
		lblUc_2.setEnabled(false);
		lblUc_2.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		lblUc_2.setBounds(87, 13, 18, 16);
		panel_31.add(lblUc_2);

		this.rdbtnuC_4 = new JRadioButton("");
		rdbtnuC_4.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					EnvIrisPowerButton.setEnabled(true);
					if (rdbtnEnv_Pwm.isSelected()) {
						for (Component c : env_pwm_type) {
							c.setEnabled(true);
						}
					} else {
						for (Component c : env_pwm_type) {
							c.setEnabled(false);
						}
					}
				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					EnvIrisPowerButton.setEnabled(false);
					for (Component c : env_pwm_type) {
						c.setEnabled(false);
					}
				}
			}
		});
		rdbtnuC_4.setEnabled(false);
		rdbtnuC_4.setBounds(133, 25, 18, 18);
		env_iris_source.add(rdbtnuC_4);
		panel_31.add(rdbtnuC_4);

		this.lblUc_3 = new JLabel("uC2");
		lblUc_3.setEnabled(false);
		lblUc_3.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		lblUc_3.setBounds(133, 13, 23, 16);
		panel_31.add(lblUc_3);

		this.rdbtn_IrisExtWht = new JRadioButton("");
		rdbtn_IrisExtWht.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					EnvIrisPowerButton.setEnabled(true);
				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					EnvIrisPowerButton.setEnabled(false);
				}
			}

		});
		rdbtn_IrisExtWht.setEnabled(false);
		rdbtn_IrisExtWht.setBounds(59, 74, 18, 18);
		env_iris_source.add(rdbtn_IrisExtWht);
		panel_31.add(rdbtn_IrisExtWht);

		this.lblExtwht = new JLabel("EXT_WHT");
		lblExtwht.setEnabled(false);
		lblExtwht.setBounds(50, 62, 45, 16);
		panel_31.add(lblExtwht);
		lblExtwht.setFont(new Font("Calibri Light", Font.PLAIN, 10));

		this.lblExtenv = new JLabel("EXT_ENV");
		lblExtenv.setEnabled(false);
		lblExtenv.setBounds(99, 62, 45, 16);
		panel_31.add(lblExtenv);
		lblExtenv.setFont(new Font("Calibri Light", Font.PLAIN, 10));

		this.rdbtn_IrisExtEnv = new JRadioButton("");
		rdbtn_IrisExtEnv.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					EnvIrisPowerButton.setEnabled(true);
				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					EnvIrisPowerButton.setEnabled(false);
				}
			}
		});
		rdbtn_IrisExtEnv.setEnabled(false);
		rdbtn_IrisExtEnv.setBounds(108, 74, 18, 18);
		env_iris_source.add(rdbtn_IrisExtEnv);
		panel_31.add(rdbtn_IrisExtEnv);

		this.EnvIrisPowerButton = new JToggleButton("");
		EnvIrisPowerButton.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent e) {

			}
		});
		EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
		EnvIrisPowerButton.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				System.out.println("ENV IRIS is called");
				ENV_Iris_Button_Flag = true;
				bytedefinitions b = new bytedefinitions();
				if (SOURCE == ENV_SOURCE) {
					b.LIGHT_SOURCE = b.SRC_ENV;
				}
				if (SOURCE == IRIS_SOURCE) {
					b.LIGHT_SOURCE = b.SRC_IRIS;
				}

				if (e.getStateChange() == ItemEvent.SELECTED) {
					if (!Response_Animator.Animation.isShowing()) {
						pause_flag = true;
						if (rdbtnIndividual.isSelected() == true) {

							if ((rdbtnEnv_CwmNr.isSelected() == true) || (rdbtnEnv_LcNr.isSelected() == true)) {
								// Current_control = CWM_NR_ON;
								// ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
								// Main_App_Worker my_worker = new Main_App_Worker(EnvIrisPowerButton,
								// Current_control, IPCPacket, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
								// dummy_byte, dummy_byte,false, serialport);
								// my_worker.execute();

								wl_onoff_flag = true;
								connect_flag = true;
								chckbxRed.setSelected(false);
								chckbxRed.setEnabled(false);
								slider_16.setEnabled(false);
								label_21.setEnabled(false);
								RedtextField.setEditable(false);
								RedtextField.setEnabled(false);
								wl_onoff_flag = false;
								connect_flag = false;
								
							} else {
								// Current_control = CWM_NR_OFF;
								// ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
								// Main_App_Worker my_worker = new Main_App_Worker(EnvIrisPowerButton,
								// Current_control, IPCPacket, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
								// dummy_byte, dummy_byte, false, serialport);
								// my_worker.execute();
								wl_onoff_flag = true;
								connect_flag = true;
								
								RedtextField.setEditable(true);
								RedtextField.setEnabled(true);
								// chckbxRed.setSelected(true);
								// chckbxRed.setEnabled(true);
								// slider_16.setEnabled(true);
								// label_21.setEnabled(true);
								wl_onoff_flag = false;
								connect_flag = false;
							}
						}

						print(SOURCE + " event");
						b.STATE = b.ON_STATE;

						int status = check_for_unselected_radiobuttons(env_iris_source);
						if (status == 0x0) {

							if (rdbtnEnv_Pwm.isSelected()) {
								b.MODE = b.TYPE_PWM;
							} else if (rdbtnEnv_Cwm.isSelected()) {
								b.MODE = b.TYPE_CWM;
							} else if (rdbtnEnv_CwmNr.isSelected()) {
								b.MODE = b.TYPE_CWM_NORED;
							} else if (rdbtnEnv_LcNr.isSelected()) {
								b.MODE = b.TYPE_LC_NORED;
							} else {

							}

							if (rdbtnEnv_Ext.isSelected()) {
								b.PWM_SOURCE = b.SOURCE_EXTERNAL;
							} else if (rdbtnuC_3.isSelected()) {
								b.PWM_SOURCE = b.SOURCE_uC1;
							} else if (rdbtnuC_4.isSelected()) {
								b.PWM_SOURCE = b.SOURCE_uC2;
							} else if(rdbtn_IrisExtWht.isSelected()) {
								b.PWM_SOURCE = b.SOURCE_EXT_WHITELIGHT;
							} else if(rdbtn_IrisExtEnv.isSelected()) {
								b.PWM_SOURCE = b.SOURCE_EXT_ENV;
							} else {

							}

							if (b.PWM_SOURCE == b.SOURCE_EXTERNAL) {
								b.PWM_TYPE = b.PWM_TYPE_FIXED;
								b.TYPE_VAL = 0x0;
							} else {
								int temp = 0;
								if (tglbtnFixed_Env.isSelected() == true) {

									b.PWM_TYPE = b.PWM_TYPE_FADE;
									temp = Integer.parseInt(formattedTextField_EnvPwm.getText());
									b.TYPE_VAL = (byte) temp;

								} else {

									b.PWM_TYPE = b.PWM_TYPE_FIXED;
									temp = Integer.parseInt(formattedTextField_EnvPwm.getText());
									b.TYPE_VAL = (byte) temp;
								}

							}

							print(Current_control);

							if (connect_flag == false && env_iris_critical_flag == false) {
								pause_flag = true;
								Current_control = ENV_IRIS_ON;
								ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
								Main_App_Worker my_worker = new Main_App_Worker(EnvIrisPowerButton, Current_control,
										IPCPacket, b.LIGHT_SOURCE, b.STATE, b.MODE, b.PWM_SOURCE, b.PWM_TYPE,
										b.TYPE_VAL, true, serialport, 0, b.PWM_FREQ);
								my_worker.execute();
								// construct_and_send_IPCPacket(Current_control,
								// IPCPacket,b.LIGHT_SOURCE,b.STATE,b.MODE,b.PWM_SOURCE,b.PWM_TYPE,b.TYPE_VAL);
							} else {
								// Current_control = ENV_IRIS_FAKE_ON;
								// ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
								// Main_App_Worker my_worker = new Main_App_Worker(EnvIrisPowerButton,
								// Current_control, IPCPacket, b.LIGHT_SOURCE, b.STATE, b.MODE, b.PWM_SOURCE,
								// b.PWM_TYPE, b.TYPE_VAL, true, serialport);
								// my_worker.execute();
								/*
								 * EnvIrisPowerButton.setEnabled(true);
								 * 
								 * print("Serial Port Flag False"); if(WlPowerButton.isSelected()) { Color en =
								 * Color.decode("#ccffff"); panel_33.setBackground(en); }
								 */

								/*
								 * if(SOURCE == ENV_SOURCE) { textArea.append("ENV Light Switched ON!!\r\n");
								 * chckbxEnv.setEnabled(false); chckbxIris.setEnabled(false); for(Component c :
								 * env_iris) { c.setEnabled(false); } for(Component c : env_source) {
								 * c.setEnabled(false);
								 * 
								 * } for(Component c: env_pwm_type) { c.setEnabled(false); }
								 * lblEnv.setEnabled(true); slider_19.setEnabled(true);
								 * label_24.setEnabled(true); chckbxIris1.setEnabled(false);
								 * chckbxIris2.setEnabled(false); chckbxIrisPowerShutdown_1.setEnabled(false);
								 * slider_20.setEnabled(false); label_25.setEnabled(false);
								 * slider_21.setEnabled(false); label_26.setEnabled(false);
								 * 
								 * }
								 */
								/*
								 * else if(SOURCE == IRIS_SOURCE) { chckbxEnv.setEnabled(false);
								 * chckbxIris.setEnabled(false); for(Component c : env_iris) {
								 * c.setEnabled(false); } for(Component c : env_source) { c.setEnabled(false);
								 * 
								 * } for(Component c: env_pwm_type) { c.setEnabled(false); }
								 * lblEnv.setEnabled(false); slider_19.setEnabled(false);
								 * label_24.setEnabled(false); iris_onoff_flag = true;
								 * chckbxIris1.setEnabled(true); chckbxIris1.setSelected(true);
								 * slider_20.setEnabled(true); label_25.setEnabled(true);
								 * chckbxIris2.setSelected(true); chckbxIris2.setEnabled(true);
								 * slider_21.setEnabled(true); label_26.setEnabled(true);
								 * chckbxIrisPowerShutdown_1.setEnabled(true); iris_onoff_flag = false;
								 * 
								 * textArea.append("IRIS Light Switched ON!!\r\n"); }
								 */

							}

						} else {
							ShowMessage("Please Choose a Valid Source!!");
							EnvIrisPowerButton.setSelected(false);
							// Current_control = ENV_IRIS_SEL_FALSE;
							// ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
							// Main_App_Worker my_worker = new Main_App_Worker(EnvIrisPowerButton,
							// Current_control, IPCPacket, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
							// dummy_byte, dummy_byte, false, serialport);
							// my_worker.execute();
						}
					}

				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					if (!Response_Animator.Animation.isShowing()) {
						pause_flag = true;
						if (rdbtnIndividual.isSelected() == true) {
							/// Current_control = INDIVIDUAL_CONTROL;
							// ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
							// Main_App_Worker my_worker = new Main_App_Worker(rdbtnIndividual,
							/// Current_control, IPCPacket, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
							/// dummy_byte, dummy_byte, false, serialport);
							// my_worker.execute();
							connect_flag = true;
							wl_onoff_flag = true;
							chckbxRed.setSelected(true);
							chckbxRed.setEnabled(true);
							slider_16.setEnabled(true);
							RedtextField.setEditable(true);
							RedtextField.setEnabled(true);
							slider_16.setValue(Stryk_Demo.slider_15.getValue());
							label_21.setEnabled(true);
							label_21.setText(Integer.toString(Stryk_Demo.slider_16.getValue()) + " %");
							connect_flag = false;
							wl_onoff_flag = false;

						}

						print("ENV IRIS Switch OFF State");
						Color en = Color.decode("#000000");
						panel_33.setBackground(en);
						if (SOURCE == ENV_SOURCE) {
							b.LIGHT_SOURCE = b.SRC_ENV;
						}
						if (SOURCE == IRIS_SOURCE) {
							b.LIGHT_SOURCE = b.SRC_IRIS;
						}
						b.STATE = b.OFF_STATE;

						if (connect_flag == false && env_iris_critical_flag == false) {
							Current_control = ENV_IRIS_OFF;
							ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
							Main_App_Worker my_worker = new Main_App_Worker(EnvIrisPowerButton, Current_control,
									IPCPacket, b.LIGHT_SOURCE, b.STATE, b.MODE, b.PWM_SOURCE, b.PWM_TYPE, b.TYPE_VAL,
									true, serialport, 0, b.PWM_FREQ);
							my_worker.execute();

							// construct_and_send_IPCPacket(Current_control,
							// IPCPacket,b.LIGHT_SOURCE,b.STATE,dummy_byte,dummy_byte,dummy_byte,dummy_byte);
						} else {
							// EnvIrisPowerButton.setSelected(false);
						}
						// EnvIrisPowerButton.setIcon(new
						// ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
					}

				}

			}
		});
		EnvIrisPowerButton.setEnabled(false);
		// EnvIrisPowerButton.setMultiClickThreshhold(500);
		EnvIrisPowerButton.setBounds(370, 44, 42, 42);
		panel_24.add(EnvIrisPowerButton);

		panel_32 = new JPanel();
		panel_32.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		panel_32.setBorder(
				new TitledBorder(null, "PWM TYPE", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
		((TitledBorder) panel_32.getBorder()).setTitleFont(new Font("Tahoma", Font.BOLD, 10));
		panel_32.setBounds(13, 240, 188, 55);
		panel_24.add(panel_32);

		panel_32.setLayout(null);

		this.tglbtnFixed_Env = new JToggleButton("FIXED");
		tglbtnFixed_Env.setEnabled(false);
		tglbtnFixed_Env.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {

					tglbtnFixed_Env.setText("FADE");
					lblNewLabel_1.setText("DURATION");

				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					tglbtnFixed_Env.setText("FIXED");
					lblNewLabel_1.setText("VALUE");
				}
			}
		});
		tglbtnFixed_Env.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		tglbtnFixed_Env.setBounds(12, 17, 87, 22);
		panel_32.add(tglbtnFixed_Env);

		this.formattedTextField_EnvPwm = new JFormattedTextField(formatter2);
		formattedTextField_EnvPwm.setEnabled(false);
		formattedTextField_EnvPwm.setBounds(120, 19, 54, 22);
		formattedTextField_EnvPwm.setValue(0);
		panel_32.add(formattedTextField_EnvPwm);

		this.lblNewLabel_1 = new JLabel("VALUE");
		lblNewLabel_1.setEnabled(false);
		lblNewLabel_1.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		lblNewLabel_1.setBounds(123, 35, 51, 16);
		panel_32.add(lblNewLabel_1);

		this.slider_19 = new JSlider();
		slider_19.setEnabled(false);
		slider_19.setMaximum(4095);
		slider_19.addChangeListener(new ChangeListener() {
			int slider_value = 0;
			bytedefinitions b = new bytedefinitions();

			public void stateChanged(ChangeEvent e) {
				if (slider_19.isEnabled()) {

					JSlider source = (JSlider) e.getSource();
					if (env_onoff_flag == false && connect_flag == false) {
						if (!source.getValueIsAdjusting()) {
							if (!Response_Animator.Animation.isShowing()) {
								pause_flag = true;
								System.out.println("Inside Laser Slider Control");
								byte value = (byte) slider_value;
								ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
								Main_App_Worker my_worker = new Main_App_Worker(slider_19, SLIDER, IPCPacket,
										b.INTENSITY_SLIDER_ENV, value, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
										false, serialport, slider_value, b.PWM_FREQ);
								my_worker.execute();
								/*
								 * // Determines whether the user gesture to move the slider's knob is complete
								 * print(Integer.toString(slider_value)); byte value = (byte) slider_value;
								 * expected_message_type = b.EXPECT_NO_RESPONSE; expected_cmd_id =
								 * b.EXPECT_NO_RESPONSE; Current_control = ENV_SLIDER; ArrayList<Byte> IPCPacket
								 * = new ArrayList<Byte>(); construct_and_send_IPCPacket(Current_control,
								 * IPCPacket,b.INTENSITY_SLIDER_ENV,value,dummy_byte,dummy_byte,dummy_byte,
								 * dummy_byte);
								 */

							}

						} else {
							slider_value = slider_19.getValue();
							String slider_2 = Integer.toString(slider_value);
							LasertextField.setText(slider_2);
							String local = Integer.toString((slider_value * 100)/Max_Val);
							label_24.setText(local + " %");
						}
					}
					else
					{
						slider_value = slider_19.getValue();
						LasertextField.setText(Integer.toString(slider_value));
						String local = Integer.toString((slider_value * 100)/Max_Val);
						label_24.setText(local + " %");
					}

				}
			}
		});
		slider_19.setValue(0);
		slider_19.setBounds(250, 98, 255, 21);
		panel_24.add(slider_19);

		this.lblEnv = new JLabel("ENV");
		lblEnv.setEnabled(false);
		lblEnv.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		lblEnv.setBounds(220, 101, 30, 16);
		panel_24.add(lblEnv);

		this.label_24 = new JLabel("0 %");
		label_24.setEnabled(false);
		label_24.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		label_24.setBounds(570, 98, 33, 16);
		panel_24.add(label_24);

		this.chckbxIris1 = new JCheckBox("IRIS-1");
		chckbxIris1.setEnabled(false);
		chckbxIris1.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				if (iris_onoff_flag == false && connect_flag == false) {
					bytedefinitions b = new bytedefinitions();
					if (e.getStateChange() == ItemEvent.SELECTED) {
						if (!Response_Animator.Animation.isShowing()) {
							pause_flag = true;
							byte value = (byte) 0x1;
							ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
							Main_App_Worker my_worker = new Main_App_Worker(chckbxIris1, ENABLE_DISABLE, IPCPacket,
									b.INTENSITY_SLIDER_IRIS1, value, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
									false, serialport, 0, b.PWM_FREQ);
							my_worker.execute();
						}

					}
					if (e.getStateChange() == ItemEvent.DESELECTED) {
						if (!Response_Animator.Animation.isShowing()) {
							pause_flag = true;
							byte value = (byte) 0x0;
							ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
							Main_App_Worker my_worker = new Main_App_Worker(chckbxIris1, ENABLE_DISABLE, IPCPacket,
									b.INTENSITY_SLIDER_IRIS1, value, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
									false, serialport, 0, b.PWM_FREQ);
							my_worker.execute();
						}

					}
					/*
					 * expected_message_type = b.EXPECT_NO_RESPONSE; expected_cmd_id =
					 * b.EXPECT_NO_RESPONSE;
					 * 
					 * ArrayList<Byte> IPCPacket = new ArrayList<Byte>(); if(e.getStateChange() ==
					 * ItemEvent.SELECTED) { Current_control = IRIS_1_EN;
					 * construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,b.INTENSITY_SLIDER_IRIS1,(byte)
					 * 0x1,dummy_byte,dummy_byte,dummy_byte,dummy_byte); } if(e.getStateChange() ==
					 * ItemEvent.DESELECTED) { Current_control = IRIS_1_DIS;
					 * construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,b.INTENSITY_SLIDER_IRIS1,(byte)
					 * 0x0,dummy_byte,dummy_byte,dummy_byte,dummy_byte); }
					 */

				}

			}
		});
		chckbxIris1.setBounds(200, 161, 48, 18);
		panel_24.add(chckbxIris1);
		chckbxIris1.setFont(new Font("Calibri Light", Font.PLAIN, 11));

		this.slider_20 = new JSlider();
		slider_20.setEnabled(false);
		slider_20.setMaximum(255);
		slider_20.addChangeListener(new ChangeListener() {
			int slider_value = 0;
			bytedefinitions b = new bytedefinitions();

			public void stateChanged(ChangeEvent e) {
				if (slider_20.isEnabled()) {

					JSlider source = (JSlider) e.getSource();
					if (iris_onoff_flag == false && connect_flag == false) {
						if (!source.getValueIsAdjusting()) {
							if (!Response_Animator.Animation.isShowing()) {
								pause_flag = true;
								System.out.println("Inside IRIS1 Slider Control");
								byte value = (byte) slider_value;
								ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
								Main_App_Worker my_worker = new Main_App_Worker(slider_20, SLIDER, IPCPacket,
										b.INTENSITY_SLIDER_IRIS1, value, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
										false, serialport, slider_value, b.PWM_FREQ);
								my_worker.execute();
								/*
								 * // Determines whether the user gesture to move the slider's knob is complete
								 * print(Integer.toString(slider_value)); byte value = (byte) slider_value;
								 * expected_message_type = b.EXPECT_NO_RESPONSE; expected_cmd_id =
								 * b.EXPECT_NO_RESPONSE; Current_control = IRIS1_SLIDER; ArrayList<Byte>
								 * IPCPacket = new ArrayList<Byte>();
								 * construct_and_send_IPCPacket(Current_control,
								 * IPCPacket,b.INTENSITY_SLIDER_IRIS1,value,dummy_byte,dummy_byte,dummy_byte,
								 * dummy_byte);
								 */
							}

						} else {
							slider_value = slider_20.getValue();
							String slider_2 = Integer.toString(slider_value);
							// label_25.setText((slider_2)+" %");
							Iris1TextField.setText(slider_2);
							String local = Integer.toString((slider_value * 100)/Max_Val_8Bit);
							label_25.setText(local + " %");
						}
					}
					else
					{
						slider_value = slider_20.getValue();
						Iris1TextField.setText(Integer.toString(slider_value));
						String local = Integer.toString((slider_value * 100)/Max_Val_8Bit);
						label_25.setText(local + " %");
					}

				}
			}
		});
		slider_20.setBounds(250, 159, 255, 21);
		panel_24.add(slider_20);
		slider_20.setValue(0);

		this.label_25 = new JLabel("0 %");
		label_25.setEnabled(false);
		label_25.setBounds(570, 162, 33, 16);
		panel_24.add(label_25);
		label_25.setFont(new Font("Calibri Light", Font.PLAIN, 11));

		this.label_26 = new JLabel("0 %");
		label_26.setEnabled(false);
		label_26.setBounds(570, 190, 33, 16);
		panel_24.add(label_26);
		label_26.setFont(new Font("Calibri Light", Font.PLAIN, 11));

		this.slider_21 = new JSlider();
		slider_21.setEnabled(false);
		slider_21.setMaximum(255);
		slider_21.addChangeListener(new ChangeListener() {
			int slider_value = 0;
			bytedefinitions b = new bytedefinitions();

			public void stateChanged(ChangeEvent e) {
				if (slider_21.isEnabled()) {

					JSlider source = (JSlider) e.getSource();
					if (iris_onoff_flag == false && connect_flag == false) {
						if (!source.getValueIsAdjusting()) {
							if (!Response_Animator.Animation.isShowing()) {
								pause_flag = true;
								System.out.println("Inside IRIS2 Slider Control");
								byte value = (byte) slider_value;
								ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
								Main_App_Worker my_worker = new Main_App_Worker(slider_21, SLIDER, IPCPacket,
										b.INTENSITY_SLIDER_IRIS2, value, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
										false, serialport, slider_value, b.PWM_FREQ);
								my_worker.execute();
								/*
								 * 
								 * // Determines whether the user gesture to move the slider's knob is complete
								 * print(Integer.toString(slider_value)); byte value = (byte) slider_value;
								 * expected_message_type = b.EXPECT_NO_RESPONSE; expected_cmd_id =
								 * b.EXPECT_NO_RESPONSE; Current_control = IRIS2_SLIDER; ArrayList<Byte>
								 * IPCPacket = new ArrayList<Byte>();
								 * construct_and_send_IPCPacket(Current_control,
								 * IPCPacket,b.INTENSITY_SLIDER_IRIS2,value,dummy_byte,dummy_byte,dummy_byte,
								 * dummy_byte);
								 */

							}

						} else {
							slider_value = slider_21.getValue();
							String slider_2 = Integer.toString(slider_value);
							Iris2TextField.setText(slider_2);
							String local = Integer.toString((slider_value * 100)/Max_Val_8Bit);
							
							// label_26.setText((slider_2)+" %");
							label_26.setText(local + " %");
						}
					}
					else
					{
						slider_value = slider_21.getValue();
						Iris2TextField.setText(Integer.toString(slider_value));
						String local = Integer.toString((slider_value * 100)/Max_Val_8Bit);
						label_26.setText(local + " %");
					}

				}
			}
		});
		slider_21.setBounds(250, 189, 255, 21);
		panel_24.add(slider_21);
		slider_21.setValue(0);

		this.chckbxIris2 = new JCheckBox("IRIS-2");
		chckbxIris2.setEnabled(false);
		chckbxIris2.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				if (iris_onoff_flag == false && connect_flag == false) {
					bytedefinitions b = new bytedefinitions();
					if (e.getStateChange() == ItemEvent.SELECTED) {
						if (!Response_Animator.Animation.isShowing()) {
							pause_flag = true;
							byte value = (byte) 0x1;
							ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
							Main_App_Worker my_worker = new Main_App_Worker(chckbxIris2, ENABLE_DISABLE, IPCPacket,
									b.INTENSITY_SLIDER_IRIS2, value, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
									false, serialport, 0, b.PWM_FREQ);
							my_worker.execute();
						}

					}
					if (e.getStateChange() == ItemEvent.DESELECTED) {
						if (!Response_Animator.Animation.isShowing()) {
							pause_flag = true;
							byte value = (byte) 0x0;
							ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
							Main_App_Worker my_worker = new Main_App_Worker(chckbxIris2, ENABLE_DISABLE, IPCPacket,
									b.INTENSITY_SLIDER_IRIS2, value, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
									false, serialport, 0, b.PWM_FREQ);
							my_worker.execute();
						}

					}
					/*
					 * bytedefinitions b = new bytedefinitions(); expected_message_type =
					 * b.EXPECT_NO_RESPONSE; expected_cmd_id = b.EXPECT_NO_RESPONSE;
					 * 
					 * ArrayList<Byte> IPCPacket = new ArrayList<Byte>(); if(e.getStateChange() ==
					 * ItemEvent.SELECTED) { Current_control = IRIS_2_EN;
					 * construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,b.INTENSITY_SLIDER_IRIS2,(byte)
					 * 0x1,dummy_byte,dummy_byte,dummy_byte,dummy_byte); } if(e.getStateChange() ==
					 * ItemEvent.DESELECTED) { Current_control = IRIS_2_DIS;
					 * construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,b.INTENSITY_SLIDER_IRIS2,(byte)
					 * 0x0,dummy_byte,dummy_byte,dummy_byte,dummy_byte); }
					 */
				}

			}
		});
		chckbxIris2.setBounds(200, 192, 48, 18);
		panel_24.add(chckbxIris2);
		chckbxIris2.setFont(new Font("Calibri Light", Font.PLAIN, 11));

		this.chckbxEnv = new JCheckBox("ENV");
		chckbxEnv.setEnabled(false);
		chckbxEnv.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					if ((rdbtnPwm.isSelected() == false) && (rdbtnCwm.isSelected() == false)
							&& (rdbtnStb.isSelected() == false)) {
						rdbtnEnv_LcNr.setEnabled(true);
						lblLcnr.setEnabled(true);
					} else {
						rdbtnEnv_LcNr.setEnabled(false);
						lblLcnr.setEnabled(false);
					}
					SOURCE = ENV_SOURCE;
					lblPwm_8.setEnabled(true);
					rdbtnEnv_Pwm.setEnabled(true);
					lblCwm_1.setEnabled(true);
					rdbtnEnv_Cwm.setEnabled(true);
					lblCwmnr.setEnabled(true);
					rdbtnEnv_CwmNr.setEnabled(true);

					if ((env_iris_mode.getSelection() != null) && (env_iris_source.getSelection() != null)) {
						EnvIrisPowerButton.setEnabled(true);
					} else {
						EnvIrisPowerButton.setEnabled(false);
					}

				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					lblPwm_8.setEnabled(false);
					rdbtnEnv_Pwm.setEnabled(false);
					lblCwm_1.setEnabled(false);
					rdbtnEnv_Cwm.setEnabled(false);
					lblCwmnr.setEnabled(false);
					rdbtnEnv_CwmNr.setEnabled(false);
					lblLcnr.setEnabled(false);
					rdbtnEnv_LcNr.setEnabled(false);
					rdbtnEnv_Ext.setEnabled(false);
					rdbtnuC_3.setEnabled(false);
					rdbtnuC_4.setEnabled(false);
					rdbtn_IrisExtWht.setEnabled(false);
					rdbtn_IrisExtEnv.setEnabled(false);
					env_iris_mode.clearSelection();
					env_iris_source.clearSelection();

				}
			}
		});
		chckbxEnv.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxEnv.setBounds(50, 29, 61, 18);
		env_iris_chckbx.add(chckbxEnv);
		panel_24.add(chckbxEnv);

		this.chckbxIris = new JCheckBox("IRIS");
		chckbxIris.setEnabled(false);
		chckbxIris.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					SOURCE = IRIS_SOURCE;
					lblPwm_8.setEnabled(true);
					rdbtnEnv_Pwm.setEnabled(true);
					lblCwm_1.setEnabled(true);
					rdbtnEnv_Cwm.setEnabled(true);
					rdbtnEnv_LcNr.setEnabled(false);
					lblLcnr.setEnabled(false);
					if ((env_iris_mode.getSelection() != null) && (env_iris_source.getSelection() != null)) {
						print("IRIS Enabled");

						EnvIrisPowerButton.setEnabled(true);
					} else {
						EnvIrisPowerButton.setEnabled(false);
					}

				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					lblPwm_8.setEnabled(false);
					rdbtnEnv_Pwm.setEnabled(false);
					lblCwm_1.setEnabled(false);
					rdbtnEnv_Cwm.setEnabled(false);
					rdbtnEnv_Ext.setEnabled(false);
					rdbtnuC_3.setEnabled(false);
					rdbtnuC_4.setEnabled(false);
					rdbtn_IrisExtWht.setEnabled(false);
					rdbtn_IrisExtEnv.setEnabled(false);

					env_iris_mode.clearSelection();
					env_iris_source.clearSelection();
					if (env_iris_mode.getSelection() == rdbtnEnv_Pwm) {
						if (env_iris_source.getSelection() == rdbtnEnv_Ext) {
							rdbtnEnv_Ext.setEnabled(true);

							EnvIrisPowerButton.setEnabled(true);

						} else if (env_iris_source.getSelection() == rdbtnuC_3) {
							EnvIrisPowerButton.setEnabled(true);
						} else if (env_iris_source.getSelection() == rdbtnuC_4) {
							EnvIrisPowerButton.setEnabled(true);
						} else {
							EnvIrisPowerButton.setEnabled(false);
						}
					} else if (env_iris_mode.getSelection() == rdbtnEnv_Cwm) {
						if (env_iris_source.getSelection() == rdbtnuC_3) {
							EnvIrisPowerButton.setEnabled(true);
						} else if (env_iris_source.getSelection() == rdbtnuC_4) {
							EnvIrisPowerButton.setEnabled(true);
						} else {
							EnvIrisPowerButton.setEnabled(false);
						}
					}

				}
			}
		});
		chckbxIris.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxIris.setBounds(120, 29, 55, 18);
		env_iris_chckbx.add(chckbxIris);
		panel_24.add(chckbxIris);

		this.chckbxIrisPowerShutdown_1 = new JCheckBox("IRIS POWER SHUTDOWN");
		chckbxIrisPowerShutdown_1.setEnabled(false);
		chckbxIrisPowerShutdown_1.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				if (iris_onoff_flag == false && connect_flag == false) {

					bytedefinitions b = new bytedefinitions();
					if (e.getStateChange() == ItemEvent.SELECTED) {
						if (!Response_Animator.Animation.isShowing()) {
							pause_flag = true;
							byte value = (byte) 0x0;
							ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
							Main_App_Worker my_worker = new Main_App_Worker(chckbxIrisPowerShutdown,
									IRIS_POWER_SHUTDOWN, IPCPacket, b.IRIS_POWER_SHUTDOWN_EN, value, dummy_byte,
									dummy_byte, dummy_byte, dummy_byte, false, serialport, 0, b.PWM_FREQ);
							my_worker.execute();
						}

					} else if (e.getStateChange() == ItemEvent.DESELECTED) {
						if (!Response_Animator.Animation.isShowing()) {
							pause_flag = true;
							byte value = (byte) 0x1;
							ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
							Main_App_Worker my_worker = new Main_App_Worker(chckbxIrisPowerShutdown,
									IRIS_POWER_SHUTDOWN, IPCPacket, b.IRIS_POWER_SHUTDOWN_EN, value, dummy_byte,
									dummy_byte, dummy_byte, dummy_byte, false, serialport, 0, b.PWM_FREQ);
							my_worker.execute();
						}

					}

					/*
					 * expected_message_type = b.EXPECT_NO_RESPONSE; expected_cmd_id =
					 * b.EXPECT_NO_RESPONSE;
					 * 
					 * ArrayList<Byte> IPCPacket = new ArrayList<Byte>(); if(e.getStateChange() ==
					 * ItemEvent.SELECTED) { chckbxIris1.setEnabled(false);
					 * chckbxIris2.setEnabled(false); slider_20.setEnabled(false);
					 * slider_21.setEnabled(false); label_25.setEnabled(false);
					 * label_26.setEnabled(false); Current_control = IRIS_POWER_SHUTDOWN;
					 * construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,b.IRIS_POWER_SHUTDOWN_EN,(byte)
					 * 0x0,dummy_byte,dummy_byte,dummy_byte,dummy_byte);
					 * 
					 * } if(e.getStateChange() == ItemEvent.DESELECTED) {
					 * chckbxIris1.setEnabled(true); chckbxIris2.setEnabled(true);
					 * slider_20.setEnabled(true); slider_21.setEnabled(true);
					 * label_25.setEnabled(true); label_26.setEnabled(true); Current_control =
					 * IRIS_POWER_SHUTDOWN; construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,b.IRIS_POWER_SHUTDOWN_EN,(byte)
					 * 0x1,dummy_byte,dummy_byte,dummy_byte,dummy_byte); }
					 * 
					 * }
					 */
				}
			}
		});
		chckbxIrisPowerShutdown_1.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxIrisPowerShutdown_1.setBounds(200, 227, 152, 18);
		panel_24.add(chckbxIrisPowerShutdown_1);

		this.panel_33 = new JPanel();
		panel_33.setBounds(200, 257, 180, 7);
		panel_24.add(panel_33);
		Color cb = Color.decode("#000000");
		panel_33.setBackground(cb);
		
		this.LasertextField = new JFormattedTextField(formattertf);
		LasertextField.setEditable(false);
		LasertextField.setEnabled(false);
		LasertextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				int val = 0;
				String val1 = LasertextField.getText();
				byte value = (byte) Integer.parseInt(val1);
				val = SignedtoUnsigned.printByte(value);
				// JOptionPane.showMessageDialog(frmStrykerVer, val, "Value Entered",
				// JOptionPane.ERROR_MESSAGE);
				bytedefinitions b = new bytedefinitions();
				if (!Response_Animator.Animation.isShowing()) {
					pause_flag = true;
					System.out.println("LaserLight Absolute Control!!");

					ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
					Main_App_Worker my_worker = new Main_App_Worker(slider_19, SLIDER, IPCPacket,
							b.INTENSITY_SLIDER_ENV, (byte) val, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
							false, serialport, Integer.parseInt(val1), b.PWM_FREQ);
					my_worker.execute();
					connect_flag = true;
					slider_19.setValue(Integer.parseInt(val1));
					//label_20.setText(Integer.toString(val));
					connect_flag = false;
					// Determines whether the user gesture to move the slider's knob is complete
					/*
					 * print(Integer.toString(slider_value)); byte value = (byte) slider_value;
					 * expected_message_type = b.MSG_TYPE_NOTIFY; expected_cmd_id =
					 * b.EXPECT_NO_RESPONSE; Current_control = RED_SLIDER; ArrayList<Byte> IPCPacket
					 * = new ArrayList<Byte>(); construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,b.INTENSITY_SLIDER_RED,value,dummy_byte,dummy_byte,dummy_byte,
					 * dummy_byte);
					 */

				}
			}
		}
		});
		LasertextField.setColumns(10);
		LasertextField.setBounds(516, 95, 42, 23);
		panel_24.add(LasertextField);
		
		this.Iris1TextField = new JFormattedTextField(formattertf1);
		Iris1TextField.setEditable(false);
		Iris1TextField.setEnabled(false);
		Iris1TextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				int val = 0;
				String val1 = Iris1TextField.getText();
				byte value = (byte) Integer.parseInt(val1);
				val = SignedtoUnsigned.printByte(value);
				// JOptionPane.showMessageDialog(frmStrykerVer, val, "Value Entered",
				// JOptionPane.ERROR_MESSAGE);
				bytedefinitions b = new bytedefinitions();
				if (!Response_Animator.Animation.isShowing()) {
					pause_flag = true;
					System.out.println("Iris1 Absolute Control!!");

					ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
					Main_App_Worker my_worker = new Main_App_Worker(slider_20, SLIDER, IPCPacket,
							b.INTENSITY_SLIDER_IRIS1, (byte) val, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
							false, serialport, Integer.parseInt(val1), b.PWM_FREQ);
					my_worker.execute();
					connect_flag = true;
					slider_20.setValue(val);
					//label_20.setText(Integer.toString(val));
					connect_flag = false;
					// Determines whether the user gesture to move the slider's knob is complete
					/*
					 * print(Integer.toString(slider_value)); byte value = (byte) slider_value;
					 * expected_message_type = b.MSG_TYPE_NOTIFY; expected_cmd_id =
					 * b.EXPECT_NO_RESPONSE; Current_control = RED_SLIDER; ArrayList<Byte> IPCPacket
					 * = new ArrayList<Byte>(); construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,b.INTENSITY_SLIDER_RED,value,dummy_byte,dummy_byte,dummy_byte,
					 * dummy_byte);
					 */

				}
			}
			}
		});
		Iris1TextField.setColumns(10);
		Iris1TextField.setBounds(516, 156, 42, 23);
		panel_24.add(Iris1TextField);
		
		this.Iris2TextField = new JFormattedTextField(formattertf1);
		Iris2TextField.setEnabled(false);
		Iris2TextField.setEditable(false);
		Iris2TextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				int val = 0;
				String val1 = Iris2TextField.getText();
				byte value = (byte) Integer.parseInt(val1);
				val = SignedtoUnsigned.printByte(value);
				// JOptionPane.showMessageDialog(frmStrykerVer, val, "Value Entered",
				// JOptionPane.ERROR_MESSAGE);
				bytedefinitions b = new bytedefinitions();
				if (!Response_Animator.Animation.isShowing()) {
					pause_flag = true;
					System.out.println("RedLight Direct Control!!");

					ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
					Main_App_Worker my_worker = new Main_App_Worker(slider_21, SLIDER, IPCPacket,
							b.INTENSITY_SLIDER_IRIS2, (byte) val, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
							false, serialport, Integer.parseInt(val1), b.PWM_FREQ);
					my_worker.execute();
					connect_flag = true;
					slider_21.setValue(val);
					//label_20.setText(Integer.toString(val));
					connect_flag = false;
					// Determines whether the user gesture to move the slider's knob is complete
					/*
					 * print(Integer.toString(slider_value)); byte value = (byte) slider_value;
					 * expected_message_type = b.MSG_TYPE_NOTIFY; expected_cmd_id =
					 * b.EXPECT_NO_RESPONSE; Current_control = RED_SLIDER; ArrayList<Byte> IPCPacket
					 * = new ArrayList<Byte>(); construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,b.INTENSITY_SLIDER_RED,value,dummy_byte,dummy_byte,dummy_byte,
					 * dummy_byte);
					 */

				}
			}
			}
		});
		Iris2TextField.setColumns(10);
		Iris2TextField.setBounds(517, 187, 42, 23);
		panel_24.add(Iris2TextField);

		panel_36 = new JPanel();
		panel_36.setBorder(
				new TitledBorder(null, "", TitledBorder.LEFT, TitledBorder.TOP, null, new Color(51, 51, 51)));
		panel_36.setBackground((Color) null);
		panel_36.setBounds(3, 536, 238, 62);
		frmStrykerVer.getContentPane().add(panel_36);
		panel_36.setLayout(null);

		this.btnSave = new JButton("SAVE");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!Response_Animator.Animation.isShowing()) {
					bytedefinitions b = new bytedefinitions();
					pause_flag = true;
					ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
					Main_App_Worker my_worker = new Main_App_Worker(btnSave, SAVE, IPCPacket, dummy_byte, dummy_byte,
							dummy_byte, dummy_byte, dummy_byte, dummy_byte, true, serialport, 0, b.PWM_FREQ);
					my_worker.execute();

					/*
					 * bytedefinitions b = new bytedefinitions(); expected_message_type =
					 * b.MSG_TYPE_RESP; expected_cmd_id = b.SAVE_COMMAND; Current_control = SAVE;
					 * ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
					 * construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,dummy_byte,dummy_byte,dummy_byte,dummy_byte,dummy_byte,dummy_byte);
					 */
				}

			}
		});
		btnSave.setEnabled(false);
		btnSave.setFont(new Font("Calibri Light", Font.BOLD, 11));
		btnSave.setBounds(12, 17, 100, 22);
		btnSave.setMultiClickThreshhold(1000L);
		panel_36.add(btnSave);

		this.btnLastMode = new JButton("LAST MODE");
		btnLastMode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				bytedefinitions b = new bytedefinitions();
				if (!Response_Animator.Animation.isShowing()) {
					pause_flag = true;
					ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
					Main_App_Worker my_worker = new Main_App_Worker(btnLastMode, LAST, IPCPacket, dummy_byte,
							dummy_byte, dummy_byte, dummy_byte, dummy_byte, dummy_byte, true, serialport, 0, b.PWM_FREQ);
					my_worker.execute();

					/*
					 * bytedefinitions b = new bytedefinitions(); expected_message_type =
					 * b.MSG_TYPE_RESP; expected_cmd_id = b.RESTORE_CMD; Current_control =
					 * LAST_MODE; ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
					 * construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,dummy_byte,dummy_byte,dummy_byte,dummy_byte,dummy_byte,dummy_byte);
					 */
				}

			}
		});
		btnLastMode.setEnabled(false);
		btnLastMode.setMultiClickThreshhold(1000L);
		btnLastMode.setFont(new Font("Calibri Light", Font.BOLD, 11));
		btnLastMode.setBounds(124, 17, 100, 22);
		panel_36.add(btnLastMode);

		this.panel_25 = new JPanel();
		panel_25.setBounds(244, 536, 609, 62);
		frmStrykerVer.getContentPane().add(panel_25);
		panel_25.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		panel_25.setBorder(new TitledBorder(
				new TitledBorder(null, "", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)), "",
				TitledBorder.CENTER, TitledBorder.TOP, null, new Color(51, 51, 51)));
		((TitledBorder) panel_25.getBorder()).setTitleFont(new Font("Tahoma", Font.BOLD, 10));
		panel_25.setLayout(null);

		this.btnManufacturingTests = new JButton("MANUFACTURING TESTS");
		btnManufacturingTests.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SwingWorker MFTest_worker = new SwingWorker<List<String>, String>() {

					@Override
					protected List<String> doInBackground() throws Exception {
						try {

							mftserialPort = portlist[comboBox.getSelectedIndex()];
							if (mftserialPort != null) {
								// mftserialPort = serialport;
								ManufacturingTests.MFTest_Flag = true;
								publish("SHOW");
								// Stryk_Demo.send_data_in_bg("ManufacturingTest",
								// Stryk_Demo.btnManufacturingTests, true, Manufacturing_Test.getBytes());
							} else {
								publish("Not a valid serial port - ManufacturingTests\r\n");
							}

						} catch (java.lang.ArrayIndexOutOfBoundsException ex) {
							textArea.append("Not a Valid COM Port!!.\r\n Could not Open ManufacuringTests\r\n\n");
						} catch (Exception ex) {
							ex.printStackTrace();
						}
						// TODO Auto-generated method stub
						return null;
					}

					protected void process(List<String> chunks) {
						for (String str : chunks) {
							if (str.equals("SHOW")) {
								frmStrykerVer.setVisible(false);
								ManufacturingTests obs = new ManufacturingTests();
							} else {
								textArea.append(str);
							}
						}
					}

				};
				MFTest_worker.execute();

				// print("Manufacturing Tests Button Pressed");
				// frmStrykerVer.setVisible(false);
				// ManufacturingTests mft = new ManufacturingTests();
				// mft.frmStrykerManufacturingTests.setVisible(true);

			}
		});
		btnManufacturingTests.setFont(new Font("Calibri Light", Font.BOLD, 11));
		btnManufacturingTests.setBounds(26, 17, 160, 22);
		panel_25.add(btnManufacturingTests);

		tglbtnFuncTest = new JToggleButton("FUNC TEST");
		tglbtnFuncTest.setBounds(209, 17, 160, 22);
		panel_25.add(tglbtnFuncTest);
		tglbtnFuncTest.setEnabled(false);
		tglbtnFuncTest.setFont(new Font("Calibri Light", Font.BOLD, 11));

		this.btnReadAll = new JButton("READ ALL");
		btnReadAll.setBounds(398, 17, 160, 22);
		panel_25.add(btnReadAll);
		btnReadAll.setEnabled(false);
		btnReadAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pause_flag = true;
				bytedefinitions b = new bytedefinitions();
				if (!Response_Animator.Animation.isShowing()) {

					ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
					Main_App_Worker my_worker = new Main_App_Worker(btnReadAll, READ_ALL, IPCPacket, dummy_byte,
							dummy_byte, dummy_byte, dummy_byte, dummy_byte, dummy_byte, true, serialport, 0, b.PWM_FREQ);
					my_worker.execute();
					/*
					 * bytedefinitions b = new bytedefinitions(); expected_message_type =
					 * b.MSG_TYPE_RESP; expected_cmd_id = b.SENSING_COMMAND; Current_control =
					 * READ_ALL; ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
					 * construct_and_send_IPCPacket(Current_control,
					 * IPCPacket,dummy_byte,dummy_byte,dummy_byte,dummy_byte,dummy_byte,dummy_byte);
					 */
				}

			}
		});
		btnReadAll.setFont(new Font("Calibri Light", Font.BOLD, 11));
		btnReadAll.setMultiClickThreshhold(1000L);

		rdbtnClear.addItemListener(new ItemListener() {
			private Timer timer;

			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					textArea.setText("");

					rdbtnClear.setEnabled(false);

					ActionListener taskPerformer = new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							// ...Perform a task...
							rdbtnClear.setSelected(false);
							rdbtnClear.setEnabled(true);

							timer.stop();
						}
					};
					this.timer = new Timer(100, taskPerformer);
					timer.setRepeats(false);
					timer.start();

				}
			}
		});
		wl_mode = panel_26.getComponents();
		wl_source = panel_27.getComponents();
		wl_pwm_type = panel_28.getComponents();
		env_iris = panel_30.getComponents();
		env_source = panel_31.getComponents();
		env_pwm_type = panel_32.getComponents();

		wlmode.add(rdbtnPwm);
		wlmode.add(rdbtnCwm);
		wlmode.add(rdbtnLc);
		wlmode.add(rdbtnStb);

		GrptextField = new JFormattedTextField(formattertf);
		GrptextField.setEnabled(false);
		GrptextField.setEditable(false);
		GrptextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					int val = 0;
					String val1 = GrptextField.getText().toString();
					System.out.println("#######################Values to send is : " + val1);
					byte value = (byte) Integer.parseInt(val1);
					val = SignedtoUnsigned.printByte(value);
					// JOptionPane.showMessageDialog(frmStrykerVer, val, "Value Entered",
					// JOptionPane.ERROR_MESSAGE);
					bytedefinitions b = new bytedefinitions();
					if (!Response_Animator.Animation.isShowing()) {
						pause_flag = true;
						System.out.println("WhiteLight Absolute Control!!");

						ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
						Main_App_Worker my_worker = new Main_App_Worker(slider_15, SLIDER, IPCPacket,
								b.INTENSITY_SLIDER_WHITE, (byte) val, dummy_byte, dummy_byte, dummy_byte, dummy_byte,
								false, serialport, Integer.parseInt(val1), b.PWM_FREQ);
						my_worker.execute();
						connect_flag = true;
						slider_15.setValue(Integer.parseInt(val1));
						//label_20.setText(Integer.toString(val));
						connect_flag = false;
						// Determines whether the user gesture to move the slider's knob is complete
						/*
						 * print(Integer.toString(slider_value)); byte value = (byte) slider_value;
						 * expected_message_type = b.MSG_TYPE_NOTIFY; expected_cmd_id =
						 * b.EXPECT_NO_RESPONSE; Current_control = RED_SLIDER; ArrayList<Byte> IPCPacket
						 * = new ArrayList<Byte>(); construct_and_send_IPCPacket(Current_control,
						 * IPCPacket,b.INTENSITY_SLIDER_RED,value,dummy_byte,dummy_byte,dummy_byte,
						 * dummy_byte);
						 */

					}
					// something like...
					// mTextField.getText();
					// or...
					// mButton.doClick();
				}
			}
		});
		GrptextField.setBounds(517, 60, 42, 23);
		panel_23.add(GrptextField);
		GrptextField.setColumns(10);
		this.label_20 = new JLabel("0 %");
		label_20.setBounds(564, 63, 33, 16);
		label_20.setEnabled(false);
		label_20.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		panel_23.add(label_20);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBorder(new TitledBorder(null, "Frequency", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(59, 59, 59)));
		panel.setBounds(197, 13, 153, 55);
		panel_23.add(panel);
		panel.setLayout(null);
		
		rdbtn120hz = new JRadioButton("60Hz");
		rdbtn120hz.setEnabled(false);
		rdbtn120hz.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		rdbtn120hz.setBounds(20, 18, 52, 18);
		rdbtn120hz.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				if (e.getStateChange() == ItemEvent.SELECTED) {
					rdbtnExt.setEnabled(true);
					rdbtnuC_1.setEnabled(true);
					rdbtnuC_2.setEnabled(true);
					tglbtnFixed_Wl.setText("FIXED");
					formattedTextField_WlPwm.setEnabled(true);
					lblFixedValue_2.setEnabled(true);
				} else {
					rdbtnExt.setEnabled(false);
					rdbtnuC_1.setEnabled(true);
					rdbtnuC_2.setEnabled(false);
					formattedTextField_WlPwm.setEnabled(true);
					lblFixedValue_2.setEnabled(true);
				}
			}
		});
		panel.add(rdbtn120hz);
		
		rdbtn70khz = new JRadioButton("100Khz");
		rdbtn70khz.setEnabled(false);
		rdbtn70khz.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		rdbtn70khz.setBounds(84, 18, 52, 18);
		rdbtn70khz.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				if (e.getStateChange() == ItemEvent.SELECTED) {
					rdbtnExt.setEnabled(false);
					rdbtnuC_1.setEnabled(true);
					rdbtnuC_2.setEnabled(false);
					tglbtnFixed_Wl.setText("FIXED");
					lblFixedValue_2.setText("VALUE");
				} else {
					rdbtnExt.setEnabled(true);
					rdbtnuC_1.setEnabled(true);
					rdbtnuC_2.setEnabled(true);
				}
			}
		});
		panel.add(rdbtn70khz);
		
		wlpwmfrequency.add(rdbtn70khz);
		wlpwmfrequency.add(rdbtn120hz);

		this.Live_Update_Panel = new JPanel();
		Live_Update_Panel.setLayout(null);
		Live_Update_Panel.setBorder(new TitledBorder(
				new TitledBorder(null, "LIVE", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)), "",
				TitledBorder.CENTER, TitledBorder.TOP, null, new Color(51, 51, 51)));
		Live_Update_Panel.setBackground((Color) null);
		Live_Update_Panel.setBounds(1086, 11, 269, 168);
		frmStrykerVer.getContentPane().add(Live_Update_Panel);
		this.Live_Updates = new JToggleButton("START");
		Live_Updates.setEnabled(false);
		Live_Updates.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				boolean button_state = false;
				byte ON = 0x1;
				byte OFF = 0x0;
				bytedefinitions b = new bytedefinitions();
				byte value = 0;

				if (e.getStateChange() == ItemEvent.SELECTED) {
					if (!button_state) {
						if (rdbtnWhitelight.isSelected()) {
							value = (byte) 0x1;
						} else if (rdbtnLaserlight.isSelected()) {
							value = (byte) 0x2;
						}
						if ((rdbtnWhitelight.isSelected()) || (rdbtnLaserlight.isSelected())) {
							timer_textField.setEnabled(true);
							Live_Updates.setText("STOP");
							// Call Initialize on timer and start the timer
							System.out.println("Value : " + value);
							initialize_timer(value, ON);
							rdbtnLaserlight.setEnabled(false);
							rdbtnWhitelight.setEnabled(false);

						} else {
							timer_textField.setEnabled(false);
							button_state = true;
							JOptionPane.showMessageDialog(frmStrykerVer, "Please Select a Light Source", "Live Update",
									JOptionPane.ERROR_MESSAGE);
							Live_Updates.setSelected(false);
							button_state = false;
						}
					}

				} else if (e.getStateChange() == ItemEvent.DESELECTED) {
					timer_textField.setEnabled(false);
					Live_Updates.setText("START");
					if (!button_state) {
						if (rdbtnWhitelight.isSelected()) {
							value = (byte) 0x1;
						} else if (rdbtnLaserlight.isSelected()) {
							value = (byte) 0x2;
						}
						initialize_timer(value, OFF);
						rdbtnLaserlight.setEnabled(true);
						rdbtnWhitelight.setEnabled(true);
					}

				}

			}
		});
		Live_Updates.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		Live_Updates.setBounds(138, 51, 92, 23);
		Live_Update_Panel.add(Live_Updates);
		this.timer_textField = new JTextField();
		timer_textField.setText("0");
		timer_textField.setFont(new Font("Calibri Light", Font.PLAIN, 17));
		timer_textField.setEnabled(false);
		timer_textField.setEditable(false);
		timer_textField.setColumns(10);
		timer_textField.setBounds(38, 94, 119, 43);
		Live_Update_Panel.add(timer_textField);
		JLabel label = new JLabel("uS");
		label.setBounds(156, 106, 34, 16);
		Live_Update_Panel.add(label);
		this.rdbtnWhitelight = new JRadioButton("WhiteLight");
		rdbtnWhitelight.setEnabled(false);
		rdbtnWhitelight.setSelected(true);
		rdbtnWhitelight.setFont(new Font("Calibri Light", Font.PLAIN, 13));
		rdbtnWhitelight.setBounds(38, 30, 115, 18);
		Live_Mode_Light_Selection.add(rdbtnWhitelight);
		Live_Update_Panel.add(rdbtnWhitelight);
		this.rdbtnLaserlight = new JRadioButton("LaserLight");
		rdbtnLaserlight.setEnabled(false);
		rdbtnLaserlight.setFont(new Font("Calibri Light", Font.PLAIN, 13));
		rdbtnLaserlight.setBounds(38, 52, 115, 18);
		Live_Mode_Light_Selection.add(rdbtnLaserlight);
		Live_Update_Panel.add(rdbtnLaserlight);
		JLabel lblForEvaluationPurpose = new JLabel("For evaluation purposes only!!");
		lblForEvaluationPurpose.setFont(new Font("Gill Sans MT Condensed", Font.BOLD, 25));
		lblForEvaluationPurpose.setBounds(558, 595, 273, 31);
		frmStrykerVer.getContentPane().add(lblForEvaluationPurpose);
		JLabel lblVer = new JLabel("v3.1.1");
		lblVer.setFont(new Font("SansSerif", Font.PLAIN, 14));
		lblVer.setBounds(1290, 605, 55, 16);
		frmStrykerVer.getContentPane().add(lblVer);

		System.out.println(LocalDateTime.now());

	}

	public void initialize_timer(final byte Value, final byte Control) {

		try {
			System.out.println("Value while entry : " + Control);
			System.out.println("Light SOurce while Entry : " + Value);
			ActionListener listener = new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					byte val = 0x0;
					if (!pause_flag) {
						val = Value;

						System.out.println("VALUE : " + Value);

						// TODO Auto-generated method stub
						System.out.println("Running Timer..");
						bytedefinitions b = new bytedefinitions();
						ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
						System.out.println("Going to call main_app_worker");
						// System.out.println("Value : " + val);
						Main_App_Worker my_worker = new Main_App_Worker(Live_Updates, LIVE, IPCPacket, Control, val,
								dummy_byte, dummy_byte, dummy_byte, dummy_byte, true, serialport, 0, b.PWM_FREQ);
						my_worker.execute();

					} else {
						// live_flag = false;
						System.out.println(
								"########################################################################################");
						System.out.println("Some other command has got access!!");
						System.out.println(
								"########################################################################################");
					}

				}
			};
			if (Control == (byte) 0x1) {
				System.out.println("Iside Byte 0x1");
				if (live_timer_on == null) {
					System.out.println("Inside Live Timer Not null");
					live_timer_on = new Timer(1000, null);
					live_timer_on.addActionListener(listener);
				}

				// live_timer_on.setRepeats(true);
				live_timer_on.start();
			} else {
				System.out.println("ELSE PART");
				if (live_timer_on != null) {
					if (live_timer_on.isRunning()) {
						System.out.println("Cancelling already running timer");

						// live_timer_on.removeActionListener(listener);
						live_timer_on.stop();
						live_timer_on = null;
					}

					else {
						System.out.println("Timer is not running!!");
					}
					Thread.sleep(100);
					bytedefinitions b = new bytedefinitions();
					ArrayList<Byte> IPCPacket = new ArrayList<Byte>();
					System.out.println("Going to call main_app_worker");
					// System.out.println("Value : " + val);
					Main_App_Worker my_worker = new Main_App_Worker(Live_Updates, LIVE, IPCPacket, Control, Value,
							dummy_byte, dummy_byte, dummy_byte, dummy_byte, false, serialport, 0, b.PWM_FREQ);
					// No need to wait for serial response. Waiting Updates the PWM Count text field
					// with the Live_Stop Response byte
					my_worker.execute();
					timer_textField.setText("0"); // Change Suggested by Ratna Madam. Set Value to Zero, when stopped.
				}

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void Show_ManufacturingTests() {

		try {

			Stryk_Demo.setlistener(serialport);
			if (Stryk_Demo.serialport != null) {
				ManufacturingTests.MFTest_Flag = true;
				Stryk_Demo.serialport.openPort();
				if (Stryk_Demo.serialport.isOpen() == true) {

					frmStrykerVer.setVisible(false);
					ManufacturingTests obs = new ManufacturingTests();

					// Stryk_Demo.send_data_in_bg("ManufacturingTest",
					// Stryk_Demo.btnManufacturingTests, true, Manufacturing_Test.getBytes());
				} else {
					JOptionPane.showMessageDialog(frmStrykerVer, "Serial Port could not be opened", "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			} else {
				JOptionPane.showMessageDialog(frmStrykerVer, "Invalid Serial Port", "Error", JOptionPane.ERROR_MESSAGE);
			}
		} catch (Exception ex) {
			ex.printStackTrace();

		}

	}

	// Initialize function ends here
	public int check_for_unselected_radiobuttons(ButtonGroup grp) {
		int unselected_flag = 0x1;
		Enumeration<AbstractButton> allRadioButton = grp.getElements();
		while (allRadioButton.hasMoreElements()) {
			JRadioButton temp = (JRadioButton) allRadioButton.nextElement();
			if (temp.isSelected()) {
				// LOG//system.out.println("Atleast One source selected"
				unselected_flag = 0x0;

				break;

			}

		}
		return unselected_flag;

	}

	public void enable_components(Component[] comp) {
		for (int i = 0; i < comp.length; i++) {
			comp[i].setEnabled(true);
		}
	}

	public static void print(String stringtoprint) {
		System.out.println(stringtoprint);
	}

	public void disable_components(Component[] comp) {
		for (int i = 0; i < comp.length; i++) {

			comp[i].setEnabled(false);

		}
	}

	public int[] extract_pwm_values(byte[] pwm) {
		int[] int_buff = new int[pwm.length];

		System.out.println(pwm.length);
		for (int i = 0; i < pwm.length; i++) {
			int_buff[i] = pwm[i];
		}
		return int_buff;
	}

	public String[] get_comma_separated_values_from(String string) {
		System.out.println("The received string is : " + string);
		System.out.println("Splitting with commas...");
		return null;

	}

	public String[] return_comma_separated_values_without_braces(String[] comma_separated_values) {
		try {
			for (int i = 0; i < comma_separated_values.length; i++) {

				if (comma_separated_values[i].startsWith("{")) {
					// ##System.out.println("{ is found");
					comma_separated_values[i] = comma_separated_values[i].replace("{", "");
				}
				if (comma_separated_values[i].endsWith("}")) {
					// ##System.out.println("} is found");
					comma_separated_values[i] = comma_separated_values[i].replace("}", "");
				}

			}
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(frmStrykerVer, "Some Error while removing { and } from the data");
		}

		return comma_separated_values;

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void updateportlist(boolean comportemptyflag) {

		if (comportemptyflag) {
			System.out.println("Flag True");
			for (int i = 0; i < portlistforcombobox.length; i++) {
				portlistforcombobox[i] = "";
			}
			this.comboBox = new JComboBox(portlistforcombobox);
		} else if (!comportemptyflag) {
			// #System.out.println("Flag False");
			this.comboBox = new JComboBox(portlistforcombobox);
		}

	}

	public String Hex(String b) {
		String.format("0x%04X", b);
		return b;

	}

	/***
	 * public void update_sliders_and_fields(String[] field_buff) { connected_flag =
	 * true; extractpwmflag = true; // System.out.println("The Array to be processed
	 * : // "+Arrays.toString(field_buff)); // ##System.out.println("Setting up
	 * sliders and Controls..."); // ##System.out.println("Total number of elements
	 * in the buffer : "+ // field_buff.length); for (int i = 0; i <
	 * field_buff.length; i++) {
	 * 
	 * if (i == 0) slider.setValue(Integer.parseInt(field_buff[i]));
	 * textField.setValue(slider.getValue()); if (i == 1)
	 * slider_1.setValue(Integer.parseInt(field_buff[i]));
	 * textField_1.setValue(slider_1.getValue()); if (i == 2)
	 * slider_2.setValue(Integer.parseInt(field_buff[i]));
	 * textField_2.setValue(slider_2.getValue()); if (i == 3)
	 * slider_3.setValue(Integer.parseInt(field_buff[i]));
	 * textField_3.setValue(slider_3.getValue()); if (i == 4) { if
	 * (Integer.parseInt(field_buff[i]) == 1) {
	 * rdbtnNewRadioButton.setSelected(true); } else {
	 * rdbtnNewRadioButton.setSelected(false); } } if (i == 5) { if
	 * (Integer.parseInt(field_buff[i]) == 1) { comboBox_1.setSelectedIndex(1); }
	 * else { comboBox_1.setSelectedIndex(0); } } if (i == 6) {
	 * formattedTextField.setValue(Integer.parseInt(field_buff[i])); } if (i == 7) {
	 * formattedTextField_1.setValue(Integer.parseInt(field_buff[i])); }
	 * 
	 * // Included for newer version of controller if (i == 8) {
	 * slider_4.setValue(Integer.parseInt(field_buff[i]));
	 * textField_4.setValue(slider_4.getValue()); } if (i == 9) {
	 * slider_5.setValue(Integer.parseInt(field_buff[i]));
	 * textField_5.setValue(slider_5.getValue()); } if (i == 10) {
	 * slider_6.setValue(Integer.parseInt(field_buff[i]));
	 * textField_6.setValue(slider_6.getValue()); } if (i == 11) {
	 * slider_7.setValue(Integer.parseInt(field_buff[i]));
	 * textField_7.setValue(slider_7.getValue()); } if (i == 12) { if
	 * (Integer.parseInt(field_buff[i]) == 1) {
	 * rdbtnNewRadioButton_1.setSelected(true); } else {
	 * rdbtnNewRadioButton_1.setSelected(false); } } if (i == 13) { if
	 * (Integer.parseInt(field_buff[i]) == 1) { comboBox_2.setSelectedIndex(1); }
	 * else { comboBox_2.setSelectedIndex(0); } } if (i == 14) {
	 * formattedTextField_2.setValue(Integer.parseInt(field_buff[i])); } if (i ==
	 * 15) { formattedTextField_3.setValue(Integer.parseInt(field_buff[i])); }
	 * 
	 * if (i == 17) { if (Integer.parseInt(field_buff[i]) == 1)
	 * chckbxIrisPwrEn.setSelected(true); } else {
	 * chckbxIrisPwrEn.setSelected(false); } // if(i ==
	 * 16){if(Integer.parseInt(field_buff[i]) == 1 //
	 * )chckbxLsrFanCtrl.setSelected(true);}else{chckbxLsrFanCtrl.setSelected(false);}
	 * if (i == 16) { if (Integer.parseInt(field_buff[i]) == 1) {
	 * chckbxLsrFanCtrl.setSelected(true); } else {
	 * chckbxLsrFanCtrl.setSelected(false); } } }
	 * 
	 * /*** for(int i = 0; i<field_buff.length;i++){
	 * 
	 * 
	 * System.out.println("Value " + i + " is : " + field_buff[i]); }
	 * 
	 * extractpwmflag = false; connected_flag = false;
	 * 
	 * }
	 ***/

	public int get_array_nonzero(byte[] p) {
		int count = 0;
		if (p.length > 0) {

			try {

				// System.out.println(Integer.toString(p.length));
				if (p.length == 0) {
					count = 0;
				} else {
					for (int k = 0; k < p.length; k++) {
						if (p[k] != 0) {
							count++;
						}
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}
		System.out.print(Integer.toString(count));
		return count;
	}

	// Duplicate of the process serial data function-for test purposes
	public static void process_serialport_response(int whichcommand) {
		try {

			bytedefinitions b = new bytedefinitions();
			if (whichcommand == b.CONNECT_COMMAND) {
				Thread Connect = new Thread(new Runnable() {

					public void run() {

						connect_flag = true;
						WlPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
						EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
						panel_3.setBackground(Color.GREEN);
						comboBox.setEditable(false);
						comboBox.setEnabled(false);
						btnRefreshPortList.setEnabled(false);
						chckbxReset.setEnabled(true);
						textArea.append("Connected\r\n\n");
						btnConnect.setEnabled(false);
						btnDisconnect.setEnabled(true);
						chckbxReset.setEnabled(true);
						chckbxLoadDefaults.setEnabled(true);
						rdbtnuC1.setEnabled(true);
						rdbtnuC2.setEnabled(true);
						lblPowerSupply.setEnabled(true);
						lblHeatSink.setEnabled(true);
						lblFanFailure.setEnabled(true);
						label_19.setEnabled(false);
						lblIris.setEnabled(true);
						label_5.setEnabled(true);
						label_17.setEnabled(true);
						label_18.setEnabled(true);
						slider_12.setEnabled(true);
						slider_13.setEnabled(true);
						slider_14.setEnabled(true);
						label_6.setEnabled(false);
						label_7.setEnabled(false);
						label_8.setEnabled(false);
						label_9.setEnabled(false);
						lblGrp.setEnabled(false);
						btnManufacturingTests.setEnabled(false);
						//btnCalibration.setEnabled(false);
						Color en = Color.decode("#000000");
						panel_33.setBackground(en);
						lblFibre1_Detect.setEnabled(true);
						lblFibre2_Detect.setEnabled(true);
						lblBeamSensor.setEnabled(true);
						lblIris_Pgood.setEnabled(true);
						btnSave.setEnabled(true);
						btnLastMode.setEnabled(true);
						// btnCalibration.setEnabled(true);
						btnReadAll.setEnabled(true);
						chckbxEnv.setEnabled(true);
						chckbxIris.setEnabled(true);
						lblCs.setEnabled(true);
						lblCs_1.setEnabled(true);
						lblCs_2.setEnabled(true);
						lblCs_3.setEnabled(true);
						lblCs_4.setEnabled(true);
						lblCs_5.setEnabled(true);
						lblCs_6.setEnabled(true);
						lblCs_7.setEnabled(true);
						lblLl.setEnabled(true);
						lblLWl.setEnabled(true);
						lblAdc_Red.setEnabled(true);
						lblAdc_Green.setEnabled(true);
						lblAdc_Blue.setEnabled(true);
						lblAdc_Laser.setEnabled(true);
						lblIris_2.setEnabled(true);
						lblIris_3.setEnabled(true);
						lblIris1pd.setEnabled(true);
						lblIris2pd.setEnabled(true);
						lblEnvpd.setEnabled(true);
						lblV.setEnabled(true);
						lblI.setEnabled(true);
						slider_12.setValue(0);
						chckbxRed.setSelected(false);
						chckbxGreen.setSelected(false);
						chckbxBlue.setSelected(false);
						chckbxIris1.setSelected(false);
						chckbxIris2.setSelected(false);
						rdbtnIndividual.setSelected(false);

						label_5.setText(Integer.toString(slider_12.getValue()) + " %");

						slider_13.setValue(0);
						label_17.setText(Integer.toString(slider_13.getValue()) + " %");

						slider_14.setValue(0);
						label_18.setText(Integer.toString(slider_14.getValue()) + " %");

						slider_15.setValue(0);
						slider_15.setEnabled(false);
						label_20.setText(Integer.toString(slider_15.getValue()) + " %");
						label_20.setEnabled(false);
						slider_16.setValue(0);
						label_21.setText(Integer.toString(slider_16.getValue()) + " %");

						slider_17.setValue(0);
						label_22.setText(Integer.toString(slider_17.getValue()) + " %");

						slider_18.setValue(0);
						label_23.setText(Integer.toString(slider_18.getValue()) + " %");

						slider_19.setValue(0);
						label_24.setText(Integer.toString(slider_19.getValue()) + " %");

						slider_20.setValue(0);
						label_25.setText(Integer.toString(slider_20.getValue()) + " %");

						slider_21.setValue(0);
						label_26.setText(Integer.toString(slider_21.getValue()) + " %");

						reset_ld_rdbtn.clearSelection();
						wlmode.clearSelection();
						wlsource.clearSelection();
						tglbtnFixed_Wl.setSelected(false);
						lblFixedValue_2.setText("VALUE");
						formattedTextField_WlPwm.setText(Integer.toString(0));
						formattedTextField_WlPwm.setEnabled(false);
						lblFixedValue_2.setEnabled(false);

						WlPowerButton.setSelected(false);
						EnvIrisPowerButton.setSelected(false);
						lblTemperature.setEnabled(true);

						env_iris_chckbx.clearSelection();
						env_iris_mode.clearSelection();
						env_iris_source.clearSelection();
						chckbxIris1.setSelected(false);
						chckbxIris2.setSelected(false);

						tglbtnFixed_Env.setSelected(false);
						lblNewLabel_1.setText("VALUE");
						formattedTextField_EnvPwm.setText(Integer.toString(0));
						formattedTextField_EnvPwm.setEnabled(false);
						lblNewLabel_1.setEnabled(false);
						textField_28.setText("");
						textField_29.setText("");
						textField_30.setText("");
						textField_31.setText("");
						textField_32.setText("");
						textField_33.setText("");
						textField_34.setText("");
						textField_35.setText("");

						textField_8.setText("");
						textField_9.setText("");
						textField_10.setText("");
						textField_11.setText("");
						textField_12.setText("");
						textField_13.setText("");
						textField_14.setText("");
						textField_15.setText("");
						textField_16.setText("");
						textField_17.setText("");
						textField_18.setText("");
						textField_20.setText("");
						textField_21.setText("");
						textField_22.setText("");
						textField_23.setText("");
						textField_24.setText("");
						textField_25.setText("");
						textField_26.setText("");
						textField_27.setText("");

						for (Component c : wl_mode) {
							c.setEnabled(true);
						}
						connect_flag = false;
					}
				});
				Connect.start();

			} else if (whichcommand == b.DISCONNECT_COMMAND) {
				WlPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
				EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
				// WlPowerButton.setIcon(off_switch);

				// EnvIrisPowerButton.setIcon(off_switch);
				Stryk_Demo.frmStrykerVer.repaint();
				Stryk_Demo.frmStrykerVer.revalidate();
				reset_ld_rdbtn.clearSelection();
				panel_3.setBackground(Color.RED);
				comboBox.setEditable(true);
				comboBox.setEnabled(true);
				btnRefreshPortList.setEnabled(true);
				textArea.append("Disconnected\r\n\n");
				chckbxReset.setEnabled(false);
				chckbxLoadDefaults.setEnabled(false);
				rdbtnuC1.setEnabled(false);
				lblExt.setEnabled(false);
				rdbtnExt.setEnabled(false);
				lblUc1.setEnabled(false);
				rdbtnuC_1.setEnabled(false);
				lblUc2.setEnabled(false);
				rdbtnuC_2.setEnabled(false);
				WlPowerButton.setEnabled(false);
				rdbtnuC2.setEnabled(false);
				btnDisconnect.setEnabled(false);
				btnConnect.setEnabled(true);
				lblPowerSupply.setEnabled(false);
				lblHeatSink.setEnabled(false);
				lblIris.setEnabled(false);
				label_5.setEnabled(false);
				label_17.setEnabled(false);
				btnManufacturingTests.setEnabled(true);
				//btnCalibration.setEnabled(true);
				label_18.setEnabled(false);
				slider_12.setEnabled(false);
				slider_13.setEnabled(false);
				slider_14.setEnabled(false);
				lblFanFailure.setEnabled(false);
				label_19.setEnabled(false);
				label_6.setEnabled(false);
				label_7.setEnabled(false);
				label_8.setEnabled(false);
				label_9.setEnabled(false);
				lblFibre1_Detect.setEnabled(false);
				lblFibre2_Detect.setEnabled(false);
				lblBeamSensor.setEnabled(false);
				lblIris_Pgood.setEnabled(false);
				btnSave.setEnabled(false);
				btnLastMode.setEnabled(false);
				// btnCalibration.setEnabled(false);
				btnReadAll.setEnabled(false);
				lblPwm_4.setEnabled(false);
				rdbtnPwm.setEnabled(false);
				rdbtnCwm.setEnabled(false);
				lblCwm.setEnabled(false);
				lblStb.setEnabled(false);
				rdbtnStb.setEnabled(false);
				lblLc.setEnabled(false);
				rdbtnLc.setSelected(false);
				rdbtnLc.setEnabled(false);
				chckbxEnv.setEnabled(false);
				chckbxIris.setEnabled(false);
				lblCs.setEnabled(false);
				lblCs_1.setEnabled(false);
				lblCs_2.setEnabled(false);
				lblCs_3.setEnabled(false);
				lblCs_4.setEnabled(false);
				lblCs_5.setEnabled(false);
				lblCs_6.setEnabled(false);
				lblCs_7.setEnabled(false);
				lblLl.setEnabled(false);
				slider_15.setEnabled(false);
				slider_16.setEnabled(false);
				slider_17.setEnabled(false);
				slider_18.setEnabled(false);
				label_20.setEnabled(false);
				label_21.setEnabled(false);
				label_22.setEnabled(false);
				label_23.setEnabled(false);
				lblGrp.setEnabled(false);
				rdbtnIndividual.setEnabled(false);
				chckbxRed.setEnabled(false);
				chckbxGreen.setEnabled(false);
				chckbxBlue.setEnabled(false);
				chckbxEnv.setEnabled(false);
				chckbxIris.setEnabled(false);
				EnvIrisPowerButton.setEnabled(false);
				lblEnv.setEnabled(false);
				slider_19.setEnabled(false);
				label_24.setEnabled(false);
				slider_20.setEnabled(false);
				label_25.setEnabled(false);
				slider_21.setEnabled(false);
				label_26.setEnabled(false);
				chckbxIris1.setEnabled(false);
				chckbxIris2.setEnabled(false);
				label_25.setEnabled(false);
				label_26.setEnabled(false);
				chckbxIrisPowerShutdown_1.setEnabled(false);
				panel_33.setEnabled(false);
				for (Component c : env_iris) {
					c.setEnabled(false);
				}
				for (Component c : env_source) {
					c.setEnabled(false);
				}
				for (Component c : env_pwm_type) {
					c.setEnabled(false);
				}

				lblLWl.setEnabled(false);
				lblAdc_Red.setEnabled(false);
				lblAdc_Green.setEnabled(false);
				lblAdc_Blue.setEnabled(false);
				lblAdc_Laser.setEnabled(false);
				lblIris_2.setEnabled(false);
				lblIris_3.setEnabled(false);
				lblIris1pd.setEnabled(false);
				lblIris2pd.setEnabled(false);
				lblEnvpd.setEnabled(false);
				lblV.setEnabled(false);
				lblI.setEnabled(false);
				lblTemperature.setEnabled(false);
				for (Component c : wl_pwm_type) {
					c.setEnabled(false);
				}
				if (serialport.isOpen()) {
					serialport.closePort();
				}

			} else if (whichcommand == b.EXPECT_NO_RESPONSE) {
				print("May be a Notification Message");
				print(Current_control);
			}
			/*
			 * else if(whichcommand == b.RESET_uC_COMMAND) { if(chckbxuC1.isSelected() ==
			 * true) { chckbxuC1.setSelected(false);
			 * textArea.append("uC1 - Reset Successful!!"); } else
			 * if(chckbxuC2.isSelected()) { chckbxuC2.setSelected(false);
			 * textArea.append("uC2 - Reset Successful!!"); }
			 * 
			 * }
			 */
			/*
			 * else if(whichcommand == b.LOAD_DEFAULT_COMMAND) { if(chckbxuC1.isSelected()
			 * == true) { chckbxuC1.setSelected(false);
			 * textArea.append("uC1 - Load Default Successful!!"); } else
			 * if(chckbxuC2.isSelected()) { chckbxuC2.setSelected(false);
			 * textArea.append("uC2 - Load Default Successful!!"); } }
			 */

			// Process response for mode command
			else if (whichcommand == b.MODE_COMMAND) {
				if (switch_flag) {
					display("Error Received for sending ");
					wl_critical_flag = true;
					env_iris_critical_flag = true;
					if (Wl_Button_Flag == true) {
						if (WlPowerButton.isSelected() == true) {
							print("WL ON State");
							WlPowerButton.setSelected(false);
						} else if (WlPowerButton.isSelected() == false) {
							print("WL OFF State");
							WlPowerButton.setSelected(true);
						}
						Wl_Button_Flag = false;
					}
					if (ENV_Iris_Button_Flag == true) {
						if (EnvIrisPowerButton.isSelected() == true) {
							print("ENV_IRIS ON State");
							EnvIrisPowerButton.setSelected(false);
						} else if (EnvIrisPowerButton.isSelected() == false) {
							print("ENV_IRIS OFF State");
							EnvIrisPowerButton.setSelected(true);
						}
						ENV_Iris_Button_Flag = false;
					}

					else {

					}
					wl_critical_flag = false;
					env_iris_critical_flag = false;
					switch_flag = false;

				} else {
					if (Current_control == WL_PON) {

						if (EnvIrisPowerButton.isSelected()) {
							Color en = Color.decode("#ccffff");
							panel_33.setBackground(en);
						}

						textArea.append("White Light Switched ON!!\r\n\n");
						Component[] p26 = panel_26.getComponents();
						Component[] p27 = panel_27.getComponents();
						Component[] p28 = panel_28.getComponents();

						for (Component c : p26) {
							c.setEnabled(false);
						}
						for (Component c : p27) {
							c.setEnabled(false);
						}
						for (Component c : p28) {
							c.setEnabled(false);
						}
						slider_15.setEnabled(true);
						lblGrp.setEnabled(true);
						label_20.setEnabled(true);
						rdbtnIndividual.setEnabled(true);
						wl_onoff_flag = true;
						label_21.setEnabled(false);
						label_22.setEnabled(false);
						label_23.setEnabled(false);
						chckbxRed.setSelected(true);
						chckbxGreen.setSelected(true);
						chckbxBlue.setSelected(true);
						lblLc.setEnabled(false);
						rdbtnLc.setEnabled(false);
						wl_onoff_flag = false;

						WlPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/on.jpg")));
					} else if (Current_control == WL_POFF) {
						String current_rdbtn_mode = "";
						String current_rdbtn_source = "";
						textArea.append("White Light Switched OFF!!\r\n\n");

						Component[] p26 = panel_26.getComponents();
						Component[] p27 = panel_27.getComponents();

						for (Component c : p26) {
							c.setEnabled(true);

						}
						for (Component c : p27) {
							c.setEnabled(true);
						}

						/////////////////////////////////////////////////////////////////////////
						if (rdbtnExt.isSelected()) {
							current_rdbtn_source = radiobuttonExt;
						} else if (rdbtnuC_1.isSelected()) {
							current_rdbtn_source = radiobuttonuC_1;

						} else if (rdbtnuC_2.isSelected()) {
							current_rdbtn_source = radiobuttonuC_2;
						} else {

						}
						wlsource.clearSelection();

						/////////////////////////////////////////////////////////
						/////////////////////////////////////
						if (rdbtnPwm.isSelected()) {
							current_rdbtn_mode = radiobuttonPwm;
						} else if (rdbtnCwm.isSelected()) {
							current_rdbtn_mode = radiobuttonCwm;
						} else if (rdbtnStb.isSelected()) {
							current_rdbtn_mode = radiobuttonStb;
						} else if (rdbtnLc.isSelected()) {
							current_rdbtn_mode = radiobuttonLc;
						} else {

						}
						wlmode.clearSelection();
						if (current_rdbtn_mode == radiobuttonPwm) {
							rdbtnPwm.setSelected(true);
						} else if (current_rdbtn_mode == radiobuttonCwm) {
							rdbtnCwm.setSelected(true);

						} else if (current_rdbtn_mode == radiobuttonStb) {
							rdbtnStb.setSelected(true);
						} else if (current_rdbtn_mode == radiobuttonLc) {
							rdbtnLc.setSelected(true);
						}
						current_rdbtn_mode = "";
						if (current_rdbtn_source == radiobuttonExt) {
							rdbtnExt.setSelected(true);
						} else if (current_rdbtn_source == radiobuttonuC_1) {
							rdbtnuC_1.setSelected(true);
						} else if (current_rdbtn_source == radiobuttonuC_2) {
							rdbtnuC_2.setSelected(true);
						} else {
							print("No valid source");
						}
						current_rdbtn_source = "";
						/////////////////////////////////////////////////////
						wl_onoff_flag = true;

						rdbtnIndividual.setSelected(false);
						rdbtnIndividual.setEnabled(false);

						slider_15.setEnabled(false);
						label_20.setText(Integer.toString(slider_15.getValue()) + " %");
						label_20.setEnabled(false);

						slider_16.setEnabled(false);
						slider_16.setValue(0);
						label_21.setText(Integer.toString(slider_16.getValue()) + " %");
						label_21.setEnabled(false);

						slider_17.setValue(0);
						slider_17.setEnabled(false);
						label_22.setText(Integer.toString(slider_17.getValue()) + " %");
						label_22.setEnabled(false);

						slider_18.setValue(0);
						slider_18.setEnabled(false);
						label_23.setText(Integer.toString(slider_18.getValue()) + " %");
						label_23.setEnabled(false);

						chckbxRed.setSelected(false);
						chckbxRed.setEnabled(false);

						chckbxGreen.setSelected(false);
						chckbxGreen.setEnabled(false);

						chckbxBlue.setSelected(false);
						chckbxBlue.setEnabled(false);

						wl_onoff_flag = false;
						lblGrp.setEnabled(false);
						label_20.setEnabled(false);
						lblLc.setEnabled(true);
						rdbtnLc.setEnabled(true);
						WlPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
					} else if (Current_control == ENV_IRIS_ON) {
						if (WlPowerButton.isSelected()) {
							Color en = Color.decode("#ccffff");
							panel_33.setBackground(en);
						}
						if (SOURCE == ENV_SOURCE) {
							textArea.append("ENV Light Switched ON!!\r\n\n");
							chckbxEnv.setEnabled(false);
							chckbxIris.setEnabled(false);
							for (Component c : env_iris) {
								c.setEnabled(false);
							}
							for (Component c : env_source) {
								c.setEnabled(false);

							}
							for (Component c : env_pwm_type) {
								c.setEnabled(false);
							}
							lblEnv.setEnabled(true);
							slider_19.setEnabled(true);
							label_24.setEnabled(true);
							chckbxIris1.setEnabled(false);
							chckbxIris2.setEnabled(false);
							chckbxIrisPowerShutdown_1.setEnabled(false);
							slider_20.setEnabled(false);
							label_25.setEnabled(false);
							slider_21.setEnabled(false);
							label_26.setEnabled(false);

						} else if (SOURCE == IRIS_SOURCE) {
							chckbxEnv.setEnabled(false);
							chckbxIris.setEnabled(false);
							for (Component c : env_iris) {
								c.setEnabled(false);
							}
							for (Component c : env_source) {
								c.setEnabled(false);

							}
							for (Component c : env_pwm_type) {
								c.setEnabled(false);
							}
							lblEnv.setEnabled(false);
							slider_19.setEnabled(false);
							label_24.setEnabled(false);
							iris_onoff_flag = true;
							chckbxIris1.setEnabled(true);
							chckbxIris1.setSelected(true);
							slider_20.setEnabled(true);
							label_25.setEnabled(true);
							chckbxIris2.setSelected(true);
							chckbxIris2.setEnabled(true);
							slider_21.setEnabled(true);
							label_26.setEnabled(true);
							chckbxIrisPowerShutdown_1.setEnabled(true);
							iris_onoff_flag = false;

							textArea.append("IRIS Light Switched ON!!\r\n");
						}
						EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/on.jpg")));
					} else if (Current_control == ENV_IRIS_OFF) {
						// EnvIrisPowerButton.setIcon(new
						// ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
						Color en = Color.decode("#000000");
						panel_33.setBackground(en);
						String current_rdbtn_mode = "";
						String current_rdbtn_source = "";
						if (SOURCE == ENV_SOURCE) {

							for (Component c : env_iris) {
								c.setEnabled(true);
							}

							chckbxEnv.setEnabled(true);
							chckbxIris.setEnabled(true);
							if (rdbtnEnv_Ext.isSelected()) {
								current_rdbtn_source = radiobuttonExtEnv;
							} else if (rdbtnuC_3.isSelected()) {
								current_rdbtn_source = radiobuttonuC_3;
							} else if (rdbtnuC_4.isSelected()) {
								current_rdbtn_source = radiobuttonuC_4;
							} else if (rdbtn_IrisExtWht.isSelected()) {
								current_rdbtn_source = radiobuttonIrisExtWht;
							} else if (rdbtn_IrisExtEnv.isSelected()) {
								current_rdbtn_source = radiobuttonIrisExtEnv;
							} else {

							}
							env_iris_source.clearSelection();
							if (rdbtnEnv_Pwm.isSelected()) {
								current_rdbtn_mode = radiobuttonEnvPwm;
							} else if (rdbtnEnv_Cwm.isSelected()) {
								current_rdbtn_mode = radiobuttonEnvCwm;
							} else if (rdbtnEnv_CwmNr.isSelected()) {
								current_rdbtn_mode = radiobuttonEnvCwmNr;
							} else if (rdbtnEnv_LcNr.isSelected()) {
								current_rdbtn_mode = radiobuttonEnvLcNr;
							} else {

							}
							env_iris_mode.clearSelection();
							if (current_rdbtn_mode == radiobuttonEnvPwm) {
								rdbtnEnv_Pwm.setSelected(true);
							} else if (current_rdbtn_mode == radiobuttonEnvCwm) {
								rdbtnEnv_Cwm.setSelected(true);
							} else if (current_rdbtn_mode == radiobuttonEnvCwmNr) {
								rdbtnEnv_CwmNr.setSelected(true);
							} else if (current_rdbtn_mode == radiobuttonEnvLcNr) {
								rdbtnEnv_LcNr.setSelected(true);
							} else {

							}
							if (current_rdbtn_source == radiobuttonExtEnv) {
								rdbtnEnv_Ext.setSelected(true);
							} else if (current_rdbtn_source == radiobuttonuC_3) {
								rdbtnuC_3.setSelected(true);
							} else if (current_rdbtn_source == radiobuttonuC_4) {
								rdbtnuC_4.setSelected(true);
							} else if (current_rdbtn_source == radiobuttonIrisExtWht) {
								rdbtn_IrisExtWht.setSelected(true);
							} else if (current_rdbtn_source == radiobuttonIrisExtEnv)

							{
								rdbtn_IrisExtEnv.setSelected(true);
							} else {

							}

							lblEnv.setEnabled(false);
							slider_19.setEnabled(false);
							label_24.setEnabled(false);
							chckbxIris1.setEnabled(false);
							chckbxIris2.setEnabled(false);
							chckbxIrisPowerShutdown_1.setEnabled(false);
							slider_20.setEnabled(false);
							label_25.setEnabled(false);
							slider_21.setEnabled(false);
							label_26.setEnabled(false);

							textArea.append("ENV Light Switched OFF\r\n\n");
						} else if (SOURCE == IRIS_SOURCE) {
							chckbxEnv.setEnabled(true);
							chckbxIris.setEnabled(true);

							iris_onoff_flag = true;
							chckbxIris1.setSelected(false);
							chckbxIris2.setSelected(false);
							iris_onoff_flag = false;
							if (rdbtnEnv_Ext.isSelected()) {
								current_rdbtn_source = radiobuttonExtEnv;
							} else if (rdbtnuC_3.isSelected()) {
								current_rdbtn_source = radiobuttonuC_3;
							} else if (rdbtnuC_4.isSelected()) {
								current_rdbtn_source = radiobuttonuC_4;
							} else if (rdbtn_IrisExtWht.isSelected()) {
								current_rdbtn_source = radiobuttonIrisExtWht;
							} else if (rdbtn_IrisExtEnv.isSelected()) {
								current_rdbtn_source = radiobuttonIrisExtEnv;
							} else {

							}
							env_iris_source.clearSelection();
							if (rdbtnEnv_Pwm.isSelected()) {
								current_rdbtn_mode = radiobuttonEnvPwm;
							} else if (rdbtnEnv_Cwm.isSelected()) {
								current_rdbtn_mode = radiobuttonEnvCwm;
							} else if (rdbtnEnv_CwmNr.isSelected()) {
								current_rdbtn_mode = radiobuttonEnvCwmNr;
							} else if (rdbtnEnv_LcNr.isSelected()) {
								current_rdbtn_mode = radiobuttonEnvLcNr;
							} else {

							}
							env_iris_mode.clearSelection();
							if (current_rdbtn_mode == radiobuttonEnvPwm) {

								rdbtnEnv_Pwm.setSelected(true);
							} else if (current_rdbtn_mode == radiobuttonEnvCwm) {

								rdbtnEnv_Cwm.setSelected(true);
							} else if (current_rdbtn_mode == radiobuttonEnvCwmNr) {
								rdbtnEnv_CwmNr.setEnabled(true);
								rdbtnEnv_CwmNr.setSelected(true);
							} else if (current_rdbtn_mode == radiobuttonEnvLcNr) {
								rdbtnEnv_LcNr.setEnabled(true);
								rdbtnEnv_LcNr.setSelected(true);
							} else {

							}
							if (current_rdbtn_source == radiobuttonExtEnv) {
								rdbtnEnv_Ext.setSelected(true);
							} else if (current_rdbtn_source == radiobuttonuC_3) {
								rdbtnuC_3.setSelected(true);
							} else if (current_rdbtn_source == radiobuttonuC_4) {
								rdbtnuC_4.setSelected(true);
							} else if (current_rdbtn_source == radiobuttonIrisExtWht) {
								rdbtn_IrisExtWht.setSelected(true);
							} else if (current_rdbtn_source == radiobuttonIrisExtEnv)

							{
								rdbtn_IrisExtEnv.setSelected(true);
							} else {

							}
							rdbtnEnv_Pwm.setEnabled(true);
							lblPwm_8.setEnabled(true);
							lblCwm_1.setEnabled(true);
							rdbtnEnv_Cwm.setEnabled(true);
							lblEnv.setEnabled(false);
							slider_19.setEnabled(false);
							label_24.setEnabled(false);
							chckbxIris1.setEnabled(false);
							chckbxIris2.setEnabled(false);
							chckbxIrisPowerShutdown_1.setEnabled(false);
							slider_20.setEnabled(false);
							label_25.setEnabled(false);
							slider_21.setEnabled(false);
							label_26.setEnabled(false);
							textArea.append("IRIS Light Switched OFF\r\n");
						}
						EnvIrisPowerButton.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/off.jpg")));
					}
				}

			}

			else if (whichcommand == b.SAVE_COMMAND) {
				textArea.append("Values Saved Successfully!!\r\n");
			} else if (whichcommand == b.RESTORE_CMD) {
				byte[] Restore_Values;
				// Contents of Restore Values
				// 1. WLENABLE 2. ENVENABLE 3. IRISENABLE 4. REDENABLE
				// 5. GREENENABLE 6. BLUEENABLE 7. IRIS1ENABLE 8. IRIS2ENABLE
				// 9. WL_MODE 10. ENV_MODE 11. IRIS_MODE 12. WL_SRC
				// 13. ENV_SRC 14. IRIS_SRC 15. PWM_WL_TYPE 16. PWM_ENV_TYPE
				// 17. PWM_WL_DUTY 18. PWM_ENV_DUTY 19. RED_INTENSITY 20. GREEN_INTENSITY
				// 21. BLUE_INTENSITY 22. WHITE_INTENSITY 23. ENVINTENSITY 24. IRIS1_INTENSITY
				// 25. IRIS2_INTENSITY 26. POWERSUPPLYFANSPEED 27. HEATSINKFANSPEED
				Restore_Values = Arrays.copyOfRange(destuff_buffer, 7, destuff_buffer.length - 2);
				print("Restore Buffer : " + Arrays.toString(Restore_Values));
				if (Restore_Values.length != 32) {
					if (Restore_Values.length == 2) {
						if (Restore_Values[0] == 2) {

						} else if (Restore_Values[0] == 9) {

						} else {
							display("Unknown Response For Read All Cmd");
						}
					}
					print("Receive Buffer Doesn't meet the specific Requirements");
					ShowMessage("Failed to restore Values");
				}

				else {
					print("Last Mode Packet Valid");
					connect_flag = true;
					DataInputStream DIS = new DataInputStream(new ByteArrayInputStream(Restore_Values));

					Update_LastMode_Values(DIS);
					connect_flag = false;

				}
			} else if (whichcommand == b.SENSING_COMMAND) {
				ByteArrayOutputStream out = new ByteArrayOutputStream();

				// Buffer that hold the complete values(101 Bytes)
				byte[] UI_Values;
				UI_Values = Arrays.copyOfRange(destuff_buffer, 7, destuff_buffer.length - 2);
				print(Arrays.toString(UI_Values));
				full_buffer.reset();
				print("Full Buffer Reset");
				for (int i = 0; i < destuff_buffer.length; i++) {
					destuff_buffer[i] = 0;
				}
				// Byte Buffer to hold ADC Fields(80 bytes)
				byte[] ADC_Values;

				// Byte Buffer to hold Color Sensor Values(16 bytes)
				byte[] Color_Sensor_Values;

				// Byte Buffer to hold Sense gpio Values (5 bytes)
				byte[] Sense_Gpio_Values;

				// Float ArrayList to hold ADC Values
				ArrayList<Float> ADC_Buffer = new ArrayList<Float>();

				// Integer ArrayList to hold Color Sensor Values
				ArrayList<Integer> CS_Buffer = new ArrayList<Integer>();

				label_6.setEnabled(true);
				label_7.setEnabled(true);
				label_8.setEnabled(true);
				label_9.setEnabled(true);
				label_19.setEnabled(true);

				if (UI_Values.length != 101) {
					print(Integer.toString(UI_Values.length));
					print("Buffer Doesn't meet Expected Count!!");
				} else {
					textArea.append("Successfully Read All Values From the Device!!\r\n\n");
					out.write(UI_Values[0]);
					out.write(UI_Values[1]);
					out.write(UI_Values[2]);
					out.write(UI_Values[99]);
					out.write(UI_Values[100]);

					Sense_Gpio_Values = out.toByteArray();
					ADC_Values = Arrays.copyOfRange(UI_Values, 3, 83);
					Color_Sensor_Values = Arrays.copyOfRange(UI_Values, 83, 99);

					ADC_Buffer = Get_Float_From_ByteBuffer(ADC_Values);
					CS_Buffer = Get_Integer_From_ByteBuffer(Color_Sensor_Values);
					Update_ADC_Fields(ADC_Buffer);
					ADC_Buffer.clear();
					Update_Color_Sensor_Fields(CS_Buffer);
					CS_Buffer.clear();
					Update_GPIO_Status(Sense_Gpio_Values);

				}

			} else if (whichcommand == b.VERSION_NO_TEST_COMMAND) {

			} else if (whichcommand == b.IPC_TEST_COMMAND) {

			} else if (whichcommand == b.EEPROM_TEST_COMMAND) {

			} else if (whichcommand == b.FLASH_TEST_COMMAND) {

			} else if (whichcommand == b.UART_TEST_COMMAND) {

			} else if (whichcommand == b.FAN_TEST_COMMAND) {

			} else if (whichcommand == b.GUI_TEST_COMMAND) {

			} else if (whichcommand == b.IRIS_TEST_COMMAND) {

			} else if (whichcommand == b.RGBL_TEST_COMMAND) {

			} else if (whichcommand == b.COLOR_SENSOR_COMMAND) {

			} else if (whichcommand == b.PD_TEST_COMMAND) {

			}

		} catch (Exception ex) {
			ex.printStackTrace();
			ShowMessage("Error While Processing the Received Response!!");
		}

	}

	public static void Update_LastMode_Values(DataInputStream in) throws IOException {
		try {
			wl_critical_flag = true;
			env_iris_critical_flag = true;
			connect_flag = true;

			print("Inside Update Restore Mode Option");
			bytedefinitions bytes = new bytedefinitions();
			LastMode_Structure LastModeValues = new LastMode_Structure(in);

			// WhiteLight Mode Switch Enable
			print("Updating Fields");

			// Red Slider Enable
			if (LastModeValues.RedEnable == true) {
				print("RED Enable");
				chckbxRed.setSelected(true);
			}
			// Red Slider Disable
			else if (LastModeValues.RedEnable == false) {
				print("Red Disable");
				chckbxRed.setSelected(false);
			} else {
			}
			// Green Slider Enable
			if (LastModeValues.GreenEnable == true) {
				print("Green Enable");
				chckbxGreen.setSelected(true);
			}
			// Green Slider disable
			else if (LastModeValues.GreenEnable == false) {
				print("Green Disable");
				chckbxGreen.setSelected(false);
			} else {
			}
			// Blue Slider Enable
			if (LastModeValues.BlueEnable == true) {
				print("Blue Enable");
				chckbxBlue.setSelected(true);
			}
			// Blue Slider disable
			else if (LastModeValues.BlueEnable == false) {
				print("Blue Disable");
				chckbxBlue.setSelected(false);
			} else {
			}
			// IRIS1 Slider Enable
			if (LastModeValues.IRIS1Enable == true) {
				print("IRIS 1 Enable");
				chckbxIris1.setSelected(true);
			}
			// IRIS1 Slider Disable
			else if (LastModeValues.IRIS1Enable == false) {
				print("IRIS 1 Disable");
				chckbxIris1.setSelected(false);
			} else {
			}
			// IRIS2 Slider Enable
			if (LastModeValues.IRIS2Enable == true) {
				print("IRIS2 Enable");
				chckbxIris2.setSelected(true);
			}
			// IRIS2 Slider Disable
			else if (LastModeValues.IRIS2Enable == false) {
				print("IRIS2 Disable");
				chckbxIris2.setSelected(false);
			} else {
			}
			// WhiteLight Mode - PWM
			if (LastModeValues.WLMode == bytes.TYPE_PWM || LastModeValues.WLMode == 0) {
				print("WL PWM Mode");
				rdbtnPwm.setSelected(true);
			}
			// WhiteLight Mode - CWM
			else if (LastModeValues.WLMode == bytes.TYPE_CWM) {
				print("WL CWM Mode");
				rdbtnCwm.setSelected(true);
			}
			// WhiteLight Mode - STB
			else if (LastModeValues.WLMode == bytes.TYPE_STROBE) {
				print("WL Strobe Mode");
				rdbtnStb.setSelected(true);
			}
			// WhiteLight Mode - LC
			else if (LastModeValues.WLMode == bytes.TYPE_LC) {
				print("WL LC Mode");
				rdbtnLc.setSelected(true);
			} else {
			}
			// ENV Mode PWm
			if (LastModeValues.ENVMode == bytes.TYPE_PWM || LastModeValues.ENVMode == 0) {
				print("ENV PWM Mode");
				rdbtnEnv_Pwm.setSelected(true);
			}
			// ENV Mode CWM
			else if (LastModeValues.ENVMode == bytes.TYPE_CWM) {
				print("ENV CWM Mode");
				rdbtnEnv_Cwm.setSelected(true);
			}
			// ENV Mode CWM_NORED
			else if (LastModeValues.ENVMode == bytes.TYPE_CWM_NORED) {
				print("ENV CWMNORED Mode");
				rdbtnEnv_CwmNr.setSelected(true);
			}
			// ENV Mode LC_NORED
			else if (LastModeValues.ENVMode == bytes.TYPE_LC_NORED) {
				print("ENV LC_NORED Mode");
				rdbtnEnv_LcNr.setSelected(true);
			} else {
			}
			// IRIS Mode PWM
			if (LastModeValues.IRISMode == bytes.TYPE_PWM || LastModeValues.IRISMode == 0) {
				print("IRIS PWM Mode");
				rdbtnEnv_Pwm.setSelected(true);
			}
			// IRIS Mode CWM
			else if (LastModeValues.IRISMode == bytes.TYPE_CWM) {
				print("IRIS CWM Mode");
				rdbtnEnv_Cwm.setSelected(true);
			} else {
			}
			// White Light Source Selection
			if (LastModeValues.WLSrc == bytes.SOURCE_EXTERNAL || LastModeValues.WLSrc == 0) {
				print("WL Ext Source");
				rdbtnExt.setSelected(true);
			} else if (LastModeValues.WLSrc == bytes.SOURCE_uC1) {
				print("WL uC1 Source");
				rdbtnuC_1.setSelected(true);
			} else if (LastModeValues.WLSrc == bytes.SOURCE_uC2) {
				print("WL uC2 Source");
				rdbtnuC_2.setSelected(true);
			} else {
			}
			// Env Source Selection

			// IRIS Source Selection
			if (LastModeValues.IRISSrc == bytes.SOURCE_uC1) {
				print("IRIS uC1 Source");
				rdbtnuC_3.setSelected(true);
			} else if (LastModeValues.IRISSrc == bytes.SOURCE_uC2) {
				print("IRIS uC2 Source");
				rdbtnuC_4.setSelected(true);
			} else if (LastModeValues.IRISSrc == bytes.SOURCE_EXT_WHITELIGHT || LastModeValues.IRISSrc == 0) {
				print("IRIS Ext WL Source");
				rdbtn_IrisExtWht.setSelected(true);
			} else if (LastModeValues.IRISSrc == bytes.SOURCE_EXT_ENV) {
				print("IRIS Ext Env Source");
				rdbtn_IrisExtEnv.setSelected(true);
			} else {
			}
			if (LastModeValues.ENVSrc == bytes.SOURCE_EXTERNAL || LastModeValues.ENVSrc == 0) {
				print("ENV Ext Source");
				rdbtnEnv_Ext.setSelected(true);

			} else if (LastModeValues.ENVSrc == bytes.SOURCE_uC1) {
				print("ENV uC1 Source");
				rdbtnuC_3.setSelected(true);
			} else if (LastModeValues.ENVSrc == bytes.SOURCE_uC2) {
				print("ENV uC2 Source");
				rdbtnuC_4.setSelected(true);
			} else {
			}
			// PWM Type - WL
			if (LastModeValues.PWM_WL_TYPE == bytes.PWM_TYPE_FIXED || LastModeValues.PWM_WL_TYPE == 0) {
				// Set the duty here
				tglbtnFixed_Wl.setSelected(false);
				lblFixedValue_2.setText("VALUE");
				formattedTextField_WlPwm.setValue(LastModeValues.PWM_WL_DUTY);
			} else if (LastModeValues.PWM_WL_TYPE == bytes.PWM_TYPE_FADE) {
				// Set the duration here
				lblFixedValue_2.setText("DURATION");
				tglbtnFixed_Wl.setSelected(true);
				formattedTextField_WlPwm.setValue(LastModeValues.PWM_WL_DUTY);

			} else {
			}
			// PWM-Type ENV/IRIS
			if (LastModeValues.PWM_ENV_TYPE == bytes.PWM_TYPE_FIXED || LastModeValues.PWM_ENV_TYPE == 0) {
				// Set the duty here
				tglbtnFixed_Env.setSelected(false);
				lblNewLabel_1.setText("VALUE");
				formattedTextField_EnvPwm.setValue(LastModeValues.PWM_ENV_DUTY);
			} else if (LastModeValues.PWM_ENV_TYPE == bytes.PWM_TYPE_FADE) {
				// Set the duration here
				tglbtnFixed_Env.setSelected(true);
				lblNewLabel_1.setText("DURATION");
				formattedTextField_EnvPwm.setValue(LastModeValues.PWM_ENV_DUTY);
			} else {
			}
			if (LastModeValues.WLEnable == true) {
				print("Switch ON WhiteLight");
				WlPowerButton.setSelected(true);

			}
			// WhiteLight Mode Switch Disable
			else if (LastModeValues.WLEnable == false) {
				print("Switch OFF WhiteLight");
				WlPowerButton.setSelected(false);
			} else {
			}

			// IRIS Mode Switch Enable
			if (LastModeValues.IRISEnable == true) {
				chckbxIris.setSelected(true);
				print("IRIS Power ON");
				EnvIrisPowerButton.setSelected(true);
			}
			// IRIS Mode Switch Disable
			else if (LastModeValues.IRISEnable == false) {
				print("IRIS POFF");
				chckbxIris.setSelected(false);

				EnvIrisPowerButton.setSelected(false);
			} else {
			}
			// ENV Mode Switch Enable
			if (LastModeValues.ENVEnable == true) {
				print("Switch ON ENV");
				chckbxEnv.setSelected(true);
				EnvIrisPowerButton.setSelected(true);
			}
			// ENV Mode Switch Disable
			else if (LastModeValues.ENVEnable == false) {
				if (LastModeValues.IRISEnable == false) {
					chckbxEnv.setSelected(false);
					print("Switch OFF ENV");

					EnvIrisPowerButton.setSelected(false);
				} else {
					EnvIrisPowerButton.setSelected(true);
				}

			} else {
			}
			slider_15.setValue(LastModeValues.WhiteIntensity);
			label_20.setText(Integer.toString(slider_15.getValue()) + " %");

			slider_16.setValue(LastModeValues.RedIntensity);
			label_21.setText(Integer.toString(slider_16.getValue()) + " %");

			slider_17.setValue(LastModeValues.GreenIntensity);
			label_22.setText(Integer.toString(slider_17.getValue()) + " %");

			slider_18.setValue(LastModeValues.BlueIntensity);
			label_23.setText(Integer.toString(slider_18.getValue()) + " %");

			slider_19.setValue(LastModeValues.ENVIntensity);
			label_24.setText(Integer.toString(slider_19.getValue()) + " %");

			slider_20.setValue(LastModeValues.IRIS1Intensity);
			label_25.setText(Integer.toString(slider_20.getValue()) + " %");

			slider_21.setValue(LastModeValues.IRIS2Intensity);
			label_26.setText(Integer.toString(slider_21.getValue()) + " %");

			slider_12.setValue(LastModeValues.PowerSupplyFanSpeed);
			label_5.setText(Integer.toString(slider_12.getValue()) + " %");

			slider_13.setValue(LastModeValues.HeatSinkFanSpeed);
			label_17.setText(Integer.toString(slider_13.getValue()) + " %");

			slider_14.setValue(LastModeValues.IRISFanSpeed);
			label_18.setText(Integer.toString(slider_14.getValue()) + " %");

			wl_critical_flag = false;
			env_iris_critical_flag = false;
			connect_flag = false;
			display("Values Restored Successfully");
			print("Out of restore mode option");
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public static void display(String str) {
		textArea.append(str);
	}

	public static void Update_ADC_Fields(ArrayList<Float> flt) {

		for (int i = 0; i < flt.size(); i++) {
			try {
				// print("ADC Value-" + Integer.toString(i+1) + " : " +
				// Float.toString(flt.get(i)));
				if (i == 0) {
					textField_10.setText(String.format("%.3f", flt.get(i)));
				} else if (i == 1) {
					textField_13.setText(String.format("%.3f", flt.get(i)));
				} else if (i == 2) {
					textField_16.setText(String.format("%.3f", flt.get(i)));
				} else if (i == 3) {
					textField_20.setText(String.format("%.3f", flt.get(i)));
				} else if (i == 4) {
					textField_8.setText(String.format("%.3f", flt.get(i)));
				} else if (i == 5) {
					textField_11.setText(String.format("%.3f", flt.get(i)));
				} else if (i == 6) {
					textField_14.setText(String.format("%.3f", flt.get(i)));
				} else if (i == 7) {
					textField_17.setText(String.format("%.3f", flt.get(i)));
				} else if (i == 8) {
					textField_21.setText(String.format("%.3f", flt.get(i)));
				} else if (i == 9) {
					textField_22.setText(String.format("%.3f", flt.get(i)));
				} else if (i == 10) {
					textField_23.setText(String.format("%.3f", flt.get(i)));
				} else if (i == 11) {
					textField_24.setText(String.format("%.3f", flt.get(i)));
				} else if (i == 12) {
					textField_9.setText(String.format("%.3f", flt.get(i)));
				} else if (i == 13) {
					textField_12.setText(String.format("%.3f", flt.get(i)));
				} else if (i == 14) {
					textField_15.setText(String.format("%.3f", flt.get(i)));
				} else if (i == 15) {
					textField_18.setText(String.format("%.3f", flt.get(i)));
				} else if (i == 16) {
					textField_27.setText(String.format("%.3f", flt.get(i)));
				} else if (i == 17) {
					textField_26.setText(String.format("%.3f", flt.get(i)));
				} else if (i == 18) {
					textField_25.setText(String.format("%.3f", flt.get(i)));
				}

			} catch (Exception ex) {
				ex.printStackTrace();
			}

			// numberFormatter.format(flt.get(i));

		}
	}

	// Function that Updates the Color Sensor Field Values
	public static void Update_Color_Sensor_Fields(ArrayList<Integer> data_buffer) {
		System.out.print("Size of the Buffer : ");
		System.out.println(data_buffer.size());
		try {

			print("Function Called");
			textField_28.setText("");
			textField_29.setText("");
			textField_30.setText("");
			textField_31.setText("");
			textField_32.setText("");
			textField_33.setText("");
			textField_34.setText("");
			textField_35.setText("");

			for (int i = 0; i < data_buffer.size(); i++) {

				System.out.println(HEX(data_buffer.get(i), 4));
				if (i == 0) {
					textField_28.setText(HEX(data_buffer.get(i), 4));
				} else if (i == 1) {
					textField_29.setText(HEX(data_buffer.get(i), 4));
				} else if (i == 2) {
					textField_30.setText(HEX(data_buffer.get(i), 4));
				} else if (i == 3) {
					textField_31.setText(HEX(data_buffer.get(i), 4));
				} else if (i == 4) {
					textField_32.setText(HEX(data_buffer.get(i), 4));
				} else if (i == 5) {
					textField_33.setText(HEX(data_buffer.get(i), 4));
				} else if (i == 6) {
					textField_34.setText(HEX(data_buffer.get(i), 4));
				} else if (i == 7) {
					textField_35.setText(HEX(data_buffer.get(i), 4));
				} else {
				}

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}// Function Update_Color_Sensor Ends here

	// Function that parses the given byte[] buffer and picks 4 bytes and converts
	// it into float
	public static ArrayList<Float> Get_Float_From_ByteBuffer(byte[] data_buffer) {
		print("ADC Count : " + Integer.toString(data_buffer.length));
		ArrayList<Float> float_buffer = new ArrayList<Float>();
		int count = 0;
		byte[] local_buffer = new byte[4];
		for (int i = 0; i < data_buffer.length; i++) {
			local_buffer[count] = data_buffer[i];

			count = count + 1;
			if (count == 4) {
				count = 0;
				float float_value = ByteBuffer.wrap(local_buffer).order(ByteOrder.LITTLE_ENDIAN).getFloat();
				float_buffer.add(float_value);

			}

		}

		return float_buffer;

	}

	public static ArrayList<Integer> Get_Integer_From_ByteBuffer(byte[] data_buffer) {
		print("Count of CS : " + Integer.toString(data_buffer.length));
		ArrayList<Integer> Integer_buffer = new ArrayList<Integer>();
		int count = 0;
		byte[] local_buffer = new byte[2];
		for (int i = 0; i < data_buffer.length; i++) {
			local_buffer[count] = data_buffer[i];

			count = count + 1;
			if (count == 2) {
				ByteBuffer wrapped = ByteBuffer.wrap(local_buffer).order(ByteOrder.LITTLE_ENDIAN); // big-endian by
																									// default
				int cs_value = wrapped.getShort();

				count = 0;

				Integer_buffer.add(cs_value);
			}

		}
		return Integer_buffer;

	}

	public static void Update_GPIO_Status(byte[] data_buffer) {
		for (int i = 0; i < data_buffer.length; i++) {
			if (i == 0) {
				if (data_buffer[i] == (byte) 0x1) {
					label_6.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/green2.png")));
				} else {
					label_6.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/red2.png")));
				}
			} else if (i == 1) {
				if (data_buffer[i] == (byte) 0x1) {
					label_7.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/green2.png")));
				} else {
					label_7.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/red2.png")));
				}
			} else if (i == 2) {
				if (data_buffer[i] == (byte) 0x1) {
					label_8.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/green2.png")));
				} else {
					label_8.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/red2.png")));
				}
			} else if (i == 3) {
				if (data_buffer[i] == (byte) 0x1) {
					lblNewLabel.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/green2.png")));
				} else {
					lblNewLabel.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/red2.png")));
				}
			} else if (i == 4) {
				if (data_buffer[i] == (byte) 0x1) {
					label_3.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/green2.png")));
				} else {
					label_3.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/red2.png")));
				}
			} else if (i == 5) {
				if (data_buffer[i] == (byte) 0x1) {
					label_4.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/green2.png")));
				} else {
					label_4.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/red2.png")));
				}
			} else if (i == 6) {
				if (data_buffer[i] == (byte) 0x1) {
					label_19.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/green2.png")));
				} else {
					label_19.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/red2.png")));
				}
			} else if (i == 7) {
				if (data_buffer[i] == (byte) 0x1) {
					label_9.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/green2.png")));
				} else {
					label_9.setIcon(new ImageIcon(Stryk_Demo.class.getResource("/resources/red2.png")));
				}
			} else {
			}
		}
	}

	// ################################################################################################################################
	/*
	 * Function Name : writebytestoserialport Input : A Byte Array, A boolean
	 * representing the enable or disable state of the serial port data listener
	 * Output : void
	 */
	// ################################################################################################################################

	public static void writebytestoserialport(byte[] serialbytes, boolean listener_flag) {
		if (serialport != null) {
			try {
				// ##System.out.println("Opening Serial Port...");

				serialport.openPort();
				if (serialport.isOpen() == true) {
					try {
						if (listener_flag) {
							setlistener(serialport);
							System.out.println("Listener Set");
						} else if (listener_flag == false) {
							serialport.removeDataListener();
						} else {
						}
						serialport.writeBytes(serialbytes, serialbytes.length);
						writeflag = true;
						System.out.println("Data Sent ");
						// #uart_response_timer.start();
					} catch (Exception ex) {
						writeflag = false;
						ex.printStackTrace();
					}

					// serialport.closePort();
				} else {

					textArea.append("Access to COM Port Denied!!. Port is in Access by Another Application!!\r\n\n");

					// serialport.closePort();
				}
			} catch (Exception ex) {
				writeflag = false;
				JOptionPane.showMessageDialog(frmStrykerVer,
						"Exception while opening the serial port to write bytes" + ex.toString(), "Error Message",
						JOptionPane.ERROR_MESSAGE);
			}
		} else {
			writeflag = false;
			JOptionPane.showMessageDialog(frmStrykerVer, "Invalid COM Port", "Erro Information",
					JOptionPane.ERROR_MESSAGE);
		}

	} // Function Ends Here

	// ################################################################################################################################
	/*
	 * Function Name : writebytestoserialport Input : A String to be written to
	 * serial port, Output : void
	 */
	// ################################################################################################################################

	public void writestringasbytestoserialport(String serialbytes) {
		// System.out.println("Inside write bytes Function\r\n");
		writeflag = false;
		null_charflag = false;
		timeoutflag = 0;
		if (serialport != null) {
			// System.out.println("Current Port " + serialport.getSystemPortName());
			try {
				// ##System.out.println("Opening Serial Port...");
				serialport.openPort();

				if (serialport.isOpen() == true) {
					// ##System.out.println("Port Opened Successfully");
					byte[] stringbytes = serialbytes.getBytes();
					setlistener(serialport);
					// ##System.out.println("Data Sent and Listener set");

					try {

						serialport.writeBytes(stringbytes, stringbytes.length);
						// #System.out.println("Serial Data Written : " + serialbytes);
						writeflag = true;
						// ##System.out.println("Write Flag Set...");
					} catch (Exception ex) {

						JOptionPane.showMessageDialog(frmStrykerVer,
								"Could not send : " + serialbytes + " command to serial port.", "Caution",
								JOptionPane.ERROR_MESSAGE);
					}
					// serialport.closePort();
				} else {

					// JOptionPane.showMessageDialog(frmStrykerVer, "Port Could
					// not be opened for communication. The port is either
					// disconnected or in use by another application!!","Error
					// Message",JOptionPane.ERROR_MESSAGE);
					textArea.append(
							"Access to COM Port denied!!. The port is either disconnected or in use by another application!!\r\n\n");
					textArea.update(textArea.getGraphics());
					// serialport.closePort();
				}
			} catch (Exception ex) {

				JOptionPane.showMessageDialog(frmStrykerVer,
						"Exception while opening to serial port to write string as bytes" + ex.toString(),
						"Error Message", JOptionPane.ERROR_MESSAGE);
			}
		} else {
			ShowMessage("Invalid COM Port!!");
			textArea.append("COM Port name cannot be Empty!!\r\n\n");

		}

	} // Function ends here

	public static void ShowMessage(final String message) {

		Thread t = new Thread(new Runnable() {

			public void run() {
				if (message != "") {
					print(message);
				} else {
					print("Hi");
				}

				@SuppressWarnings("unused")
				UIManager UI = new UIManager();
				UIManager.put("OptionPane.background", Color.WHITE);
				UIManager.put("Panel.background", Color.WHITE);
				UIManager.put("OptionPane.messagebackground", Color.WHITE);

				JOptionPane.showMessageDialog(frmStrykerVer, message, Error_Title, Error_Message);

			}
		});
		t.start();
	}

	public static void setlistener(final SerialPort serialport) {
		// print("Serial Port Listener Set");
		if (serialport != null) {

			serialport.addDataListener(new SerialPortDataListener() {

				@Override
				public int getListeningEvents() {
					// System.out.println("Getting the list of events");
					return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
				}

				@Override
				public void serialEvent(SerialPortEvent event) {

					if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE) {
						textArea.append("No response\r\n\n");
						return;
					}

					try {
						// System.out.println("AA");
						byte[] newData = new byte[serialport.bytesAvailable()];
						// System.out.println("A");
						numRead = serialport.readBytes(newData, newData.length);
						// System.out.println("B");
						total_bytes = total_bytes + numRead;
						full_buffer.write(newData, 0, numRead);
						print("Something data arrived in the serial port");
						if (future != null) {
							future.cancel(true);
						}

						future = scheduler.schedule(uart_recv_timeout_task, serial_read_byte_timeout,
								TimeUnit.MILLISECONDS);

						// #uart_response_timer.start();
					} catch (Exception ex) {
						ex.printStackTrace();

						return;
					}

				}

			});

		} else {
			System.out.println("No Serial ports found in the system");
		}
	}

	// A Common background worker that handles all the bg worker requests
	// ################################################################################################################################
	/*
	 * 
	 * Function Name : setup_bg_worker(String,Component,boolean,byte[]) Description
	 * : A Function that handles all the background events - Basically A Swing
	 * Worker Input : 1. A String representing the control name - "Connect Button"
	 * 2. A Component Object 3. A Boolean representing the serialport listener flag
	 * 4. A Byte Array Output : void
	 */
	// ################################################################################################################################

	public static void setup_bg_worker(final String control_name, final Component component,
			final boolean listener_flag, final byte[] sli) {
		bg_worker = new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() throws Exception {
				System.out.println("Listener Flag : " + listener_flag);
				try {
					if (component instanceof JSlider) {
						writebytestoserialport(sli, listener_flag);
					} else {
						writebytestoserialport(sli, listener_flag);
					}
					if (writeflag == true) {

						// ##textArea.append(control_name + " Command Sent" );
					} else {
						// textArea.append("Could not access COM Port. Check whether it is in used by
						// another application or Please Retry!!\r\n");

					}
				} catch (Exception ex) {

				}
				return null;

			}

			public void done() {
				if (component instanceof JSlider) {
					component.setEnabled(true);
				}
			}

		};
	}

	// ################################################################################################################################
	/*
	 * Function Name : writebytestoserialport Input : A Byte Array, A boolean
	 * representing the enable or disable state of the serial port data listener
	 * Output : void
	 */
	// ################################################################################################################################
	public static SwingWorker start_response_or_timeout_scheduler(final Component h, final String Cmd) {
		response_worker = new SwingWorker<List<String>, String>() {

			@Override
			protected List<String> doInBackground() throws Exception {

				// TODO Auto-generated method stub
				// This function is called when the timeout event occurs i.e, if at all no data
				// is received in the serial port
				final Runnable timeout_task_performer = new Runnable() {
					@Override
					public void run() {
						synchronized (lockObject) {
							timeoutflag = 1;
							lockObject.notifyAll();
						}

					}

				};
				final Runnable update_GUI_state = new Runnable() {
					@Override
					public void run() {
						synchronized (lockObject) {

							while (timeoutflag == 0 && notify_flag == 0) {
								if (h instanceof JSlider) {
									print("Slider");
									futuree.cancel(true);
									scheduler.shutdown();

									break;
								} else if (h instanceof JCheckBox) {
									print("CheckBox");
									futuree.cancel(true);
									scheduler.shutdown();
									break;
								}

								else {
									try {
										synchronized (lockObject) {
											lockObject.wait(10);
											print("Waiting...");
										}
									} catch (Exception e) {
										e.printStackTrace();
									}
								}

							}
							futuree.cancel(true);
							print("exited while LOPP");
						}
						// While ends here

						// No Data Received for sending command to device
						if (timeoutflag == 1 && notify_flag == 0) {
							serialport.openPort();
							if (serialport.isOpen() == true) {
								byte[] temp = null;
								int bytes_available = serialport.bytesAvailable();

								System.out.println("Total Bytes available at timeout : " + bytes_available);
								if (bytes_available > 0) {
									temp = new byte[bytes_available];
									serialport.readBytes(temp, bytes_available);
								}

								serialport.removeDataListener();
								// serialport.closePort();

							}
							timeoutflag = 0;
							print("Response Timedout. No data received in the Serial Port");

							if (!frmStrykerVer.isShowing()) {
								publish("RUN_BUTTON");

								ManufacturingTests.logs_logger.severe("Response Received\tResponse Timedout");
								ManufacturingTests.report_logger.severe("Result\t\t\tTimeout\r\n\n");
								ManufacturingTests.report_logger.severe(ManufacturingTests.ArrowDelimitter);
								ManufacturingTests.report_logger.severe(ManufacturingTests.ArrowDelimitter);

							} else {
								System.out.println("Handled Correctly");
							}

							if (h instanceof JButton) {
								if (h.getName() != "SYNC") {
									h.setEnabled(true);
								}

							}
							if (h instanceof JSlider) {
								if (h.isEnabled() == false) {
									h.setEnabled(true);
								}
							}

							// #scheduler.shutdownNow();
							if (Current_control == Connect) {
								publish("CONNECT_BUTTON");

							} else if (Current_control == Disconnect) {
								publish("DISCONNECT_BUTTON");

							} else if (Current_control == WL_PON) {
								publish("WL_PON");

							} else if (Current_control == WL_POFF) {
								publish("WL_POFF");

							} else if (Current_control == ENV_IRIS_ON) {
								publish("ENV_IRIS_ON");

							} else if (Current_control == ENV_IRIS_OFF) {
								publish("ENV_IRIS_OFF");

							}

							textArea.update(textArea.getGraphics());

							// textArea.append("Response Timedout for " + Cmd + " command!!\r\n\n");
							timeoutflag = 0;
						}
						// Valid Response received for sending command to the device
						else if (notify_flag == 1 && null_charflag == true) {
							ManufacturingTests.finish_button_flag = false;
							System.out.println("Error Free Data Received!!");
							notify_flag = 0;
							null_charflag = false;
							@SuppressWarnings("unused")
							bytedefinitions b = new bytedefinitions();
							// System.out.println("Valid Response in UART");
							if (expected_cmd_id != b.SENSING_COMMAND) {
								full_buffer.reset();
							} else if (expected_cmd_id != b.RESTORE_CMD) {
								full_buffer.reset();
							}
							process_serialport_response(expected_cmd_id);
							futuree.cancel(true);
							if (serialport.isOpen() == true) {
								serialport.closePort();
							}
						}

						// Data Received with some errors
						else if (notify_flag == 1 && null_charflag == false) {
							ManufacturingTests.finish_button_flag = false;
							notify_flag = 0;
							System.out.println("Error Response Received for sending " + Current_control
									+ " Command to the Device!!");
							timeoutflag = 0;
							full_buffer.reset();
							futuree.cancel(true);
							// #scheduler.shutdownNow();
							if (Current_control == Connect) {
								publish("CONNECT_BUTTON");

							} else if (Current_control == Disconnect) {
								publish("DISCONNECT_BUTTON");

							}
							if (Current_control == WL_PON) {
								publish("WL_PON");

							} else if (Current_control == WL_POFF) {
								publish("WL_POFF");

							} else if (Current_control == ENV_IRIS_ON) {
								publish("ENV_IRIS_ON");

							} else if (Current_control == ENV_IRIS_OFF) {
								publish("ENV_IRIS_OFF");

							}

							textArea.update(textArea.getGraphics());
							if (serialport.isOpen() == true) {
								serialport.closePort();
							}
						}
					}
				};

				closed_flag = true;

				// Check WriteFlag whether the data is sent to serialport successfully
				if (writeflag == true) {
					System.out.println("Write Flag Set");
					// Sets the timeout_task_performer to start exactly after
					// serial_read_cmd_timeout milliseconds

					print("Triggering Timer...");
					futuree = scheduler.schedule(timeout_task_performer, serial_read_cmd_timeout,
							TimeUnit.MILLISECONDS);
					print("Timer Triggered!!");

					publish("TIMERMFT\r\n");

					Thread appThread = new Thread() {
						public void run() {
							try {
								// Entry point of the function
								scheduler.schedule(update_GUI_state, 0, TimeUnit.MILLISECONDS);
								// SwingUtilities.invokeLater(update_GUI_state);
							} catch (Exception ex) {
								System.out.println("Some Error");
								ex.printStackTrace();
							}
						}
					};
					appThread.run();
				} else {
					System.out.println("WriteFlag is False");
				}
				return null;
			}

			protected void process(List<String> chunks) {
				for (String str : chunks) {
					if (str.equals("RUN_BUTTON")) {
						ManufacturingTests.btnStartButton.setText("RUN");
						ManufacturingTests.textArea.append("Response Received : Response Timedout\r\n");
						ManufacturingTests.UpdateTestResults(Timeout, "", "", "");
						synchronized (ManufacturingTests.lockObject) {
							ManufacturingTests.busyflag = false;
							ManufacturingTests.lockObject.notifyAll();
						}

					} else if (str.equals("TIMERMFT")) {
						ManufacturingTests.textArea.append("Timer Started\r\n");
					} else if (str.equals("CONNECT_BUTTON")) {
						btnConnect.setEnabled(true);
					} else if (str.equals("DISCONNECT_BUTTON")) {
						btnDisconnect.setEnabled(true);
					} else if (str.equals("WL_PON")) {
						wl_critical_flag = true;
						WlPowerButton.setSelected(false);
						wl_critical_flag = false;
					} else if (str.equals("WL_POFF")) {
						wl_critical_flag = true;
						WlPowerButton.setSelected(true);
						wl_critical_flag = false;
					} else if (str.equals("ENV_IRIS_ON")) {
						env_iris_critical_flag = true;
						EnvIrisPowerButton.setSelected(false);
						env_iris_critical_flag = false;
					} else if (str.equals("ENV_IRIS_OFF")) {
						env_iris_critical_flag = true;
						EnvIrisPowerButton.setEnabled(true);
						env_iris_critical_flag = false;
					}
				}
			}

		};
		response_worker.execute();
		// TODO Auto-generated method stub
		// This function is called when the timeout event occurs i.e, if at all no data
		// is received in the serial port
		return response_worker;

	} // Function "writebytestoserialport" Ends here

	// ################################################################################################################################
	/*
	 * Function Name : send_data_in_bg(String,Component,boolean,byte[]) Input : A
	 * Byte Array, A boolean representing the enable or disable state of the serial
	 * port data listener Output : void
	 */
	// ################################################################################################################################

	public static void send_data_in_bg(final String command_name, final Component component,
			final boolean listener_flag, final byte[] serialbytes) {
		synchronized (ManufacturingTests.lockObject) {
			ManufacturingTests.busyflag = true;
		}

		final Runnable my_thread = new Runnable() {
			@Override
			public void run() {
				if (command_name == Connect) {
					textArea.append("Trying to Connect...\r\n");
				} else if (command_name == Disconnect) {
					textArea.append("Trying to Disconnect...\r\n");
				} else {
					textArea.append("Sending " + command_name + " Command \r\n");
					// System.out.println("Sending " + command_name + " Command \r\n");
				}

				setup_bg_worker(command_name, component, listener_flag, serialbytes);

				bg_worker.execute();
				while (!bg_worker.isDone()) {
					// Wait until the Swing Worker is done. This is a negligible time period.
				}
				// If Write_flag == true, data is written successfully on to the serialport
				if (writeflag == true) {
					// Check whether to listen for incoming data - Interrupt
					if (listener_flag) {
						textArea.append(command_name + " Command Sent\r\nWaiting For Response...\r\n");
						// System.out.println(command_name + "Command Sent.\nWaiting for response");

						start_response_or_timeout_scheduler(component, command_name);
					}
					// No need to wait for incoming data
					else {
						serialport.removeDataListener();
						bytedefinitions b = new bytedefinitions();
						if (chckbxReset.isSelected()) {
							reset_ld_chckbx.clearSelection();
						}
						if (chckbxLoadDefaults.isSelected()) {
							destuff_buffer = null;
							print("Buffer Nullified");
							reset_ld_chckbx.clearSelection();
						}

						if (chckbxRed.isEnabled() && chckbxGreen.isEnabled() && chckbxBlue.isEnabled()) {
							if (chckbxRed.isSelected()) {
								print("Red selected");
								slider_16.setEnabled(true);
								label_21.setText(Integer.toString(slider_16.getValue()) + " %");
								label_21.setEnabled(true);

							}
							if (!chckbxRed.isSelected()) {
								print("Red Unselected");
								slider_16.setEnabled(false);
								label_21.setEnabled(false);
							}
							if (chckbxGreen.isSelected()) {
								print("Green selected");
								slider_17.setEnabled(true);
								label_22.setEnabled(true);

							}
							if (!chckbxGreen.isSelected()) {
								print("Green Unselected");
								slider_17.setEnabled(false);
								label_22.setEnabled(false);
							}
							if (chckbxBlue.isSelected()) {
								print("Blue selected");
								slider_18.setEnabled(true);
								label_23.setEnabled(true);
							}
							if (!chckbxBlue.isSelected()) {
								print("Blue Unselected");
								slider_18.setEnabled(false);
								label_23.setEnabled(false);
							}
						}
						if (chckbxIris1.isEnabled() && chckbxIris2.isEnabled()) {
							if (!chckbxIris1.isSelected()) {
								print("chckbx Iris1 Unselected");
								slider_20.setEnabled(false);
								label_25.setEnabled(false);

							}
							if (chckbxIris1.isSelected()) {
								slider_20.setEnabled(true);
								label_25.setEnabled(true);

							}
							if (!chckbxIris2.isSelected()) {
								print("chckbx Iris2 Unselected");
								slider_21.setEnabled(false);
								label_26.setEnabled(false);
							}
							if (chckbxIris2.isSelected()) {
								slider_21.setEnabled(true);
								label_26.setEnabled(true);
							}
						}

						textArea.append(command_name + " Command Sent Successfully.\r\n\n");
						if (Current_control == RESET_uC2) {
							process_serialport_response(b.DISCONNECT_COMMAND);
							full_buffer.reset();
							if (destuff_buffer != null) {
								for (int i = 0; i < destuff_buffer.length; i++) {
									destuff_buffer[i] = 0;
								}
							}
							if (recv_buff != null) {
								for (int i = 0; i < recv_buff.length; i++) {
									recv_buff[i] = 0;
								}
							}
						}
					}
				} else {
					textArea.append(command_name + " Command was not sent!!\r\n\n");
				}

			}

		};
		Thread appThread = new Thread() {
			@Override
			public void run() {
				try {
					SwingUtilities.invokeLater(my_thread);
				} catch (Exception ex) {
					System.out.println("Error Occurred while calling SwingUtility Commmand for Connect Button");
					ex.printStackTrace();
				}
			}
		};
		appThread.run();

	}// Send_data_in_bg ends here

	public long getBtnConnectMultiClickThreshhold() {
		return btnConnect.getMultiClickThreshhold();
	}

	public void setBtnConnectMultiClickThreshhold(long multiClickThreshhold) {
		btnConnect.setMultiClickThreshhold(multiClickThreshhold);
	}

	// ################################################################################################################################
	/*
	 * Function Name :
	 * Construct_and_send_IPCPacket(String,ArrayList<Byte>,byte,byte,byte,byte,byte,
	 * byte) Description : Function that constructs the IPC Packet, based on the
	 * input values, which has to be sent to UC2 Output : void
	 */
	// ################################################################################################################################
	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	public static void construct_and_send_IPCPacket(String source, ArrayList<Byte> w, byte LIGHT_SOURCE, byte STATE,
			byte MODE, byte PWM_SOURCE, byte PWM_TYPE, byte TYPE_VAL) {
		// print("Source : "+source);
		byte[] payload_length = new byte[2];
		byte[] payload_byte = new byte[2];
		int chk_sum;
		byte[] chksum = new byte[2];

		// IPC Command Packet for Connect Command
		if (source == Connect) {
			bytedefinitions b = new bytedefinitions();
			w.add(b.MSG_TYPE_CMD);
			w.add((byte) b.CONNECT_COMMAND);
			// For Pay load Length Bytes
			payload_length[0] = (byte) (0x0 & 0xff); // NO Payload
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);

			// Add Pay load Bytes
			w.add(payload_length[0]); // LSB
			w.add(payload_length[1]); // MSB

			// Add Reserved Field Bytes
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte); // add end byte at the end of the ArrayList

			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);

			// Send data frame to serial port, trigger timer and wait for a response
			try {

				send_data_in_bg(Current_control, btnConnect, true, serial_bytes);

			} catch (Exception ex) {
				print("Exception While Sending Connect Command!!");
				ex.printStackTrace();
			}

		}
		// IPC Command Packet for Disconnect Command
		else if (source == Disconnect) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);
			w.add((byte) b.DISCONNECT_COMMAND);
			payload_length[0] = (byte) (0x0 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);

			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList
			w.add(0, b.START_byte);
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, btnDisconnect, true, serial_bytes);
		}

		// IPC Command Packet for Switching ON White Light
		else if (source == WL_PON) {

			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);
			w.add((byte) b.MODE_COMMAND);
			payload_length[0] = (byte) (0x6 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);
			w.add(MODE);
			w.add(PWM_SOURCE);
			w.add(PWM_TYPE);
			w.add(TYPE_VAL);
			// #print(Integer.toString(w.get(13)));

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList
			w.add(0, b.START_byte);
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, WlPowerButton, true, serial_bytes);
		}

		// IPC Command Packet for switching OFF White Light
		else if (source == WL_POFF) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);
			w.add((byte) b.MODE_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {

				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);
			w.add(MODE);
			w.add(PWM_SOURCE);
			w.add(PWM_TYPE);
			w.add(TYPE_VAL);
			// #print(Integer.toString(w.get(13)));

			// #print("Size : " + Integer.toString(w.size()));

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList
			w.add(0, b.START_byte);
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, WlPowerButton, true, serial_bytes);
		}

		// IPC Command Packet for Setting White Light Slider value
		else if (source == GRP_SLIDER) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.INTENSITY_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);
			// #print(Integer.toString(w.get(13)));

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, slider_15, false, serial_bytes);
		}

		// IPC Command Packet for Enabling Red LED & it's slider
		// Enable - 1
		// Disable - 0
		else if (source == RED_SLIDER_EN) {
			print("RED ENABLE");
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.ENADIS_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);
			// #print(Integer.toString(w.get(13)));

			// #print("Size : " + Integer.toString(w.size()));

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList

			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, chckbxRed, false, serial_bytes);
		}
		// IPC Command Packet for Disabling Red LED and it's Slider
		// Enable - 1
		// Disable - 0
		else if (source == RED_SLIDER_DIS) {
			print("RED DISABLE");
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.ENADIS_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);
			// #print(Integer.toString(w.get(13)));

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, chckbxRed, false, serial_bytes);
		}

		// IPC Command Packet for Enabling Green LED and it's Slider
		// Enable - 1
		// Disable - 0
		else if (source == GREEN_SLIDER_EN) {
			print("GREEN ENABLE");
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.ENADIS_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);
			// #print(Integer.toString(w.get(13)));

			// #print("Size : " + Integer.toString(w.size()));

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, chckbxGreen, false, serial_bytes);
		}
		// IPC Command Packet for Disabling Green LED and it's Slider
		// Enable - 1
		// Disable - 0
		else if (source == GREEN_SLIDER_DIS) {
			print("GREEN DISABLE");
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.ENADIS_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);
			// #print(Integer.toString(w.get(13)));

			// #print("Size : " + Integer.toString(w.size()));

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, chckbxGreen, false, serial_bytes);
		}
		// IPC Command Packet for Enabling Blue LED and it's Slider
		// Enable - 1
		// Disable - 0
		else if (source == BLUE_SLIDER_EN) {
			print("BLUE ENABLE");
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.ENADIS_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList

			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, chckbxBlue, false, serial_bytes);
		}
		// IPC Command Packet for Disabling Blue LED and it's Slider
		// Enable - 1
		// Disable - 0
		else if (source == BLUE_SLIDER_DIS) {
			print("BLUE DISABLE");
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.ENADIS_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);
			// #print(Integer.toString(w.get(13)));
			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, chckbxBlue, false, serial_bytes);
		}

		// IPC Command Packet for Resetting uC1
		// Enable - 1
		// Disable - 0
		else if (source == RESET_uC1) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.RESET_uC_COMMAND);
			payload_length[0] = (byte) (0x1 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}

			w.add(LIGHT_SOURCE);
			// #print("Size : " + Integer.toString(w.size()));

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(source, chckbxReset, false, serial_bytes);

		} else if (source == RESET_uC2) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.RESET_uC_COMMAND);
			payload_length[0] = (byte) (0x1 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}

			w.add(LIGHT_SOURCE);
			// #print("Size : " + Integer.toString(w.size()));

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(source, chckbxLoadDefaults, false, serial_bytes);

		} else if (source == LOAD_DEF1) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.LOAD_DEFAULT_COMMAND);
			payload_length[0] = (byte) (0x1 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}

			w.add(LIGHT_SOURCE);
			// #print("Size : " + Integer.toString(w.size()));

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(source, chckbxReset, false, serial_bytes);
		} else if (source == LOAD_DEF2) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.LOAD_DEFAULT_COMMAND);
			payload_length[0] = (byte) (0x1 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}

			w.add(LIGHT_SOURCE);
			// #print("Size : " + Integer.toString(w.size()));

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(source, chckbxLoadDefaults, false, serial_bytes);
		} else if (source == SAVE) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);
			w.add((byte) b.SAVE_COMMAND);
			// No Payload Length for Save Command
			payload_length[0] = (byte) (0x0 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(source, btnSave, true, serial_bytes);
		} else if (source == LAST) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);
			w.add((byte) b.RESTORE_CMD);
			// No Payload Length for Save Command
			payload_length[0] = (byte) (0x0 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(source, btnLastMode, true, serial_bytes);
		} else if (source == RED_SLIDER) {
			print("RedWhale");
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.INTENSITY_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);
			// #print(Integer.toString(w.get(13)));

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, slider_16, false, serial_bytes);
		} else if (source == GREEN_SLIDER) {
			print("GreenWhale");
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.INTENSITY_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, slider_17, false, serial_bytes);
		} else if (source == BLUE_SLIDER) {
			print("BlueWhale");
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.INTENSITY_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, slider_18, false, serial_bytes);
		} else if (source == POWER_SUPPLY_SLIDER) {
			print("PowerSupply");
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.INTENSITY_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, slider_12, false, serial_bytes);
		} else if (source == HEATSINK_SLIDER) {
			print("HSINK");
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.INTENSITY_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, slider_13, false, serial_bytes);
		} else if (source == IRIS_FAN_SLIDER) {
			print("IRISFan");
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.INTENSITY_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, slider_14, false, serial_bytes);
		} else if (source == ENV_IRIS_ON) {
			print("ENV PON Command");
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);
			w.add((byte) b.MODE_COMMAND);
			payload_length[0] = (byte) (0x6 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);
			w.add(MODE);
			w.add(PWM_SOURCE);
			w.add(PWM_TYPE);
			w.add(TYPE_VAL);

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, EnvIrisPowerButton, true, serial_bytes);
		} else if (source == ENV_IRIS_OFF) {
			print("Env_IRIS_OFF");
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);
			w.add((byte) b.MODE_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {

				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);
			w.add(MODE);
			w.add(PWM_SOURCE);
			w.add(PWM_TYPE);
			w.add(TYPE_VAL);
			// #print(Integer.toString(w.get(13)));

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, EnvIrisPowerButton, true, serial_bytes);
		} else if (source == "IRIS_PON") {
			print("IRIS_PON");
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);
			w.add((byte) b.MODE_COMMAND);
			payload_length[0] = (byte) (0x6 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);
			w.add(MODE);
			w.add(PWM_SOURCE);
			w.add(PWM_TYPE);
			w.add(TYPE_VAL);

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, EnvIrisPowerButton, false, serial_bytes);
		} else if (source == "IRIS_POFF") {
			print("IRIS_POFF");
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);
			w.add((byte) b.MODE_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {

				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);
			w.add(MODE);
			w.add(PWM_SOURCE);
			w.add(PWM_TYPE);
			w.add(TYPE_VAL);

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, EnvIrisPowerButton, false, serial_bytes);
		} else if (source == ENV_SLIDER) {
			print("Env Slider");
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.INTENSITY_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size())); // Stuff the buffer
			// Buffer contents for stuffing - buffer without start byte and end byte
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear(); // Clear the Old ArrayList
			w = new ArrayList(Stuffed_Buffer); // populate it with the stuffed ArrayList

			w.add(0, b.START_byte); // add start byte at the 0'th location of the ArrayList
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, slider_19, false, serial_bytes);
		} else if (source == IRIS_1_EN) {

			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.ENADIS_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);// Has Payload
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);

			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size()));
			// #LOGprint("###################Stuffed Buffer###################");
			// #LOGfor(byte by : Stuffed_Buffer)
			// #LOG{
			// #LOG print(Hex(by,2));
			// #LOG}
			w.clear();
			w = new ArrayList(Stuffed_Buffer);
			w.add(0, b.START_byte);
			w.add(b.END_byte);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, chckbxIris1, false, serial_bytes);
		} else if (source == IRIS_2_EN) {
			print("IRIS2_EN");
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.ENADIS_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);// Has Payload
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);

			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size()));
			// #LOGprint("###################Stuffed Buffer###################");
			// #LOGfor(byte by : Stuffed_Buffer)
			// #LOG{
			// #LOG print(Hex(by,2));
			// #LOG}
			w.clear();
			w = new ArrayList(Stuffed_Buffer);
			w.add(0, b.START_byte);
			w.add(b.END_byte);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, chckbxIris2, false, serial_bytes);
		} else if (source == IRIS_1_DIS) {
			print("IRIS1_DIS");
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.ENADIS_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);// Has Payload
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size()));
			// #LOGprint("###################Stuffed Buffer###################");
			// #LOGfor(byte by : Stuffed_Buffer)
			// #LOG{
			// #LOG print(Hex(by,2));
			// #LOG}
			w.clear();
			w = new ArrayList(Stuffed_Buffer);
			w.add(0, b.START_byte);
			w.add(b.END_byte);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, chckbxIris2, false, serial_bytes);
		} else if (source == IRIS_2_DIS) {
			print("IRIS2_DIS");
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.ENADIS_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);// #Has Payload
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);

			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size()));
			// #LOGprint("###################Stuffed Buffer###################");
			// #LOGfor(byte by : Stuffed_Buffer)
			// #LOG{
			// #LOG print(Hex(by,2));
			// #LOG}
			w.clear();
			w = new ArrayList(Stuffed_Buffer);
			w.add(0, b.START_byte);
			w.add(b.END_byte);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, chckbxIris2, false, serial_bytes);
		} else if (source == IRIS1_SLIDER) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.INTENSITY_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);// This Function has payload
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);

			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size()));
			// #LOGprint("###################Stuffed Buffer###################");
			// #LOGfor(byte by : Stuffed_Buffer)
			// #LOG{
			// #LOG print(Hex(by,2));
			// #LOG}
			w.clear();
			w = new ArrayList(Stuffed_Buffer);
			w.add(0, b.START_byte);
			w.add(b.END_byte);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, slider_20, false, serial_bytes);
		} else if (source == IRIS2_SLIDER) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.INTENSITY_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff);// This Function has payload
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);

			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size()));
			// #LOGprint("###################Stuffed Buffer###################");
			// #LOGfor(byte by : Stuffed_Buffer)
			// #LOG{
			// #LOG print(Hex(by,2));
			// #LOG}
			w.clear();
			w = new ArrayList(Stuffed_Buffer);
			w.add(0, b.START_byte);
			w.add(b.END_byte);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, slider_21, false, serial_bytes);
		} else if (source == IRIS_POWER_SHUTDOWN) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_NOTIFY);
			w.add((byte) b.ENADIS_COMMAND);
			payload_length[0] = (byte) (0x2 & 0xff); // This Function Has Payload
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}
			// w.add(b.TYPE_PWM);
			w.add(LIGHT_SOURCE);
			w.add(STATE);

			chk_sum = b.calculateCRC(w.subList(0, w.size()));
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);

			// An Empty Buffer to Store the Stuffed Version of the input buffer
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer - Buffer that contains MSG_TYPE to
			// CHECKSUM

			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size()));
			// #LOGprint("###################Stuffed Buffer###################");
			// #LOGfor(byte by : Stuffed_Buffer)
			// #LOG{
			// #LOG print(Hex(by,2));
			// #LOG}
			w.clear();
			w = new ArrayList(Stuffed_Buffer);
			w.add(0, b.START_byte);
			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, chckbxIrisPowerShutdown, false, serial_bytes);
		} else if (source == READ_ALL) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);

			w.add((byte) b.SENSING_COMMAND);
			payload_length[0] = (byte) (0x0 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}

			// List to hold the bytes except StartByte and End Byte - For CheckSum
			// Calculation

			// Get the sublist into a collection (Buffer without Start and End Byte)
			// w.subList(1, w.size());//1-MsgType Upto w.Size() - Omits Start Byte
			//////////////// Buffer//////////////////////////////////////////////////////////////////////////////
			//////// |MSG_TYPE|CMD_ID|PAYLOADLEN-LOW|PAYLOADLEN-HIGH|RESFIELD0|RESFIELD1|RESFIELD2|PAYLOAD|//////
			////////////////////////////////////////////////////////////////////////////////////////////////////
			// Variable that stores the above values
			chk_sum = b.calculateCRC(w.subList(0, w.size())); // Variable that holds the checkSum
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			// List<Byte> d = new ArrayList<Byte>();
			// d = w.subList(1, w.size());
			// LOG$print("##############Actual Buffer####################");
			// LOG$for(byte byt : d)
			// LOG${
			// LOG$print(Hex(byt,2));
			// LOG$}

			// An Empty Buffer to Store the Stuffed Version of the input buffer
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size()));
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear();
			w = new ArrayList(Stuffed_Buffer);
			w.add(0, b.START_byte);

			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// LOG$Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, btnReadAll, true, serial_bytes);
		}
		// Additional checks included for Manufacturing Tests
		else if (source == VERSION_NUMBER_CHECK) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);

			w.add((byte) b.VERSION_NO_TEST_COMMAND);
			payload_length[0] = (byte) (0x0 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}

			// List to hold the bytes except StartByte and End Byte - For CheckSum
			// Calculation

			// Get the sublist into a collection (Buffer without Start and End Byte)
			// w.subList(1, w.size());//1-MsgType Upto w.Size() - Omits Start Byte
			//////////////// Buffer//////////////////////////////////////////////////////////////////////////////
			//////// |MSG_TYPE|CMD_ID|PAYLOADLEN-LOW|PAYLOADLEN-HIGH|RESFIELD0|RESFIELD1|RESFIELD2|PAYLOAD|//////
			////////////////////////////////////////////////////////////////////////////////////////////////////
			// Variable that stores the above values
			chk_sum = b.calculateCRC(w.subList(0, w.size())); // Variable that holds the checkSum
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			// List<Byte> d = new ArrayList<Byte>();
			// d = w.subList(1, w.size());
			// LOG$print("##############Actual Buffer####################");
			// LOG$for(byte byt : d)
			// LOG${
			// LOG$print(Hex(byt,2));
			// LOG$}

			// An Empty Buffer to Store the Stuffed Version of the input buffer
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size()));
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear();
			w = new ArrayList(Stuffed_Buffer);
			w.add(0, b.START_byte);

			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// LOG$Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, null, true, serial_bytes);
		} else if (source == IPC_CHECK) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);

			w.add((byte) b.IPC_TEST_COMMAND);
			payload_length[0] = (byte) (0x0 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}

			// List to hold the bytes except StartByte and End Byte - For CheckSum
			// Calculation

			// Get the sublist into a collection (Buffer without Start and End Byte)
			// w.subList(1, w.size());//1-MsgType Upto w.Size() - Omits Start Byte
			//////////////// Buffer//////////////////////////////////////////////////////////////////////////////
			//////// |MSG_TYPE|CMD_ID|PAYLOADLEN-LOW|PAYLOADLEN-HIGH|RESFIELD0|RESFIELD1|RESFIELD2|PAYLOAD|//////
			////////////////////////////////////////////////////////////////////////////////////////////////////
			// Variable that stores the above values
			chk_sum = b.calculateCRC(w.subList(0, w.size())); // Variable that holds the checkSum
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			// List<Byte> d = new ArrayList<Byte>();
			// d = w.subList(1, w.size());
			// LOG$print("##############Actual Buffer####################");
			// LOG$for(byte byt : d)
			// LOG${
			// LOG$print(Hex(byt,2));
			// LOG$}

			// An Empty Buffer to Store the Stuffed Version of the input buffer
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size()));
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear();
			w = new ArrayList(Stuffed_Buffer);
			w.add(0, b.START_byte);

			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// LOG$Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, null, true, serial_bytes);
		} else if (source == EEPROM_CHECK) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);

			w.add((byte) b.EEPROM_TEST_COMMAND);
			payload_length[0] = (byte) (0x0 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}

			// List to hold the bytes except StartByte and End Byte - For CheckSum
			// Calculation

			// Get the sublist into a collection (Buffer without Start and End Byte)
			// w.subList(1, w.size());//1-MsgType Upto w.Size() - Omits Start Byte
			//////////////// Buffer//////////////////////////////////////////////////////////////////////////////
			//////// |MSG_TYPE|CMD_ID|PAYLOADLEN-LOW|PAYLOADLEN-HIGH|RESFIELD0|RESFIELD1|RESFIELD2|PAYLOAD|//////
			////////////////////////////////////////////////////////////////////////////////////////////////////
			// Variable that stores the above values
			chk_sum = b.calculateCRC(w.subList(0, w.size())); // Variable that holds the checkSum
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			// List<Byte> d = new ArrayList<Byte>();
			// d = w.subList(1, w.size());
			// LOG$print("##############Actual Buffer####################");
			// LOG$for(byte byt : d)
			// LOG${
			// LOG$print(Hex(byt,2));
			// LOG$}

			// An Empty Buffer to Store the Stuffed Version of the input buffer
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size()));
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear();
			w = new ArrayList(Stuffed_Buffer);
			w.add(0, b.START_byte);

			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// LOG$Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, null, true, serial_bytes);
		} else if (source == FLASH_CHECK) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);

			w.add((byte) b.FLASH_TEST_COMMAND);
			payload_length[0] = (byte) (0x0 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}

			// List to hold the bytes except StartByte and End Byte - For CheckSum
			// Calculation

			// Get the sublist into a collection (Buffer without Start and End Byte)
			// w.subList(1, w.size());//1-MsgType Upto w.Size() - Omits Start Byte
			//////////////// Buffer//////////////////////////////////////////////////////////////////////////////
			//////// |MSG_TYPE|CMD_ID|PAYLOADLEN-LOW|PAYLOADLEN-HIGH|RESFIELD0|RESFIELD1|RESFIELD2|PAYLOAD|//////
			////////////////////////////////////////////////////////////////////////////////////////////////////
			// Variable that stores the above values
			chk_sum = b.calculateCRC(w.subList(0, w.size())); // Variable that holds the checkSum
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			// List<Byte> d = new ArrayList<Byte>();
			// d = w.subList(1, w.size());
			// LOG$print("##############Actual Buffer####################");
			// LOG$for(byte byt : d)
			// LOG${
			// LOG$print(Hex(byt,2));
			// LOG$}

			// An Empty Buffer to Store the Stuffed Version of the input buffer
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size()));
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear();
			w = new ArrayList(Stuffed_Buffer);
			w.add(0, b.START_byte);

			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// LOG$Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, null, true, serial_bytes);
		} else if (source == UART_CHECK) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);

			w.add((byte) b.UART_TEST_COMMAND);
			payload_length[0] = (byte) (0x0 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}

			// List to hold the bytes except StartByte and End Byte - For CheckSum
			// Calculation

			// Get the sublist into a collection (Buffer without Start and End Byte)
			// w.subList(1, w.size());//1-MsgType Upto w.Size() - Omits Start Byte
			//////////////// Buffer//////////////////////////////////////////////////////////////////////////////
			//////// |MSG_TYPE|CMD_ID|PAYLOADLEN-LOW|PAYLOADLEN-HIGH|RESFIELD0|RESFIELD1|RESFIELD2|PAYLOAD|//////
			////////////////////////////////////////////////////////////////////////////////////////////////////
			// Variable that stores the above values
			chk_sum = b.calculateCRC(w.subList(0, w.size())); // Variable that holds the checkSum
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			// List<Byte> d = new ArrayList<Byte>();
			// d = w.subList(1, w.size());
			// LOG$print("##############Actual Buffer####################");
			// LOG$for(byte byt : d)
			// LOG${
			// LOG$print(Hex(byt,2));
			// LOG$}

			// An Empty Buffer to Store the Stuffed Version of the input buffer
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size()));
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear();
			w = new ArrayList(Stuffed_Buffer);
			w.add(0, b.START_byte);

			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// LOG$Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, null, true, serial_bytes);
		} else if (source == FAN_CHECK) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);

			w.add((byte) b.FAN_TEST_COMMAND);
			payload_length[0] = (byte) (0x0 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}

			// List to hold the bytes except StartByte and End Byte - For CheckSum
			// Calculation

			// Get the sublist into a collection (Buffer without Start and End Byte)
			// w.subList(1, w.size());//1-MsgType Upto w.Size() - Omits Start Byte
			//////////////// Buffer//////////////////////////////////////////////////////////////////////////////
			//////// |MSG_TYPE|CMD_ID|PAYLOADLEN-LOW|PAYLOADLEN-HIGH|RESFIELD0|RESFIELD1|RESFIELD2|PAYLOAD|//////
			////////////////////////////////////////////////////////////////////////////////////////////////////
			// Variable that stores the above values
			chk_sum = b.calculateCRC(w.subList(0, w.size())); // Variable that holds the checkSum
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			// List<Byte> d = new ArrayList<Byte>();
			// d = w.subList(1, w.size());
			// LOG$print("##############Actual Buffer####################");
			// LOG$for(byte byt : d)
			// LOG${
			// LOG$print(Hex(byt,2));
			// LOG$}

			// An Empty Buffer to Store the Stuffed Version of the input buffer
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size()));
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear();
			w = new ArrayList(Stuffed_Buffer);
			w.add(0, b.START_byte);

			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// LOG$Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, null, true, serial_bytes);
		} else if (source == GUI_CHECK) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);

			w.add((byte) b.GUI_TEST_COMMAND);
			payload_length[0] = (byte) (0x0 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}

			// List to hold the bytes except StartByte and End Byte - For CheckSum
			// Calculation

			// Get the sublist into a collection (Buffer without Start and End Byte)
			// w.subList(1, w.size());//1-MsgType Upto w.Size() - Omits Start Byte
			//////////////// Buffer//////////////////////////////////////////////////////////////////////////////
			//////// |MSG_TYPE|CMD_ID|PAYLOADLEN-LOW|PAYLOADLEN-HIGH|RESFIELD0|RESFIELD1|RESFIELD2|PAYLOAD|//////
			////////////////////////////////////////////////////////////////////////////////////////////////////
			// Variable that stores the above values
			chk_sum = b.calculateCRC(w.subList(0, w.size())); // Variable that holds the checkSum
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			// List<Byte> d = new ArrayList<Byte>();
			// d = w.subList(1, w.size());
			// LOG$print("##############Actual Buffer####################");
			// LOG$for(byte byt : d)
			// LOG${
			// LOG$print(Hex(byt,2));
			// LOG$}

			// An Empty Buffer to Store the Stuffed Version of the input buffer
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size()));
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear();
			w = new ArrayList(Stuffed_Buffer);
			w.add(0, b.START_byte);

			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// LOG$Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, null, true, serial_bytes);
		} else if (source == IRIS_CHECK) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);

			w.add((byte) b.IRIS_TEST_COMMAND);
			payload_length[0] = (byte) (0x0 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}

			// List to hold the bytes except StartByte and End Byte - For CheckSum
			// Calculation

			// Get the sublist into a collection (Buffer without Start and End Byte)
			// w.subList(1, w.size());//1-MsgType Upto w.Size() - Omits Start Byte
			//////////////// Buffer//////////////////////////////////////////////////////////////////////////////
			//////// |MSG_TYPE|CMD_ID|PAYLOADLEN-LOW|PAYLOADLEN-HIGH|RESFIELD0|RESFIELD1|RESFIELD2|PAYLOAD|//////
			////////////////////////////////////////////////////////////////////////////////////////////////////
			// Variable that stores the above values
			chk_sum = b.calculateCRC(w.subList(0, w.size())); // Variable that holds the checkSum
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			// List<Byte> d = new ArrayList<Byte>();
			// d = w.subList(1, w.size());
			// LOG$print("##############Actual Buffer####################");
			// LOG$for(byte byt : d)
			// LOG${
			// LOG$print(Hex(byt,2));
			// LOG$}

			// An Empty Buffer to Store the Stuffed Version of the input buffer
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size()));
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear();
			w = new ArrayList(Stuffed_Buffer);
			w.add(0, b.START_byte);

			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// LOG$Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, null, true, serial_bytes);
		} else if (source == RGBL_CHECK) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);

			w.add((byte) b.RGBL_TEST_COMMAND);
			payload_length[0] = (byte) (0x0 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}

			// List to hold the bytes except StartByte and End Byte - For CheckSum
			// Calculation

			// Get the sublist into a collection (Buffer without Start and End Byte)
			// w.subList(1, w.size());//1-MsgType Upto w.Size() - Omits Start Byte
			//////////////// Buffer//////////////////////////////////////////////////////////////////////////////
			//////// |MSG_TYPE|CMD_ID|PAYLOADLEN-LOW|PAYLOADLEN-HIGH|RESFIELD0|RESFIELD1|RESFIELD2|PAYLOAD|//////
			////////////////////////////////////////////////////////////////////////////////////////////////////
			// Variable that stores the above values
			chk_sum = b.calculateCRC(w.subList(0, w.size())); // Variable that holds the checkSum
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			// List<Byte> d = new ArrayList<Byte>();
			// d = w.subList(1, w.size());
			// LOG$print("##############Actual Buffer####################");
			// LOG$for(byte byt : d)
			// LOG${
			// LOG$print(Hex(byt,2));
			// LOG$}

			// An Empty Buffer to Store the Stuffed Version of the input buffer
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size()));
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear();
			w = new ArrayList(Stuffed_Buffer);
			w.add(0, b.START_byte);

			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// LOG$Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, null, true, serial_bytes);
		} else if (source == COLOR_SENSOR_CHECK) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);
			w.add((byte) b.COLOR_SENSOR_COMMAND);
			payload_length[0] = (byte) (0x0 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}

			// List to hold the bytes except StartByte and End Byte - For CheckSum
			// Calculation

			// Get the sublist into a collection (Buffer without Start and End Byte)
			// w.subList(1, w.size());//1-MsgType Upto w.Size() - Omits Start Byte
			//////////////// Buffer//////////////////////////////////////////////////////////////////////////////
			//////// |MSG_TYPE|CMD_ID|PAYLOADLEN-LOW|PAYLOADLEN-HIGH|RESFIELD0|RESFIELD1|RESFIELD2|PAYLOAD|//////
			////////////////////////////////////////////////////////////////////////////////////////////////////
			// Variable that stores the above values
			chk_sum = b.calculateCRC(w.subList(0, w.size())); // Variable that holds the checkSum
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			// List<Byte> d = new ArrayList<Byte>();
			// d = w.subList(1, w.size());
			// LOG$print("##############Actual Buffer####################");
			// LOG$for(byte byt : d)
			// LOG${
			// LOG$print(Hex(byt,2));
			// LOG$}

			// An Empty Buffer to Store the Stuffed Version of the input buffer
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size()));
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear();
			w = new ArrayList(Stuffed_Buffer);
			w.add(0, b.START_byte);

			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// LOG$Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, null, true, serial_bytes);
		} else if (source == PD_CHECK) {
			bytedefinitions b = new bytedefinitions();

			w.add(b.MSG_TYPE_CMD);

			w.add((byte) b.PD_TEST_COMMAND);
			payload_length[0] = (byte) (0x0 & 0xff);
			payload_length[1] = (byte) ((0x0 >> 8) & 0xff);
			w.add(payload_length[0]);
			w.add(payload_length[1]);
			for (int i = 0; i < b.RESERVED_FIELDS.length; i++) {
				w.add(b.RESERVED_FIELDS[i]);
			}

			// List to hold the bytes except StartByte and End Byte - For CheckSum
			// Calculation

			// Get the sublist into a collection (Buffer without Start and End Byte)
			// w.subList(1, w.size());//1-MsgType Upto w.Size() - Omits Start Byte
			//////////////// Buffer//////////////////////////////////////////////////////////////////////////////
			//////// |MSG_TYPE|CMD_ID|PAYLOADLEN-LOW|PAYLOADLEN-HIGH|RESFIELD0|RESFIELD1|RESFIELD2|PAYLOAD|//////
			////////////////////////////////////////////////////////////////////////////////////////////////////
			// Variable that stores the above values
			chk_sum = b.calculateCRC(w.subList(0, w.size())); // Variable that holds the checkSum
			chksum[0] = (byte) (chk_sum & 0xff);
			chksum[1] = (byte) ((chk_sum >> 8) & 0xff);
			w.add(chksum[0]);
			w.add(chksum[1]);
			// List<Byte> d = new ArrayList<Byte>();
			// d = w.subList(1, w.size());
			// LOG$print("##############Actual Buffer####################");
			// LOG$for(byte byt : d)
			// LOG${
			// LOG$print(Hex(byt,2));
			// LOG$}

			// An Empty Buffer to Store the Stuffed Version of the input buffer
			List<Byte> Stuffed_Buffer = new ArrayList<Byte>();
			// Function to Stuff the Actual Buffer
			Stuffed_Buffer = Stuff_Packet(w.subList(0, w.size()));
			// LOG$print("###################Stuffed Buffer###################");
			// LOG$for(byte by : Stuffed_Buffer)
			// LOG${
			// LOG$ print(Hex(by,2));
			// LOG$}
			w.clear();
			w = new ArrayList(Stuffed_Buffer);
			w.add(0, b.START_byte);

			w.add(b.END_byte);
			// #print_al(w);
			byte[] serial_bytes = al_to_byte(w);
			// LOG$Send data frame to serialport and wait for a response
			send_data_in_bg(Current_control, null, true, serial_bytes);
		} else {
			print("Some Other Command");
		}
		// print("Source : "+source);

	}

	// Function that returns the Hex Value of the given byte.
	// Input - Byte, places
	// Output - Formatted String
	// Example
	// Input byte = 0xEE; Places = 2; Output = 0xEE
	// Input byte = 0xEE; Places : 4 - Output : 0xFFEE
	// Input byte = 0xEE; Places : 8 - Output : 0xFFFFFFEE
	public static String Hex(byte byt, int Places) {
		String Out = "";
		if (Places == 2) {
			Out = String.format("0x%02x", byt);
		} else if (Places == 4) {
			Out = String.format("0x%04X", byt);
		} else if (Places == 8) {
			Out = String.format("0x%08X", byt);
		} else {

		}
		return Out;

	}

	public static String HEX(int byt, int Places) {
		String Out = "";
		if (Places == 2) {
			Out = String.format("%02X", (0xFFFF & byt));
		} else if (Places == 4) {
			Out = String.format("%04X", (0xFFFF & byt));
		} else if (Places == 8) {
			Out = String.format("%08X", (0xFFFF & byt));
		} else {

		}
		Places = 0;
		System.out.print("Output : ");
		print(Out);
		return Out;

	}

	public static List<Byte> Stuff_Packet(List<Byte> buffer_to_send) {
		bytedefinitions b = new bytedefinitions();
		List<Byte> Stuffed_List = new ArrayList<Byte>();
		for (int i = 0; i < buffer_to_send.size(); i++) {

			if (buffer_to_send.get(i) == b.START_byte || buffer_to_send.get(i) == b.END_byte
					|| buffer_to_send.get(i) == b.ESC_byte) {
				Stuffed_List.add(b.ESC_byte);
			}
			Stuffed_List.add(buffer_to_send.get(i));
		}
		return Stuffed_List;

	}

	public static byte[] al_to_byte(ArrayList<Byte> al) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(baos);
		for (Byte element : al) {
			try {
				// System.out.println("Element : " + element);
				out.write(element);
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		byte[] bytes = baos.toByteArray();

		return bytes;
	}

	public void print_al(ArrayList<Byte> w) {
		System.out.print("{");
		for (int i = 0; i < w.size(); i++) {
			System.out.print(String.format("0x%02x", w.get(i)));
			if (i != w.size() - 1) {
				System.out.print(",");
			}
		}
		System.out.println("}");
	}

	public JRadioButton getRdbtnIndividual() {
		if (rdbtnIndividual == null) {

		}

		return rdbtnIndividual;

	}

	public Border getScrollViewportBorder() {
		return scroll.getViewportBorder();
	}

	public void setScrollViewportBorder(Border viewportBorder) {
		scroll.setViewportBorder(viewportBorder);
	}
}
