import java.awt.Color;
import java.awt.Component;
import java.awt.HeadlessException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;

/**
 * 
 */

/**
 * @author LakshmiNarasimman R
 *
 */
public class bg_worker extends SwingWorker<List<String>, String> {

	/**
	 * 
	 */
	//Future executor definitions
	public static ScheduledFuture<?> future;
	public static  ScheduledFuture<?> futuree;
	public static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(3);
	
	
	//Serialport associated variables
	public static  int numRead;
	public static int total_bytes;
	public static int Timeout_Flag = 0;
	public static int Notify_Flag = 0;
	
	public static int Initializer_Value = 0;
	public static int State_Machine = 0;
	public static int Init_state = 0;
	public static int Paused_State = 2;
	
	public static int pass_count = 0;
	public static int fail_count = 0;
	//Variable declarations
	public static JComponent component;
	public static String Command_Name;
	
	//Boolean variable definitions
	public static boolean Listener_Flag;
	public static boolean MFTest_Flag = false;
	public static boolean Is_processing_response = false;
	public static boolean Write_flag = true;
	public static boolean Initial_Flag = true;
	public static boolean First_time_flag = false;
	public boolean Is_task_running = false;
	public boolean test_can_run = false;
	public static  int test_count = 0;
	
	public static  String BNAME_EMPTY = "BNAME_EMPTY";
	
	//serialport definitions
	public static SerialPort serialport;
	
	
	//HashMap that holds Timeout Mappings
	public static HashMap<String, Long> timeout_mapping =  new HashMap<String, Long>();
	
	//Timeout Mappings
	public static long MTest_Start_Timeout = 2000;
	//COMTEST
	public static long Uc1_Com_Test_Timeout = 2000;
	public static long Uc2_Com_Test_Timeout = 2000;
	//IPC TEST
	public static long Ipc_Test_Timeout = 2000;
	//PWR TEST
	public static long Uc1_Pwr_Test_Timeout = 2000;
	public static long Uc2_Pwr_Test_Timeout = 2000;
	//EEPROM TEST
	public static long Uc1_Eeprom_Test_Timeout = 2000;
	public static long Uc2_Eeprom_Test_Timeout = 2000;
	//FLASH TEST
	public static long Uc2_Flash_Test_Timeout = 2000;
	//uc1 UART TEST
	public static long Uc1_Cam_Uart_Test_Timeout = 2000;
	public static long Uc1_Debug_Uart_Test_Timeout = 2000;
	//Uc2 UART TEST
	public static long Uc2_Cam_Uart_Test_Timeout = 2000;
	public static long Uc2_Debug_Uart_Test_Timeout = 2000;
	public static long Uc2_Gui_Uart_Test_Timeout = 2000;
	public static long Uc2_Add_Uart_Test_Timeout = 2000;
	
	//FAN TEST
	public static long LightEngine_Fan_Test_Timeout = 7000;
	public static long HeatSink_Fan_Test_Timeout = 7000;
	public static long Iris_Fan_Test_Timeout =7000;
	public static long Fan_Failure_Test_Timeout = 7000;
	
	//PWM MUXTEST
	public static long Wl_Uc1_PwmMux_Test_Timeout = 2000;
	public static long Env_Uc1_PwmMux_Test_Timeout = 2000;
	public static long Iris_Uc1_PwmMux_Test_Timeout = 2000;
	public static long Wl_Uc2_PwmMux_Test_Timeout = 2000;
	public static long Env_Uc2_PwmMux_Test_Timeout = 2000;
	public static long Iris_Uc2_PwmMux_Test_Timeout = 2000;
	public static long Wl_Ext_PwmMux_Test_Timeout = 2000;
	public static long Env_Ext_PwmMux_Test_Timeout = 2000;
	public static long Iris_Ext_Wl_PwmMux_Test_Timeout = 2000;
	public static long Iris_Ext_Env_PwmMux_Test_Timeout = 2000;
	
	//LED TEST ZERO
	public static long Red_Led_Test_Zero_Timeout = 7000;
	public static long Green_Led_Test_Zero_Timeout = 7000;
	public static long Blue_Led_Test_Zero_Timeout = 7000;
	public static long Laser_Led_Test_Zero_Timeout = 7000;
	public static long Iris1_Led_Test_Zero_Timeout = 7000;
	public static long Iris2_Led_Test_Zero_Timeout = 7000;
	
	//LED TEST FIFTY
	public static long Red_Led_Test_Fifty_Timeout = 7000;
	public static long Green_Led_Test_Fifty_Timeout = 7000;
	public static long Blue_Led_Test_Fifty_Timeout = 7000;
	public static long Laser_Led_Test_Fifty_Timeout = 7000;
	public static long Iris1_Led_Test_Fifty_Timeout = 7000;
	public static long Iris2_Led_Test_Fifty_Timeout = 7000;
	
	//LED TEST SEVENTY FIVE
	public static long Red_Led_Test_Seventy_Five_Timeout = 7000;
	public static long Green_Led_Test_Seventy_Five_Timeout = 7000;
	public static long Blue_Led_Test_Seventy_Five_Timeout = 7000;
	public static long Laser_Led_Test_Seventy_Five_Timeout = 7000;
	public static long Iris1_Led_Test_Seventy_Five_Timeout = 7000;
	public static long Iris2_Led_Test_Seventy_Five_Timeout = 7000;
	
	//LED TEST HUNDRED
	public static long Red_Led_Test_Hundred_Timeout = 7000;
	public static long Green_Led_Test_Hundred_Timeout = 7000;
	public static long Blue_Led_Test_Hundred_Timeout = 7000;
	public static long Laser_Led_Test_Hundred_Timeout = 7000;
	public static long Iris1_Led_Test_Hundred_Timeout = 7000;
	public static long Iris2_Led_Test_Hundred_Timeout = 7000;
	
	//FIBER DETECT TEST
	public static long Iris1_Fiber_Detect_Test_Timeout = 2000;
	public static long Iris2_Fiber_Detect_Test_Timeout = 2000;
	
	//BEAN SENSOR TEST
	public static long Beam_Sensor_Test_Timeout = 2000;
	//ESST TEST
	public static long ESST_Test_Timeout = 2000;
	//ASST TEST
	public static long ASST_Test_Timeout = 2000;
	//FRONT BOARD POWER TEST
	public static long Front_Board_Power_Test_Timeout = 2000;
	//IRIS PGOOD TEST
	public static long Iris_Pgood_On_Test_Timeout = 2000;
	public static long Iris_Pgood_Off_Test_Timeout = 2000;
	
	//PDTEST
	public static long Iris1_PD_Zero_Test_Timeout = 7000;
	public static long Iris1_PD_Hundred_Test_Timeout = 7000;
	public static long Iris2_PD_Zero_Test_Timeout = 7000;
	public static long Iris2_PD_Hundred_Test_Timeout = 7000;
	public static long Env_PD_Zero_Test_Timeout = 7000;
	public static long Env_PD_Hundred_Test_Timeout = 7000;
	
	//COLOR SENSOR TEST
	public static long Color_Sensor1_Test_Timeout = 7000;
	public static long Color_Sensor2_Test_Timeout = 7000;
	
	public static boolean Fail_Flag = false;
	
	
	public static JToggleButton jtgbtn = new JToggleButton();
	
	public static long serial_read_byte_timeout = 50;
	public static long serial_read_cmd_timeout;
	
	
	//btnname to panel maping
	public static String btnname_mapping_topanel = "";
	//Error String Definitions
	public static String INVALID_PORT = "INVALID_PORT";
	public static String SP_OPEN_ERROR = "SP_OPEN_ERROR";
	public static String MTESTSTART = "SYNCING...";
	
	public static String RESPONSE_OK		= "{OK,";
	public static String RESPONSE_ERROR		= "{FAIL,";
	
	public static String Pass = "PASS";
	public static String Fail = "FAIL";
	public static String init_state = "N/A";
	public static String Timeout = "RESPONSE TIMEDOUT";
	
	//Command String Definitions
	public static String START_COMMAND = "{MTESTSTART}";
	public static String STOP_COMMAND = "{MTESTEXIT}";
	public static String RESUME_COMMAND = "{MTESTRESUME}";
	
	
	//lock object definition
	public final static Object lockObject = new Object();
	
	//Full_buffer definition
	public static ByteArrayOutputStream full_buffer = new ByteArrayOutputStream();
	
	
	//Constructor for this class
	public bg_worker(JComponent component, boolean Initial_Flag, boolean Listener_Flag, SerialPort sp, int Initializer_Value) {
		
		this.component = component;
		this.Command_Name = Command_Name;
		this.Listener_Flag = Listener_Flag;
		this.serialport = sp;
		this.Initializer_Value = Initializer_Value;
		this.Initial_Flag = Initial_Flag;
		//JOptionPane.showMessageDialog(ManufacturingTests.frmStrykerManufacturingTests, Integer.toString(Initializer_Value), "Initializer Value", JOptionPane.ERROR_MESSAGE);
		//JOptionPane.showMessageDialog(ManufacturingTests.frmStrykerManufacturingTests, "Have been here for the first time", "Entry", JOptionPane.ERROR_MESSAGE);
		timeout_mapping.put("btnmteststart", MTest_Start_Timeout);
		timeout_mapping.put("btnuc1comtestselector", Uc1_Com_Test_Timeout);
		timeout_mapping.put("btnuc2comtestselector", Uc2_Com_Test_Timeout);
		timeout_mapping.put("btnipctestselector", Ipc_Test_Timeout);
		timeout_mapping.put("btnuc1pwrtestselector", Uc1_Pwr_Test_Timeout);
		timeout_mapping.put("btnuc2pwrtestselector", Uc2_Pwr_Test_Timeout);
		timeout_mapping.put("btnuc1eepromtestselector", Uc1_Eeprom_Test_Timeout);
		timeout_mapping.put("btnuc2eepromtestselector", Uc2_Eeprom_Test_Timeout);
		timeout_mapping.put("btnuc2flashtestselector", Uc2_Flash_Test_Timeout);
		timeout_mapping.put("btnuc1camuarttestselector", Uc1_Cam_Uart_Test_Timeout);
		timeout_mapping.put("btnuc1debuguarttestselector", Uc1_Debug_Uart_Test_Timeout);
		
		timeout_mapping.put("btnuc2camuarttestselector", Uc2_Cam_Uart_Test_Timeout);
		timeout_mapping.put("btnuc2debuguarttestselector", Uc2_Debug_Uart_Test_Timeout);
		timeout_mapping.put("btnuc2guiuarttestselector", Uc2_Gui_Uart_Test_Timeout);
		timeout_mapping.put("btnuc2adduarttestselector", Uc2_Add_Uart_Test_Timeout);
		
		
		timeout_mapping.put("btnlightenginefantestselector", LightEngine_Fan_Test_Timeout);
		timeout_mapping.put("btnheatsinkfantestselector", HeatSink_Fan_Test_Timeout);
		timeout_mapping.put("btnirisfantestselector", Iris_Fan_Test_Timeout);
		
		timeout_mapping.put("btnfanfailuretestselector", Fan_Failure_Test_Timeout);
		
		timeout_mapping.put("btnwluc1pwmmuxtestselector", Wl_Uc1_PwmMux_Test_Timeout);
		timeout_mapping.put("btnenvuc1pwmmuxtestselector", Env_Uc1_PwmMux_Test_Timeout);
		timeout_mapping.put("btnirisuc1pwmmuxtestselector", Iris_Uc1_PwmMux_Test_Timeout);
		timeout_mapping.put("btnwluc2pwmmuxtestselector", Wl_Uc2_PwmMux_Test_Timeout);
		timeout_mapping.put("btnenvuc2pwmmuxtestselector", Env_Uc2_PwmMux_Test_Timeout);
		timeout_mapping.put("btnirisuc2pwmmuxtestselector", Iris_Uc2_PwmMux_Test_Timeout);
		timeout_mapping.put("btnwlextpwmmuxtestselector", Wl_Ext_PwmMux_Test_Timeout);
		timeout_mapping.put("btnenvextpwmmuxtestselector", Env_Ext_PwmMux_Test_Timeout);
		timeout_mapping.put("btnirisextwlpwmmuxtestselector", Iris_Ext_Wl_PwmMux_Test_Timeout);
		timeout_mapping.put("btnirisextenvpwmmuxtestselector", Iris_Ext_Env_PwmMux_Test_Timeout);
		
		timeout_mapping.put("btnredledzerotestselector", Red_Led_Test_Zero_Timeout);
		timeout_mapping.put("btngreenledzerotestselector", Green_Led_Test_Zero_Timeout);
		timeout_mapping.put( "btnblueledzerotestselector", Blue_Led_Test_Zero_Timeout);
		timeout_mapping.put("btnlaserledzerotestselector", Laser_Led_Test_Zero_Timeout);
		timeout_mapping.put("btniris1ledzerotestselector", Iris1_Led_Test_Zero_Timeout);
		timeout_mapping.put("btniris2ledzerotestselector", Iris2_Led_Test_Zero_Timeout);
		
		timeout_mapping.put( "btnredledfiftytestselector", Red_Led_Test_Fifty_Timeout);
		timeout_mapping.put( "btngreenledfiftytestselector", Green_Led_Test_Fifty_Timeout);
		timeout_mapping.put( "btnblueledfiftytestselector", Blue_Led_Test_Fifty_Timeout);
		timeout_mapping.put("btnlaserledfiftytestselector", Laser_Led_Test_Fifty_Timeout);
		timeout_mapping.put( "btniris1ledfiftytestselector", Iris1_Led_Test_Fifty_Timeout);
		timeout_mapping.put( "btniris2ledfiftytestselector", Iris2_Led_Test_Fifty_Timeout);
		
		timeout_mapping.put( "btnredledseventyfivetestselector", Red_Led_Test_Seventy_Five_Timeout);
		timeout_mapping.put( "btngreenledseventyfivetestselector", Green_Led_Test_Seventy_Five_Timeout);
		timeout_mapping.put( "btnblueledseventyfivetestselector", Blue_Led_Test_Seventy_Five_Timeout);
		timeout_mapping.put("btnlaserledseventyfivetestselector", Laser_Led_Test_Seventy_Five_Timeout);
		timeout_mapping.put( "btniris1ledseventyfivetestselector", Iris1_Led_Test_Seventy_Five_Timeout);
		timeout_mapping.put( "btniris2ledseventyfivetestselector", Iris2_Led_Test_Seventy_Five_Timeout);
		
		timeout_mapping.put( "btnredledhundredtestselector", Red_Led_Test_Hundred_Timeout);
		timeout_mapping.put( "btngreenledhundredtestselector", Green_Led_Test_Hundred_Timeout);
		timeout_mapping.put( "btnblueledhundredtestselector", Blue_Led_Test_Hundred_Timeout);
		timeout_mapping.put( "btnlaserledhundredtestselector", Laser_Led_Test_Hundred_Timeout);
		timeout_mapping.put( "btniris1ledhundredtestselector", Iris1_Led_Test_Hundred_Timeout);
		timeout_mapping.put("btniris2ledhundredtestselector", Iris2_Led_Test_Hundred_Timeout);
		
		timeout_mapping.put( "btniris1fiberdetecton_offtestselector", Iris1_Fiber_Detect_Test_Timeout);
		timeout_mapping.put( "btniris2fiberdetecton_offtestselector", Iris2_Fiber_Detect_Test_Timeout);
		
		timeout_mapping.put( "btnbeamsensoron_offtestselector", Beam_Sensor_Test_Timeout);
		timeout_mapping.put( "btnesston_offtestselector", ESST_Test_Timeout);
		timeout_mapping.put( "btnasston_offtestselector", ASST_Test_Timeout);
		timeout_mapping.put("btnfbpwrenableon_offtestselector", Front_Board_Power_Test_Timeout);
		timeout_mapping.put("btnirispgoodon_testselector",Iris_Pgood_On_Test_Timeout );
		timeout_mapping.put("btnirispgoodoff_testselector", Iris_Pgood_Off_Test_Timeout);
		
		timeout_mapping.put( "btniris1zeropdtestselector", Iris1_PD_Zero_Test_Timeout);
		timeout_mapping.put( "btniris1hundredpdtestselector", Iris1_PD_Hundred_Test_Timeout);
		timeout_mapping.put( "btniris2zeropdtestselector", Iris2_PD_Zero_Test_Timeout);
		timeout_mapping.put( "btniris2hundredpdtestselector", Iris2_PD_Hundred_Test_Timeout);
		timeout_mapping.put( "btnenvzeropdtestselector", Env_PD_Zero_Test_Timeout);
		timeout_mapping.put( "btnenvhundredpdtestselector", Env_PD_Hundred_Test_Timeout);
		
		
		timeout_mapping.put( "btncolorsensor1testselector", Color_Sensor1_Test_Timeout);
		timeout_mapping.put("btncolorsensor2testselector", Color_Sensor2_Test_Timeout);
		
		
		
		
		
		
		
		//System.out.println("Inside bg Worker");
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected List<String> doInBackground() throws Exception 
	{
		
		
		// TODO Auto-generated method stub
		try
		{
			System.out.println("Inside bg");	
			//publish("ACTIONITEMS");
			String Query_String = "";
			
			
			if(Initial_Flag)
			{
				System.out.println("##########################################INITIALIZED##################################");
				State_Machine = 0;
				Initializer_Value = 0;
				publish("STOP");
				System.out.println("<---------------STOP PUBLISHED----------->");
			}
			
			else if(!Initial_Flag)
			{
				System.out.println("#################################RUN or RESUMED#####################################");
				State_Machine = 1;
				
				
			
			}
			//Change the Initialization count of the for loop based on the Current State Machine Value		
			
			if(State_Machine == 0)
			{
				btnname_mapping_topanel = "";
				btnname_mapping_topanel = "btnmteststart";
				ManufacturingTests.textField_Failedtests.setText("0");
				ManufacturingTests.textField_Passedtests.setText("0");
				System.out.println("Sending MTESTSTART Command");
				//setlistener_mft(serialport);
				if(serialport != null)
				{
					if(!serialport.isOpen())
					{
						System.out.println(LocalDateTime.now());
						serialport.openPort();
						System.out.println(LocalDateTime.now());
					}
					
					if(serialport.isOpen() == true)
					{
						byte[] temp = null ;
						int bytes_available = serialport.bytesAvailable();
						System.out.println("Total Bytes available at the buffer inside performtest : " + bytes_available);
						if(bytes_available > 0)
						{
							temp = new byte[bytes_available];
							int d = serialport.readBytes(temp, bytes_available);
						}
					    try {
					    	full_buffer.reset();
							full_buffer.flush();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					    setlistener_mft(serialport);
				    	
					    serialport.writeBytes("{MTESTSTART}".getBytes(), "{MTESTSTART}".length());
					    publish("Command Sent : {MTESTSTART}\r\n");
					    Write_flag = true; 
					    if(Write_flag)
					    {
					    	System.out.println("Calling Function");
					    	int status = Send_cmd_wait_for_Timeout();
					    	System.out.println("Executed Function");
					    	System.out.println("STATUS : " + status);
					    	
					    	if(status == 1)
					    	{
					    		status = 0;
					    		Timeout_Flag = 0;
					    		State_Machine = 0;
					    		ManufacturingTests.btnStartButton.setName("RUN");
					    		JOptionPane.showMessageDialog(ManufacturingTests.frmStrykerManufacturingTests, "Reponse Timedout for {MTESTSTART} Command. Please try again", "MTEST START", JOptionPane.ERROR_MESSAGE);
					    		return null;
					    	}
					    }
					    else
					    {
					    	JOptionPane.showMessageDialog(ManufacturingTests.frmStrykerManufacturingTests, "Could not send {MTESTSTART} Command", "ManufacturingTests", JOptionPane.ERROR_MESSAGE);
					    }
					}
					else
					{
						publish(SP_OPEN_ERROR);
					}
				}
				else
				{
					publish(INVALID_PORT);
				}
			    
			    
			    
			}
			else
			{
				//Initializer_Value = Initializer_Value + 1;
			}
			
			if(Fail_Flag)
			{
				ManufacturingTests.togglebuttons.clear();
			}
					
					
			
			//Send MTEST START Command if the index of for loop is zero
			
			if(!(ManufacturingTests.togglebuttons.size() == 0))
			{
				System.out.println("Total No Of Tests : " + ManufacturingTests.togglebuttons.size());
				for(test_count = Initializer_Value;test_count<ManufacturingTests.togglebuttons.size();test_count++)
				{
					System.out.println("#########################################################0th position########################");
					System.out.println(ManufacturingTests.togglebuttons.get(0));
					System.out.println("##############################################################################################");
					
					Query_String = "";
					//JOptionPane.showMessageDialog(ManufacturingTests.frmStrykerManufacturingTests, "SM=1", "State Machine", JOptionPane.ERROR_MESSAGE);
					System.out.println("###########################################################################################");
					System.out.println("State Machine : 1. Current Iteration Count : " + test_count);
					btnname_mapping_topanel = "";
					
					jtgbtn = (JToggleButton) ManufacturingTests.togglebuttons.get(test_count);
					btnname_mapping_topanel = jtgbtn.getName();
					//System.out.println(btnname_mapping_topanel);
					
					Query_String = ManufacturingTests.command_name_mappings.get(btnname_mapping_topanel);
					
							
					ManufacturingTests.check_string = Query_String;
					ManufacturingTests.logs_logger.info("Test\t\t\t" + Query_String);
					ManufacturingTests.report_logger.info("\r\n\nTest\t\t\t" + Query_String);
			    	Query_String = ManufacturingTests.return_command_string(Query_String);
			    	ManufacturingTests.logs_logger.info("Command String\t:\t" + Query_String);
			    	if(serialport != null)
					{
			    		System.out.print("Query String : ");
						System.out.println(Query_String);
						if(!serialport.isOpen())
						{
							System.out.println(LocalDateTime.now());
							serialport.openPort();
							System.out.println(LocalDateTime.now());
						}
						if(serialport.isOpen() == true)
						{
							byte[] temp = null ;
							int bytes_available = serialport.bytesAvailable();
							System.out.println("Total Bytes available at the buffer inside performtest : " + bytes_available);
							if(bytes_available > 0)
							{
								temp = new byte[bytes_available];
								int d = serialport.readBytes(temp, bytes_available);
							}
						    try {
						    	full_buffer.reset();
								full_buffer.flush();
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						    
					    	System.out.println("Starting Test...");
					    	setlistener_mft(serialport);
					    	System.out.println("Autotest : Data Listener Set!!");
					    	serialport.writeBytes(Query_String.getBytes(), Query_String.length());
					    	Write_flag  = true;
					    	//ManufacturingTests.btnStartButton.setName("PAUSE");
					    	publish("Command Sent : " + Query_String + "\r\n");
					    	if(Write_flag)
						    {
					    		ManufacturingTests.btnStartButton.setName("PAUSE");
					    		publish("PBUTTON");
					    		publish("DISABLE_ALL_TOGGLE_BUTTONS");
					    		publish("TEST_CONFIG_DISABLE");
					    		System.out.println(LocalDateTime.now());
					    		int status = Send_cmd_wait_for_Timeout();
					    		System.out.println(LocalDateTime.now());

						    	if(status == 1)
						    	{
						    		
						    		
						    		//JOptionPane.showMessageDialog(ManufacturingTests.frmStrykerManufacturingTests, "Reponse Timedout for sending " + Query_String +  "Command", "ManufacturingTests", JOptionPane.ERROR_MESSAGE);
						    		System.out.println("Response Timedout for sending " + Query_String + "Command");
						    		
						    	}
						    	else
						    	{
						    		//JOptionPane.showMessageDialog(ManufacturingTests.frmStrykerManufacturingTests, "Status : " + status, "Status", JOptionPane.INFORMATION_MESSAGE);
						    		System.out.println("Status : " + status);
						    	}
						    }
					    	 	
						} 
						else 
						{
							publish(SP_OPEN_ERROR);
							System.out.println("Could not open Serial port. Exiting Now!!");
					    	publish("Could not send " + Query_String + " command \r\n");
					    	publish("Access to COM Port Denied!!. Port is in Access by Another Application!!\r\n\n");
					    	break;
							
						}
					}
			    	else
			    	{
			    		System.out.println("Serial Port is Null");
			    		publish(INVALID_PORT);
			    		break;
			    	}
					    	
					
					if(ManufacturingTests.worker_cancel_flag)
					{
						
						if(futuree != null)
						{
							
							System.out.println("Cancelling Futuree");
							
							futuree.cancel(true);
						}
						break;
					}
					else
					{
						System.out.println("Worker_Cancel_Flag = " + ManufacturingTests.worker_cancel_flag);
					}
				
			

				}
				if(test_count == ManufacturingTests.togglebuttons.size())
				{
					publish("BIAS_TOGGLE_BUTTONS");
					publish("TEST_CONFIG_ENABLE");
					publish("TEST_RESULT");
				}
				
				//System.out.println("Test Count : " + test_count);
				//System.out.println("Toggle Button Size : " + ManufacturingTests.togglebuttons.size());
				//ManufacturingTests.btnStartButton.setName("RUN");
				System.out.println("Loop Broke");
				
			}
			else
			{
				if(Fail_Flag)
				{
					Fail_Flag = false;
					publish("STARTFAIL");
					
					return null;
				}
				
				
				publish("NO_TESTS");
				
			}
					
										
						
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	

	protected void process (List<String> chunks)
	{
		for(String str: chunks)
		{
			if(str.equals(INVALID_PORT))
			{
				JOptionPane.showMessageDialog(ManufacturingTests.frmStrykerManufacturingTests, "Invalid SerialPort", "COM Port", JOptionPane.ERROR_MESSAGE);
			}
			else if(str.equals(SP_OPEN_ERROR))
			{
				JOptionPane.showMessageDialog(ManufacturingTests.frmStrykerManufacturingTests, "COM Port is closed!!. Check whether the port is in access by another application", "COM Port", JOptionPane.ERROR_MESSAGE);
			}
			
			else if(str.equals(BNAME_EMPTY))
			{
				JOptionPane.showMessageDialog(ManufacturingTests.frmStrykerManufacturingTests, "Board-Number cannot be null", "Board Number", JOptionPane.ERROR_MESSAGE);
			}
			else if(str.equals("FIRST_TIME_ERROR"))
			{
				JOptionPane.showMessageDialog(ManufacturingTests.frmStrykerManufacturingTests, "Error Response Received for sending MTEST START Command.", "MTESTSTART", JOptionPane.ERROR_MESSAGE);
				State_Machine = 0;
			}
			else if(str.equals("NO_TESTS"))
			{
				JOptionPane.showMessageDialog(ManufacturingTests.frmStrykerManufacturingTests, "Select a test", "Test Item", JOptionPane.ERROR_MESSAGE);
				State_Machine = 0;
			}
			else if(str.equals("ACTIONITEMS"))
			{
				JOptionPane.showMessageDialog(ManufacturingTests.frmStrykerManufacturingTests, "Successful Entry", "bg worker", JOptionPane.INFORMATION_MESSAGE);
			}
			else if(str.startsWith("VER"))
			{
				String[] local = str.split(",");
				if(local.length > 0)
				{
					String Ver_No = local[2];
					String Ver_uc1 = local[1];
					ManufacturingTests.textField_FirmWareVerNo.setText("");
					ManufacturingTests.textField_FirmWareVerNo.setText(Ver_No);
					ManufacturingTests.textField_Firmware_UC1.setText("");
					ManufacturingTests.textField_Firmware_UC1.setText(Ver_uc1);
				}
				else
				{
					JOptionPane.showMessageDialog(ManufacturingTests.frmStrykerManufacturingTests, "Invalid FirmWare Version", "FirmWare", JOptionPane.ERROR_MESSAGE);
				}
				
			}
			else if(str.equals("PBUTTON"))
			{
				ManufacturingTests.btnStartButton.setIcon(new ImageIcon(ManufacturingTests.class.getResource("/resources/Pause.png")));
				
			}
			else if(str.startsWith("Fail,"))
			{
				String[] local = str.split(",");
				String cnt =  local[1];
				ManufacturingTests.textField_Failedtests.setText(cnt);
			}
			else if(str.startsWith("Pass,"))
			{
				String[] local = str.split(",");
				String cnt =  local[1];
				ManufacturingTests.textField_Passedtests.setText(cnt);
			}
			else if(str.equals("TEST_RESULT"))
			{
				if(fail_count == 0)
				{
					if(pass_count > 0 )
					{
						pass_count = 0;
						System.out.println("PASSSE");
						ManufacturingTests.panel_4.setBackground(Color.GREEN);
						
						ManufacturingTests.lblTestStatus.setText("PASS");
					}
				}
				else if(fail_count > 0)
				{
					fail_count = 0;
					System.out.println("FAIL");
					ManufacturingTests.panel_4.setBackground(Color.RED);
					ManufacturingTests.lblTestStatus.setText("FAILED");
				}
				ManufacturingTests.total_test_count = 0;
				pass_count = 0;
				fail_count = 0;
				System.out.println("While Loop Breaks here");
				ManufacturingTests.logs_logger.info(ManufacturingTests.ArrowDelimitter);
				ManufacturingTests.report_logger.info(ManufacturingTests.ArrowDelimitter);
				ManufacturingTests.report_logger.info("#########################  END OF REPORT  ##################################");
				ManufacturingTests.report_logger.removeHandler(ManufacturingTests.fh);
			}
			
			else if(str.equals("TEST_CONFIG_DISABLE"))
			{
				Component[] comp = ManufacturingTests.test_configuration_panel.getComponents();
				for(Component com : comp)
				{
					com.setEnabled(false);
				}
			}
			else if(str.equals("TEST_CONFIG_ENABLE"))
			{
				Component[] comp = ManufacturingTests.test_configuration_panel.getComponents();
				for(Component com : comp)
				{
					com.setEnabled(true);
				}
			}
			else if(str.equals("STARTFAIL"))
			{
				ManufacturingTests.textArea.append(str);
				JOptionPane.showMessageDialog(ManufacturingTests.frmStrykerManufacturingTests, "Failure Response received for MTESTSTART Command", "MTESTSTART", JOptionPane.ERROR_MESSAGE);
			}
			else if(str.equals("ENABLE_ALL_TOGGLE_BUTTONS"))
			{
				Component[] panel_1comp = ManufacturingTests.SubTestPanel_1.getComponents();
				Component[] panel_2comp = ManufacturingTests.SubTestPanel_2.getComponents();
				Component[] panel_3comp = ManufacturingTests.SubTestPanel_3.getComponents();
				Component[] panel_4comp = ManufacturingTests.SubTestPanel_4.getComponents();
				for(Component cmp : panel_1comp)
				{
					if(cmp instanceof JToggleButton)
					{
						((JToggleButton) cmp).setEnabled(true);
					}
				}
				for(Component cmp : panel_2comp)
				{
					if(cmp instanceof JToggleButton)
					{
						((JToggleButton) cmp).setEnabled(true);
					}
				}
				for(Component cmp : panel_3comp)
				{
					if(cmp instanceof JToggleButton)
					{
						((JToggleButton) cmp).setEnabled(true);
					}
				}
				for(Component cmp : panel_4comp)
				{
					if(cmp instanceof JToggleButton)
					{
						((JToggleButton) cmp).setEnabled(true);
					}
				}
			}
			else if(str.equals("DISABLE_ALL_TOGGLE_BUTTONS"))
			{
				{
					Component[] panel_1comp = ManufacturingTests.SubTestPanel_1.getComponents();
					Component[] panel_2comp = ManufacturingTests.SubTestPanel_2.getComponents();
					Component[] panel_3comp = ManufacturingTests.SubTestPanel_3.getComponents();
					Component[] panel_4comp = ManufacturingTests.SubTestPanel_4.getComponents();
					for(Component cmp : panel_1comp)
					{
						if(cmp instanceof JToggleButton)
						{
							((JToggleButton) cmp).setEnabled(false);
						}
					}
					for(Component cmp : panel_2comp)
					{
						if(cmp instanceof JToggleButton)
						{
							((JToggleButton) cmp).setEnabled(false);
						}
					}
					for(Component cmp : panel_3comp)
					{
						if(cmp instanceof JToggleButton)
						{
							((JToggleButton) cmp).setEnabled(false);
						}
					}
					for(Component cmp : panel_4comp)
					{
						if(cmp instanceof JToggleButton)
						{
							((JToggleButton) cmp).setEnabled(false);
						}
					}
				}
			}
			else if(str.equals("STOP"))
			{
				ManufacturingTests.btnStartButton.setName("RUN");
				ManufacturingTests.panel_4.setBackground(Color.pink);
				ManufacturingTests.lblTestStatus.setText("READY");
				for(int t=0;t<ManufacturingTests.subpanellist.size();t++)
				{
					Initializer_Value = 0;
					ManufacturingTests.total_test_count = 0;
					pass_count = 0;
					fail_count = 0;
					Component[] comp = ManufacturingTests.subpanellist.get(t).getComponents();
					for(Component c : comp)
					{
						if(c instanceof JPanel)
						{
							if(c.getBackground() == Color.green)
							{
								c.setBackground(Color.decode("#e3e3e3"));
							}
							else if(c.getBackground() == Color.red)
							{
								c.setBackground(Color.decode("#e3e3e3"));
							}
							else if(c.getBackground() == Color.MAGENTA)
							{
								c.setBackground(Color.decode("#e3e3e3"));
							}
						}
						if(c instanceof JToggleButton)
						{
							if(((JToggleButton) c).isSelected())
							{
								
							}
						}
					}
					
				}
				ManufacturingTests.report_logger.removeHandler(ManufacturingTests.report_handler);
				ManufacturingTests.logs_logger.removeHandler(ManufacturingTests.logs_handler);
				ManufacturingTests.btnStartButton.setIcon(new ImageIcon(ManufacturingTests.class.getResource("/resources/Play.png")));
				
				//Send MTESTEXIT Command on stop button press
				
			}
			else if(str.equals("BIAS_TOGGLE_BUTTONS"))
			{
				Component[] comp = ManufacturingTests.test_configuration_panel.getComponents();
				for(Component com : comp)
				{
					com.setEnabled(true);
				}
				for(int i = 0;i<ManufacturingTests.subpanellist.size();i++)
				{
					Component[] compo = ManufacturingTests.subpanellist.get(i).getComponents();
					for(Component c : compo)
					{
						if(c instanceof JToggleButton)
						{
							((JToggleButton) c).setEnabled(true);
							//LIGHT ENGINE 
							if(c.getName() == ManufacturingTests.btn_name_mapping.get(ManufacturingTests.colorsensor1test))
							{
								if(ManufacturingTests.chckbxLightEngine.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
							}
							if(c.getName() == ManufacturingTests.btn_name_mapping.get(ManufacturingTests.colorsensor2test))
							{
								if(ManufacturingTests.chckbxLightEngine.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
							}
							if(c.getName() == ManufacturingTests.btn_name_mapping.get(ManufacturingTests.envpdtestzero))
							{
								if(ManufacturingTests.chckbxLightEngine.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
							}
							if(c.getName() == ManufacturingTests.btn_name_mapping.get(ManufacturingTests.envpdtesthundred))
							{
								if(ManufacturingTests.chckbxLightEngine.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
							}
							
							//IRIS_MODULE
							if(c.getName() == ManufacturingTests.btn_name_mapping.get(ManufacturingTests.iris1pdtestzero))
							{
								if(ManufacturingTests.chckbxIrisModule.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
								
								
							}
							if(c.getName() == ManufacturingTests.btn_name_mapping.get(ManufacturingTests.iris2pdtestzero))
							{
								if(ManufacturingTests.chckbxIrisModule.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
							}
							if(c.getName() == ManufacturingTests.btn_name_mapping.get(ManufacturingTests.iris1pdtesthundred))
							{
								if(ManufacturingTests.chckbxIrisModule.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
							}
							if(c.getName() == ManufacturingTests.btn_name_mapping.get(ManufacturingTests.iris2pdtesthundred))
							{
								if(ManufacturingTests.chckbxIrisModule.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
							}
							//UART LOOPBACK TESTS
							if(c.getName() == ManufacturingTests.btn_name_mapping.get(ManufacturingTests.uc1camuarttest))
							{
								if(ManufacturingTests.chckbxUartLoopBack.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
								
								
							}
							if(c.getName() == ManufacturingTests.btn_name_mapping.get(ManufacturingTests.uc1debuguarttest))
							{
								if(ManufacturingTests.chckbxUartLoopBack.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
							}
							if(c.getName() == ManufacturingTests.btn_name_mapping.get(ManufacturingTests.uc2camuarttest))
							{
								if(ManufacturingTests.chckbxUartLoopBack.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
							}
							if(c.getName() == ManufacturingTests.btn_name_mapping.get(ManufacturingTests.uc2debuguarttest))
							{
								if(ManufacturingTests.chckbxUartLoopBack.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
							}
							if(c.getName() == ManufacturingTests.btn_name_mapping.get(ManufacturingTests.uc2guiuarttest))
							{
								if(ManufacturingTests.chckbxUartLoopBack.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
							}
							if(c.getName() ==ManufacturingTests. btn_name_mapping.get(ManufacturingTests.uc2adduarttest))
							{
								if(ManufacturingTests.chckbxUartLoopBack.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
							}
							//EXTERNAL CAM FEED
							if(c.getName() ==ManufacturingTests. btn_name_mapping.get(ManufacturingTests.wlextpwmmuxtest))
							{
								if(ManufacturingTests.chckbxExternalCamFeed.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
								
								//((JToggleButton) c).setSelected(true);
							}
							if(c.getName() == ManufacturingTests.btn_name_mapping.get(ManufacturingTests.envextpwmmuxtest))
							{
								if(ManufacturingTests.chckbxExternalCamFeed.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
							}
							if(c.getName() == ManufacturingTests.btn_name_mapping.get(ManufacturingTests.irisextwlpwmmuxtest))
							{
								if(ManufacturingTests.chckbxExternalCamFeed.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
							}
							if(c.getName() == ManufacturingTests.btn_name_mapping.get(ManufacturingTests.irisextenvpwmmuxtest))
							{
								if(ManufacturingTests.chckbxExternalCamFeed.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
							}
							//FANS TESTS
							if(c.getName() == ManufacturingTests.btn_name_mapping.get(ManufacturingTests.lightenginefantest))
							{
								if(ManufacturingTests.chckbxFans.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
								
								//((JToggleButton) c).setSelected(true);
							}
							if(c.getName() == ManufacturingTests.btn_name_mapping.get(ManufacturingTests.heatsinkfantest))
							{
								if(ManufacturingTests.chckbxFans.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
								
							}
							if(c.getName() == ManufacturingTests.btn_name_mapping.get(ManufacturingTests.irisfantest))
							{
								if(ManufacturingTests.chckbxFans.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
								
							}
							if(c.getName() == ManufacturingTests.btn_name_mapping.get(ManufacturingTests.fanfailuretest))
							{
								if(ManufacturingTests.chckbxFans.isSelected())
								{
									((JToggleButton) c).setEnabled(true);
								}
								else
								{
									((JToggleButton) c).setEnabled(false);
								}
								
							}
						}
						
						
					}
				}
			}
			else
			{
				ManufacturingTests.textArea.append(str);
				//ManufacturingTests.textArea.update(ManufacturingTests.textArea.getGraphics());
			}
		}
	}
	public void done()
	{
		
		
		
		
		if(!ManufacturingTests.btnStartButton.getName().equals("PAUSE"))
		{
			System.out.println(test_count);
			System.out.println("Count Initialized");
			if(test_count == 0)
			{
				pass_count = 0;
				fail_count = 0;
				for(int t=0;t<ManufacturingTests.subpanellist.size();t++)
				{
					Component[] comp = ManufacturingTests.subpanellist.get(t).getComponents();
					for(Component c : comp)
					{
						if(c instanceof JPanel)
						{
							if(c.getBackground() == Color.green)
							{
								c.setBackground(Color.decode("#e3e3e3"));
							}
							else if(c.getBackground() == Color.red)
							{
								c.setBackground(Color.decode("#e3e3e3"));
							}
							else if(c.getBackground() == Color.MAGENTA)
							{
								c.setBackground(Color.decode("#e3e3e3"));
							}
						}
						if(c instanceof JToggleButton)
						{
							if(((JToggleButton) c).isSelected())
							{
								
							}
						}
					}
					
				}
			}
			else
			{
				
			}
			
		}
		else
		{
			
		}
		ManufacturingTests.btnStartButton.setName("RUN");
		
		Write_flag = false;
		System.out.println("Inside Done method");
		ManufacturingTests.btnStartButton.setIcon(new ImageIcon(ManufacturingTests.class.getResource("/resources/Play.png")));
		//make the global flag false here
	}

//########################################################Timer for handling timeouts or success or fail response after sending a command frame to device
	public int Send_cmd_wait_for_Timeout()
	{
		
		
		final Runnable timeout_task_performer = new Runnable()
    	{
			@Override
			public void run() 
			{
				synchronized(lockObject)
				{
					UpdateTestResults(Timeout, "", "", "");
					//Initializer_Value = test_count;
					if(jtgbtn != null)
					{
						jtgbtn.setEnabled(false);
					}
					
					Timeout_Flag = 1;
					lockObject.notifyAll();
				}
				
			}
			
		};
		final Runnable update_GUI_state = new Runnable() 
		{
			@Override
			public void run() 
			{
				System.out.println("Waiting in timeout here..");
				synchronized(lockObject)
				{
					
					while (Timeout_Flag == 0 && Notify_Flag == 0 && ManufacturingTests.worker_cancel_flag == false)
					{
						
						try 
						{
							lockObject.wait(10);
						} 
						catch (Exception e)
						{
							e.printStackTrace();
						}
						

					}
					if(ManufacturingTests.worker_cancel_flag)
					{
						synchronized(lockObject)
						{
							System.out.println("Paused");
							Write_flag = false;
							lockObject.notifyAll();
						}
						
						
						
					}
					Stryk_Demo.print("exited while LOPP");
					System.out.println("Timeout Flag : " + Timeout_Flag);
					System.out.println("Notify_Flag : " + Notify_Flag);
					futuree.cancel(true);
				}
				// While ends here
				
				//No Data Received for sending command to device
				if (Timeout_Flag == 1 && Notify_Flag == 0) 
				{
					publish("Response Received : Response Timedout\r\n");
					serialport.openPort();
					if(serialport.isOpen() == true)
					{
						byte[] temp = null;
						int bytes_available = serialport.bytesAvailable();
						System.out.println("Total Bytes available at timeout : " + bytes_available);
						if(bytes_available > 0)
						{
							temp = new byte[bytes_available];
							serialport.readBytes(temp, bytes_available);
						}
						serialport.removeDataListener();
						
						
					}
					
					
					System.out.println("Response Timedout. No data received in the Serial Port");
					synchronized(lockObject)
					{
						Write_flag = false;
						lockObject.notifyAll();
					}
					
					
						
					
					//Update UI here
					
					ManufacturingTests.logs_logger.severe("Response Received\tResponse Timedout");
					ManufacturingTests.report_logger.severe("Result\t\t\tTimeout\r\n\n");
					ManufacturingTests.report_logger.severe(ManufacturingTests.ArrowDelimitter);
					ManufacturingTests.report_logger.severe(ManufacturingTests.ArrowDelimitter);
				
					ManufacturingTests.textArea.update(ManufacturingTests.textArea.getGraphics());
					
					
					//textArea.append("Response Timedout for " + Cmd + " command!!\r\n\n");
					
				}
				//Valid Response received for sending command to the device
				else if (Notify_Flag == 1 && Timeout_Flag == 0) 
				{
					
					System.out.println("Error Free Data Received!!");
					Notify_Flag = 0;
					
					@SuppressWarnings("unused")
					bytedefinitions b = new bytedefinitions();
					//System.out.println("Valid Response in UART");
					
				} 
				
				
				else
				{
					System.out.println("Peculiar");
				}
			}
		};

		
		
		//Check WriteFlag whether the data is sent to serialport successfully
		if (Write_flag == true)
		{
			serial_read_cmd_timeout = timeout_mapping.get(btnname_mapping_topanel);
			System.out.println("Timeout for current command : " + serial_read_cmd_timeout);
			Timeout_Flag = 0;
			System.out.println("Write Flag Set");
			//Sets the timeout_task_performer to start exactly after serial_read_cmd_timeout milliseconds
			
			System.out.println("Triggering Timer...");
			futuree = scheduler.schedule(timeout_task_performer, serial_read_cmd_timeout, TimeUnit.MILLISECONDS);
			System.out.println("Timer Triggered!!");
			
			//publish("Waiting For Response!!\r\n");
			
			
				
			Thread appThread = new Thread() {
				public void run() {
					try {
						//Entry point of the function
						scheduler.schedule(update_GUI_state, 0, TimeUnit.MILLISECONDS);
						//SwingUtilities.invokeLater(update_GUI_state);
					} catch (Exception ex) {
						System.out.println("Some Error");
						ex.printStackTrace();
					}
				}
			};
			appThread.run();
				
		} 
		else
		{
			System.out.println("WriteFlag is False");
		}
		while(Write_flag && !ManufacturingTests.worker_cancel_flag)
    	{
			synchronized(lockObject)
    		{
				try {
					lockObject.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    		//TEST Print $System.out.println("Write Flag Inside While Loop : " + Write_flag);
    		
    		
    	}
		System.out.println("Timeout_Flag while returning from function : " + Timeout_Flag);
		return Timeout_Flag;
		
	}
	
//###############################################################################################################################################
	Runnable uart_recv_timeout_task = new Runnable() {
		public void run()
		{
			if (full_buffer.size() ==0)
			{
				
				System.out.println("No data in the response!!");
				serialport.removeDataListener(); // Removes data listener associated with serial port Object
				total_bytes = 0;
				
			} 
			else 
			{
				
					System.out.println("Full Buffer size before calling function :" + full_buffer.size());
					byte[] recv_buff = full_buffer.toByteArray();
					
					try {
						full_buffer.reset();
						full_buffer.flush();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println("Receive Buffer size before calling function :" + recv_buff.length );
					
					serialport.removeDataListener();
					String result = "";
					try {
						result = new String(recv_buff,"UTF-8");
						ManufacturingTests.logs_logger.severe("Response Received\t"+ result);
					} catch (UnsupportedEncodingException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					catch(Exception ex)
					{
						ex.printStackTrace();
					}
					System.out.println("Response to be processed : " + result);
					System.out.println("Inside the function d");
					
					System.out.println("HEREEEE");
					
						
					System.out.println("Response Received : " + result + "\r\n");
					//publish("Response Received : " + result + "\r\n");
					System.out.println("Receive Buffer in MTEST START: " + Arrays.toString(recv_buff));
					//System.out.println("Manufacturing Test Flag Set");
					
					try 
					{
						String Resp = new String(recv_buff,"UTF-8");
						
						recv_buff = null;
						System.out.println("Response Received : " + Resp + " Of Length" + Resp.length());
						
						if(Resp.contains(RESPONSE_OK))
						{
							
							try
							{
								String resp1 = Resp.replace("{", "");
								resp1 = resp1.replace("}", "");
								String[] resp = resp1.split(",");
								ManufacturingTests.logs_logger.severe("Response OK Received");
								//System.out.println(Arrays.deepToString(resp));
								//System.out.println("Response OK Received!!");
								recv_buff = null;
								Resp = "";
								
								
								if(State_Machine == 0)
								{
									Fail_Flag = false;
									State_Machine = 1;
									Initializer_Value = 0;
									System.out.println("Actual Array : " + Arrays.toString(resp));
									String Ver_No = resp[2] + "," + resp[3];
									publish("VER_NO," + Ver_No);
									publish("Response Received : " + result + "\r\n");
									//publish("SUB_PANEL");
									synchronized(lockObject)
									{
										Write_flag = false;
										Notify_Flag = 1;
										lockObject.notifyAll();
									}
									
									
								}
								else
								{
									if(jtgbtn != null)
									{
										jtgbtn.setEnabled(false);
									}
									
									String src = ManufacturingTests.get_source(resp[1]);
									if(ManufacturingTests.worker_cancel_flag)
									{
										
										if(futuree != null)
										{
											System.out.println("#############################################################################");
											System.out.println("#########################CANCELLING FUTUREE BEFORE UI UPDATE###############################");
											System.out.println("#############################################################################");
											
											
											futuree.cancel(true);
										}
										if(future != null)
										{
											future.cancel(true);
										}
									}
									
									UpdateTestResults(Pass,"",src,"");
									//Initializer_Value = test_count;
									//UpdateTestResults(.Current_Query,Pass,"",src,"");
								
									ManufacturingTests.logs_logger.severe("Result\t\t\tPass");
									ManufacturingTests.report_logger.severe("Result\t\t\tPass" );
									//LIGHT ENGINE FAN TEST
									if(ManufacturingTests.lightenginefantest.equals(ManufacturingTests.check_string))
									{
										String Fan_Count = resp[2];
										ManufacturingTests.report_logger.severe("Expected Tach Count -->  : " + ManufacturingTests.Expected_LightEngine_FanTachCount+ "\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Tach Count -->  : " + ManufacturingTests.Expected_LightEngine_FanTachCount+ "\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
										ManufacturingTests.check_string = "";
									}//HEAT SINK FAN TEST
									else if(ManufacturingTests.heatsinkfantest.equals(ManufacturingTests.check_string))
									{
										String Fan_Count = resp[2];
										ManufacturingTests.report_logger.severe("Expected Tach Count -->  : " + ManufacturingTests.Expected_HeatSink_FanTachCount+ "\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Tach Count -->  : " + ManufacturingTests.Expected_HeatSink_FanTachCount+ "\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
										ManufacturingTests.check_string = "";
									}
									//IRIS FAN TEST
									else if(ManufacturingTests.irisfantest.equals(ManufacturingTests.check_string))
									{
										String Fan_Count = resp[2];
										ManufacturingTests.report_logger.severe("Expected Tach Count -->  : " + ManufacturingTests.Expected_Iris_FanTachCount+ "\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Tach Count -->  : " + ManufacturingTests.Expected_Iris_FanTachCount+ "\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
										ManufacturingTests.check_string = "";
									}
									
									//LED TEST RESULT : LED ZERO PERCENT
									else if(ManufacturingTests.redledtestzero.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.greenledtestzero.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.blueledtestzero.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.laserledtestzero.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.iris1ledtestzero.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.iris2ledtestzero.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										ManufacturingTests.check_string = "";
									}
									
									
									//LED TEST RESULT : LED FIFTY PERCENT
					
									else if(ManufacturingTests.redledtestfifty.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.greenledtestfifty.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.blueledtestfifty.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.laserledtestfifty.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.iris1ledtestfifty.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_fiftypercent+" V\t\tCurrent : " +ManufacturingTests.Exp_Iris1Led_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.iris2ledtestfifty.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									
									//LED TEST RESULT : LED SEVENTY FIVE PERCENT
									
									else if(ManufacturingTests.redledtestseventyfive.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.greenledtestseventyfive.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.blueledtestseventyfive.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.laserledtestseventyfive.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.iris1ledtestseventyfive.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_seventyfivepercent+" V\t\tCurrent : " +ManufacturingTests.Exp_Iris1Led_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.iris2ledtestseventyfive.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									
									//LED TEST RESULT : LED HUNDRED PERCENT
									
									else if(ManufacturingTests.redledtesthundred.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " +ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.greenledtesthundred.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.blueledtesthundred.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
									}
									else if(ManufacturingTests.laserledtesthundred.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.iris1ledtesthundred.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.iris2ledtesthundred.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = resp[2];
										String LedCurrent = resp[3];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										
										ManufacturingTests.check_string = "";
									}
									
									//PDTESTS : IRIS1ZERO and HUNDRED
									else if(ManufacturingTests.iris1pdtestzero.equals(ManufacturingTests.check_string))
									{
										String PhotoVoltage = resp[2];
										ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_iris1pdvoltage_zero+" V\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_iris1pdvoltage_zero+" V\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
									}
									else if(ManufacturingTests.iris1pdtesthundred.equals(ManufacturingTests.check_string))
									{
										String PhotoVoltage = resp[2];
										ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_iris1pdvoltage_hundred+" V\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_iris1pdvoltage_hundred+" V\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
									}
									
									//PDTESTS : IRIS2 ZERO AND HUNDRED
									
									else if(ManufacturingTests.iris2pdtestzero.equals(ManufacturingTests.check_string))
									{
										String PhotoVoltage = resp[2];
										ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_iris2pdvoltage_zero+" V\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_iris2pdvoltage_zero+" V\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
									}
									else if(ManufacturingTests.iris2pdtesthundred.equals(ManufacturingTests.check_string))
									{
										String PhotoVoltage = resp[2];
										ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_iris2pdvoltage_hundred+" V\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_iris2pdvoltage_hundred+" V\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
									}
//										PDTESTS : ENV ZERO AND HUNDRED
									
									else if(ManufacturingTests.envpdtestzero.equals(ManufacturingTests.check_string))
									{
										String PhotoVoltage = resp[2];
										ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_envpdvoltage_zero+" V\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_envpdvoltage_zero+" V\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
									}
									else if(ManufacturingTests.envpdtesthundred.equals(ManufacturingTests.check_string))
									{
										String PhotoVoltage = resp[2];
										ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_envpdvoltage_hundred+" V\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_envpdvoltage_hundred+" V\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
									}
//										CS TESTS : COLORSENSOR 1
									
									else if(ManufacturingTests.colorsensor1test.equals(ManufacturingTests.check_string))
									{
										String ColorsensorVal_1 = resp[2];
										String ColorsensorVal_2 = resp[3];
										String ColorsensorVal_3 = resp[4];
										String ColorsensorVal_4 = resp[5];
										ManufacturingTests.report_logger.severe("Expected Value --> \t >100 and below \"FFFF\"" );
										ManufacturingTests.report_logger.severe("Observed Value --> \tVal 1 : " + ColorsensorVal_1 +" Val 2 : " + ColorsensorVal_2 +" Val 3 : " + ColorsensorVal_3 +" Val 4 : " + ColorsensorVal_4 + "\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t >100 and below \"FFFF\"" );
										ManufacturingTests.logs_logger.severe("Observed Value --> \tVal 1 : " + ColorsensorVal_1 +" Val 2 : " + ColorsensorVal_2 +" Val 3 : " + ColorsensorVal_3 +" Val 4 : " + ColorsensorVal_4 + "\r\n\n");
									}
									else if(ManufacturingTests.colorsensor2test.equals(ManufacturingTests.check_string))
									{
										String ColorsensorVal_1 = resp[2];
										String ColorsensorVal_2 = resp[3];
										String ColorsensorVal_3 = resp[4];
										String ColorsensorVal_4 = resp[5];
										ManufacturingTests.report_logger.severe("Expected Value --> \t >100 and below \"FFFF\"" );
										ManufacturingTests.report_logger.severe("Observed Value --> \tVal 1 : " + ColorsensorVal_1 +" Val 2 : " + ColorsensorVal_2 +" Val 3 : " + ColorsensorVal_3 +" Val 4 : " + ColorsensorVal_4+ "\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t >100 and below \"FFFF\"" );
										ManufacturingTests.logs_logger.severe("Observed Value --> \tVal 1 : " + ColorsensorVal_1 +" Val 2 : " + ColorsensorVal_2 +" Val 3 : " + ColorsensorVal_3 +" Val 4 : " + ColorsensorVal_4+ "\r\n\n");
									}
									
									//PWM MUXTESTS 
									else if(ManufacturingTests.wluc1pwmmuxtest.equals(ManufacturingTests.check_string))
									{
										String MuxInterrupt_Count = resp[2];
										ManufacturingTests.report_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_wluc1pwmmuxtest_intcount + " (+/- 3)");
										ManufacturingTests.report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_wluc1pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
									}
									else if(ManufacturingTests.envuc1pwmmuxtest.equals(ManufacturingTests.check_string))
									{
										String MuxInterrupt_Count = resp[2];
										ManufacturingTests.report_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_envuc1pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_envuc1pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
									}
									else if(ManufacturingTests.irisuc1pwmmuxtest.equals(ManufacturingTests.check_string))
									{
										String MuxInterrupt_Count = resp[2];
										ManufacturingTests.report_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_irisuc1pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_irisuc1pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
									}
									else if(ManufacturingTests.wluc2pwmmuxtest.equals(ManufacturingTests.check_string))
									{
										String MuxInterrupt_Count = resp[2];
										ManufacturingTests.report_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_wluc2pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_wluc2pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
									}
									else if(ManufacturingTests.envuc2pwmmuxtest.equals(ManufacturingTests.check_string))
									{
										String MuxInterrupt_Count = resp[2];
										ManufacturingTests.report_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_envuc2pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_envuc2pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
									}
									else if(ManufacturingTests.irisuc2pwmmuxtest.equals(ManufacturingTests.check_string))
									{
										String MuxInterrupt_Count = resp[2];
										ManufacturingTests.report_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_irisuc2pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_irisuc2pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
									}
									else if(ManufacturingTests.wlextpwmmuxtest.equals(ManufacturingTests.check_string))
									{
										String MuxInterrupt_Count = resp[2];
										ManufacturingTests.report_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_wlextpwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_wlextpwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
									}
									else if(ManufacturingTests.envextpwmmuxtest.equals(ManufacturingTests.check_string))
									{
										String MuxInterrupt_Count = resp[2];
										ManufacturingTests.report_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_envextpwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_envextpwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
									}
									else if(ManufacturingTests.irisextwlpwmmuxtest.equals(ManufacturingTests.check_string))
									{
										String MuxInterrupt_Count = resp[2];
										ManufacturingTests.report_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_irisextwlpwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+ "\r\n\n" );
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_irisextwlpwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+ "\r\n\n" );
									}
									else if(ManufacturingTests.irisextenvpwmmuxtest.equals(ManufacturingTests.check_string))
									{
										
										String MuxInterrupt_Count = resp[2];
										ManufacturingTests.report_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_irisextenvpwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_irisextenvpwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
									}
									ManufacturingTests.report_logger.severe(ManufacturingTests.ArrowDelimitter);
									//publish("Status : Pass\r\n");
									//publish("UPDATE,PASS," + ""+"," + src + "," + "");
									
									
									synchronized(lockObject)
									{
										Write_flag = false;
										Notify_Flag = 1;
										lockObject.notifyAll();
									}
									
									publish("Response Received : " + result + "\r\n");
									
								}
								
							}catch(java.lang.ArrayIndexOutOfBoundsException ex)
							{
								ex.printStackTrace();
								publish("Invalid Response Received!!\r\n");
								UpdateTestResults(Fail, "", "", "");
								
							}
							catch(java.lang.NullPointerException ex)
							{
								ex.printStackTrace();
								publish("Invalid Response Received!!\r\n");
								UpdateTestResults(Fail, "", "", "");
							}
							
							
						}
						else if(Resp.contains(RESPONSE_ERROR))
						{
							
							try
							{
								
								System.out.println("Error Response Received!!");
								
								String resp = Resp.replace("{", "");
								resp = resp.replace("}", "");
								String[] actual = resp.split(",");
								String reason = ManufacturingTests.get_error_source(actual[1]);
								String source = ManufacturingTests.get_source(actual[2]);
								String value = "";
								if(State_Machine == 0)
								{
									Fail_Flag = true;
									State_Machine = 0;
									Initializer_Value = 0;
									System.out.println("Actual Array : " + Arrays.toString(actual));
									
									
									//publish("SUB_PANEL");
									synchronized(lockObject)
									{
										Write_flag = false;
										Notify_Flag = 1;
										lockObject.notifyAll();
									}
									return;
									
									
								}
								else
								{
									if(jtgbtn != null)
									{
										jtgbtn.setEnabled(false);
									}
									UpdateTestResults(Fail,reason,source,value);
									
									ManufacturingTests.report_logger.severe("Result\t\t\tFail" );
									ManufacturingTests.logs_logger.severe("Result\t\t\tFail" );
									if(ManufacturingTests.lightenginefantest.equals(ManufacturingTests.check_string))
									{
										String Fan_Count = actual[3];
										ManufacturingTests.report_logger.severe("Expected Tach Count -->  : " + ManufacturingTests.Expected_LightEngine_FanTachCount + "\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Tach Count -->  : " + ManufacturingTests.Expected_LightEngine_FanTachCount+ "\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
										ManufacturingTests.check_string = "";
									}//HEAT SINK FAN TEST
									else if(ManufacturingTests.heatsinkfantest.equals(ManufacturingTests.check_string))
									{
										String Fan_Count = actual[3];
										ManufacturingTests.report_logger.severe("Expected Tach Count -->  : " + ManufacturingTests.Expected_HeatSink_FanTachCount+ "\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Tach Count -->  : " + ManufacturingTests.Expected_HeatSink_FanTachCount+ "\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
										ManufacturingTests.check_string = "";
									}
									//IRIS FAN TEST
									else if(ManufacturingTests.irisfantest.equals(ManufacturingTests.check_string))
									{
										String Fan_Count = actual[3];
										ManufacturingTests.report_logger.severe("Expected Tach Count -->  : " + ManufacturingTests.Expected_Iris_FanTachCount+ "\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Tach Count -->  : " + ManufacturingTests.Expected_Iris_FanTachCount+ "\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.redledtestzero.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.greenledtestzero.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.blueledtestzero.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.laserledtestzero.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.iris1ledtestzero.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.iris2ledtestzero.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_zeropercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										ManufacturingTests.check_string = "";
									}
									
									
									//LED TEST RESULT : LED FIFTY PERCENT
					
									else if(ManufacturingTests.redledtestfifty.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.greenledtestfifty.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.blueledtestfifty.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.laserledtestfifty.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.iris1ledtestfifty.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " +ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.iris2ledtestfifty.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_fiftypercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_fiftypercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									
									//LED TEST RESULT : LED SEVENTY FIVE PERCENT
									
									else if(ManufacturingTests.redledtestseventyfive.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.greenledtestseventyfive.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.blueledtestseventyfive.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.laserledtestseventyfive.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.iris1ledtestseventyfive.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " +ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.iris2ledtestseventyfive.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_seventyfivepercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									
									//LED TEST RESULT : LED HUNDRED PERCENT
									
									else if(ManufacturingTests.redledtesthundred.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_RedLed_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.greenledtesthundred.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_GreenLed_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.blueledtesthundred.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_BlueLed_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
									}
									else if(ManufacturingTests.laserledtesthundred.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_LaserLed_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.iris1ledtesthundred.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris1Led_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										ManufacturingTests.check_string = "";
									}
									else if(ManufacturingTests.iris2ledtesthundred.equals(ManufacturingTests.check_string))
									{
										String LedVoltage = actual[3];
										String LedCurrent = actual[4];
										if(ManufacturingTests.chckbxLightEngine.isSelected())
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_hundredpercent+" V\t\tCurrent : " + ManufacturingTests.Exp_Iris2Led_Current_hundredpercent + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										else
										{
											ManufacturingTests.report_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											
											ManufacturingTests.logs_logger.severe("Expected Values --> \tVoltage : " + ManufacturingTests.Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ManufacturingTests.ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
											ManufacturingTests.logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
										}
										
										
										ManufacturingTests.check_string = "";
									}
									
									//PDTESTS : IRIS1ZERO and HUNDRED
									else if(ManufacturingTests.iris1pdtestzero.equals(ManufacturingTests.check_string))
									{
										String PhotoVoltage = actual[3];
										ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_iris1pdvoltage_zero+" V\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_iris1pdvoltage_zero+" V\t(+/- 25%");
										ManufacturingTests.logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
									}
									else if(ManufacturingTests.iris1pdtesthundred.equals(ManufacturingTests.check_string))
									{
										String PhotoVoltage = actual[3];
										ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_iris1pdvoltage_hundred+" V\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : " +ManufacturingTests.Exp_iris1pdvoltage_hundred+" V\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
									}
									
									//PDTESTS : IRIS2 ZERO AND HUNDRED
									
									else if(ManufacturingTests.iris2pdtestzero.equals(ManufacturingTests.check_string))
									{
										String PhotoVoltage = actual[3];
										ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_iris2pdvoltage_zero+" V\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_iris2pdvoltage_zero+" V\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
									}
									else if(ManufacturingTests.iris2pdtesthundred.equals(ManufacturingTests.check_string))
									{
										String PhotoVoltage = actual[3];
										ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_iris2pdvoltage_hundred+" V\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_iris2pdvoltage_hundred+" V\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
									}
//										PDTESTS : ENV ZERO AND HUNDRED
									
									else if(ManufacturingTests.envpdtestzero.equals(ManufacturingTests.check_string))
									{
										String PhotoVoltage = actual[3];
										ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_envpdvoltage_zero+" V\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_envpdvoltage_zero+" V\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
									}
									else if(ManufacturingTests.envpdtesthundred.equals(ManufacturingTests.check_string))
									{
										String PhotoVoltage = actual[3];
										ManufacturingTests.report_logger.severe("Expected Value --> \tVoltage : " + ManufacturingTests.Exp_envpdvoltage_hundred+" V\t(+/- 25%)");
										ManufacturingTests.report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \tVoltage : " +ManufacturingTests. Exp_envpdvoltage_hundred+" V\t(+/- 25%)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
									}
//										CS TESTS : COLORSENSOR 1
									
									else if(ManufacturingTests.colorsensor1test.equals(ManufacturingTests.check_string))
									{
										String ColorsensorVal_1 = actual[3];
										String ColorsensorVal_2 = actual[4];
										String ColorsensorVal_3 = actual[5];
										String ColorsensorVal_4 = actual[6];
										ManufacturingTests.report_logger.severe("Expected Value --> \t >100 and below \"FFFF\"" );
										ManufacturingTests.report_logger.severe("Observed Value --> \tVal 1 : " + ColorsensorVal_1 +" Val 2 : " + ColorsensorVal_2 +" Val 3 : " + ColorsensorVal_3 +" Val 4 : " + ColorsensorVal_4 + "\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t >100 and below \"FFFF\"" );
										ManufacturingTests.logs_logger.severe("Observed Value --> \tVal 1 : " + ColorsensorVal_1 +" Val 2 : " + ColorsensorVal_2 +" Val 3 : " + ColorsensorVal_3 +" Val 4 : " + ColorsensorVal_4 + "\r\n\n");
									}
									else if(ManufacturingTests.colorsensor2test.equals(ManufacturingTests.check_string))
									{
										String ColorsensorVal_1 = actual[3];
										String ColorsensorVal_2 = actual[4];
										String ColorsensorVal_3 = actual[5];
										String ColorsensorVal_4 = actual[6];
										ManufacturingTests.report_logger.severe("Expected Value --> \t >100 and below \"FFFF\"" );
										ManufacturingTests.report_logger.severe("Observed Value --> \tVal 1 : " + ColorsensorVal_1 +" Val 2 : " + ColorsensorVal_2 +" Val 3 : " + ColorsensorVal_3 +" Val 4 : " + ColorsensorVal_4+"\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t >100 and below \"FFFF\"" );
										ManufacturingTests.logs_logger.severe("Observed Value --> \tVal 1 : " + ColorsensorVal_1 +" Val 2 : " + ColorsensorVal_2 +" Val 3 : " + ColorsensorVal_3 +" Val 4 : " + ColorsensorVal_4+"\r\n\n");
									}
									
									//PWM MUXTESTS 
									else if(ManufacturingTests.wluc1pwmmuxtest.equals(ManufacturingTests.check_string))
									{
										String MuxInterrupt_Count = actual[3];
										ManufacturingTests.report_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_wluc1pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count +"\r\n\n" );
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_wluc1pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count +"\r\n\n" );
									}
									else if(ManufacturingTests.envuc1pwmmuxtest.equals(ManufacturingTests.check_string))
									{
										String MuxInterrupt_Count = actual[3];
										ManufacturingTests.report_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_envuc1pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+"\r\n\n" );
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_envuc1pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+"\r\n\n" );
									}
									else if(ManufacturingTests.irisuc1pwmmuxtest.equals(ManufacturingTests.check_string))
									{
										String MuxInterrupt_Count = actual[3];
										ManufacturingTests.report_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_irisuc1pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+"\r\n\n" );
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_irisuc1pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+"\r\n\n" );
									}
									else if(ManufacturingTests.wluc2pwmmuxtest.equals(ManufacturingTests.check_string))
									{
										String MuxInterrupt_Count = actual[3];
										ManufacturingTests.report_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_wluc2pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count +"\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_wluc2pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count +"\r\n\n");
									}
									else if(ManufacturingTests.envuc2pwmmuxtest.equals(ManufacturingTests.check_string))
									{
										String MuxInterrupt_Count = actual[3];
										ManufacturingTests.report_logger.severe("Expected Value --> \t" +ManufacturingTests. Exp_envuc2pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+"\r\n\n" );
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_envuc2pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+"\r\n\n" );
									}
									else if(ManufacturingTests.irisuc2pwmmuxtest.equals(ManufacturingTests.check_string))
									{
										String MuxInterrupt_Count = actual[3];
										ManufacturingTests.report_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_irisuc2pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+"\r\n\n" );
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_irisuc2pwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+"\r\n\n" );
									}
									else if(ManufacturingTests.wlextpwmmuxtest.equals(ManufacturingTests.check_string))
									{
										String MuxInterrupt_Count = actual[3];
										ManufacturingTests.report_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_wlextpwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count +"\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_wlextpwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count +"\r\n\n");
									}
									else if(ManufacturingTests.envextpwmmuxtest.equals(ManufacturingTests.check_string))
									{
										String MuxInterrupt_Count = actual[3];
										ManufacturingTests.report_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_envextpwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+"\r\n\n" );
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_envextpwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+"\r\n\n" );
									}
									else if(ManufacturingTests.irisextwlpwmmuxtest.equals(ManufacturingTests.check_string))
									{
										String MuxInterrupt_Count = actual[3];
										ManufacturingTests.report_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_irisextwlpwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count +"\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_irisextwlpwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count +"\r\n\n");
									}
									else if(ManufacturingTests.irisextenvpwmmuxtest.equals(ManufacturingTests.check_string))
									{
										String MuxInterrupt_Count = actual[3];
										ManufacturingTests.report_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_irisextenvpwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count +"\r\n\n");
										
										ManufacturingTests.logs_logger.severe("Expected Value --> \t" + ManufacturingTests.Exp_irisextenvpwmmuxtest_intcount+ " (+/- 3)");
										ManufacturingTests.logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count +"\r\n\n");
									}
									
									ManufacturingTests.report_logger.severe(ManufacturingTests.ArrowDelimitter);
									synchronized(lockObject)
									{
										Write_flag = false;
										Notify_Flag = 1;
										lockObject.notifyAll();
									}
									String Failure = "{FAIL,"+reason+"";
									publish("{FAIL," + reason +"}\r\n");
									//publish("UPDATE," + Fail + "," + reason + "," + source + "," + value);
									//publish("Status : Fail\r\n"); 
									
									recv_buff = null;
									Resp = "";
								}
								
							}catch(Exception ex)
							{
								ex.printStackTrace();
								
							}
							
						}
						else
						{
							
							synchronized(lockObject)
							{
								Write_flag = false;
								Notify_Flag = 1;
								lockObject.notifyAll();
							}
							
							//.table.getModel().setValueAt("PASSED", 1, 4);
							
							
							ManufacturingTests.btnStartButton.setEnabled(true);
							ManufacturingTests.btnStartButton.setText("RUN");
							//ManufacturingTests.btnBack.setEnabled(true);
							recv_buff = null;
							Resp = "";
						}
						
						
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						UpdateTestResults(Fail,"UNKNOWN DATA FORMAT","DATA","DATA");
					}
				
				
				
				
				recv_buff = null;
				//serialport.removeDataListener(); // Removes data listener associated with serial port Object
				total_bytes = 0;
				
				
			}
		}
		
		
		

	};
	
//#######################################################################################################################################################
	public void UpdateTestResults( String result, String reason, String source, String value)
	{
	
	try
		{
			
			System.out.println("btnname to be mapped : " + btnname_mapping_topanel);
			//No Error
			if(reason.equals(""))
			{
				//Timeout Response
				if(result.equals(Stryk_Demo.Timeout))
				{
					fail_count = fail_count + 1;
					if(ManufacturingTests.result_indicator_mappings.get(btnname_mapping_topanel) != null)
					{
						ManufacturingTests.result_indicator_mappings.get(btnname_mapping_topanel).setBackground(Color.MAGENTA);
						publish("Fail," + fail_count);
						Initializer_Value = test_count;
					}
					else
					{
						//btnSyncMTest.setText("SYNC");
					}
					
					
				}
				//Failure response
				else if(result.equals(Stryk_Demo.Fail))
				{
					fail_count = fail_count + 1;
					if(ManufacturingTests.result_indicator_mappings.get(btnname_mapping_topanel) != null)
					{
						ManufacturingTests.result_indicator_mappings.get(btnname_mapping_topanel).setBackground(Color.RED);
						publish("Fail," + fail_count + "," + reason + "," + source);
						Initializer_Value = test_count;
					}
					else
					{
						//btnSyncMTest.setText("SYNC");
					}
				
					
				}
				//Success response
				else if(result.equals(Stryk_Demo.Pass))
				{
					pass_count = pass_count + 1;
					ManufacturingTests.result_indicator_mappings.get(btnname_mapping_topanel).setBackground(Color.green);
					publish("Pass," + pass_count);
					Initializer_Value = test_count;
					//System.out.println("Pass Count Incremented");
					
				}

				    // the following statement is used to log any messages  
				
				
				
				source = "Source : " + source;
				
			}
			else if(!reason.equals(""))
			{
				//Timeout Response
				if(result.equals(Stryk_Demo.Timeout))
				{
					fail_count = fail_count + 1;
					if(ManufacturingTests.result_indicator_mappings.get(btnname_mapping_topanel) != null)
					{
						ManufacturingTests.result_indicator_mappings.get(btnname_mapping_topanel).setBackground(Color.MAGENTA);
						publish("Fail," + fail_count);
					}
					else
					{
						//btnSyncMTest.setText("SYNC");
					}
					
				}
				//Failure response
				else if(result.equals(Stryk_Demo.Fail))
				{
					fail_count = fail_count + 1;
					if(ManufacturingTests.result_indicator_mappings.get(btnname_mapping_topanel) != null)
					{
						ManufacturingTests.result_indicator_mappings.get(btnname_mapping_topanel).setBackground(Color.RED);
						publish("Fail," + fail_count);
					}
					else
					{
						//btnSyncMTest.setText("SYNC");
					}
					
				}
				//Success response
				else if(result.equals(Stryk_Demo.Pass))
				{
					pass_count = pass_count + 1;
					ManufacturingTests.result_indicator_mappings.get(btnname_mapping_topanel).setBackground(Color.green);
					publish("Pass," + pass_count);
					//System.out.println("Pass Count Incremented");
					
				}
			}
			//Error Response Received
			
			
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		
	}
//#######################################################################################################################################################
	//Function that sets incoming data listener for serial port
	public void setlistener_mft(final SerialPort serialport) 
	{
		//print("Serial Port Listener Set");
		if (serialport != null) 
		{
	
			serialport.addDataListener(new SerialPortDataListener() 
			{
				@Override
				public int getListeningEvents() 
				{
					// System.out.println("Getting the list of events");
					return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
				}
	
				@Override
				public void serialEvent(SerialPortEvent event) 
				{
					if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE) 
					{
						
						return;
					}
	
					try 
					{
						//System.out.println("AA");
						byte[] newData = new byte[serialport.bytesAvailable()];
						//	System.out.println("A");
						numRead = serialport.readBytes(newData, newData.length);
						//	System.out.println("B");
						total_bytes = total_bytes + numRead;
						full_buffer.write(newData, 0, numRead);
						System.out.println("Something dfdf data arrived in theee serial port");
						
						if(future!= null)
						{
							future.cancel(true);
						}
						if(!ManufacturingTests.worker_cancel_flag)
						{
							future = scheduler.schedule(uart_recv_timeout_task ,serial_read_byte_timeout , TimeUnit.MILLISECONDS);
						}
						else
						{
							
							System.out.println("<<------------------FUTURE CANCELLED HERE---------------------->>>>");
							ManufacturingTests.btnStartButton.setName("RUN");
							
						}
						
						
						
						
						
						// #uart_response_timer.start();
					} 
					catch (Exception ex) 
					{
						ex.printStackTrace();
						return;
					}
	
				}
	
			});
	
		} 
		else 
		{
			System.out.println("No Serial ports found in the system");
		}
	}



}
	
	
	
	
	
	
	









