import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.UIManager;

import java.awt.Font;
/**
 * 
 */
import java.awt.SystemColor;

/**
 * @author LakshmiNarasimman R
 *
 */
public class ObservationSettings {

	//Button Definitions
	public static JButton btnNext;
	public static JButton btnFinish;
	
	//Frame Definitions
	public static JFrame frmObservationSettings;
	
	
	//Boolean Variable definitions
	public static boolean withload = false;
	
	
	//Label Definitions
	public static JLabel lblPleaseMakeSure;
	public static JLabel lblStatement;
	public static JLabel lblStatement_1;
	public static JLabel lblThingsToObserve;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ObservationSettings window = new ObservationSettings();
					window.frmObservationSettings.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ObservationSettings() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmObservationSettings = new JFrame();
		frmObservationSettings.setTitle("Test Configuration");
		
		frmObservationSettings.setBounds(100, 100, 402, 313);
		frmObservationSettings.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmObservationSettings.setForeground(UIManager.getColor("InternalFrame.borderColor"));

		frmObservationSettings.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		frmObservationSettings.getContentPane().setBackground(SystemColor.textHighlightText);
		frmObservationSettings.getContentPane().setLayout(null);
		
		this.btnNext = new JButton("Next");
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnNext.setEnabled(false);
				frmObservationSettings.getContentPane().removeAll();
				JButton jh = new JButton();
				jh.setBounds(20, 10, 89, 23);
				ObservationSettings2 obs = new ObservationSettings2();
				frmObservationSettings.setContentPane(ObservationSettings2.frmObservationSettings2.getContentPane());
				
				
				btnFinish.setEnabled(true);
				
				frmObservationSettings.revalidate();
				frmObservationSettings.repaint();
			}
		});
		btnNext.setBounds(130, 205, 89, 23);
		frmObservationSettings.getContentPane().add(btnNext);
		
		this.btnFinish = new JButton("Finish");
		btnFinish.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnFinish.setEnabled(false);
		btnFinish.setBounds(229, 205, 89, 23);
		frmObservationSettings.getContentPane().add(btnFinish);
		
		this.lblPleaseMakeSure = new JLabel("--->  Please Make Sure that the LED's D20(uC1) and D24(uC2) are toggling at the rate of 1 sec.");
		lblPleaseMakeSure.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		lblPleaseMakeSure.setBounds(10, 55, 414, 14);
		frmObservationSettings.getContentPane().add(lblPleaseMakeSure);
		
		this.lblStatement = new JLabel("--->  Make Sure that the Power LEDs are ON");
		lblStatement.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		lblStatement.setBounds(10, 92, 402, 14);
		frmObservationSettings.getContentPane().add(lblStatement);
		
		this.lblStatement_1 = new JLabel("--->  Statement 3");
		lblStatement_1.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		lblStatement_1.setBounds(10, 131, 96, 14);
		frmObservationSettings.getContentPane().add(lblStatement_1);
		
		this.lblThingsToObserve = new JLabel("Things to Observe");
		lblThingsToObserve.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblThingsToObserve.setBounds(158, 11, 119, 14);
		frmObservationSettings.getContentPane().add(lblThingsToObserve);
		
		frmObservationSettings.setVisible(true);
	}
}
