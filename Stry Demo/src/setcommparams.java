import com.fazecast.jSerialComm.SerialPort;

public class setcommparams
{
	private int brate = 115200; // Baud Rate
	private int startbits = 8; // Startbits
	private int stopbits = SerialPort.ONE_STOP_BIT; // Stop Bits
	private int parity = SerialPort.NO_PARITY; // Parity type
	private int serialportreadtimeout = 2000; // read timeout //Changed on 27/04/2017
	private int serialportwritetimeout = 1000; // Write timeout
	@SuppressWarnings("static-access")
	public void setcommparams(SerialPort serialport)  {
		try {
			//##System.out.println("Inside setcommparams() function");
			serialport.setBaudRate(brate);
			//##System.out.println("Baudrate is : " + Integer.toString(brate));
			serialport.setNumDataBits(startbits);
			serialport.setNumStopBits(stopbits);
			serialport.setFlowControl(serialport.FLOW_CONTROL_DISABLED);
			serialport.setParity(parity);
			serialport.setComPortTimeouts(serialport.TIMEOUT_READ_BLOCKING, serialportreadtimeout,
					serialportwritetimeout);
			//#System.out.println("COMM Port Parameters Set successfully");
		} catch (Exception e) {
			System.out.println("Error while configuring Serialport parameters : " + e.toString());
		}

	}
}

